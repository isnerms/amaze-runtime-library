
'use strict';

var pkg = require('./package.json'),
    fs = require('fs'),
    path = require('path');

var runtime = path.join(__dirname, 'dist', 'amaze.js'),
    docs = path.join(__dirname, 'docs', 'amaze-' + pkg.version + '.html'),
    copyright = path.join(__dirname, 'copyright.txt');

exports.code = fs.readFileSync(runtime).toString();

exports.version = pkg.version;

exports.docs = fs.readFileSync(docs).toString();

exports.copyright = fs.readFileSync(copyright).toString();
