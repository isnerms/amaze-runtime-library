# Amaze Runtime Library

  The client-side JavaScript for Amaze.

## Building and testing

  Before doing anything, run `npm install`.

  To build the runtime:

    $ node build

  To build the runtime with `require` exposed:

    $ node build --dev

  To run the unit tests:

    $ node test

  To build the coverage reports, run the following commands and navigate to `/test/unit/{array,dom,markup,...}/coverage.html`:

    $ node build --dev
    $ serve --port 9876
    $ open http://localhost:9876/test/unit
