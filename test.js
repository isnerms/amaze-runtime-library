
'use strict';

var spawn = require('win-fork'),
    path = require('path'),
    join = path.join.bind(path, __dirname),
    globs = require('globs'),
    uncolor = require('uncolor'),
    st = require('st'),
    http = require('http'),
    Socket = require('net').Socket,
    which = require('which').sync,
    fs = require('fs');

var windows = !!process.platform.match(/^win/);
var timeout = process.env.MOCHA_TIMEOUT || 2000;
var xunit = -1 !== process.argv.indexOf('--xunit');
var reporter = xunit
  ? 'xunit'
  : process.env.MOCHA_REPORTER || 'spec';


var tests = [
  join('test', 'unit', '**/*.html'),
  join('test', 'acceptance', '*.html')
];

var phantomjs = (function () {
  if (windows) {
    try {
      return which('phantomjs');
    } catch (err) {
      console.error([
        'phantomjs must be installed and on PATH',
        '',
        '  please follow the steps at http://phantomjs.org/download.html',
        '  and update your PATH enviroment variable accordingly',
        ''
      ].join('\n'));
      process.exit(-1);
    }
  }

  return binary('phantomjs');
}());


globs(tests, function (err, files) {
  if (err) {
    throw err;
  }

  files = files
    .filter(coverage)
    .map(testToUrl);

  server(function (handle) {
    var mocha = binary('mocha-phantomjs'),
        failures = 0,
        data = '';

    var stream = xunit
        ? fs.createWriteStream('test-output.xml')
        : process.stdout;

    /**
     * Close the server and exit the process with the
     * number of failures.  Will only run once.
     *
     * @api private
     */
    function done() {
      if (done.done) {
        return;
      }

      // close the server when applicable
      if (handle) {
        handle.close();
      }

      done.done = true;

      if (xunit) {
        stream.end();
      }

      if (failures > 10) {
        console.log();
        console.log([
          '\x1B[31m',
          'oh no!  you\'ve got some work to do',
          '\x1B[39m'
        ].join(''));
        console.log();
      }

      data = uncolor(data);

      var expr = /\n  ([\d]+) passing \(/g,
          passes = 0,
          match = null;

      while (match = expr.exec(data)) {
        passes += parseInt(match[1], 10);
      }

      console.log();
      log('passed', passes, 32);
      log('failed', failures, 31);
      log('total', passes + failures, 36);
      console.log();

      process.exit(failures);
    }

    next(0);

    /**
     * Run the *next* test at `index`
     *
     * @api private
     * @param {Number} index
     */
    function next(index) {
      var test = files[index],
          args = [];

      if (!test) {
        return done();
      }

      args = args.concat([ '-R', reporter ]);
      args = args.concat([ '-t', parseInt(timeout, 10) ]);
      args = args.concat([ '-p', phantomjs ]);
      args.push(test);

      console.log();
      log('testing', test, 33);
      console.log();

      // start mocha
      var proc = spawn(mocha, args, {});

      proc
      .on('close', function (code) {
        failures += code;
        index++;
        next(index);
      })
      .stdout.on('data', function (d) {
        d = String(d);
        data += d;

        // if we're running as an xUnit reporter,
        // we only want to write XML-ish stuff
        // to the stream
        if (xunit) {
          if ('<' === d[0]) {
            stream.write(d);
          }
        } else {
          stream.write(d);
        }
      });
    }
  });
});



/**
 * Log the given `type` with `msg`.
 *
 * Stolen from component.
 *
 * @param {String} type
 * @param {String} msg
 * @api public
 */

function log(type, msg, color) {
  color = color || '36';
  var w = 10;
  var len = Math.max(0, w - type.length);
  var pad = new Array(len + 1).join(' ');

  if (!xunit) {
    console.log('  \x1B[' + color + 'm%s\x1B[m : \x1B[90m%s\x1B[m', pad + type, msg);
  } else {
    console.log('  %s : %s', pad + type, msg);
  }
}

/**
 * Get a path to the binary `name` in
 * `./node_modules/.bin`.
 *
 * @api private
 * @param {String}
 * @return {String}
 */

function binary(name) {
  return join('node_modules', '.bin', name);
}


/**
 * Test if port 9876 is open and create a
 * static file server if not
 *
 * @api private
 * @param {Function} cb `function (server_handle)`
 */

function server(cb) {

  /**
   * Callback for back connections
   *
   * Will start a server (st) and invoke `cb(server)`
   *
   * @api private
   */

  function notOpen() {
    try {
      // jshint -W040
      this.destroy();
    } catch (err) {}

    var s = null,
        mount = st({ path: process.cwd() });

    s = http.createServer(function (req, res) {
      if (!mount(req, res)) {
        res.statusCode = 404;
        res.end('404');
      }
    });

    s.listen(9876, function () {
      cb(s);
    });
  }

  var socket = new Socket();

  socket
    .on('connect', function () {
      this.end();
      cb(null);
    })
    .on('error', notOpen)
    .on('timeout', notOpen)
    .setTimeout(400);

  socket.connect(9876, 'localhost');
}

/**
 * Map a .html file on the fs to a URL
 *
 * @api private
 * @param {String} test
 * @return {String}
 */

function testToUrl(test) {
  if (windows) {
    var dir = __dirname.replace(/\\/g, '/');
    return test
      .replace(/\\/g, '/')
      .replace(dir, 'http://localhost:9876');
  }

  return test.replace(__dirname, 'http://localhost:9876');
}

/**
 * Filter-out coverage files
 *
 * @param {String} file
 * @api private
 */

function coverage(file) {
  var basename = path.basename(file);
  return -1 === basename.indexOf('coverage');
}
