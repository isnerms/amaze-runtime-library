
var rimraf = require('rimraf').sync;

exports = module.exports = clean;

exports.paths = [
  './components',
  './dist'
];

function clean() {
  console.log('rm -rf %s', exports.paths.join(' '));
  for (var i = 0, path; path = exports.paths[i]; i++) {
    rimraf(path);
  }
}

if (!module.parent) {
  clean();
}