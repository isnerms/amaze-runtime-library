
var spawn = require('win-fork');
var path = require('path');
var Builder = require('component-builder');
var fs = require('fs');
var pkg = require('./package.json');
var mkdir = require('mkdirp').sync;
var copyleft =
  fs.readFileSync(path.join(__dirname, 'copyright.txt'), 'utf-8');

var component = path.join('.', 'node_modules', '.bin', 'component');

var dev = -1 !== process.argv.indexOf('--dev');

install(dev, function (err) {
  if (err) {
    throw err;
  }

  var builder = new Builder('.');

  if (dev) {
    builder.addSourceURLs();
    builder.development();
  }

  builder.build(function (err, res) {
    if (err) {
      throw err;
    }

    var js;

    if (dev) {
      js = copyleft
         + 'var amaze;\n'
         + res.require
         + filter(res.js)
         + '\namaze = require("amaze");';
    } else {
      js = copyleft
         + 'var amaze;\n'
         + '(function (window, document, undefined) {\n'
         + '"use strict";\n'
         + res.require
         + filter(res.js)
         + 'amaze = require("amaze");\n'
         + '}(window, document));';
    }

    js = js.replace(/{{version}}/g, pkg.version);

    var dist = path.join('.', 'dist');
    log('mkdir -p', dist);
    mkdir(dist);
    var file = path.join(dist, 'amaze.js');
    log('write', file);
    fs.writeFileSync(file, js);
  });

});


/**
 * Install dependency components
 *
 * @api private
 * @param {Boolean} [dev]
 */

function install(dev, fn) {
  var args = [ 'install' ];
  if (dev) {
    args.push('--dev');
  }
  spawn(component, args, { stdio: 'inherit' })
  .on('close', function (code) {
    if (code) {
      var err = new Error('component install exited with ' + code);
      err.code = code;
      return fn(err);
    }

    fn(null);
  });
}

/**
 * Filter duplicate aliases form the given `js`
 *
 * @api private
 * @param {String} js
 * @return {String}
 */

function filter(js) {
  var lines = js.split('\n');
  var res = [];

  lines.forEach(function (line) {
    if (line.substring(0, 15) !== 'require.alias("') {
      //
      // non-alais lines of code are always added
      //
      res.push(line);
    } else if (res.indexOf(line) === -1) {
      //
      // non-duplicate aliases are added
      //
      res.push(line);
    }
  });

  return res.join('\n');
}

/**
 * Log the given `type` with `msg`.
 *
 * @param {String} type
 * @param {String} msg
 * @api public
 */

function log(type, msg, color){
  color = color || '36';
  var w = 10;
  var len = Math.max(0, w - type.length);
  var pad = Array(len + 1).join(' ');
  console.log('  \033[' + color + 'm%s\033[m : \033[90m%s\033[m', pad + type, msg);
}
