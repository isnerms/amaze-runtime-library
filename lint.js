
var spawn = require('win-fork');
var path = require('path');
var globs = require('globs').sync;

var JSHINT = path.join(__dirname, 'node_modules', '.bin', 'jshint');

/**
 * Lint the given `files`, invoking `cb(exit_code)`
 *
 * @api private
 * @param {Array} files
 * @param {Function} cb
 */

function lint(files, cb) {
  var args = [ '--verbose' ].concat(files);
  spawn(JSHINT, args, { stdio: 'inherit' }).on('close', cb);
}


var source = globs(path.join('src', '**', '*.js')).filter(function (file) {
  // ignore components
  return !~file.indexOf('components');
});

lint(source, function (code) {
  if (code) {
    process.exit(code);
  }

  var tests = globs(path.join('test', '**', '*.js'));
  lint(tests, function (code) {
    if (code) {
      process.exit(code);
    }

    lint(globs('*.js'), process.exit);
  });
});
