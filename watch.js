
'use strict';

var monocle = require('monocle')(),
    path = require('path'),
    fs = require('fs'),
    log = require('component').utils.log,
    spawn = require('win-fork');

var ROOT_DIR = __dirname,
    SRC_DIR = path.join(ROOT_DIR, 'src');

/**
 * Get all local library dependencies
 *
 * @api private
 * @return {Array}
 */
function libraryDependencies() {
  var file = path.join(SRC_DIR, 'library/component.json'),
      data = fs.readFileSync(file, 'utf8'),
      json = JSON.parse(data);

  return json.local;
}

/**
 * Check if the given `stats` belongs to a dependency
 *
 * @api private
 * @param {fs.Stats} stats
 * @return {Boolean}
 */
function isDependency(stats) {
  return !!~stats.path.indexOf('/components/');
}

/**
 * Check if the given `stats` belong to the library or one of its dependencies
 *
 * @api private
 * @param {fs.Stats} stats
 * @return {Boolean}
 */
function isLibrary(stats) {
  var libs = libraryDependencies(),
      parent = stats.parentDir;

  if (parent === 'core') {
    return false;
  }

  if (parent === 'library') {
    return true;
  }

  return !!~libs.indexOf(parent);
}

/**
 * Build the `mod` based on controller `obj`
 *
 * @api private
 * @param {String} mod
 * @param {Object} obj
 */
function build(mod, obj) {
  if (obj.running) {
    return;
  }

  if (obj.timeout) {
    clearTimeout(obj.timeout);
  }

  obj.timeout = setTimeout(function () {
    obj.running = true;

    var args = [ path.join(ROOT_DIR, 'build.js'), '--dev', mod ],
        opts = { stdio: 'inherit' };

    spawn('node', args, opts).on('close', function (code) {
      if (code) {
        var err = new Error('Failed to build ' + mod + ': ' + code);
        console.error(err);
      }

      obj.timeout = null;
      obj.running = false;
    });
  }, 1000);
}

// controller objects
//   won't let two of the same builds occur at the same time
var library = {},
    core = {};



/**
 * Callback for file alteration (change, addition, removal, etc.)
 *
 * @api private
 * @param {fs.Stats} stats
 */
function handleChange(stats) {
  if (isDependency(stats)) {
    return;
  }

  console.log();
  log('changed', stats.path, 33);

  if (isLibrary(stats)) {
    return build('library', library);
  }

  build('core', core);
}


monocle.watchPaths({
  path: [ SRC_DIR ],
  // we want .js and .json
  fileFilter: '*.js*',
  listener: handleChange,
  complete: function () {
    console.log('watching src/**/*.js* for changes...');
  }
});
