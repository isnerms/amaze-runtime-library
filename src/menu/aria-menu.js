// jshint maxstatements: 31

'use strict';

var selector = require('selector'),
    events = require('events'),
    array = require('array'),
    collections = require('collections'),
    dom = require('dom');

/**
 * Marks up an unordered list hierarchy that is implemented as a mouse menu such that it works as a
 * keyboard accessible WAI-ARIA menu
 *
 * **Selectors Array**:
 *
 * - `selector` _String_ the selector from the container object used to identify the set of
 *    elements that make up the children (e.g. the menuitems or sub-menus). If the selector is dynamically generated
 *    then the string '{index}' is the placeholder for the dynamically generated portion of the selector.
 *    This is used for example when some attribute on the parent menu (or some other state variable) contains a
 *    reference to the sub-menu.
 * - `indexGetter` _Function_ *Optional* if a selector uses a dynamically generated portion, the dynamically generated
 *    portion is the index of the item within its parent (indexed at 0). This mechanism cannot deal with all
 *    situations, when this is the case, indexGetter can be used to supply a function that will return the dynamic
 *    portion of the selector. The function receives the parent menu item's element as an argument.
 * - `role` _String_ the role of the children elements. This should almost always be 'menuitem' which is the default
 * - `sanitize` _Function_ *Optional* a function to call on each of the children elements to
 *    clear out any unrequired attributes on the children (e.g. remove event handlers, remove attributes etc.)
 * - `expand` _Object_ *Optional* information about how to expand the sub-menu that contains the children. If
 *    not supplied, ariaMenu will fire a mouseover event followed by a mousemove event onto the element itself:
 *    - `target` _String_ contains either 'self' or 'child' indicating whether the event is to
 *       be fired on the container (self) or the child node
 *    - `selector` _String_ *Optional* selector to find the element within the target on which to fire expand event
 *    - `event` _String_ *Optional* which event must be fired to expand the sub-menu
 *    - `addClass` _String_ *Optional* a class to be added to the final target in order to expand it
 *    - `removeClass` _String_ *Optional* a class to be removed from the final target in order to expand it
 * - `timeout` _Integer_ *Optional* if this value is supplied and the child element is not visible when the focus is
 *    to be placed on that element, then a timeout will be set with a duration of the value of this attribute. When
 *    the timeout fires, if the child is visible, it will receive focus, otherwise the parent menu will retain focus.
 * - `clickSelector` _String_ *Optional* selector to use to select the target of the click event
 *    that is fired when the user presses the Enter key to activate a menu item
 * - `preselection` _String_ *Optional* contains the (document level) selector that will be the context of the
 *    submenu selector and the ownsSelector. Normally set to 'body', which will have the effect of making the
 *    children selector operate on the entire document body. This is normally used in conjunction with ownsSelector
 *    to deal with menus whose submenus are not contained within the HTML hierarchy of the menu itself.
 * - `ownsSelector` _String_ *Optional* selector to find the container of the children elements when
 *    these are not in the DOM subtree of their parent menu. This will result in WAI-ARIA aria-owns attributes being
 *    applied to the element selected by this selector to create the correct relationship with the parent menu.
 *    If the selector is dynamically generated then the string '{index}' is the placeholder for the dynamically
 *    generated portion of the selector. This is used for example when some attribute on the parent menu (or some other
 *    state variable) contains a reference to the sub-menu.
 * - `collapse` _Object_ *Optional* information about how to collapse the menu in which these children are
 *    contained, if not supplied, ariaMenu will fire a mouseout event on the parent menu:
 *    - `target` _String_ either 'parent' or 'self' indicates where the event should be fired
 *    - `selector` _String_ *Optional* selector within the target to use to find the actual node to
 *       deliver the event to.
 *    - `event` _String_ *Optional* which event to fire to collapse the menu
 *    - `addClass` _String_ *Optional* a class to be added to the final target in order to collapse it
 *    - `removeClass` _String_ *Optional* a class to be removed from the final target in order to collapse it
 *
 * Example:
 *
 * ```javascript
 *   var ariaMenu = require( 'lib/menu').ariaMenu,
 *     ul = '#primaryNavigation',
 *     childrenSelectors = [
 *       {
 *         preselection: [],
 *         selector: '>li',
 *         role: 'menuitem',
 *         sanitize: function () {
 *           this.removeAttribute('onfocus');
 *           this.removeAttribute('onclick');
 *         },
 *         expand: { event: 'click', target: 'self', selector: 'a>span'},
 *         clickSelector: 'a>span'
 *       }, {
 *         preselection: [],
 *         ownsSelector: null,
 *         selector: '>ul>li',
 *         collapse: { event: 'click', target: 'parent', selector: 'span'},
 *         role: 'menuitem',
 *         clickSelector: 'a'
 *       }
 *     ];
 *
 *   ariaMenu(ul, childrenSelectors);
 *```
 *
 * Example:
 *
 * ```js
 * var ariaMenu = require( 'lib/menu').ariaMenu,
 *   ul = '#primaryNav>ul',
 *   childrenSelectors = [
 *    {
 *      selector: '>li',
 *      role: 'menuitem',
 *      sanitize: function () {
 *      },
 *      expand: { addClass: 'amaze-menu-expanded', target: 'self', selector: '>ul'},
 *      clickSelector: 'a>span'
 *    }, {
 *      selector: '>ul>li>ul>li',
 *      collapse: { removeClass: 'amaze-menu-expanded', target: 'parent', selector: '>ul'},
 *      role: 'menuitem',
 *      clickSelector: 'a'
 *    }
 *   ];
 *
 * ariaMenu(ul, childrenSelectors);
 * ```
 * Example:
 *
 * ```js
 * // This example shows the use of a timeout of 100ms when opening up the third level menu,
 * // an example of a preselection that resets to the body before performing the selectors on the submenus,
 * // an example of an ownsSelector in combination with the preselector, and
 * // an example of selectors with a dynamically generated portion using an indexGetter callback
 *
 * var ariaMenu = require('lib/menu').ariaMenu,
 *   options, topmenuSelector;
 *
 * topmenuSelector = '#nav-bar-inner';
 * options = [
 *   {
 *     selector : 'a.nav-shop-all-button',
 *     role : 'menu'
 *   },
 *   {
 *     preselection : 'body',
 *     ownsSelector : '#nav_cats',
 *     selector : '#nav_cats>li',
 *     timeout : 100,
 *     clickSelector : '>a',
 *     role : 'menuitem'
 *   },
 *   {
 *     preselection : 'body',
 *     indexGetter : function(el) {
 *       var id = el.getAttribute('id');
 *       return id.substring(8);
 *     },
 *     ownsSelector : '#nav_subcats_{index}',
 *     selector : '#nav_subcats_{index}>ul>li',
 *     clickSelector : '>a',
 *     role : 'menuitem'
 *   }
 * ];
 * ariaMenu( topmenuSelector, options);
 * ```
 *
 *
 * @api public
 * @name menu.ariaMenu
 * @param {String} containers  Selector for the top level ul
 * @param {Array} selectors    array of option objects for the sub menus:
 * @author  dylan.barrell@deque.com
 */
module.exports = (function () {
  var queryAll = selector.queryAll,
    query = selector.query,
    on = events.on,
    clone = array.clone,
    fire = events.fire,
    each = collections.each,
    addClass = dom.addClass,
    removeClass = dom.removeClass,
    getElementCoordinates = dom.getElementCoordinates,
    isVisible = dom.isVisible,
    ignoreFocus = false;

  function executeSelectorOptions(that, options) {
    if (options.selector) {
      that = query(options.selector, that);
    }
    if (options.event) {
      fireEvent(that, { type : options.event});
    }
    if (options.removeClass) {
      removeClass(that, options.removeClass);
    }
    if (options.addClass) {
      addClass(that, options.addClass);
    }
  }

  function expandMenu(that, ourSelector, children) {
    var bcr, x, y;

    if (children && children.length > 0) {
      if (ourSelector.expand) {
        if (ourSelector.expand.target && ourSelector.expand.target === 'child') {
          that = children[0];
        }
        executeSelectorOptions(that, ourSelector.expand);
      } else {
        bcr = getElementCoordinates(that);
        x = bcr.left + bcr.width / 2;
        y = bcr.top + bcr.height / 2;
        fireEvent(that, { type: 'mouseover'}, { pageX : x, pageY : y, clientX : x, clientY : y });
        fireEvent(that, { type: 'mousemove'}, { pageX : x, pageY : y, clientX : x, clientY : y });
      }

      if (isVisible(children[0]) || !ourSelector.timeout) {
        children[0].focus();
      } else if (ourSelector.timeout) {
        setTimeout(function () {
          if (isVisible(children[0])) {
            children[0].focus();
          } else {
            that.focus();
          }
        }, ourSelector.timeout);
      }
    }
  }
  function collapseMenu(that, ourSelector, parentMenu) {

    if (ourSelector.collapse) {
      if (ourSelector.collapse.target && ourSelector.collapse.target === 'parent') {
        that = parentMenu;
      }
      executeSelectorOptions(that, ourSelector.collapse);
    } else {
      fireEvent(that, { type : 'mouseout'});
    }
    if (parentMenu) {
      parentMenu.focus();
    }
  }
  /**
   *
   * private function used to fire events
   *
   * @api private
   * @param {HTMLNode}  target     the element that is to receive the event
   * @param {Object}    eInfo      event info structure with type, charCode, ctrlKey, altKey, shiftKey,
   *                               metaKey and keyCode
   * @return undefined
   */
  function fireEvent(target, eInfo, augment) {

    fire(target, eInfo.type, augment);
  }

  function makeAriaMenu(parentMenu, container, ourSelector, childSelectors, nextMenu, prevMenu, menuBar, ourIndex) {
    var parent, selector, children, id, owns;

    function getParentAt(el, index) {
      var retVal, i;
      retVal = el;
      for (i = 0; i < index && retVal; i++) {
        retVal = retVal.parentNode;
        if (retVal.nodeName === 'BODY') {
          break;
        }
      }
      return retVal;
    }

    function findCommonParent(arr) {
      var index = 0, p1, p2;
      while (true) {
        p1 = getParentAt(arr[0], index);
        p2 = getParentAt(arr[1], index);
        if (p1 === p2) {
          return p1;
        }
        if (p1.nodeName === 'BODY') {
          return null;
        }
        index += 1;
      }
    }
    function disableFocusOnSubtree(container) {
      var anchors;
      // make all anchors non-naturally tab focussable so tab out will work
      anchors = queryAll('a', container);
      each(anchors, function (value) {
        value.setAttribute('tabindex', '-1');
      });
      container.setAttribute('tabindex', '0');
      on(container, 'focus', function () {
        if (!ignoreFocus) {
          children[0].focus();
        }
      });
    }
    function processChildren() {
      // jshint maxcomplexity:10
      var csCopy, context, ownsSelector, childSelector, gotIndex = ourIndex, commonParent;

      csCopy = clone(childSelectors);
      context = container;
      selector = csCopy.splice(0, 1)[0];
      if (selector.indexGetter) {
        gotIndex = selector.indexGetter(container);
      }
      if (selector.preselection) {
        context = query(selector.preselection);
        if (selector.ownsSelector) {
          ownsSelector = selector.ownsSelector;
          // need aria-owns
          if (ownsSelector.indexOf('{index}') !== -1) {
            ownsSelector = ownsSelector.replace('{index}', gotIndex);
          }
          owns = query(ownsSelector, context);
          id = owns.getAttribute('id');
          if (!id) {
            // create an id
            id = 'amaze-' + Math.random().toString().substring(2);
            owns.setAttribute({'id': id});
          }
          owns.setAttribute('role', 'menu');
          container.setAttribute('aria-owns', id);
        }
      }
      childSelector = selector.selector;
      if (childSelector.indexOf('{index}') !== -1) {
        childSelector = childSelector.replace('{index}', gotIndex);
      }
      children = queryAll(childSelector, context);
      if (!owns) {
        commonParent = findCommonParent(children);
        if (commonParent) {
          commonParent.setAttribute('role', 'menu');
        }
      }
      if (children.length) {
        container.setAttribute('aria-haspopup', 'true');
      }
      container.setAttribute('role', ourSelector.role);
      each(children, function (value, index) {
        var pMenu = container;

        if (selector.sanitize) {
          selector.sanitize.call(value);
        }
        if (ourSelector.role === 'menubar') {
          pMenu = null; // make next level's parents null so keyboard nav works
        }
        makeAriaMenu(pMenu, value, selector, csCopy,
          (index < children.length - 1) ? children[index + 1] : children[0],
          (index > 0) ? children[index - 1] : children[children.length - 1],
          menuBar, index);
      });
    }
    if (ourSelector.role === 'menubar') {
      disableFocusOnSubtree(container);
    }
    if (childSelectors.length > 0) {
      try {
        processChildren();
      } catch (err) {
        //debugOut(err);
      }
    } else {
      container.setAttribute('role', ourSelector.role);
    }
    on(container, 'keydown', function (e) {
      // jshint maxcomplexity: 21
      var keyCode = e.which || e.keyCode,
        stop = false;

      // the only key that can be modified by shift is TAB
      if (e.ctrlKey || (keyCode !== 9 && e.shiftKey)) {
        return;
      }

      switch (keyCode) {
      case 13: // ENTER
        if (children && children.length > 0) {
          expandMenu(this, ourSelector, children);
        } else {
          if (ourSelector.clickSelector) {
            query(ourSelector.clickSelector, this).click();
          } else {
            this.click();
          }
        }
        stop = true;
        break;
      case 37: // LEFT
        if (!parentMenu) {
          prevMenu.focus();
        } else {
          collapseMenu(this, ourSelector, parentMenu);
        }
        stop = true;
        break;
      case 27: // ESC
        collapseMenu(this, ourSelector, parentMenu);
        stop = true;
        break;
      case 38: // UP
        if (prevMenu) {
          prevMenu.focus();
        }
        stop = true;
        break;
      case 39: // RIGHT
        if (!parentMenu) {
          nextMenu.focus();
          stop = true;
          break;
        } else if (children && children.length > 0) {
          expandMenu(this, ourSelector, children);
        }
        stop = true;
        break;
      case 40: // DOWN
        if (!parentMenu) {
          expandMenu(this, ourSelector, children);
        } else {
          if (nextMenu) {
            nextMenu.focus();
          }
        }
        stop = true;
        break;
      case 9: // TAB
        if (e.shiftKey && e.target !== menuBar) {
          ignoreFocus = true;
        } else {
          ignoreFocus = false;
        }
        collapseMenu(this, ourSelector, parentMenu);
        break;
      }
      if (stop) {
        e.preventDefault();
        e.stopPropagation();
      }
    });
    if (ourSelector.role !== 'menubar') {
      parent = container.parentNode;
      while (parent && parent.setAttribute && parent !== owns) {
        if (!parent.getAttribute('role')) {
          parent.setAttribute('role', 'presentation');
        } else {
          // will break at the menubar
          break;
        }
        parent = parent.parentNode;
      }
      container.setAttribute('tabindex', '-1');
    }
  }

  function entry(containers, selectors) {
    var elements = queryAll(containers);

    each(elements, function (value, index) {
      makeAriaMenu(null, value, { role: 'menubar'}, selectors, null, null, value, index);
    });
  }
  return entry;
}());
