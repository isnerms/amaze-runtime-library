
'use strict';

var dom = require('dom'),
    array = require('array');

//
//
// TODO
//
// replace with something better tested that we
// don't have to maintain ourselves
//
//     component/clone
//
//

/**
 * Clones an object.  Does not operate on DOM nodes; if
 * you need to clone a node, use `Node#cloneNode()`.
 *
 * Examples:
 *
 * ```js
 * var clone = require('lib/clone');
 * // Use clone
 * a = clone(b);
 * ```
 *
 * @api public
 * @name clone
 * @param {Mixed} obj The thing to clone.
 * @return {Mixed} A copy of what was passed in
 */
module.exports = function clone(obj) {
  var index, length,
    out = obj;

  if (obj !== null && typeof obj === 'object' && !dom.isNode(obj)) {

    if (array.isArray(obj)) {
      out = [];
      for (index = 0, length = obj.length; index < length; index++) {
        out[index] = clone(obj[index]);
      }

    } else {
      out = {};

      // jshint forin: false
      for (index in obj) {
        out[index] = clone(obj[index]);
      }

    }

  }

  // primitive types are passed by value
  return out;
};
