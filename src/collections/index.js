
/*!
 *
 * The "collections" namespace.  All of these methods are
 * aliased directly as "library.{method}" for legacy
 * purposes.
 *
 * TODO deprecate direct usage off of "library.{method}".
 *
 */

exports.keys = require('./keys');

exports.each = require('./each');

exports.extend = require('./extend');

exports.clone = require('./clone');
