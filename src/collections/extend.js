
'use strict';

var clone = require('./clone'),
    array = require('array');

//
//
// TODO
//
// replace with something better tested that we
// don't have to maintain ourselves
//
//    segmentio/extend
//
// also, this is embarrsingly hard to use it
// **NEEDS A REFACTOR** if it isn't just replaced
// all together
//

/**
 * Extend an object.  Will not work on Nodes and will optionally deep copy
 *
 * Examples:
 *
 * ```js
 * var extend = require('lib/extend');
 * // Use extend
 * a = extend( a, b);
 * ```
 *
 * @api public
 * @name extend
 * @param {Object} to The object to extend
 * @param {Object} from The object that will extend `to`
 * @param {Boolean} deep True to deep extend
 */
module.exports = function extend(to, from, deep) {
  var prop, value;

  deep = deep || false;

  for (prop in from) {
    if (from.hasOwnProperty(prop)) {
      value = from[prop];
      // deep extend an object's objects, but not arrays (when deep is truthy)
      if (typeof value === 'object' && value !== null && deep && !array.isArray(value)) {
        to[prop] = extend(to[prop] ? to[prop] : {}, from[prop], deep);
      } else {
        to[prop] = clone(from[prop]);
      }
    }
  }

  return to;
};
