
'use strict';

//
//
// TODO
//
// replace with something better tested that we
// don't have to maintain ourselves
//
//    component/each
//

/**
 * Execute a `Function` once per item of an `Array` or enumerbale property
 * of an `Object`.  This is not an `Array#forEach` polyfil; the `this`
 * keyword is **not** the `Array` itself.
 *
 * Examples:
 *
 * ```js
 * var each = require('lib/each');
 * // Array
 * each([1, 2, 3], function (item, index, array) {
 *   assert(array[index] === item)
 *   //=> true
 * });
 *
 * // Object
 * each({ foo: 'bar', no: true }, function (value, property, object) {
 *   assert(object[property] === value)
 *   //=> true
 * })
 * ```
 *
 * @api public
 * @name each
 * @param {Array|Object} object An Array-like Object, or plain Object
 * @param {Function} iterator Function to be executed once per item/property
 */
module.exports = function (object, iterator) {
  var index, prop,
    length = object.length;

  //
  // has a `length` property which is a whole number
  //
  // Array, NodeList, Arguments, ...
  if (length && parseInt(length, 10) === length) {
    for (index = 0, length = object.length; index < length; index += 1) {
      iterator(object[index], index, object);
    }
    return;
  }

  // Object, Node, ...
  for (prop in object) {
    if (object.hasOwnProperty(prop)) {
      iterator(object[prop], prop, object);
    }
  }
};
