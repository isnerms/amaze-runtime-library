
'use strict';

//
//
// TODO
//
// replace with something better tested that we
// don't have to maintain ourselves
//
//     matthewp/keys
//
//

/**
 * Get an Object's enumerable owned properties as an Array
 *
 * Example:
 *
 * ```js
 * var keys = require('lib/keys'),
 *   o = {
 *     foo: 'bar',
 *     baz: 'true',
 *     quax: false
 *   };
 * keys(o);
 * //=> [ 'foo', 'baz', 'quax' ]
 * ```
 *
 * @api public
 * @name keys
 * @param {Object} obj
 * @return {Array}
 */
module.exports = Object.keys || function (obj) {
  var res = [],
      prop = null;

  for (prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      res.push(prop);
    }
  }

  return res;
};
