
'use strict';

var direct = require('./direct');
var delegate = require('./delegate');

// export "direct" event functions (on, off, once)
for (var fn in direct) {
  if (direct.hasOwnProperty(fn) && 'function' === typeof direct[fn]) {
    exports[fn] = direct[fn];
  }
}

// export "delegate" event listeners (delegate, undelegate)
for (var fn in delegate) {
  if (delegate.hasOwnProperty(fn) && 'function' === typeof delegate[fn]) {
    exports[fn] = delegate[fn];
  }
}

exports.fire = require('./fire');
