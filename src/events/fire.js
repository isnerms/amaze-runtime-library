
'use strict';

var selector = require('selector');
var object = require('collections');
var util = require('./util');

/**
 * Create and fire an `HTMLEvent` on an element
 *
 * Examples:
 *
 * ```js
 * var events = require('lib/events');
 *
 * events.fire(document.body, 'click');
 *
 * events.fire('body', 'click');
 * ```
 *
 * @api public
 * @name events.fire
 * @param {Mixed}  element   The HTMLElement (or selector for one) to fire an event on
 * @param {String} eventType The type of event to fire (e.g. click, mouseover)
 * @param {Object} [augment] Augments the event object (e.g. `{ which: 13 }`)
 */

module.exports = (function () {
  if (document.createEvent) {
    return function (element, eventType, augment) {
      var event;

      if (eventType === 'click') {
        event = document.createEvent('MouseEvents');
      } else {
        event = document.createEvent('HTMLEvents');
      }

      if (typeof element === 'string') {
        element = selector.query(element);
      }

      if (augment) {
        util.validateProperties(augment);
        object.extend(event, augment);
      }

      event.initEvent(eventType, true, true);
      element.dispatchEvent(event);
    };
  }

  return function (element, eventType, augment) {
    var event = document.createEventObject();

    if (typeof element === 'string') {
      element = selector.query(element);
    }

    if (augment) {
      // IE will not set which unless keyCode is also set
      if (augment.which && !augment.keyCode) {
        augment.keyCode = augment.which;
      }
      util.validateProperties(augment);
      object.extend(event, augment);
    }

    element.fireEvent('on' + eventType, event);
  };
}());
