
'use strict';

var selector = require('selector');
var util = require('./util');
var ev = require('./direct');


/**
 * Attach a delegated event handler to an element
 *
 * Examples:
 *
 * ```js
 * var events = require('lib/events');
 *
 * events.delegate(document.body, 'span.foo', 'click', clickHandlerCallback)
 *
 * events.delegate('body', 'span.foo', 'click', clickHandlerCallback)
 *
 * events.delegate('body', 'span.foo', 'keydown', function (keyboardEvent) {
 *   if (keyboardEvent.which === 13) {
 *     keyboardEvent.click();
 *   }
 * });
 * ```
 *
 * @api public
 * @name events.delegate
 * @param {HTMLElement|String} root The context element, or a selector for one
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 */

exports.delegate = function (root, target, type, fn) {
  var _fn = fn[util.delegateExpando] = function (event) {
    var element = event.target;
    if (selector.matches(element, target)) {
      fn.call(element, event);
    }
  };

  if ('string' === typeof root) {
    root = selector.query(root);
  }

  ev.on(root, type, _fn);
  return _fn;
};

/**
 * Remove a delegated event listener from an HTMLElement.  Does **not**
 * remove events bound with `events.on`.
 *
 * Examples:
 *
 * ```js
 * var events = require('lib/events');
 *
 * // remove a single delegated event
 * events.undelegate(element, 'click', clickHandler);
 *
 * events.undelegate('#foo', 'click', clickHandler);
 *
 * // remove all delegated events
 * events.undelegate(element, 'click');
 *
 * events.undelegate('#foo', 'click');
 * ```
 *
 * @api public
 * @name events.undelegate
 * @param {HTMLElement|String} element The HTMLElement (or selector for one) to remove a delegated event handler from
 * @param {String} type The event type to remove
 * @param {Function} [fn] The listener to remove
 */

exports.undelegate = function (element, type, fn) {
  if (!fn) {
    return ev.off(element, type);
  }

  return ev.off(element, type, fn[util.delegateExpando] || fn);
};
