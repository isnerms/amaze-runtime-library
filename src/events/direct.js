
'use strict';

var selector = require('selector');
var array = require('array');
var util = require('./util');

/**
 * Attach an event handler to an element
 *
 * Examples:
 *
 * ```js
 * var events = require('lib/events');
 *
 * events.on(document.getElementById('foo'), 'click', function (e) { // ... });
 *
 * events.on('#foo', 'click', function (e) { // ... });
 * ```
 *
 * @api public
 * @name events.on
 * @param {HTMLElement|String} element The HTMLElement or a selector for an HTMLElement
 * @param {String} type
 * @param {Function} fn
 */

exports.on = function (element, type, fn) {
  if ('string' === typeof element) {
    element = selector.query(element);
  }

  if (element.addEventListener) {
    util.addEventData(element, type, fn);
    element.addEventListener(type, fn, false);
    return fn;
  }

  // fallback
  var _fn;
  if (fn[util.expando]) {
    _fn = fn[util.expando];
  } else {
    _fn = fn[util.expando] = function (event) {
      fn.call(element, util.normalize(event));
    };
  }

  util.addEventData(element, type, _fn);
  element.attachEvent('on' + type, _fn);
  return _fn;
};

/**
 * Remove an event listener from an element.  If the event
 * callback is not provided, will remove all events which were
 * bound by `events.on`.
 *
 * Examples:
 *
 * ```js
 * var events = require('lib/events');
 *
 * // remove a single click listener
 * events.off(document.getElementById('foo'), 'click', clickHandlerCallback)
 *
 * events.off('#foo', 'click', clickHandlerCallback)
 *
 * // remove all click listeners
 * events.off(document.getElementById('foo'), 'click');
 *
 * events.off('#foo', 'click')
 *
 * ```
 *
 * @api public
 * @name events.off
 * @param {HTMLElement|String} element The HTMLElement or selector for one
 * @param {String} type
 * @param {Function} [fn]
 */

exports.off = function (element, type, fn) {
  // jshint maxstatements: 20, maxcomplexity: 10
  if (!type) {
    throw new TypeError('An event type must be provided');
  }

  if ('string' === typeof element) {
    element = selector.query(element);
  }

  var events = util.getBoundEvents(element, type);
  if (!events || !events.length) {
    return;
  }

  // support for removing all events of `type`
  if (!fn) {
    for (var i = 0, l = events.length; i < l; i++) {
      exports.off(element, type, events[i]);
    }
    return;
  }

  if (fn[util.expando]) {
    fn = fn[util.expando];
  }

  var index = array.indexOf(events, fn);
  if (-1 === index) {
    return;
  }

  // remove fn from data
  element[util.data].events[type].splice(index, 1);

  if (element.removeEventListener) {
    element.removeEventListener(type, fn);
  } else {
    element.detachEvent('on' + type, fn);
  }
};


/**
 * Attach an event handler to an element which will only fire
 * once.  The handler will be removed after its first
 * invocation.
 *
 * Examples:
 *
 * ```js
 * var events = require('lib/events');
 *
 * events.once(element, 'click', function () {
 *   alert('i only happen once!');
 *   // no need for events.off(...) here :)
 * });
 *
 * events.once('#hello .world', 'click', function () {
 *   alert('hello world!');
 * })
 * ```
 *
 * @api public
 * @name events.once
 * @param {HTMLElement|String} element The HTMLElement or selector for one
 * @param {String} type
 * @param {Function} callback
 */

exports.once = function (element, type, fn) {
  function handler(event) {
    // jshint validthis:true
    exports.off(this, type, handler);
    fn.call(this, event);
  }

  if ('string' === typeof element) {
    element = selector.query(element);
  }

  exports.on(element, type, handler);
};
