
'use strict';

var object = require('collections');
var array = require('array');

var rnd = Math.random() * 0x0deadbeef;
var DATA = exports.data = 'amaze-data-' + rnd;

exports.expando = 'amaze-on-' + rnd;
exports.delegateExpando = 'amaze-delegate-' + rnd;


/**
 * Get all events which were bound by `events.on`.  When
 * `type` is provided, will return all events of that
 * type.
 *
 * @api private
 * @param {HTMLElement} element
 * @param {String} type
 * @return {Object}
 */

exports.getBoundEvents = function (element, type) {
  if (!element || !element[DATA] || !element[DATA].events) {
    return null;
  }

  var events = element[DATA].events;

  return type
       ? events[type]
       : events;
};

/**
 * Add an event to the element's `data.events` namespace
 *
 * @api private
 * @param {HTMLElement} element
 * @param {String} type
 * @param {Function} fn
 */

exports.addEventData = function (element, type, fn) {
  element[DATA] = element[DATA] || {};
  element[DATA].events = element[DATA].events || {};
  element[DATA].events[type] = element[DATA].events[type] || [];
  element[DATA].events[type].push(fn);
};

/**
 * Normalizes the event object by adding:
 *
 * - offset[X/Y]
 * - relatedTarget
 * - which
 * - preventDefault
 * - stopPropagation
 * - target
 *
 * @api private
 * @param {Event} e
 * @return {Event}
 */

exports.normalize = function (event) {
  // jshint maxcomplexity:9
  event = event || window.event;

  // layers
  if (event.layerX) {
    event.offsetX = event.layerX;
    event.offsetY = event.layerY;
  }

  // set relatedTarget for mouse events
  if (!event.relatedTarget) {
    if (event.type === 'mouseover') {
      event.relatedTarget = event.fromElement;
    } else if (event.type === 'mouseout') {
      event.relatedTarget = event.toElement;
    }
  }

  // KeyboardEvent#which
  event.which = event.keyCode || event.charCode;

  // Event#target
  event.target = event.target || event.srcElement;

  // preventDefault and stopPropagation
  if (typeof event.preventDefault !== 'function') {
    event.preventDefault = function () {
      this.returnValue = false;
    };
    event.stopPropagation = function () {
      this.cancelBubble = true;
    };
  }

  return event;
};

/**
 * Validates event augment properties
 *
 * @api private
 * @param {Object} obj
 */

exports.validateProperties = function (obj) {
  var keys = object.keys(obj);
  if (array.inArray(keys, 'type')) {
    throw new Error('Cannot set event type');
  }

  if (array.inArray(keys, 'target')) {
    throw new Error('Cannot set event target');
  }
};
