
'use strict';

var trim = require('trim');

var html = document.documentElement;

/**
 * Get text of the provided `id` from the current `site`
 * with an optional `language`.  When `language` is
 * omitted, will default to the language of the page
 * (`<html lang="xxx" />`), or `en` for english.  If no
 * text is found for the `id` at the provided `language`,
 * `null` will be returned.
 *
 * Examples:
 *
 * ```js
 * var text = require('lib/locale').text;
 *
 * text('greeting', 'es');
 * //=> the spanish "greeting"
 *
 * text('greeting');
 * //=> the "greeting" in the locale of the page,
 * // or if no page locale is detected, english (en)
 *
 * text('foo', 'de');
 * //=> the "foo" text in german
 *
 * text('an invalid key', 'es');
 * //=> null
 *
 * text('greeting', 'an unsupported language');
 * //=> null
 * ```
 *
 * @api public
 * @name locale.text
 * @param {String} id The ID of the requested text
 * @param {String} [language] The requested language
 * @return {String} The text which matches the provided `id` and `language`
 */
exports = module.exports = function (id, language) {
  // cache the language if it hasn't been looked up already
  language = language || (exports.htmlLanguage = exports.htmlLanguage || lang());

  if (typeof amaze === 'undefined') {
    throw new Error('This method may only be used in unison with Amaze');
  }

  //exception prevention
  return amaze.__text[id] && amaze.__text[id][language] || null;
};

/**
 * Cache for `<html lang="..." />` lookups
 *
 * @api private
 * @type {String}
 */
exports.htmlLanguage = null;

/**
 * Get the `<html lang="..." />` attribute, broken
 * into a 2-character lower case string.
 *
 * @api private
 * @return {String}
 */
function lang() {
  var l = trim(html.getAttribute('lang') || 'en');
  if (l.length > 2) {
    l = l.substring(0, 2);
  }
  return l.toLowerCase();
}
