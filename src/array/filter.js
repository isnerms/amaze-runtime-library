
'use strict';

/**
 * Filter results from an Array.  This _acts_ like `Array#filter`, but is
 * not `Array#filter`.  The `this` keyword is not set.  You are only provided
 * the `item` parameter.  This is meant for speed, not to replicate ES5.
 *
 * Example:
 *
 * ```js
 * var filter = require('lib/array').filter,
 *   original = [1, 2, 3, 4, 5, 6];
 *
 * array.filter(original, function (item) {
 *   return !(item % 2);
 * });
 * // [2, 4, 6]
 * ```
 *
 * @api public
 * @name array.filter
 * @param {Array} array The Array to filter
 * @param {Function} fn The filtration test Function
 * @return {Array} The filtered Array
 */
module.exports = function (array, fn) {
  var index, item,
      result = [],
      length = array.length;

  for (index = 0; index < length; index += 1) {
    item = array[index];
    if (fn(item)) {
      result.push(item);
    }
  }

  return result;
};
