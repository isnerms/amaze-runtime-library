
'use strict';

var indexOf = require('./index-of');

/**
 * Check if an item is contained within an Array
 *
 * Examples:
 *
 * ```js
 * var inArray = require('lib/array').inArray;
 *
 * inArray([], 'hello'); //=> false
 * inArray([ 'hello' ], 'hello'); //=> true
 *
 * // it works on nodes
 * var nodes = document.getElementsByTagName('*');
 * inArray(nodes, document.body); //=> true
 * ```
 *
 * @api public
 * @name array.inArray
 * @param {Array} arr The array to test
 * @param {Mixed} needle The element to find
 * @param {Number} fromIndex The index to begin searching from (zero-based)
 * @return {Boolean}
 */
module.exports =  function (arr, needle, fromIndex) {
  return indexOf(arr, needle, fromIndex) !== -1;
};
