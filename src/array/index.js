
exports = module.exports = require('./to-array');

exports.toArray = module.exports;

exports.indexOf = require('./index-of');

exports.isArray = require('./is-array');

exports.inArray = require('./in-array');

exports.filter = require('./filter');

exports.clone = require('./clone');
