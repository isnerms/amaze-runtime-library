
'use strict';

/**
 * @api private
 * 
 * An unknown party left an anonymous comment in this file
 * noting that following code reproduces this module's intended
 * functionality:
 *
 *    module.exports = function (arr) {
 *      return [].slice.call(arr);
 *    }
 *
 * The above code passes existing unit tests, even in IE8 and IE5
 * quirks mode. It is assumed that there was a good reason for 
 * not making this change, but we should revisit this and possibly 
 * update this library module.
 *
 * matthew.isner@deque.com (2013-09-30)
 */

/**
 * Deep copy elements in the array
 *
 * Example:
 *
 * ```js
 * var copyOfElements,
 *   elements = ['foo', 'bar', 'baz'];
 *
 * copyOfElements = clone(elements);
 * copyOfElements === elements; // false
 * ```
 *
 * @api public
 * @name array.clone
 * @param {Array} arr The array over which to iterate
 * @return {Array} deep copy of the array passed in
 */

module.exports = function (arr) {
  var retVal = [], i, _ilen, item, prop, org;
  for (i = 0, _ilen = arr.length; i < _ilen; i++) {
    org = arr[i];
    if (typeof org === 'object') {
      item = {};
      for (prop in org) {
        if (org.hasOwnProperty(prop)) {
          item[prop] = org[prop];
        }
      }
    } else {
      item = org;
    }
    retVal[i] = item;
  }
  return retVal;
};