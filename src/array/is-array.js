
'use strict';

var toString = Object.prototype.toString;

/**
 * Check if something is an Array
 *
 * Examples:
 *
 * ```js
 * var isArray = require('lib/array').isArray;
 *
 * isArray(document.getElementsByTagName('*')); //=> false
 * isArray({}); //=> false
 * isArray(new Array()); //=> true
 * ```
 *
 * @api public
 * @name array.isArray
 * @param {Mixed} candidate The object to test
 * @return {Boolean}
 */
module.exports = Array.isArray || function (candidate) {
  return toString.call(candidate) === '[object Array]';
};
