
'use strict';

var slice = Array.prototype.slice,
    threw = false;

/**
 * Convert the given `list` to an Array, IE-style
 *
 * @api private
 * @param {Object} list
 * @return {Array}
 */
function ieConvert(list) {
  var i,
      len = list.length,
      item,
      result = [];

  for (i = 0; i < len; i++) {
    item = list[i];
    if (item !== undefined) {
      // don't do push in order to support sparse arrays properly
      result[i] = list[i];
    }
  }

  return result;
}

/**
 * Convert the given `list` to an Array
 *
 * @api private
 * @param {Object} list
 * @return {Array}
 */
function standardConvert(list) {
  return slice.call(list);
}

// check for ie8 bug
try {
  slice.call(document.getElementsByTagName('cats'));
} catch (iesux) {
  threw = true;
}

/**
 * Handles cohesion from a list to an Array
 *
 * @api public
 * @name array.toArray
 * @param {Object} list The list (Object with length)
 * @return {Array}
 */
module.exports = threw ? ieConvert : standardConvert;
