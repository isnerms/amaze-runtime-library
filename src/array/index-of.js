
'use strict';

/**
 * Get the index of an item in an Array
 *
 * Example:
 *
 * ```js
 * var indexOf = require('lib/array').indexOf;
 *
 * indexOf(['foo', 'bar'], 'foo')
 * //=> 0
 *
 * // it works on nodes
 * var nodes = document.getElementsByTagName('body');
 * indexOf(nodes, document.body);
 * //=> 0
 * ```
 *
 * @api public
 * @name array.indexOf
 * @param {Array|Array-like-object} list The array to test
 * @param {Mixed} needle The element to find
 * @param {Number} fromIndex The index to begin searching from (zero-based)
 * @return {Number} The index of the item within the Array; -1 if the item is not found
 */
module.exports = function (list, needle, fromIndex) {
  var length = list.length;

  fromIndex = fromIndex || 0;

  for (fromIndex; fromIndex < length; fromIndex += 1) {
    if (list[fromIndex] === needle) {
      return fromIndex;
    }
  }

  return -1;
};
