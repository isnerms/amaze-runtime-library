
//
// TODO
//   - require('live-regions')(TTL)
//   - proper tests

'use strict';

var domready = require('domready'),
    css = require('css');

/*!
 * @author David Sturley
 */

// the TTL for assertions (30 seconds)
var intervalID,
    ASSERTION_TTL = 30000,
    EXPIRE_INTERVAL = 1000,
    liveRegionStylesAdded = false,
    liveRegions = [];

/**
 * Makes an annoucement into the live region supplied
 *
 * @param {String} text - the text of the announcement
 * @param {HTMLElement} el - the live region into which the announcement is added
 * @api private
 */
function announce(text, el) {
  var p = document.createElement('p');

  p.innerHTML = text;
  el.appendChild(p);

  liveRegions.push({
    element: p,
    expires: new Date().getTime() + ASSERTION_TTL
  });

  if (!intervalID) {
    intervalID = window.setInterval(expire, EXPIRE_INTERVAL);
  }
}

/**
 * Interval callback
 * @api private
 */
function expire() {
  //jshint maxstatements:20
  var index, item, element,
    now = new Date().getTime(), region,
    windows = (navigator.userAgent.indexOf('Windows') !== -1);

  // loop backwards as we are splicing elements off
  for (index = liveRegions.length - 1; index >= 0; index--) {
    item = liveRegions[index];
    if (item.expires < now) {
      if (windows) {
        // JAWS messes up by announcing the removals as well as the additions
        // So remove and add the liveregions wholesale as soon as anything expires
        region = addLiveRegion('polite');
        region.parentNode.removeChild(region);
        addLiveRegion('polite');
        region = addLiveRegion('assertive');
        region.parentNode.removeChild(region);
        addLiveRegion('assertive');
        liveRegions = []; // clear the list
        return;
      }

      element = item.element;
      if (element && element.parentNode) {
        element.parentNode.removeChild(element);
      }
      liveRegions.splice(index, 1);
    }
  }

  if (!liveRegions.length) {
    intervalID = window.clearInterval(intervalID);
  }

}

/**
 * Bootstrap that adds the two live regions to the
 * document and the CSS required to position those
 * live regions off screen
 *
 * @api private
 * @param {HTMLElement} parent The element to add the live regions to; defaults to the `<body />`
 * @return {Number} The interval register
 */
function addLiveRegion(type, parent) {
  // optional parent
  parent = parent || document.body;

  var id = 'amaze_liveregions_' + type,
    element = document.getElementById(id);

  if (!element) {

    // Create the log regions
    // polite region
    element = document.createElement('div');
    element.id = id;
    element.className = 'amaze-offscreen';
    element.setAttribute('role', 'log');
    element.setAttribute('aria-live', type);
    element.setAttribute('aria-atomic', 'false');
    element.setAttribute('aria-relevant', 'additions');
    parent.appendChild(element);

    if (!liveRegionStylesAdded) {
      liveRegionStylesAdded = true;
      css([ '.amaze-offscreen {',
            '  position: absolute !important;',
            '  left: -999em !important;',
            '  top: -999em !important;',
            '}' ].join(''));
    }
  }

  return element;
}


// Create the public interfaces

var liveregions = module.exports = {};

/**
 * Make a polite announcement
 *
 * Example:
 *
 * ```js
 * var polite = require('lib/liveregions/polite');
 * polite('Hello');
 * ```
 *
 * @api public
 * @name liveregions.polite
 * @param {String} text The text of the polite announcment
 */
liveregions.polite = function (text) {
  var element = addLiveRegion('polite');
  announce(text, element);
};

/**
 * Make an assertive announcement
 *
 * Example:
 *
 * ```js
 * var assertive = require('lib/liveregions/assertive');
 * assertive('Hello');
 * ```
 *
 * @api public
 * @name liveregions.assertive
 * @param {String} text The text of the assertive announcement
 */
liveregions.assertive = function (text) {
  var element = addLiveRegion('assertive');
  announce(text, element);
};

// if live regions are not added before
// window.onload; they will not work in VoiceOver
domready(function () {
  addLiveRegion('polite');
  addLiveRegion('assertive');
});
