
'use strict';

var array = require('array'),
    collections = require('collections'),
    css = require('css'),
    date = require('date'),
    dom = require('dom'),
    events = require('events'),
    forms = require('forms'),
    liveRegions = require('live-regions'),
    locale = require('locale'),
    markup = require('markup'),
    menu = require('menu'),
    mutation = require('mutation'),
    selector = require('selector'),
    string = require('string');

exports.array = array;

exports.collections = collections;

exports.css = css;

exports.date = date;

exports.dom = dom;

exports.events = events;

exports.forms = forms;

exports.liveregions = exports['live-regions'] = liveRegions;

exports.locale = locale;

exports.markup = markup;

exports.menu = menu;

exports.mutation = mutation;

exports.selector = selector;

exports.string = string;

//
// for legacy reasons, collections.* are aliased as library.*
//
for (var method in collections) {
  if (collections.hasOwnProperty(method) && typeof collections[method] === 'function') {
    exports[method] = collections[method];
  }
}
