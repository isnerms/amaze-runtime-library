
'use strict';

var qwery = require('qwery');
var unique = require('uniq-selector');

require('qwery/pseudos/qwery-pseudos');

// export stuff

exports = module.exports = all;
exports.query = one;
exports.queryAll = all;
exports.matches = matches;

/**
 * Generate a unique selector for the given `element`.
 *
 * @api public
 * @name selector.unique
 * @param {HTMLElement} element
 * @return {String}
 */

exports.unique = unique;


/**
 * Query the DOM for elements matching the given
 * `selector`, optionally within `context`.
 *
 * @api public
 * @name selector.queryAll
 * @param {String} selector CSS-style element selector
 * @param {HTMLElement} [context] Top-level context
 * @return {Array} The element(s) matching the provided selector
 */

function all(selector, context) {
  return qwery(selector, normalize(context));
}

/**
 * Query the DOM for an element matching the given
 * `selector`, optionally within `context`.
 *
 * @api public
 * @name selector.query
 * @param {String} selector CSS-style element selector
 * @param {HTMLElement} [context] Top-level context
 * @return {HTMLElement} The first element matching the provided selector
 */

function one(selector, context) {
  var elements = all(selector, normalize(context));
  return elements.length
      ? elements[0]
      : null;
}


/**
 * Check if an HTMLElement matches a selector
 *
 * @api public
 * @name selector.matches
 * @param {HTMLElement} element The HTMLElement
 * @param {String} selector The CSS-style selector
 * @return {Boolean} Whether the HTMLElement matches
 */

function matches(element, selector) {
  return qwery.is(element, selector);
}

/**
 * Normalize the given `context.  Will throw if
 * `context` is invalid.
 *
 * @api private
 * @param {Mixed} context
 * @return {Object}
 */

function normalize(context) {
  // if given no argument, it's safe (expected)
  // to default to the document
  if (undefined === context) {
    return document;
  }

  // can't just `typeof context`, as `typeof null` is `object`
  if (context) {
    // selector
    if ('string' === typeof context) {
      var el = one(context, document);
      // ensure it resolves to an element
      return normalize(el);
    }

    // element
    if ('object' === typeof context) {
      return context;
    }
  }

  throw new TypeError('Invalid context');
}
