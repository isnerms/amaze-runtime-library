
'use strict';

var library = null;
var utilities = require('./utilities');
var $ = require('list');

// get the library namespace
try {
  library = require('library');
} catch (err) {}

var special = {
  css: require('css'),
  debugOut: require('./debug-out'),
  emitter: require('./emitter')
};

/**
 * Require the given `path`
 *
 * @api private
 * @param {String} path
 * @return {Mixed}
 */
exports = module.exports = function (path) {
  var module;
  var paths = path.split('/');
  var root = paths.shift();

  if (special[path]) {
    return special[path];
  }

  if (!paths.length) {
    // we will not just emit this error, but actually
    // throw it.  this should *not* be handled gracefully.
    throw new Error('You must require a module, you cannot require("' + path + '")');
  }

  if (root === 'lib') {
    // in standalone mode, there is no library
    if (!library) { throw new Error('No library provided'); }
    module = exports.get(library, paths, false);
  } else if (root === 'utils') {
    module = exports.get(utilities.get(), paths, true);
  }

  if (module === undefined) {
    // also should not be handled gracefully
    //
    // example exception:
    //   require('foo/bar/baz')
    //   Error: Failed to require "bar/baz" from "foo"
    //
    // yes, it's a bit hacky, but the thrown exception
    // looks very nice :p
    throw new Error('Failed to require "'
      + path.split('/').slice(1).join('/')
      + '" from "' + root + '"');
  }

  return module;
};

/**
 * Recurse an `obj` until the module matching `paths` is found
 *
 * @api private
 * @param {Object} obj
 * @param {Array} paths
 * @param {Boolean} [_export]
 * @return {Mixed}
 */
exports.get = function (obj, paths, _export) {
  var mod = obj[paths.shift()];

  // at the last /? and we found something?
  if (!paths.length && mod) {
    if (!_export) {
      return mod;
    }

    if (!mod.hasOwnProperty('exports')) {
      mod.exports = {};
      // function (window, document, require, module, exports, amaze) { ... }
      // TODO sourceURLs (see component-builder)
      mod(window, document, exports, mod, mod.exports, $);
    }
    return mod.exports;
  }

  // still an object left?  recurse further
  if (mod) {
    return exports.get(mod, paths, _export);
  }
};
