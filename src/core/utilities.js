'use strict';

var emitter = require('bus');

var table = {};

/**
 * Utility registration function
 *
 * @api private
 * @param {String} name
 * @param {Function} fn
 */
exports.register = function register(name, fn) {
  // validate
  if (!name || typeof fn !== 'function') {
    return emitter.emit('error',
      new TypeError('invalid utility: ' + String(name) + ' ' + String(fn)));
  }
  // register
  table[name] = fn;
};

/**
 * Get the registered utilities
 *
 * @api private
 * @return {Object}
 */
exports.get = function () {
  return table;
};
