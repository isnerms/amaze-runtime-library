/* globals amaze:true */

'use strict';

var amaze = module.exports = require('bus');
var injections = require('./injections');
var utilities = require('./utilities');
var _require = require('./require');
var statuses = require('./statuses');
var css = require('css');
var init = require('./init');

// publish the current version number
// will be expanded in the build
amaze.version = '{{version}}';

// set the default status
amaze.status = statuses.running.bootstrap;

/**
 * Output a message to the debugging console
 *
 * @api private
 * @deprecated
 * @param {Mixed} msg...
 */

amaze.debugOut = require('./debug-out');

/**
 * Register an injection `fn` with the given `priority`
 *
 * @api private
 * @param {Function} fn
 * @param {String} [priority]
 */

amaze.injection = injections.register;

/**
 * Register a utility `fn` to the given `name`
 *
 * @api private
 * @param {String} name
 * @param {Function} fn
 */

amaze.utility = utilities.register;

/**
 * Apply the given `css` to the page
 *
 * @api private
 * @param {String} css
 * @return {HTMLElement}
 */

amaze.css = css;

/**
 * Fetch stuff.
 *
 * @api private
 * @param {String} url
 */

amaze.init = init;

/**
 * Require the given `id`
 *
 * @api private
 * @param {String} id
 * @return {Mixed}
 */

amaze.require = _require;

/**
 * Expose the `amaze()` API.
 *
 * @api private
 * @param {Mixed} selector
 * @param {Mixed} context
 * @return {List}
 */

amaze.$ = require('list');

var stack = [];

amaze.on('injection', function (injection) {
  // if we've already fired the previous injections, just fire the new one
  if (amaze.status === statuses.done) {
    injections.fire(injection);
  } else {
    // wait for ready
    stack.push(injection);
  }
});

amaze.on('status', function (status) {
  amaze.status = status;

  if (status === statuses.ready) {
    // fire all loaded injections, then wipe out the stack
    injections.fire(stack);
    stack = [];
    // emit the "done" status, as we've fired the overlays
    amaze.emit('status', statuses.done);
  }
});

amaze.on('error', function (err) {
  if (typeof amazeFlags === 'object' && amazeFlags.debug) {
    throw err;
  }

  amaze.debugOut(err, err.message, err.stack);
});

// only one "url" event matters
amaze.once('url', function (url) {
  amaze.SERVER_URL = url;
});
