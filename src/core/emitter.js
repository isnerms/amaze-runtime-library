
'use strict';

var Emitter = require('emitter');

/**
 * Our singleton Emitter
 *
 * @api private
 */

var emitter = new Emitter;

/**
 * Emitter event cache.
 *
 * @api private
 */

emitter._cache = {};

/**
 * Bind an `event` handler `fn`.
 *
 * @api public
 * @name emitter.on
 * @param {String} event
 * @param {Function} fn
 */

exports.on = emitter.on;

/**
 * Bind a single-shot `event` handler `fn`,
 * removed immediately after it is invoked
 * the first time.
 *
 * @api public
 * @name emitter.once
 * @param {String} event
 * @param {Function} fn
 */

exports.once = emitter.once;

/**
 * Unbind an `event` handler `fn`.
 *
 * @api public
 * @name emitter.off
 * @param {String} event
 * @param {Function} fn
 */

exports.off = function (event, fn) {
  if ('string' !== typeof event) {
    throw new TypeError('Missing event type');
  }

  if ('function' !== typeof fn) {
    throw new TypeError('Missing handler function');
  }

  emitter.off(event, fn);
};

/**
 * Emit an `event` with variable option args.
 *
 * @api public
 * @name emitter.emit
 * @param {String} event
 * @param {Mixed} ...
 */

exports.emit = emitter.emit;

/**
 * Emit an `injection` completion event.
 *
 * @api private
 * @param {String} injection Injection name
 */

exports.complete = function (injection) {
  emitter.emit.call(emitter, injection);
  emitter._cache[injection] = true;
};


/**
 * Emit an `injection` skipped event. Currently
 * looks just like an injection completed event
 *
 * @api private
 * @param {String} injection Injection name
 */
exports.skipped = function (injection) {
  this.complete(injection);
};


/**
 * Bind an handler `fn` for after `injection` has run.
 *
 * @api public
 * @name emitter.after
 * @param {String} injection Injection name
 * @param {Function} fn
 */

exports.after = function (injection, fn) {
  if (emitter._cache[injection]) {
    fn();
  } else {
    emitter.once(injection, fn);
  }
};
