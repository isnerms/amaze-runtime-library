
'use strict';

// global amazeFlags

var emitter = require('bus'),
    statuses = require('./statuses'),
    siteMatch = require('./url-matching').matchesSiteRules,
    domready = require('domready'),
    inject = require('load-script');

var loc = window.location;
var hostname = loc.hostname;
var path = loc.pathname + loc.search;
var uri = '?hostname=' + encodeURIComponent(loc.hostname) + '&path=' + encodeURIComponent(path);

/**
 * Signal to the runtime that its ok to start
 * processing injections
 */
function ready() {
  emitter.emit('status', statuses.ready);
}

/**
 * Detects whether there is a cookie in the current page-
 * context to inhibit amaze from running, this could be
 * set by a bookmarklet or an extension
 */
function isInhibited() {
  return /inhibitamaze *= *true/i.test(document.cookie);
}

/**
 * Entry point for initializing amaze
 *
 * @api private
 *
 * @param  {String}  url       The URL of the Amaze Server, if in single file mode, undefined
 * @param  {Obbject} siteRegex A site regular expression object, only used in single file mode
 */
module.exports = function (url, siteRegex) {

  var hasFlags = typeof amazeFlags !== 'undefined';
  var isSingleFile = hasFlags && amazeFlags.singleFile === true;
  var isExtension = hasFlags && amazeFlags.extension === true;

  if (isSingleFile && isInhibited()) {
    return emitter.emit('status', statuses.notrun);
  }

  if (isSingleFile && !siteRegex) {
    return emitter.emit('error', new Error('Single-file mode requires a site regex object'));
  }

  // skip doing anything if in single page mode and the site doesn't match
  if (isSingleFile && !siteMatch(hostname, path, siteRegex)) {
    return emitter.emit('status', statuses.skipped);
  }

  emitter
    // send out the server url so we can set it
    .emit('url', url)
    // send out the current status (waitingn for the bcl)
    .emit('status', statuses.waiting);

  // don't wait to fetch anything
  if (isSingleFile || isExtension) {
    return domready(ready);
  }

  // inject the bcl, then fire `ready` once the dom allows for it
  inject(url + '/bcl.js' + uri, function () {
    domready(ready);
  });

  inject(url + '/overlays.js' + uri);
};
