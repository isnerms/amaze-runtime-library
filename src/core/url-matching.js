
'use strict';

/**
 * Matches a site rules object against both
 * hostname and path regular expressons
 *
 * @api private
 *
 * @param {String} hostname  The hostname of the page
 * @param {String} path      The path of the page
 * @param {Object} siteRegex The site regex object like:
 *                           	{
 *                           		include: /./,
 *                           		exclude: /foo/,
 *                           		path: {
 *                           			include: /./,
 *                           			exclude: /foo/
 *                           		}
 *                           	}
 *
 * @returns {Boolean}   True if Amaze regex rules match, false otherwise
 */
exports.matchesSiteRules = function(hostname, path, siteRegex) {

  var match = test(hostname, siteRegex);

  if (match && siteRegex.path) {
    match = test(path, siteRegex.path);
  }

  return match;
};

/**
 * Tests whether a regex object has matches at the
 * current include/exclude level
 *
 * @api private
 *
 * @param  {String}  str   The string to test against
 * @param  {Object}  regex An object with (optional) include/exclude regex
 * @return {Boolean}       True: include does not exist or matches && exclude
 *                               does not exist or does not match
 *                         False: any other condition
 */
function test(str, regex) {

  if (regex.include && !regex.include.test(str)) {
    return false;
  }

  if (regex.exclude && regex.exclude.test(str)) {
    return false;
  }

  return true;
}

exports.matchesPathRules = test;
