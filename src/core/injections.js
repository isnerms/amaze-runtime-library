'use strict';

var bus = require('bus');
var emitter = require('./emitter');
var pathMatch = require('./url-matching').matchesPathRules;
var appendCss = require('css').append;
var $ = require('list');
var _require = require('./require');
var isArray = Array.isArray || function (obj) {
  return Object.prototype.toString.call(obj) === '[object Array]';
};

var path = window.location.pathname + window.location.search;

/**
 * Injection registration function.
 *
 * Named as this is public.
 *
 * Will emit an `error` upon failure.
 *
 * @api private
 * @param {Function} fn
 * @param {Object}    options  Options object - all of which are optional
 *                             {
 *                             		name: 'injection name',
 *                             		priority: 'default',
 *                             		css: '.myclass {display:none}'
 *                             }
 */
exports.register = function register(fn, options) {

  // the only compulsory argument is the function
  if (typeof fn !== 'function') {
    return bus.emit('error',
      new TypeError('invalid injection: ' + String(fn)));
  }

  options = options || {};
  var regex = options.regex;

  // does the caller seem to want us to execute this
  // injection conditionally?
  if (regex) {
    // ok we will reject the injection if we are in single file mode
    // and the regular expression does not match the path
    if (typeof amazeFlags !== 'undefined' && amazeFlags.singleFile && !pathMatch(path, regex)) {
      return emitter.skipped(name);
    }
  }

  bus.emit('injection', {
    fn: fn,
    priority: options.priority || 'default',
    name: options.name || 'Anonymous injection',
    css: options.css
  });
};

function exec(injection) {
  var fn = injection.fn;
  var name = injection.name;
  var css = injection.css;

  try {
    if (css) {
      appendCss(injection.css);
    }
    fn(window, document, _require, $);
    // AM-257: emit injection completion
    emitter.complete(name);
  } catch (err) {
    // AM-257: emit injection errored
    emitter.emit('error', name);
    bus.emit('error', err);
  }
}

/**
 * Fire the given `injections`, emitting an `error`
 * if any throw.
 *
 * @api private
 * @param {Object|Array} injections
 */
exports.fire = function (injections) {

  // handle single injections
  if (!isArray(injections)) {
    injections = [ injections ];
  }

  injections = injections.sort(exports.sorter);
  for (var index = 0, len = injections.length; index < len; index++) {
    exec(injections[index]);
  }
};

/**
 * Helper-method for sorting overlays.  Will place
 * overlays with `high` order in the front of the
 * array and `last` at the end.
 *
 * @api private
 * @param {Object} a
 * @param {Object} b
 * @return {Number}
 */

exports.sorter = function (a, b) {
  return exports.table[a.priority] - exports.table[b.priority];
};

/**
 * Simple lookup table for overlay sorting.
 *
 * Seems backwards, but we want `high` to come
 * first in an array and `last` at the end.
 *
 * @api private
 * @type {Object}
 */

exports.table = {
  'high': -1,
  'default': 0,
  'low': 1
};
