
/**
 * The statuses
 *
 * @api private
 */

module.exports = {
	running: {
		bootstrap: 'RUNNING BOOTSTRAPPER',
		overlays: 'RUNNING OVERLAYS'
	},
	notrun: 'NOT-RUN',
	ready: 'READY',
	waiting: 'WAITING',
	done: 'DONE',
	skipped: 'SKIPPED'
};
