'use strict';

var apply = Function.prototype.apply,
    console = window.console;

/**
 * Output to the developer console
 *
 * @api private
 */
module.exports = function () {
  // check for console
  if (typeof console === 'object' && console.log) {
    // in ie8, `console.log` doesn't have `apply`
    apply.call(console.log, console, arguments);
  }
};
