
'use strict';

var getScrollOffset = require('./get-scroll-offset');

module.exports = getElementCoordinates;

/**
 * Get the coordinates of the element passed into
 * the function relative to the document
 *
 * #### Returns
 *
 * Returns a `Object` with the following properties, which
 * each hold a value representing the pixels for each of the
 * respective coordinates:
 *
 * - `top`
 * - `right`
 * - `bottom`
 * - `left`
 * - `width`
 * - `height`
 *
 * @api public
 * @name dom.getElementCoordinates
 * @param {HTMLElement} [el] The HTMLElement
 */

function getElementCoordinates(elt) {

  //
  // TODO
  //   - replace with better component
  //

  var coords = {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      width: 0,
      height: 0
    };
  var frameCoords = coords;

  if (elt) {
    var scrollOffset = getScrollOffset(elt.ownerDocument);
    var xOffset = scrollOffset.x;
    var yOffset = scrollOffset.y;
    var rect = elt.getBoundingClientRect();

    coords = {
      top: rect.top + frameCoords.top + yOffset,
      right: rect.right + frameCoords.right + xOffset,
      bottom: rect.bottom + frameCoords.bottom + yOffset,
      left: rect.left + frameCoords.left + xOffset,
      width: rect.right - rect.left,
      height: rect.bottom - rect.top
    };
  }

  return coords;
}
