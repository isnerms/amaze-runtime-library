
'use strict';

var TEXT_NODE = 'undefined' === typeof Node ? 3 : Node.TEXT_NODE;

module.exports = wrap;

/**
 * Wrap the given `elements` in a `tag`, defaulting to `<div />`.
 *
 * Handles `jQuery` objects as well as `NodeList`s.
 *
 * @api public
 * @name dom.wrap
 * @param {Array|Node} elements
 * @param {String} tag
 * @return {HTMLElement}
 */

function wrap(elements, tag) {
  // handle jQuery things, NodeLists and text nodes
  //
  //   NOTE: `myTextNode.length` evaluates to the length
  //         of the text the node contains, hence the
  //         specific check
  if (TEXT_NODE === elements.nodeType || 'number' !== typeof elements.length) {
    elements = [ elements ];
  }

  // flag to determine whether wrapper has been added to the DOM
  var added = false;
  // if tag isn't provided, default to 'div'
  var wrapper = document.createElement(tag || 'div');

  for (var i = 0, len = elements.length; i < len; i++) {
    var element = elements[i];

    if (!added) {
      element.parentNode.insertBefore(wrapper, element);
      added = true; // only add wrapper once
    }
    element.parentNode.removeChild(element);
    wrapper.appendChild(element); // place the element(s) in the wrapper
  }

  return wrapper;
}
