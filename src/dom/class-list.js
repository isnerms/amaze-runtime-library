
'use strict';

var array = require('array'),
		indexOf = array.indexOf,
		inArray = array.inArray,
		selector = require('selector'),
		query = selector.query;

var rtrim = /\S/.test('\xA0') ? /^[\s\xA0]+|[\s\xA0]+$/g : /^\s+|\s+$/g,
		trim = function (string) {
			if (String.prototype.trim) {
				return string.trim();
			}
			return string.replace(rtrim, '');
		},
		re = /\s+/;


/**
 * Return a `ClassList` for an element.  `ClassList` is not an
 * `Element#classList` polyfil, but an extension thereof.
 *
 * Examples:
 *
 *     var foo = document.getElementById('foo'),
 *       dom = require('lib/dom');
 *
 *     dom.classList(foo)
 *       .add('hello')
 *       .add('world')
 *       .replace('hello', 'goodbye')
 *       .remove('world')
 *       .add('friends')
 *       .toString();
 *     //=> "goodbye friends"
 *
 *
 * @api public
 * @name dom.classList
 * @param {HTMLElement|String} element
 * @return {ClassList} A ClassList instance for the provided HTMLElement
 */
module.exports = function (element) {
	return new ClassList(element);
};


/**
 * The `ClassList` class
 *
 * @api public
 */
function ClassList(element) {
	if (typeof element === 'string') {
		element = query(element);
	}

	this.element = element;
	this.list = element.classList;
}

/**
 * Add a class to the element if not already present
 *
 * @api public
 * @param {String} name  The class to add
 * @return {ClassList}
 */
ClassList.prototype.add = function (name) {
	var array;

	// standard
	if (this.list) {
		this.list.add(name);
		return this;
	}

	// fallback
	array = this.toArray();

	if (!inArray(array, name)) {
		array.push(name);
		this.element.className = trim(array.join(' '));
	}

	return this;
};

/**
 * Remove a class from the element
 *
 * @api public
 * @param {String} name  The class to remove
 * @return {ClassList}
 */
ClassList.prototype.remove = function (name) {
	var array, index;

	// standard
	if (this.list) {
		this.list.remove(name);
		return this;
	}

	array = this.toArray();
	index = indexOf(array, name);
	if (index !== -1) {
		array.splice(index, 1);
		this.element.className = trim(array.join(' '));
	}

	return this;
};

/**
 * Replace a specific class from the element with another.  If the
 * class to be replaced is not present, the replacement class will be added.
 *
 * @api public
 * @param {String} oldClass   The class to replace
 * @param {String} newClass   The class to add
 * @return {ClassList}
 */
ClassList.prototype.replace = function (oldClass, newClass) {
	return this
		.remove(oldClass)
		.add(newClass);
};

/**
 * Toggle a class of the element
 *
 * @api public
 * @param {String} name  The class to toggle
 * @return {ClassList}
 */
ClassList.prototype.toggle = function (name) {

	// standard
	if (this.list) {
		this.list.toggle(name);
		return this;
	}

	// IE
	if (this.has(name)) {
		return this.remove(name);
	}

	return this.add(name);
};


/**
 * Return an array of classes attached to the element
 *
 * @api public
 * @return {Array} Array of trimmed classNames
 */
ClassList.prototype.toArray = function () {
	return trim(this.element.className).split(re);
};

/**
 * Check if the element has the class.  It will not match on
 * substring matches.
 *
 * @api public
 * @param {String} name  The class to check
 * @return {Boolean}
 */
ClassList.prototype.contains = function (name) {

	// standard
	if (this.list) {
		return this.list.contains(name);
	}

	// IE
	return inArray(this.toArray(), name);
};


/**
 * Alias for ClassList#contains
 *
 * @api public
 * @name ClassList#has
 * @see ClassList#has
 * @param {String} name
 * @return {Boolean}
 */
ClassList.prototype.has = ClassList.prototype.contains;

/**
 * Return a String representation of the element's ClassList
 *
 * @api public
 * @return {String}
 */
ClassList.prototype.toString = function () {
	return this.element.className;
};
