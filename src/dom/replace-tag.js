
'use strict';

var selector = require('selector');
var attributes = require('./attributes');
var moveChildNodes = require('./move-child-nodes');

module.exports = replaceTag;

/**
 * Replace an existing element's tag
 *
 * Example:
 *
 * ```js
 * var bold = document.getElementsByClassName('b')[0];
 * dom.replaceTag(bold, 'h3');
 * //=> HTMLH3Element
 * ```
 *
 * @api public
 * @name dom.replaceTag
 * @param {HTMLElement|String} tag The element to change the tag of
 * @param {String} nodeName The desired tag name
 * @param {Object} attr Attributes to set on the newly changed node
 * @return {HTMLElement} The newly created node
 */

function replaceTag(element, tag, extra) {
  if ('string' === typeof element) {
    element = selector.query(element);
  }

  var parent = element.parentNode;
  var node = document.createElement(tag);

  moveChildNodes(element, node);

  attributes.setAttributes(node, attributes.getAttributes(element));

  if (extra) {
    attributes.setAttributes(node, extra);
  }

  parent.insertBefore(node, element);
  parent.removeChild(element);

  return node;
}
