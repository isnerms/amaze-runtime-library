
'use strict';

var selector = require('selector');
var isVisible = require('./is-visible');

module.exports = isFocusable;

/**
 * Checks if an element is natively focusable.
 *
 * Examples:
 *
 * ```js
 * var dom = require('lib/dom'),
 *     input = document.createElement('input')
 *
 * input.disabled = true;
 * dom.isFocusable(input); // false
 *
 * input.disabled = false;
 * dom.isFocusable(input); // true
 * ```
 *
 * @api public
 * @name dom.isFocusable
 * @param {HTMLElement|String} element
 * @return {Boolean}
 */

function isFocusable(element) {
  // jshint maxcomplexity: 14
  if (typeof element === 'string') {
    element = selector.query(element);
  }

  if (!element ||
    (!isVisible(element, true) && element.nodeName !== 'AREA')) {
    return false;
  }

  if (element.disabled) {
    return false;
  }

  switch (element.nodeName) {
  case 'A':
  case 'AREA':
    if (element.href) {
      return true;
    }
    break;
  case 'INPUT':
    if (element.type === 'hidden') {
      return false;
    }
    return true;
  case 'TEXTAREA':
  case 'SELECT':
  case 'BUTTON':
    return true;
  }

  // check if the tabindex is specified and a parseable number
  var tabindex = element.getAttribute('tabindex');
  if (tabindex && !isNaN(parseInt(tabindex, 10))) {
    return true;
  }

  return false;
}
