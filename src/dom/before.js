
'use strict';

var selector = require('selector');
var html = require('domify');

module.exports = before;

/**
 * Insert `element` before `target`.
 *
 * #### Examples:
 *
 * Inserting an element before an element:
 *
 * ```js
 * var before = require('lib/dom').before;
 *
 * var div = document.getElementById('mydiv');
 * var span = document.getElementById('myspan');
 *
 * before(div, span);
 * ```
 *
 * Inserting a chunk of HTML before an element:
 *
 * ```js
 * var before = require('lib/dom').before;
 *
 * var div = document.getElementById('mydiv');
 *
 * before(div, '&lt;p&gt;hello world!&lt;/p&gt;');
 * ```
 *
 * @api public
 * @name dom.before
 * @param {HTMLElement|String} target
 * @param {HTMLElement|String} element The inserted element
 * @return {HTMLElement}
 */

function before(target, element) {
  if (typeof target === 'string') {
    target = selector.query(target);
  }

  if (typeof element === 'string') {
    element = html(element);
  }

  var parent = target.parentNode;
  if (!parent) {
    throw new Error('Target element must be attached to the document');
  }

  parent.insertBefore(element, target);

  return element;
}

