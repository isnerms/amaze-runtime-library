
'use strict';

var selector = require('selector');
var one = selector.query;
var all = selector.queryAll;
var array = require('array');

module.exports = findUp;

/**
 * Recusively walk up the DOM, checking for a
 * node which matches a selector
 *
 * **WARNING:** this should be used sparingly,
 * as it's not even close to being performant
 *
 * @api public
 * @name dom.findUp
 * @param {HTMLElement|String} element The starting HTMLElement
 * @param {String} selector The selector for the HTMLElement
 * @return {HTMLElement|null}
 */

function findUp(root, selector) {
  var matches = all(selector);

  if (!matches.length) {
    return null;
  }

  if ('string' === typeof root) {
    root = one(root);
  }

  var parent = root.parentNode;
  while (parent && -1 === array.indexOf(matches, parent)) {
    parent = parent.parentNode;
  }

  return parent;
}
