'use strict';

var array = require('array');

module.exports = unwrap;

/**
 * Finds the parent node and removes it, keeping all children
 * in order (including comment and text nodes).
 *
 * @api public
 * @name dom.unwrap
 * @param {HTMLElement} element The element to unwrap
 */

function unwrap(element) {
  var parent = element.parentNode;
  var elements = array.toArray(parent.childNodes);

  for (var i = 0, l = elements.length; i < l; i++) {
    parent.parentNode.insertBefore(elements[i], parent);
  }

  parent.parentNode.removeChild(parent);
}
