
'use strict';

var selector = require('selector');

var legalAttributeName = /^[a-z:_][a-z0-9_:\.\-]+$/i;

/**
 * Get all attributes from an HTMLElement
 *
 * @api public
 * @name dom.getAttributes
 * @param  {HTMLElement} element
 * @return {Object}
 */

exports.getAttributes = function (element) {
  var attrs = {};

  if (element.attributes) {
    for (var i = 0, l = element.attributes.length; i < l; i++) {
      var attribute = element.attributes[i];
      if (attribute.specified && null !== attribute.nodeValue) {
        attrs[attribute.nodeName.toLowerCase()] = attribute.nodeValue;
      }
    }
  }

  // ie
  if (element.style && element.style.cssText) {
    attrs.style = element.style.cssText;
  }

  return attrs;
};

/**
 * Set an attribute on an HTMLElement
 *
 * @api private
 * @deprecated
 * @name dom.setAttribute
 * @param {HTMLElement} node
 * @param {String} name
 * @param {Mixed|Any} value
 */

exports.setAttribute = function (node, name, value) {
  switch (name.toLowerCase()) {
    case 'style':
      if (undefined !== node.style.cssText) {
        node.style.cssText = value;
      } else {
        node.setAttribute(name, value);
      }
      break;
    case 'for':
      node.htmlFor = value;
      break;
    case 'class':
      node.className = value;
      break;
    default:
      node.setAttribute(name, value);
  }
};

/**
 * Set attributes on an HTMLElement
 *
 * @api public
 * @name dom.setAttributes
 * @param {HTMLElement|String} element
 * @param {Object} attrs
 */

exports.setAttributes = function (element, attrs) {
  if (typeof element === 'string') {
    element = selector.query(element);
  }

  for (var name in attrs) {
    if (attrs.hasOwnProperty(name) && legalAttributeName.test(name)) {
      exports.setAttribute(element, name, attrs[name]);
    }
  }
};
