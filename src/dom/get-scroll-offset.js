
'use strict';

module.exports = getScrollOffset;

/**
 * Get the scroll offset of the document passed in
 *
 * @api public
 * @name dom.getScrollOffset
 * @param {Document} [element] The element to evaluate, defaults to document
 * @return {Object} Contains the attributes `x` and `y` which contain the scroll offsets
 */

function getScrollOffset(element) {
  element = element || document;

  // Node.DOCUMENT_NODE
  if (9 === element.nodeType) {
    var html = element.documentElement;
    var body = element.body;

    return {
      x: html && html.scrollLeft || body && body.scrollLeft || 0,
      y: html && html.scrollTop || body && body.scrollTop || 0
    };
  }

  return {
    x: element.scrollLeft,
    y: element.scrollTop
  };
}
