
'use strict';

module.exports = empty;

/**
 * remove all child elements from a DOM element
 *
 * @api public
 * @name dom.empty
 * @param {HTMLElement} target The HTMLElement
 */

function empty(element) {
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
}
