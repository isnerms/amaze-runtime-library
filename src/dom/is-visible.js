
'use strict';

module.exports = isVisible;

/**
 * Determine whether an element is visible
 *
 * @api public
 * @name dom.isVisible
 * @param {HTMLElement} el The HTMLElement
 * @param {Boolean} screenReader Evaluate visibility from the perspective of a screen reader
 * @return {Boolean} The element's visibilty status
 */

function isVisible(el, screenReader) {
  // jshint maxcomplexity:10
  // jshint maxstatements:16
  var stl, getPropertyValue;
  if (!el.ownerDocument) {
    return true;
  }
  if (el.currentStyle) {
    // old IE
    stl = el.currentStyle;
    getPropertyValue = 'getAttribute';
  } else {
    stl = el.ownerDocument.defaultView.getComputedStyle(el, null);
    getPropertyValue = 'getPropertyValue';
  }
  if (stl[getPropertyValue]('display') === 'none' ||
    stl[getPropertyValue]('visibility') === 'hidden') {
    return false;
  }
  if (!screenReader &&
    (parseInt(stl[getPropertyValue]('text-indent'), 10) < -200 ||
    parseInt(stl[getPropertyValue]('top'), 10) < -200 ||
    parseInt(stl[getPropertyValue]('left'), 10) < -200)) {
    return false;
  } else if (screenReader && el.getAttribute('aria-hidden') === 'true') {
    return false;
  }
  if (el.nodeName.toLowerCase() !== 'body' && el.parentNode && el.parentNode !== el) {
    return isVisible(el.parentNode, screenReader);
  } else {
    return true;
  }
}
