
'use strict';

var TEXT_NODE = typeof Node === 'undefined' ? 3 : Node.TEXT_NODE,
    COMMENT_NODE = typeof Node === 'undefined' ? 8 : Node.COMMENT_NODE;

/**
 * Get the contained text from the given `node`.
 *
 * Works with HTMLElements, comments and text nodes.
 *
 * @api public
 * @name dom.getText
 * @param {Node} node
 * @return {String}
 */
module.exports = function getText(node) {

  var type = node.nodeType;

  if (type === TEXT_NODE || type === COMMENT_NODE) {
    return node.nodeValue;
  }

        // standard
  return node.textContent
        // ie
      || node.innerText
      || null;
};
