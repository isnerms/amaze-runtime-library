
'use strict';

var selector = require('selector');

/**
 * Move all child nodes from the `source` element
 * to the `destination` element
 *
 * @api public
 * @name dom.moveChildNodes
 * @param {HTMLElement|String} source
 * @param {HTMLElement|String} destination
 */
module.exports = function (source, destination) {
  var child;

  if (typeof source === 'string') {
    source = selector.query(source);
  }

  if (typeof destination === 'string') {
    destination = selector.query(destination);
  }

  child = source.firstChild;

  while (child) {
    destination.appendChild(child);
    child = source.firstChild;
  }
};
