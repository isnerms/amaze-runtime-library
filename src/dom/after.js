
'use strict';

var selector = require('selector');
var html = require('domify');

module.exports = after;

/**
 * Insert `element` after `target`.
 *
 * #### Examples:
 *
 * Inserting an element after an element:
 *
 * ```js
 * var after = require('lib/dom').after;
 *
 * var div = document.getElementById('mydiv');
 * var span = document.getElementById('myspan');
 *
 * after(div, span);
 * ```
 *
 * Inserting a chunk of HTML after an element:
 *
 * ```js
 * var after = require('lib/dom').after;
 *
 * var div = document.getElementById('mydiv');
 *
 * after(div, '&lt;p&gt;hello world!&lt;/p&gt;');
 * ```
 *
 * @api public
 * @name dom.after
 * @param {HTMLElement|String} target
 * @param {HTMLElement|String} element The inserted element
 * @return {HTMLElement}
 */

function after(target, element) {
  if (typeof target === 'string') {
    target = selector.query(target);
  }

  if (typeof element === 'string') {
    element = html(element);
  }

  var parent = target.parentNode;
  if (!parent) {
    throw new Error('Target element must be attached to the document');
  }

  var node = target.nextSibling;
  if (!node) {
    parent.appendChild(element);
  } else {
    parent.insertBefore(element, node);
  }

  return element;
}

