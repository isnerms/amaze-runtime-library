
'use strict';

module.exports = isNode;

/**
 * Checks if an object is a DOM node
 *
 * Examples:
 *
 * ```js
 * var dom = require('lib/dom');
 *
 * dom.isNode({}); // false
 * dom.isNode(document.body); // true
 * ```
 *
 * @api public
 * @name dom.isNode
 * @param {Object} candidate The object to test
 * @return {Boolean} Whether the candidate is a DOM node
 */

function isNode(obj) {
  if ('function' === typeof Node) {
    return obj instanceof Node;
  }

  return null !== obj
      && 'object' === typeof obj
      && 'number' === typeof obj.nodeType
      && 'string' === typeof obj.nodeName;
}
