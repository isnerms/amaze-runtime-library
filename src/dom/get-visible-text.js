
'use strict';

var string = require('string');

module.exports = getVisibleText;

/**
 * Walk the DOM structure of an element and return all of
 * its text that is visible removes unnecessary white space
 *
 * @api private
 * @deprecated
 * @name dom.getVisibleText
 * @param {HTMLElement} element
 */

function getVisibleText(element) {
  //jshint maxcomplexity:7
  //jshint maxstatements:17
  var elNm = element.nodeName.toUpperCase(), nl, node, txt, i, vtxt = '',
    radioButton, type,
    crlf = /\r\n/gi, nbsp = /&nbsp;/gi, unicodelf = /\u00A0/gi, whitespace = /[\s]+/gi;

  type = element.getAttribute('type');
  radioButton = (elNm === 'INPUT' && type === 'radio');
  if (elNm !== 'STYLE' && elNm !== 'SCRIPT' && !radioButton) {
    nl = element.childNodes;
    for (i = 0; i < nl.length; i++) {
      node = nl[i];
      if (node.nodeName === '#text') {
        txt = node.nodeValue;
        if (txt) {
          txt = txt.replace(crlf, '\n')
                  .replace(nbsp, ' ')
                  .replace(unicodelf, ' ')
                  .replace(whitespace, ' ');
          if (txt.length > 0) {
            vtxt = vtxt + txt;
          }
        }
      } else {
        vtxt = vtxt + ' ' + getVisibleText(node);
      }
    }
    return string.trim(vtxt.replace(whitespace, ' '));
  } else if (radioButton) {
    return element.getAttribute('value');
  }
  return '';
}
