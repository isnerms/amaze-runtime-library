
'use strict';

var classList = require('./class-list');

/**
 * Add a class to an element making sure to not
 * duplicate the class if it already exists
 *
 * @api public
 * @name dom.addClass
 * @param {HTMLElement} target The HTMLElement
 * @param {String} className The name of the class to be added
 * @return {ClassList} A `ClassList` representation of the given `HTMLElement`
 */

exports.addClass = function (target, className) {
  return classList(target).add(className);
};

/**
 * Remove a class from an element
 *
 * @api public
 * @name dom.removeClass
 * @param {HTMLElement} target The HTMLElement
 * @param {String} className The name of the class to be removed
 * @return {ClassList} A `ClassList` representation of the given `HTMLElement`
 */

exports.removeClass = function (target, className) {
  return classList(target).remove(className);
};

/**
 * return true if an element has a class and false otherwise
 *
 * @api public
 * @name dom.hasClass
 * @param {HTMLElement} target The HTMLElement
 * @param {String} className The name of the class to be tested
 * @return {Boolean}
 */

exports.hasClass = function (target, className) {
  return classList(target).has(className);
};

/**
 * toggle the existence of a class on an element
 *
 * @api public
 * @name dom.toggleClass
 * @param {HTMLElement} target The HTMLElement
 * @param {String} className The name of the class to be toggled
 * @return {ClassList} A `ClassList` representation of the given `HTMLElement`
 */

exports.toggleClass = function (target, className) {
  return classList(target).toggle(className);
};
