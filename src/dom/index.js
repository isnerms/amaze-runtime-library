
'use strict';

var nextElementSibling = require('next-element-sibling');
var previousElementSibling = require('previous-element-sibling');
var classes = require('./classes');
var attributes = require('./attributes');
var rndid = require('rndid');


var dom = module.exports = {};

/**
 * Generate a guaranteed unique ID.
 *
 * If given a `prefix` parameter, will prefix the
 * generated ID with the string.
 *
 * If given a `length` parameter, will generate an
 * ID of `length` characters.
 *
 * ### Examples
 *
 * ```js
 * var dom = require('lib/dom');
 *
 *
 * dom.rndid();
 * // => "aoypuqy"
 *
 * dom.rndid(13);
 * // => "blemwngquyroh"
 *
 * dom.rndid('___');
 * // => "___vpttfvv"
 *
 * dom.rndid('___', 20);
 * // => "___estgkcijdswenxeheldb"
 *
 * ```
 *
 * @api public
 * @param {String} [prefix]
 * @param {Number} [length]
 * @return {String}
 */

dom.rndid = rndid;


/**
 * Get the next element sibling of the given element
 *
 * @api public
 * @name dom.nextElementSibling
 * @param {HTMLElement} element
 * @return {HTMLElement}
 */

dom.nextElementSibling = nextElementSibling;

/**
 * Get the previous element sibling of the given `element`
 *
 * @api public
 * @name dom.previousElementSibling
 * @param {[HTMLElement} element
 * @return {[HTMLElement}
 */

dom.previousElementSibling = previousElementSibling;

// Expose `class` methods

for (var fn in classes) {
  if (classes.hasOwnProperty(fn) && 'function' === typeof classes[fn]) {
    dom[fn] = classes[fn];
  }
}

// Expose `attribute` methods

for (var fn in attributes) {
  if (attributes.hasOwnProperty(fn) && 'function' === typeof attributes[fn]) {
    dom[fn] = attributes[fn];
  }
}

// expose everything else

dom.after = require('./after');
dom.before = require('./before');
dom.children = require('./children');
dom.classList = require('./class-list');
dom.empty = require('./empty');
dom.findUp = require('./find-up');
dom.getElementCoordinates = require('./get-element-coordinates');
dom.getScrollOffset = require('./get-scroll-offset');
dom.getText = require('./get-text');
dom.getVisibleText = require('./get-visible-text');
dom.isFocusable = require('./is-focusable');
dom.isNode = require('./is-node');
dom.isVisible = require('./is-visible');
dom.moveChildNodes = require('./move-child-nodes');
dom.replaceTag = require('./replace-tag');
dom.siblings = require('./siblings');
dom.unwrap = require('./unwrap');
dom.wrap = require('./wrap');
