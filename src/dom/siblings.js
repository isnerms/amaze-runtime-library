'use strict';

var array = require('array');
var descendants = require('descendants');
var matches = require('selector').matches;
var nextElementSibling = require('next-element-sibling');
var previousElementSibling = require('previous-element-sibling');


exports = module.exports = siblings;

/**
 * Find all of the `HTMLElement` siblings of the
 * given `target`, optionally filtered by a `selector`.
 *
 * @api public
 * @name dom.siblings
 * @param {HTMLElement} target
 * @param {String} [selector] filter
 * @return {Array} siblings
 */

function siblings(target, selector) {
  var elements = descendants(target.parentNode, true);

  elements = array.filter(elements, function (v) {
    if (v === target) {
      return false;
    }

    return selector
      ? matches(v, selector)
      : true;
  });

  return elements;
}

/**
 * Find previous `HTMLElement` siblings of the
 * given `target` by traversing *up* the DOM tree,
 * optionally filtered by a `selector`.
 *
 * @api public
 * @name dom.siblings.prev
 * @param {HTMLElement} target
 * @param {String} [selector]
 * @return {Array} siblings
 */

exports.prev = function (target, selector) {
  var sibling;
  var siblings = [];

  sibling = previousElementSibling(target);

  while (sibling) {
    if (selector) {
      if (matches(sibling, selector)) {
        siblings.push(sibling);
      }
    } else {
      siblings.push(sibling);
    }
    sibling = previousElementSibling(sibling);
  }

  return siblings;
};

/**
 * Find next `HTMLElement` siblings of the given
 * `target` by traversing *down* the DOM tree,
 * optionally filtered by a `selector`.
 *
 * @api public
 * @name dom.siblings.next
 * @param {HTMLElement} target
 * @param {String} [selector]
 * @return {Array} siblings
 */

exports.next = function (target, selector) {
  var sibling;
  var siblings = [];

  sibling = nextElementSibling(target);

  while (sibling) {
    if (selector) {
      if (matches(sibling, selector)) {
        siblings.push(sibling);
      }
    } else {
      siblings.push(sibling);
    }
    sibling = nextElementSibling(sibling);
  }

  return siblings;
};
