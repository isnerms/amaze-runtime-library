
'use strict';

var descendants = require('descendants');
var array = require('array');
var selector = require('selector');
var query = selector.query;
var matches = selector.matches;

module.exports = children;

/**
 * Get direct descendants (children) of the given
 * `element`, optionally filtered by a `selector`.
 *
 * @api public
 * @name dom.children
 * @param {HTMLElement|String} element
 * @param {String} [selector]
 * @return {Array}
 */

function children(element, selector) {
  if (typeof element === 'string') {
    element = query(element);
  }

  // grab all direct descendants
  var elements = descendants(element, true);
  return selector
    ? array.filter(elements, function (element) {
        return matches(element, selector);
      })
    : elements;
}
