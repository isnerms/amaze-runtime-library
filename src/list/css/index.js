'use strict';

var css = require('css');

/**
 * Either get/set css properties.
 *
 * ### Examples:
 *
 * ```js
 * // set multiple CSS properties on every `&lt;div /&gt;`
 * amaze('div').css({
 *   color: 'red',
 *   display: 'block'
 * });
 *
 * // get the `display` value from the first `&lt;div /&gt;` in the document
 * var display = amaze('div').css('display');
 *
 * // set `display` to `block` on every `&lt;div /&gt;` in the document
 * amaze('div').css('display', 'block');
 * ```
 *
 * @api public
 * @name amaze#css
 * @param {Object|String} obj Object of properties or single property name
 * @param {String|Number} [value] Value to set
 * @return {amaze|String}
 */

module.exports = function (property, value) {
  if (property && typeof property === 'object') {
    for (var i = this.length - 1; i >= 0; i--) {
      css(this[i], property);
    }
    return this;
  }

  if (arguments.length === 2) {
    for (var j = 0, k = this.length; j < k; j++) {
      css(this[j], property, value);
    }
    return this;
  }

  return css(this[0], property);
};
