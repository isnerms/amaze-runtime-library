'use strict';

var toFunction = require('../to-function');

/**
 * Get the zero-based index of an element in
 * the list. Returns -1 if the element is not
 * found.
 *
 * @api public
 * @name amaze#indexOf
 * @param {Function|String|amaze|HTMLElement} fn fn, selector, etc.
 * @return {[type]}
 */

module.exports = function (fn) {
  fn = toFunction(fn);
  for (var i = 0, l = this.length; i < l; i++) {
    if (fn.call(this[i], this[i])) {
      return i;
    }
  }
  return -1;
};
