
'use strict';

var wrappers = require('./wrappers');
var events = require('events');
var date = require('date');
var menu = require('menu');
var dom = require('dom');
var markup = require('markup');
var collections = require('collections');
var each = collections.each;

each({

  /**
   * Bind an event listener to each element in
   * the list.
   *
   * @api public
   * @name amaze#on
   * @param {String} eventType
   * @param {Function} listener
   * @return {amaze} for chaining
   */

  on: events.on,

  /**
   * Remove one or more event listeners from each
   * element in the list.
   *
   * @api public
   * @name amaze#off
   * @param {String} eventType
   * @param {Function} [listener]
   * @return {amaze} for chaining
   */

  off: events.off,

  /**
   * Bind a single-shot event listener on every
   * element in the list.
   *
   * @api public
   * @name amaze#once
   * @param {String} eventType
   * @param {Function} listener
   * @return {amaze} for chaining
   */

  once: events.once,

  /**
   * Create and fire an event on each element in the list.
   *
   * @api public
   * @name amaze#fire
   * @param {String} eventType
   * @param {Object} [augment]
   * @return {amaze} for chaining
   */

  fire: events.fire,

  /**
   * Bind a delegated event listener to every element in the list.
   *
   * @api public
   * @name amaze#delegate
   * @param {String} eventType
   * @param {String} selector
   * @param {Function} listener
   * @return {amaze} for chaining
   */

  delegate: events.delegate,

  /**
   * Remove a delegated event listener from every element in the list.
   *
   * @api public
   * @name amaze#undelegate
   * @param {String} [varname] eventType
   * @param {Function} [listener]
   * @return {amaze} for chaining
   */

  undelegate: events.undelegate,

  /**
   * Run `date.datePicker` on every element in the list.
   *
   * @api public
   * @name amaze#datePicker
   * @param {Object} options
   * @return {amaze} for chaining
   */

  datePicker: date.datePicker,

  /**
   * Run `menu.ariaMenu` on every element in the list.
   *
   * @api public
   * @name amaze#ariaMenu
   * @param {Array} options
   * @return {amaze} for chaining
   */

  ariaMenu: menu.ariaMenu,

  /**
   * Run `markup.dataTable` on every element in the list.
   *
   * @api public
   * @name amaze#dataTable
   * @param {Array} options
   * @return {amaze} for chaining
   */

  dataTable: markup.dataTable,

  /**
   * Run `markup.ariaTabs` on every element in the list.
   *
   * @api public
   * @name amaze#ariaTabs
   * @param {Array} options
   * @return {amaze} for chaining
   */

  ariaTabs: markup.ariaTabs,

  /**
   * Run the `markup.fixModal` function on all elements
   * in the list with the given `options`.
   *
   * Returns the current `amaze` list for chaining.
   *
   * @api public
   * @name amaze#fixModal
   * @param {Object} options
   * @return {amaze}
   */

  fixModal: markup.fixModal,

  /**
   * Run the `markup.addLabel` function on all elements
   * in the list with the given `text` or `label`.
   *
   * Returns the current `amaze` list for chaining.
   *
   * @api public
   * @name amaze#addLabel
   * @param {String|HTMLElement} text or label reference
   * @return {amaze}
   */

  addLabel: markup.addLabel,

  /**
   * Mark all elements in the list as presentational.
   *
   * Returns the current `amaze` list for chaining.
   *
   * @api public
   * @name amaze#presentation
   * @return {amaze}
   */

  presentation: markup.presentation,

  /**
   * Make all elements in the list buttons by adding:
   * role="button"
   * event handlers for keyCodes 13 and 32 (Enter and Spacebar)
   * excludes input w/ type='button' && button tags
   *
   *
   * Returns the current `amaze` list for chaining.
   *
   * @api public
   * @name amaze#button
   * @return {amaze}
   */

  button: markup.button,

  /**
   * Run the `markup.skipLink.add` function on all elements
   * in the list with the given `options` and `target`.
   *
   * @api public
   * @name amaze#skipLinkAdd
   * @param {HTMLElement|String} target
   * @param {Object} [options]
   * @return {amaze}
   */

  skipLinkAdd: markup.skipLink.add,

  /**
   * Run the `markup.skipLink.fix` function on all elements
   * in the list with the given `target`.
   *
   * @api public
   * @name amaze#skipLinkFix
   * @param {HTMLElement|String} [target]
   * @return {amaze}
   */

  skipLinkFix: markup.skipLink.fix,

  /**
   * Run the `markup.skipLink.fix` function on all elements
   * in the list with the given arguments.
   *
   * @api public
   * @name amaze#toolTipAdd
   * @param {String} content
   * @param {Object} [options]
   * @return {amaze}
   */

  toolTipAdd: markup.tooltip,

  /**
   * Run the `markup.skipLink.fix` function on all elements
   * in the list with the given `target`.
   *
   * @api public
   * @name amaze#toolTipFix
   * @param {HTMLElement|String} target
   * @param {Object} [options]
   * @return {amaze} for chaining
   */

  toolTipFix: markup.tooltip.fix,

  /**
   * Add the given `class` to every element in the list.
   *
   * @api public
   * @name amaze#addClass
   * @param {String} class
   * @return {amaze} for chaining
   */

  addClass: dom.addClass,

  /**
   * Remove the given `class` to every element in the list.
   *
   * @api public
   * @name amaze#removeClass
   * @param {String} class
   * @return {amaze} for chaining
   */

  removeClass: dom.removeClass,

  /**
   * Toggle the given `class` to every element in the list.
   *
   * @api public
   * @name amaze#toggleClass
   * @param {String} class
   * @return {amaze} for chaining
   */

  toggleClass: dom.toggleClass,

  /**
   * Empty every element in the list by remove all
   * of its child nodes.
   *
   * @api public
   * @name amaze#empty
   * @return {amaze} for chaining
   */

  empty: dom.empty,

  /**
   * Set the given `map` of attributes on every
   * element in the list.
   *
   * @api public
   * @name amaze#setAttributes
   * @param {Object} map
   * @return {amaze} for chaining
   */

  setAttributes: dom.setAttributes,

  /**
   * Set the given attribute `name` to `value` on
   * every element in the list.
   *
   * @api private
   * @deprecated
   * @name amaze#setAttribute
   * @param {String} name
   * @param {String|Boolean} value
   * @return {amaze} for chaining
   */

  setAttribute: dom.setAttribute,

  /**
   * Wrap every element in the list in an element of `type`.
   *
   * Returns the updated `amaze` list for chaining.
   *
   * @api public
   * @name amaze#wrap
   * @param {String} type
   * @return {amaze}
   */

  wrap: dom.wrap,

  /**
   * Unwrap every element in the list from its parent element.
   *
   * Does not detach elements from the DOM.
   *
   * Returns the updated `amaze` list for chaining.
   *
   * @api public
   * @name amaze#unwrap
   * @return {amaze} for chaining
   */

  unwrap: dom.unwrap
}, function (fn, name) {
  exports[name] = wrappers.simple(fn);
});

each({

  /**
   * Replace every element in the list's `tag`,
   * optionally adding `attributes`.
   *
   * @api public
   * @name amaze#replaceTag
   * @param {String} newTag
   * @param {Object} [attributes]
   */

  replaceTag: dom.replaceTag
}, function (fn, name) {
  exports[name] = wrappers.modifier(fn);
});

each({

  /**
   * Get the element coordinates of the **first**
   * element in the list.
   *
   * @api public
   * @name amaze#getElementCoordinates
   * @return {Object}
   */

  getElementCoordinates: dom.getElementCoordinates,

  /**
   * Get a map of all attributes held by the **first**
   * element in the list.
   *
   * @api public
   * @name amaze#getAttributes
   * @return {Object}
   */

  getAttributes: dom.getAttributes

}, function (fn, name) {
  exports[name] = wrappers.getterFirst(fn);
});

each({

  /**
   * Check if each element in the list has the given `class`.
   *
   * @api public
   * @name amaze#hasClass
   * @param {String} class
   * @return {Boolean}
   */

  hasClass: dom.hasClass
}, function (fn, name) {
  exports[name] = wrappers.getterAny(fn);
});

each({

  /**
   * Check if each element in the list is visible.
   *
   * @api public
   * @name amaze#isVisible
   * @return {Boolean}
   */

  isVisible: dom.isVisible
}, function (fn, name) {
  exports[name] = wrappers.getterNotAny(fn);
});


each({

  /**
   * Insert the given `element` before each item in
   * the list.  Will clone `element` if the list
   * has more than one member.
   *
   * @api public
   * @name amaze#before
   * @param {HTMLElement} element
   * @return {amaze} for chaining
   */

  before: dom.before,

  /**
   * Insert the given `element` after each item in
   * the list.  Will clone `element` if the list
   * has more than one member.
   *
   * @api public
   * @name amaze#after
   * @param {HTMLElement} element
   * @return {amaze} for chaining
   */

  after: dom.after
}, function (fn, name) {
  exports[name] = wrappers.appendBefore(fn);
});

each({

  /**
   * Get the text contained in each of elements in
   * the list.
   *
   * @api public
   * @name amaze#getText
   * @return {String}
   */

  getText: dom.getText,

  /**
   * Get the visible text contained in each of
   * elements in the list.
   *
   * @api private
   * @deprecated
   * @name amaze#getVisibleText
   * @return {String}
   */

  getVisibleText: dom.getVisibleText
}, function (fn, name) {
  exports[name] = wrappers.textGetter(fn);
});

each({

  /**
   * Get all `HTMLElement` siblings of every
   * element in the list, optionally filted by
   * `selector`.  Will not provide  duplicate
   * elements.
   *
   * @api public
   * @name amaze#siblings
   * @param {String} [selector]
   * @return {amaze} for chaining
   */

  siblings: dom.siblings,

  /**
   * Get all `HTMLElement` siblings **after** every
   * element in the list, optionally filited by
   * `selector`.  Will not provide duplicate elements.
   *
   * @api public
   * @name amaze#next
   * @param {String} [selector]
   * @return {amaze} for chaining
   */

  next: dom.siblings.next,

  /**
   * Get all `HTMLElement` siblings **before** every
   * element in the list, optionally filited by
   * `selector`.  Will not provide duplicate elements.
   *
   * @api public
   * @name amaze#previous
   * @alias amaze#prev
   * @param {String} [selector]
   * @return {amaze} for chaining
   */

  previous: dom.siblings.prev,

  /**
   * Get child elements of every element in
   * the list, optionally filtered by `selector`.
   *
   * @api public
   * @name amaze#children
   * @param {String} [selector]
   * @return {amaze}
   */

  children: dom.children

}, function (fn, name) {
  exports[name] = wrappers.siblingGetter(fn);
});


// amaze#previous
exports.prev = exports.previous;

// amaze#find
exports.find = require('./find');

// amaze#attr
exports.attr = require('./attr');

// amaze#eq
exports.eq = require('./eq');

// amaze#each
exports.forEach =
exports.each = require('./each');

// amaze#findUp
exports.findUp = require('./find-up');

// amaze#parent
exports.parent = require('./parent');

// amaze#append
exports.append = require('./append');

// amaze#indexOf
exports.indexOf = require('./index-of');

// amaze#filter
exports.filter = require('./filter');

//amaze#css
exports.css = require('./css');

/**
 * Check the current set against a selector and
 * return `true` if there is at least one match
 *
 * ### Example:
 *
 * ```js
 * var div = document.createElement('div');
 * div.id = 'foo';
 * amaze('#foo').is('div'); // true
 * amaze('#foo').is('span') // false
 * ```
 *
 * @api public
 * @name  amaze#is
 * @param  {Function|String}  selector function or selector
 * @return {Boolean}          whether or not there at least one match
 */
exports.is = function (selector) {
  return !!this.filter(selector).length;
};
