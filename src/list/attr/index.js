'use strict';

var collections = require('collections');
var each = collections.each;

module.exports = attr;

function attributeSetter(element, attr, value) {
  element.setAttribute(attr, value);
}

function attributeGetter(e, attr) {
  if (!e) {
    return;
  }
  return e.getAttribute(attr);
}

function attributesHandler(list, attrs) {
  each(list, function (e) {
    each(attrs, function (val, name) {
      attributeSetter(e, name, val);
    });
  });

  return list;
}

/**
 * Get or set attribute(s).  When 'getting' an attribute, the first element
 * in the set's attribute value will be returned.  When setting a single
 * attribute or multiple attributes, the attribute/value will be applied to
 * each element in the set.
 *
 * ### Examples:
 *
 * ```js
 * // get the aria-label value for the first instance of `.foo`
 * var ariaLabelValue = amaze('.foo').attr('aria-label');
 *
 * // set `role="presentation"` on every instance of `.foo`
 * amaze('.foo').attr('role', 'presentation');
 *
 * // set `title="Foo"` and `tabindex="-1"` on every instance of `.foo`
 * amaze('.foo').attr({
 *   'title': 'Foo',
 *   'tabindex': -1
 * });
 * ```
 * @api public
 * @name  amaze#attr
 * @param  {Object|String} name  An object of attribute-value pairs to set OR an attribute-name string
 * @param  {String|Number} value The value to set for the attribute
 * @return {amaze|string}
 */
function attr(name, value) {
  if (typeof name === 'string' && value === undefined) {
    return attributeGetter(this[0], name);
  } else if (typeof name === 'object') {
    return attributesHandler(this, name);
  } else if (typeof name === 'string' && value !== undefined) {
    if (this.length) {
      each(this, function (item) {
        attributeSetter(item, name, value);
      });
    }
    return this;
  }
}
