
'use strict';

var collections = require('collections');
var each = collections.each;
var dom = require('dom');
var isNode = dom.isNode;
var selector = require('selector');
var query = selector.query;
var queryAll = selector.queryAll;
var arrayish = require('array-like');
var trim = require('trim');
var domify = require('domify');
var css = require('css');
var inherits = require('inherit');
var isHTML = require('./is-html');

// Expose `amaze`.
exports = module.exports = amaze;

// Expose `List`.
exports.List = List;

/**
 * Return an `amaze` object wrapping the given `selector`.
 *
 * @param {Mixed} selector html, element, jQuery, ...
 * @param {Mixed} [context] element, jQuery, ...
 * @return {amaze}
 * @api public
 */

function amaze(selector, context) {
  // amaze(amaze('div'))
  if (selector instanceof List) {
    return selector;
  }

  // amaze(jQuery('div'))
  // amaze(selector('div'))
  // amaze(document.getElementsByTagName('*'))
  if (arrayish(selector)) {
    return new List(selector);
  }

  // amaze(document.getElementById('foo'))
  if (isNode(selector)) {
    return new List([ selector ]);
  }

  if (typeof selector !== 'string') {
    throw new TypeError('invalid selector');
  }

  var html = trim.left(selector);
  if (isHTML(html)) {
    // amaze('<div>')
    return new List([ domify(html) ], html);
  }

  context = normalizeContext(context);
  return new List(queryAll(selector, context), selector);
}

/**
 * Mixin a function to the `amaze` prototype.
 *
 * This allows overlays outside of the current scope to access your custom function.
 *
 * ### Example:
 *
 * ```js
 * // add a shorthand method for binding click events on elements
 * amaze.use('addClickListener', function (clickListener) {
 *   this.on('click', clickListener);
 * });
 *
 * var $element = amaze('#my-element');
 * $element.addClickListener(function (e) {
 *   alert('#my-element was clicked!');
 * });
 * ```
 *
 * @api public
 * @param {String} name
 * @param {Function} fn
 * @return {amaze} for chaining
 */

amaze.use = function (name, fn) {
  if (typeof name === 'string') {
    List.prototype[name] = fn;
  } else {
    each(name, function (fn, name) {
      List.prototype[name] = fn;
    });
  }
  return amaze;
};

/**
 * Insert the given `styles` into the Amaze stylesheet.
 *
 * @api private
 * @param {String} styles
 * @return {amaze} for chaining
 */

amaze.css = function (styles) {
  css.append(styles);
  return amaze;
};

/**
 * Initialize a new `List` with the given array-ish
 * of `elements` and `selector` string.
 *
 * @api private
 * @param {Mixed} els
 * @param {String} selector
 */

function List(elements, selector) {
  elements = elements || [];
  for (var i = 0, l = elements.length; i < l; i++) {
    this.push(elements[i]);
  }
  this.selector = selector;
}

// Add `Array` prototypes to `List`.
inherits(List, Array);

// extend the prototype
amaze.use(require('./proto'));

/**
 * Normalize the given `context`.
 *
 * @api private
 * @param {Mixed} context
 * @return {Mixed}
 */

function normalizeContext(context) {
  if (!context) {
    // amaze('.foo')
    context = document;
  } else if (typeof context === 'string') {
    // amaze('.foo', '.bar')
    context = query(context);
  } else if (arrayish(context)) {
    // amaze('.foo', amaze('.bar'))
    // amaze('.foo', document.querySelectorAll('.bar'))
    // amaze('.foo', jQuery('.bar'))
    context = context[0];
  } else if (!isNode(context)) {
    throw new TypeError('invalid context');
  }

  return context;
}
