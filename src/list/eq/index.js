'use strict';

/**
 * Get the `amaze` object at the given `index` in the list.
 *
 * @api public
 * @name amaze#eq
 * @param {Number} index
 * @return {Amaze}
 */
module.exports = function (index) {
  var array = this[index]
    ? [ this[index] ]
    // prevent sparse arrays (mimic jQuery)
    : [];
  return new this.constructor(array);
};
