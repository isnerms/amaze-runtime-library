'use strict';

var dom = require('dom');

/**
 * Run `dom.findUp` on every element in the list.
 *
 * @api public
 * @name amaze#findUp
 * @param {String} selector
 * @return {amaze}
 */

module.exports = function (selector) {
  var elements = [];
  for (var i = 0, l = this.length; i < l; i++) {
    var element = dom.findUp(this[i], selector);
    if (element) {
      elements.push(element);
    }
  }
  return new this.constructor(elements);
};
