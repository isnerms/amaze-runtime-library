'use strict';

var array = require('array');
var matches = require('selector').matches;

/**
 * Grabs the parent of each element in the
 * set, optionally filtered by a selector
 *
 * @api public
 * @name  amaze#parent
 * @param  {String} parentSelector Optional filter selector
 * @return {Array}                The array of matching parents
 */
module.exports = function (parentSelector) {
  var parents = [];
  for (var i = 0, l = this.length; i < l; i++) {
    var e = this[i].parentNode;
    // don't insert a duplicate
    if (e && !~array.indexOf(parents, e)) {
      // if a parentSelector is provided,
      // make sure this parent matches it
      if (parentSelector) {
        if (matches(e, parentSelector)) {
          parents.push(e);
        }
      } else {
        parents.push(e);
      }
    }
  }
  return new this.constructor(parents);
};
