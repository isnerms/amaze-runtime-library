'use strict';

var arrayish = require('array-like');
var matches = require('selector').matches;
var dom = require('dom');

/**
 * Convert the given `selector` into a `Function`.
 *
 * @api private
 * @param {Function|String|List|HTMLElement} selector
 * @return {Function}
 */

module.exports = function toFunction(selector) {
  if (typeof selector === 'function') {
    return selector;
  }

  // assume actual selector
  if (typeof selector === 'string') {
    return function (element) {
      return matches(element, selector);
    };
  }

  // array-like object (NodeList, jQuery, amaze, etc.)
  if (arrayish(selector)) {
    return function (element) {
      return element === selector[0];
    };
  }

  // element
  if (dom.isNode(selector)) {
    return function (element) {
      return element === selector;
    };
  }
};
