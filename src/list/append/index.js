'use strict';

var dom = require('dom');
var arrayish = require('array-like');
var isNode = dom.isNode;
var trim = require('trim');
var isHTML = require('../is-html');
var domify = require('domify');
var each = require('collections').each;


module.exports = append;

function typeCheck(node) {
  if (typeof node === 'string') {
    var html = trim.left(node);
    if (isHTML(html)) {
      return 'html';
    } else {
      return 'text';
    }
  } else if (isNode(node)) {
    return 'node';
  } else if (arrayish(node)) {
    return 'arrayish';
  } else {
    return 'error';
  }
}

function loopDeLoop(list, contents) {
  each(list, function (el, i) {
    each(contents, nodeLoop);

    function nodeLoop(node) {
      var nType = typeCheck(node);

      if (nType === 'arrayish') {
        return each(node, nodeLoop);
      } else if (nType === 'html') {
        node = domify(trim.left(node));
      } else if (nType === 'text') {
        node = document.createTextNode(trim.left(node));
      }

      if (i !== 0) {
        // clone if its not the first iteration of `this`
        node = node.cloneNode(true);
      }

      // either way append
      el.appendChild(node);
    }
  });
}


/**
 * Insert content to the end of each element in the list
 *
 * @api public
 * @name amaze#append
 * @param  {String|amaze|HTMLElement} content the content to be appended
 * @return {amaze}
 */
function append(content) {
  var contents = [];
  var cType = typeCheck(content);

  if (cType === 'arrayish') {
    contents = content;
  } else if (cType === 'node') {
    contents = [content];
  } else if (cType === 'html') {
    contents = [domify(trim.left(content))];
  } else if (cType === 'text') {
    contents = [document.createTextNode(trim.left(content))];
  } else {
    // something bad happened...just return our amaze objecty thing
    return new this.constructor(this);
  }

  loopDeLoop(this, contents);

  // chain
  return new this.constructor(this);
}
