'use strict';

/**
 * Check if the string is HTML.  Stolen from component/dom/index.js.
 *
 * @param {String} str
 * @return {Boolean}
 * @api private
 */

module.exports = function isHTML(str) {
  // Faster than running regex, if str starts
  // with `<` and ends with `>`, assume it's HTML
  if (str.charAt(0) === '<'
    && str.charAt(str.length - 1) === '>'
    && str.length >= 3) {
    return true;
  }

  // Run the regex
  var match = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/.exec(str);
  return !!(match && match[1]);
};
