'use strict';

/**
 * Iterate every element in the list, calling
 * `fn(element, index)`.  The context (`this`)
 * will be set to the element being iterated.
 *
 * @api public
 * @name amaze#each
 * @alias amaze#forEach
 * @param {Function} fn
 * @return {amaze} for chaining
 */

module.exports = function (fn) {
  for (var i = 0, l = this.length; i < l; i++) {
    fn.call(this[i], this[i], i);
  }
  return this;
};
