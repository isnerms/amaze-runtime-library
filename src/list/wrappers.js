
'use strict';

var dom = require('dom');
var array = require('array');

/**
 * Decorator for the getter function `fn`.  Invokes
 * the given function on the first item in the
 * list, returning its result.
 *
 * WILL NOT CHAIN.
 *
 * @api private
 * @param {Function} fn
 * @return {Function}
 */

exports.getterFirst = function (fn) {
  return function () {
    var args = [].slice.call(arguments);
    return fn.apply(null, [ this[0] ].concat(args));
  };
};

/**
 * Decorator for the `Boolean` getter function
 * `fn`.  Invokes the given function on every
 * item in the list until it returns `true`.
 *
 * Returns `false` if no item passes `fn`.
 *
 * @api private
 * @param {Function} fn
 * @return {Function}
 */

exports.getterAny = function (fn) {
  return function () {
    var args = [].slice.call(arguments);
    for (var i = 0, l = this.length; i < l; i++) {
      if (fn.apply(null, [ this[i] ].concat(args))) {
        return true;
      }
    }
    return false;
  };
};

/**
 * Decorator for the `Boolean` getter function
 * `fn`.  Invokes the given function on every
 * item in the list until it returns `false`.
 *
 * @api private
 * @param {Function} fn
 * @return {Function}
 */

exports.getterNotAny = function (fn) {
  return function () {
    var args = [].slice.call(arguments);
    for (var i = 0, l = this.length; i < l; i++) {
      if (!fn.apply(null, [ this[i] ].concat(args))) {
        return false;
      }
    }
    return true;
  };
};

/**
 * Decorator for simple function `fn`.  Invokes
 * the given function once per item in the list
 * consecutively.
 *
 * Returns the `List` for chaining.
 *
 * @api private
 * @param {Function} fn
 * @return {Function}
 */

exports.simple = function (fn) {
  return function () {
    var args = [].slice.call(arguments);
    for (var i = 0, l = this.length; i < l; i++) {
      fn.apply(null, [ this[i] ].concat(args));
    }
    return this;
  };
};

/**
 * Decorator for modifier function `fn`.  Will
 * update the `List` with the modified results
 * returned by `fn`.
 *
 * The decorated function will return the
 * modified `List` for chaining.
 *
 * @api private
 * @param {Function} fn
 * @return {Function}
 */

exports.modifier = function (fn) {
  return function () {
    var args = [].slice.call(arguments);
    for (var i = 0, l = this.length; i < l; i++) {
      var element = fn.apply(null, [ this[i] ].concat(args));
      if (element) {
        this.splice(i, 1, element);
      }
    }
    return this;
  };
};

/**
 * Decorator for the given `append/before` function.
 *
 * Allows the given `function` to chain.
 *
 * @api private
 * @param {Function} fn
 * @return {Function}
 */

exports.appendBefore = function (fn) {
  return function (element) {
    for (var i = 0, l = this.length; i < l; i++) {
      // when given a DOM node;
      // insert it the first time, then clone it
      // for all others ... because jQuery
      if (i && dom.isNode(element)) {
        element = element.cloneNode(true);
      }
      fn(this[i], element);
    }
    return this;
  };
};

/**
 * Decorator for the given `text` getter function.
 *
 * @api private
 * @param {Function} fn
 * @return {Function}
 */

exports.textGetter = function (fn) {
  return function () {
    var text = '';
    for (var i = 0, l = this.length; i < l; i++) {
      var s = fn(this[i]);
      if (s) {
        text += s;
      }
    }
    return text;
  };
};

/**
 * Decorator for the given `sibling` getter function.
 *
 * Will not allow duplicates in the returned `amaze` list.
 *
 * @api private
 * @param {Function} fn
 * @return {Function}
 */

exports.siblingGetter = function (fn) {
  return function (selector) {
    var elements = [];
    for (var i = 0, l = this.length; i < l; i++) {
      var r = fn.call(null, this[i], selector);
      if (r && r.length) {
        for (var j = 0, k = r.length; j < k; j++) {
          // no dupes
          // TODO: big-O(n^2
          if (!~array.indexOf(elements, r[j])) {
            elements.push(r[j]);
          }
        }
      }
    }

    return new this.constructor(elements);
  };
};
