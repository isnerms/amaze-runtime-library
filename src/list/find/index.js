'use strict';

var array = require('array');
var queryAll = require('selector').queryAll;

/**
 * Get descendant elements of every element in
 * the list which match selector.
 *
 * @api public
 * @name amaze#find
 * @param {String} selector
 * @return {amaze}
 */
module.exports = function (selector) {
  var elements = [];
  for (var i = 0, l = this.length; i < l; i++) {
    var r = queryAll(selector, this[i]);
    if (r && r.length) {
      for (var j = 0, k = r.length; j < k; j++) {
        // no dupes
        if (!~array.indexOf(elements, r[j])) {
          elements.push(r[j]);
        }
      }
    }
  }
  return new this.constructor(elements);
};
