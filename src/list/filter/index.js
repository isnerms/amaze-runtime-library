'use strict';

var toFunction = require('../to-function');

/**
 * Reduce the set of matched elements to
 * those that match the selector or pass
 * the function’s test.
 *
 * @api public
 * @name amaze#filter
 * @param {Function|String} fn or selector
 * @return {amaze}
 */

module.exports = function (fn) {
  fn = toFunction(fn);
  var elements = [];
  for (var i = 0, l = this.length; i < l; i++) {
    if (fn.call(this[i], this[i])) {
      elements.push(this[i]);
    }
  }
  return new this.constructor(elements);
};
