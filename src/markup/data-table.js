'use strict';


//
// TODO
//
//   - overly complicated, extremely difficult to
//     understand and unmaintainable.  needs a refactor.
//   - don't actually know if this thing works
//   - test coverage is severaly lacking
//   - duplicate logic everywhere
//   - a single method doesn't need a novella written about it
//



// Setting maxlen longer due to HTML markup in the documentation
//jshint maxlen:150, maxstatements:20


var array = require('array'),
    inArray = array.inArray,
    dom = require('dom'),
    replaceTag = dom.replaceTag,
    getVisibleText = dom.getVisibleText,
    collections = require('collections'),
    each = collections.each,
    extend = collections.extend,
    clone = collections.clone,
    selector = require('selector'),
    queryAll = selector.queryAll,
    string = require('string');

var tableIndex = 0;


/**
 * Helper function to get cells actual cellIndex (offset by colspans);
 *
 * @api private
 * @param  {HTMLElement} table   The table element reference
 * @param  {Array} columns   Columns to get table cells for
 * @param  {Object} ignore   Object (Members: x, y) of columns and rows to ignore
 * @return {Array}           The array of cells that match
 */
function getCellsByColumns(table, columns, ignore) {

  var rowIndex, candidate, cellIndex, cellLength, row, columnIndex,
      matches = [],
      ignoreRows = ignore.rows,
      ignoreColumns = ignore.columns,
      rowLength = table.rows.length;

  for (rowIndex = 0; rowIndex < rowLength; rowIndex++) {
    row = table.rows[rowIndex];
    columnIndex = 0;
    if (!ignoreRows || !inArray(ignoreRows, rowIndex)) {
      for (cellIndex = 0, cellLength = row.cells.length; cellIndex < cellLength; cellIndex++) {
        candidate = row.cells[cellIndex];

        if ((!ignoreColumns || !inArray(ignoreColumns, columnIndex)) && inArray(columns, columnIndex)) {
          matches.push(candidate);
        }

        columnIndex += candidate.colSpan;

      }
    }
  }

  return matches;
}

/**
 * Creates the table caption
 *
 * @api private
 * @param {HTMLElement} table   The table reference
 * @param {Mixed} caption If string; it will use that literal text,
 *                        if a number if will take the text from that rowIndex
 */
function setCaption(table, caption) {
  var node = table.createCaption();

  if (typeof caption === 'string') {
    node.innerHTML = caption;
    return;
  }

  if (typeof caption === 'number' && table.rows[caption]) {
    // @todo append childNodes of each cell
    node.innerHTML = table.rows[caption].cells[0].innerHTML;
    table.deleteRow(caption);
    return;
  }
}

/**
 * Gives a header element an id if it doesn't have one, so it can be referenced
 *
 * @api private
 * @param {HTMLElement} cell   The cell reference
 * @param {Object} options  The options object
 */
function setHeaderId(cell, options) {
  var id = cell.id;
  if (id === null || id === '') {
    id = options.unique + '-' + cell.cellIndex + '-' + cell.parentNode.rowIndex;
    cell.id = id;
  }
  return id;
}

/**
 * Sets column headers
 *
 * @api private
 * @param {HTMLElement} table   The table reference
 * @param {Object} options  The options object
 */
function setColumnHeaders(table, options) {
  var index, length, rowIndex, cellIndex, cells, cellLength, id,
      ignoreCols = options.ignore.columns,
      headerRows = options.header.columnHeaders,
      spannedCellIndex;

  for (index = 0, length = headerRows.length; index < length; index++) {
    spannedCellIndex = 0;
    rowIndex = headerRows[index];
    cells = table.rows[rowIndex].cells;
    for (cellIndex = 0, cellLength = cells.length; cellIndex < cellLength; cellIndex++) {
      if (!inArray(ignoreCols, spannedCellIndex)) {
        id = setHeaderId(cells[cellIndex], options);
        replaceTag(cells[cellIndex], 'th', {
          scope: 'col'
        });
      }
      spannedCellIndex += cells[cellIndex].colSpan;
    }
  }
}

/**
 * Sets row headers
 *
 * @api private
 * @param {HTMLElement} table   The table reference
 * @param {Object} options  The options object
 */
function setRowHeaders(table, options) {
  var index, length, cell, id,
      header = options.header,
      ignore = options.ignore,
      cells = getCellsByColumns(table, header.rowHeaders, ignore);

  for (index = 0, length = cells.length; index < length; index++) {
    cell = cells[index];
    if (!inArray(ignore.rows, cell.parentNode.rowIndex)) {
      id = setHeaderId(cell, options);

      replaceTag(cell, 'th', {
        scope: 'row'
      });

    }
  }
}

/**
 * Calls `setRowHeaders` and `setColumnHeaders`
 *
 * @api private
 * @param {HTMLElement} table   The table reference
 * @param {Object} options  The options object
 */
function setHeaders(table, options) {
  var header = options.header;

  if (header.rowHeaders.length) {
    setRowHeaders(table, options);
  }

  if (header.columnHeaders.length) {
    setColumnHeaders(table, options);
  }
}

/**
 * Finds and replaces any `th` in the table with a `td`
 *
 * @api private
 * @param  {HTMLElement} table   The table reference
 */
function removeOldHeaders(table) {
  var cell, index, cells;

  cells = table.getElementsByTagName('th');
  for (index = cells.length; index--;) {
    cell = cells[index];
    replaceTag(cell, 'td');
  }
}

/**
 * Appends a value to the headers or aria-labelledby attributes of a cell
 *
 * @api private
 * @param {HTMLElement} cell    The cell reference
 * @param {Array} setHeaders   The list of headers to add
 * @param {Object} options  The options object
 */
function setCellHeaders(cell, setHeader, options) {
  var prop = options.aria ? 'aria-labelledby' : 'headers',
      headers = cell.getAttribute(prop) || '';

  if (headers.indexOf(setHeader) === -1) {
    headers += ' ' + setHeader;
    cell.setAttribute(prop, headers);
  }
}

/**
 * Mark cspan*rspan array cells as pointing to table cell cellIndex, rowIndex
 *
 * @api private
 * @param {Array} cells  The array of cells added to
 * @param {Integer} cspan  The number of columns to span
 * @param {Integer} rspan  The number of rows to span
 * @param {Integer} cellIndex  The cell index of the table cell
 * @param {Integer} rowIndex  The row index of the table cell
 * @param {Integer} logicalCol  cell index of the table cell accounting for colspans
 * @param {HTMLElement} cell  The table cell being pointed to
 */
function markFlatCells(cells, cspan, rspan, cellIndex, rowIndex, logicalCol, cell) {
  var i, j;
  for (i = cspan; i--;) {
    for (j = rspan; j--;) {
      cells[rowIndex + j] = cells[rowIndex + j] || [];
      cells[rowIndex + j][logicalCol + i] = {
        rowHeaders: cellIndex,
        columnHeaders: rowIndex,
        th: cell.nodeName === 'TH',
        scope: cell.scope || false
      };
    }
  }
}
/**
 * Generate a uniform 2d array of pointers to table cells occupying that space.
 *
 * @api private
 * @param {HTMLElement} cell    The cell reference
 * @param {Array} setHeaders   The list of headers to add
 * @param {Object} options  The options object
 * @return {Array}   A 2d array where each element has a [x, y] such that
 *                   table.rows[y].cells[x] occupies that cell.  One actual
 *                   cell can occupy mulitple spaces in this array due to
 *                   colspan and rowspan.
 */
function getFlatIndex(table) {
  var rowIndex, cellIndex,
      logicalCol, cell, row, cspan, rspan,
      cells = [];

  // Create a flat 2d array of cells, accounting for colspans and rowspans
  for (rowIndex = 0; rowIndex < table.rows.length; rowIndex++) {
    row = table.rows[rowIndex];
    cells[rowIndex] = cells[rowIndex] || [];
    logicalCol = 0;
    for (cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
      cell = row.cells[cellIndex];
      cspan = cell.colSpan || 1;
      rspan = cell.rowSpan || 1;
      while (cells[rowIndex][logicalCol] !== undefined) {
        // skip over cells that intrude here because of rowspan
        logicalCol++;
      }
      markFlatCells(cells, cspan, rspan, cellIndex, rowIndex, logicalCol, cell);
      logicalCol += cell.colSpan;
    }
  }
  return cells;
}


/**
 * Sets headers attributes or aria-labelledby for all cells in a column for one TH
 *
 * @api private
 * @param {HTMLElement} table   The table reference
 * @param {Object} options  The options object
 */
function setHeaderRefsCol(table, flat, fcell, x, y, options) {
  var id, n, index,
      range = [0, flat.length - 1];
  if (range[1] < 1) {
    range[1] = flat.length - 1 + range[1];
  }
  id = table.rows[fcell.columnHeaders].cells[fcell.rowHeaders].id;
  for (n = range[0]; n <= range[1]; n++) {
    if (n === y) {
      continue; // skip self
    }
    index = flat[n][x];
    setCellHeaders(table.rows[index.columnHeaders].cells[index.rowHeaders], id, options);
  }
}

/**
 * Sets headers attributes or aria-labelledby for all cells in a row for one TH
 *
 * @api private
 * @param {HTMLElement} table   The table reference
 * @param {Object} options  The options object
 */
function setHeaderRefsRow(table, flat, fcell, x, y, options) {
  var id, n, index,
      row = flat[y],
  range = [0, row.length - 1];
  if (range[1] < 1) {
    range[1] = row.length - 1 + range[1];
  }
  id = table.rows[fcell.columnHeaders].cells[fcell.rowHeaders].id;
  for (n = range[0]; n <= range[1]; n++) {
    if (n === x) {
      continue; // skip self
    }
    index = flat[y][n];
    setCellHeaders(table.rows[index.columnHeaders].cells[index.rowHeaders], id, options);
  }
}

/**
 * Sets headers attributes or aria-labelledby for all cells in the table.
 *
 * @api private
 * @param {HTMLElement} table   The table reference
 * @param {Object} options  The options object
 */
function setHeaderRefs(table, options) {
  var x, y, row, fcell, l, l2,
      flat = getFlatIndex(table);

  for (y = 0, l = flat.length; y < l; y++) {
    row = flat[y];
    for (x = 0, l2 = row.length; x < l2; x++) {
      fcell = row[x];
      if (fcell.th) {
        if (fcell.scope === 'col') {
          setHeaderRefsCol(table, flat, fcell, x, y, options);
        } else {
          setHeaderRefsRow(table, flat, fcell, x, y, options);
        }
      }
    }
  }
}

/**
 * Fills empty cells with some offscreen text.
 *
 * @api private
 * @param  {HTMLElement} table   The table reference
 */
function fillEmptyCells(table, text) {
  var cells = queryAll('th,td', table),
      cell, span;

  for (cell = cells.length; cell--;) {
    if (cells[cell].innerHTML === '') {
      span = document.createElement('span');
      span.className = 'amaze-offscreen';
      span.innerHTML = text;
      cells[cell].appendChild(span);
    }
  }
}

/**
 * Adds aria roles
 *
 * @api private
 * @param  {HTMLElement} table   The table reference
 */
function setAriaRoles(table) {
  //jshint maxcomplexity:7
  var trs = queryAll('tr', table),
      tds = queryAll('td', table),
      ths = queryAll('th', table),
      tbodies = queryAll('tbody', table),
      theads = queryAll('thead', table),
      tfoots = queryAll('tfoot', table),
      element;

  table.setAttribute('role', 'grid');
  for (element = trs.length; element--;) {
    trs[element].setAttribute('role', 'row');
  }
  for (element = ths.length; element--;) {
    ths[element].setAttribute('role', 'gridcell');
  }
  for (element = tds.length; element--;) {
    tds[element].setAttribute('role', 'gridcell');
  }
  if (tbodies && tbodies.length > 0) {
    tbodies[0].setAttribute('role', 'presentation');
  }
  if (theads && theads.length > 0) {
    theads[0].setAttribute('role', 'presentation');
  }
  if (tfoots && tfoots.length > 0) {
    tfoots[0].setAttribute('role', 'presentation');
  }
}
/**
 * Will make all the apprpriate associations between the header cells and their respective target cells
 * for the region options
 *
 * @api private
 * @param {HTMLElement} table - the target table
 * @param {Object} options - the options structure
 */
function setRegionRefs(table, options) {
  var trs, att = options.aria ? 'aria-labelledby' : 'headers';
  if (!options.regions || options.regions.length === 0) {
    return;
  }
  trs = queryAll('tr', table);
  each(options.regions, function (region) {
    var hCell = trs[region.headerCell.row].cells[region.headerCell.column], id;
    id = hCell.getAttribute('id');
    if (!id) {
      setHeaderId(hCell, options);
    }
    if (hCell.nodeName === 'TD') {
      replaceTag(hCell, 'th');
    }
    each(region.targetCells, function (tc) {
      var i, last, cell, first, attVal, tr = trs[tc.row];
      last = tc.columns[1];
      first = tc.columns[0];
      if (last === 0) {
        if (first === -1) {
          // only first cell
          first = 0;
          last = 1;
        } else {
          last = tr.cells.length;
        }
      } else if (last < 0) {
        last = tr.cells.length + last;
      } else {
        last += 1;
      }
      for (i = first; i < last; i++) {
        cell = tr.cells[i];
        attVal = cell.getAttribute(att);
        if (!attVal) {
          attVal = id;
        } else {
          attVal += ' ' + id;
        }
        cell.setAttribute(att, attVal);
      }
    });
  });
}

/**
 * Will generate offscreen text that match all the headers attributes in the table
 *
 * @api private
 * @param  {Object} options   The options reference
 */
function iOSMarkup(table, options) {
  var headersAtt = options.aria ? 'aria-labelledby' : 'headers',
      cells = queryAll('[' + headersAtt + ']', table);
  each(cells, function (value) {
    var headersVal = value.getAttribute(headersAtt),
        headers = string.trim(headersVal).split(' '),
        oText = '',
        span;
    if (getVisibleText(value) === '') {
      return; // do nothing
    }
    each(headers, function (id) {
      var hCell = document.getElementById(id);
      oText += ' ' + getVisibleText(hCell);
      hCell.removeAttribute('scope');
      replaceTag(hCell, 'td');
    });
    if (string.trim(oText) !== '') {
      span = document.createElement('span');
      span.className = 'amaze-offscreen';
      span.innerHTML = oText;
      value.insertBefore(span, value.firstChild);
    }
    value.removeAttribute(headersAtt);
  });
}

/*
* For Backwards compatibility, turn options.headers.x|y and similar options into their
* new and more descriptive equivalents
*
* @api private
* @param {Object} optionsIn the user supplied options
*/
function compatibleOptionsConversion(optionsIn) {
  //jshint maxcomplexity:10
  if (optionsIn) {
    if (optionsIn.header) {
      // Need the tests here so that missing properties do not overwrite the defaults
      if (optionsIn.header.hasOwnProperty('x')) {
        optionsIn.header.rowHeaders = optionsIn.header.x;
      }
      if (optionsIn.header.hasOwnProperty('y')) {
        optionsIn.header.columnHeaders = optionsIn.header.y;
      }
    }
    if (optionsIn.ignore) {
      if (optionsIn.ignore.hasOwnProperty('x')) {
        optionsIn.ignore.columns = optionsIn.ignore.x;
      }
      if (optionsIn.ignore.hasOwnProperty('y')) {
        optionsIn.ignore.rows = optionsIn.ignore.y;
      }
    }
  }
}

/**
 * check if the table is simple (true) or not (false)
 *
 * @api private
 * @param {Object} [optsIn] Options provided by the developer
 * @param {Object} opts Options merged with defaults
 * @return {Boolean}
 */
function tableSimplicity(options, optionsIn) {
  /**
   * Check to see if the provided `header` has exactly one member and its only member is `0`.
   *
   * Will return `false` if the table `header` coressponds to a simple table
   *
   * @api private
   * @param {Array} header
   * @return {Boolean}
   */
  function checkHeader(header) {
    // no headers provided?  it can't be complex
    if (!header || header.length === 0) {
      return false;
    }

    // more than one header or the only header provided isn't `0`?  it's complex
    if (header.length > 1 || header[0] !== 0) {
      return true;
    }

    return false;
  }

  // verify the user did indeed provide options
  if (optionsIn) {
    // check for the user providing:
    //   - aria=true
    //   - regions=[ one or more region ]
    //   - iOS=true
    // any of the above will result in a "non"-simple table
    if (optionsIn.aria || optionsIn.iOS) {
      return false;
    }
  }

  // if regions were provided or if more than one
  // column or row header was provided, it's not simple
  if (options.regions || checkHeader(options.header.columnHeaders) || checkHeader(options.header.rowHeaders)) {
    return false;
  }

  return true;
}

/**
 * Single element entry point.
 *
 * @api private
 * @param  {HTMLElement} table   The table reference
 * @param {Object} options   The options object
 */
function dataTable(table, optionsIn) {
  var isSimpleTable,
      options = clone(exports.defaults);

  compatibleOptionsConversion(optionsIn);

  options = extend(options, optionsIn, true);
  options.unique = 'amaze-table-' + tableIndex++;

  //Check if table is 2D/simple (true) or 3D/NOT simple (false)
  isSimpleTable = tableSimplicity(options, optionsIn);

  //Both removeOldHeaders() and setHeaders()
  //need to run on simple AND complex tables
  removeOldHeaders(table);
  setHeaders(table, options);

  //only complex tables should deal with header/region refs
  if (!isSimpleTable) {
    setHeaderRefs(table, options);
    setRegionRefs(table, options);
  }

  if (options.caption !== undefined && options.caption !== null) {
    setCaption(table, options.caption);
  }

  if (options.header.iOS) {
    iOSMarkup(table, options);
  } else if (options.aria === true) {
    setAriaRoles(table);
    fillEmptyCells(table, options.emptyText);
  }
}




/**
 * Marks up a table with column/row headers. Will automatically adjust the markup so that it works for the target operating
 * system. Large differences in markup are generated on Mac OS X and on iOS. On Mac OS X, aria markup is used for the table
 * header association. On iOS, the limitations of the iOS VoiceOver screen reader are accomodated to ensure that maximum
 * additional information is supplied to the user. `dataTable` will detect the operating system it is operating on and will
 * choose the appropriate options for this operating system. The `aria` and the `iOs` options are available to force a particular
 * markup (see below). Take care when applying this function to tables that have sorting functionality (see note below).
 *
 * **NOTE: THIS FUNCTION WILL REMOVE ALL EVENT HANDLERS FROM ALL EXISTING TH CELLS (BECAUSE IT REPLACES THEM) AND IT MAY
 *       DO THE SAME FOR TD CELLS. THIS MAY CAUSE FUNCTIONALITY TO BREAK**
 *
 * **Options object**:
 *
 * - `[header]` _Object_ **Optional**
 *    - `rowHeaders` _Array_ The list of column indices that serve as headers for rows. For example
 *       if the first cell in each row is a header for that row, then the array would contain a 0. Note that dataTable
 *       will evaluate the colspan and rowspan attributes of the table and utilize this information to construct the mappings
 *       to the header cells.
 *    - `columnHeaders` _Array_ The list of row indices that serve as headers for columns. For example if the first
 *       and the second rows contain headers for each column, then this array would equal [0, 1]
 * - `[ignore]` _Object_ **Optional**
 *    - `columns` _Array_ The list of column indices in each row that are not to be marked up as header cells
 *    - `rows` _Array_ The list of row indices which are not to be marked up as header cells
 * - `[caption]` _Mixed_  **Optional** If integer, it convert cells in that rowIndex to a
 *    caption: if its a string, it will use that literal text as the caption;
 *    if `null`, `undefined` or generally falsey; no caption will be added.
 * - `[aria]` _Bool_  **Optional** If true, adds wai-aria attributes to all cells.  This is
 *    true by default on MacOS, false by default elsewhere. If you require ARIA markup on all operating systems including iOS
 *    the be sure to set the `iOS` option (see below) to `false` to ensure that the OS detection does not override this option
 * - `[iOs]` _Bool_  **Optional** If true, then `dataTable` will force the markup into a state that will work for 3D tables
 *    on iOS devices (note this overrides the aria flag).
 * - `[emptyText]` _String_  **Optional** If supplied, this is the offscreen text to be added to all empty cells. On Mac
 *    OS X, the default value is 'empty'. If you have multiple languages then you will want to pass in your language specific
 *    value here for the page's current language. This option is only used when ARIA markup is being generated.
 * - `[regions]` _Object_ **Optional** if supplied this provides a mapping of header cells to the cells that they map. This allows
 *    `dataTable` to support headers for discontinuous cells or cell ranges that span multiple rows AND columns
 *    - `headerCell` _Object_ the coordinates of the cell that is the header of the target cells
 *       - `row` _Integer_ the 0-based index of the row in which the header cell is to be found
 *       - `column` _Integer_ the 0-based index of the column in which the header cell is to be found
 *    - `targetCells` _Array_ an array of the row and column extents
 *       - `row` _Integer_ the 0-based index of the row in which the target cells exist
 *       - `columns` _Array_ an array of two elements being the 0-based indexes of the start and end of the range of target
 *          cells within the row
 *
 *
 *
 *
 * Example: Evaluation of rowspan and colspan to correctly markup a table
 *
 * ```javascript
 * // Given the following table structure
 * //
 * &lt;thead&gt;
 *  &lt;tr&gt;
 *    &lt;th class="stubhead" rowspan="3"&gt;Raw Materials Production&lt;/th&gt;
 *    &lt;th colspan="4"&gt;By Weight&lt;/th&gt;
 *    &lt;th colspan="4"&gt;By Value&lt;/th&gt;
 *  &lt;/tr&gt;
 *  &lt;tr&gt;
 *    &lt;th colspan="2"&gt;Adjusted&lt;/th&gt;
 *    &lt;th colspan="2"&gt;Unadjusted&lt;/th&gt;
 *    &lt;th colspan="2"&gt;Adjusted&lt;/th&gt;
 *    &lt;th colspan="2"&gt;Unadjusted&lt;/th&gt;
 *  &lt;/tr&gt;
 *  &lt;tr&gt;
 *    &lt;th&gt;2001&lt;/th&gt;
 *    &lt;th&gt;2002&lt;/th&gt;
 *    &lt;th&gt;2001&lt;/th&gt;
 *    &lt;th&gt;2002&lt;/th&gt;
 *    &lt;th&gt;2001&lt;/th&gt;
 *    &lt;th&gt;2002&lt;/th&gt;
 *    &lt;th&gt;2001&lt;/th&gt;
 *    &lt;th&gt;2002&lt;/th&gt;
 *  &lt;/tr&gt;
 * &lt;/thead&gt;
 * //
 * // The following call to dataTable will mark up the table correctly on all platforms
 * //
 * dataTable(ourTable, {
 *    header: {
 *      rowHeaders: [0],
 *      columnHeaders: [0, 1, 2]
 *    }
 *  });
 * //
 * // Resulting in the following structure on a Mac OS X machine (plus of course the appropriate cell
 * // associations in the data cells)
 * //
 * &lt;thead role="presentation"&gt;
 *  &lt;tr role="row"&gt;
 *    &lt;th class="stubhead" rowspan="3" id="amaze-table-0-0-0" role="presentation"&gt;Raw Materials Production&lt;/th&gt;
 *    &lt;th colspan="4" id="amaze-table-0-1-0" role="presentation"&gt;By Weight&lt;/th&gt;
 *    &lt;th colspan="4" id="amaze-table-0-2-0" role="presentation"&gt;By Value&lt;/th&gt;
 *  &lt;/tr&gt;
 *  &lt;tr role="row"&gt;
 *    &lt;th colspan="2" id="amaze-table-0-0-1" aria-labelledby=" amaze-table-0-1-0" role="presentation"&gt;Adjusted&lt;/th&gt;
 *    &lt;th colspan="2" id="amaze-table-0-1-1" aria-labelledby=" amaze-table-0-1-0" role="presentation"&gt;Unadjusted&lt;/th&gt;
 *    &lt;th colspan="2" id="amaze-table-0-2-1" aria-labelledby=" amaze-table-0-2-0" role="presentation"&gt;Adjusted&lt;/th&gt;
 *    &lt;th colspan="2" id="amaze-table-0-3-1" aria-labelledby=" amaze-table-0-2-0" role="presentation"&gt;Unadjusted&lt;/th&gt;
 *  &lt;/tr&gt;
 *  &lt;tr role="row"&gt;
 *    &lt;th id="amaze-table-0-0-2" aria-labelledby=" amaze-table-0-0-1 amaze-table-0-1-0" role="presentation"&gt;2001&lt;/th&gt;
 *    &lt;th id="amaze-table-0-1-2" aria-labelledby=" amaze-table-0-0-1 amaze-table-0-1-0" role="presentation"&gt;2002&lt;/th&gt;
 *    &lt;th id="amaze-table-0-2-2" aria-labelledby=" amaze-table-0-1-1 amaze-table-0-1-0" role="presentation"&gt;2001&lt;/th&gt;
 *    &lt;th id="amaze-table-0-3-2" aria-labelledby=" amaze-table-0-1-1 amaze-table-0-1-0" role="presentation"&gt;2002&lt;/th&gt;
 *    &lt;th id="amaze-table-0-4-2" aria-labelledby=" amaze-table-0-2-1 amaze-table-0-2-0" role="presentation"&gt;2001&lt;/th&gt;
 *    &lt;th id="amaze-table-0-5-2" aria-labelledby=" amaze-table-0-2-1 amaze-table-0-2-0" role="presentation"&gt;2002&lt;/th&gt;
 *    &lt;th id="amaze-table-0-6-2" aria-labelledby=" amaze-table-0-3-1 amaze-table-0-2-0" role="presentation"&gt;2001&lt;/th&gt;
 *    &lt;th id="amaze-table-0-7-2" aria-labelledby=" amaze-table-0-3-1 amaze-table-0-2-0" role="presentation"&gt;2002&lt;/th&gt;
 *  &lt;/tr&gt;
 * &lt;/thead&gt;
 * ```
 *
 *
 * Example:
 *
 * ```javascript
 * var dataTable = require('lib/markup/dataTable');
 * // Simple example that marks the first row as column headers
 * dataTable('.cats', {
 *   header: {
 *     columnHeaders: [0]
 *   }
 * });
 *
 * // marks up first two columns and first row as table headers,
 * // ignores the 8th column and 16 and 17th rows, and adds a caption of 'Cats'
 * dataTable('.cats', {
 *   header: {
 *     rowHeaders: [0, 1],
 *     columnHeaders: [0]
 *   },
 *   ignore: {
 *     columns: [7],
 *     rows: [15, 16]
 *   },
 *   caption: 'Cats'
 * });
 *
 * ```
 *
 * Example:
 *
 * ```javascript
 * //
 * // The following options structure will create an ARIA-marked up table with the cell in row 2, column 1
 * // as the header for all the cells in row 5 and columns 1 through 4 of rows 3 and 4
 * //
 *  options = {
 *    regions : [
 *      {
 *        headerCell : {
 *          column : 0,
 *          row : 1
 *        },
 *        targetCells : [
 *          {
 *            row : 2,
 *            columns : [0, 3]
 *          },
 *          {
 *            row : 3,
 *            columns : [0, 3]
 *          },
 *          {
 *            row : 4,
 *            columns : [0, 0]
 *          }
 *        ]
 *      }
 *    ],
 *    aria: true
 *  };
 * ```
 *
 * #### Simple Tables:
 *
 * - Tables with 1 or less columnHeader and 1 or less rowHeader set
 *    - Neither rowHeaders nor columnHeaders can have more than one value set (in it's array) for it to be a simple table.
 * - Do not deal with inputted option values for iOS, Regions, and Aria.
 *     - No cells should have an aria-labelledby attribute
 *     - No header attributes (headers=" amaze-table-xx")
 * - columnHeaders will have scope="col" and rowHeaders will have scope="row"
 *
 * Example:
 *
 * ```js
 * //rowHeaders[0] will set rowHeaders (<th />'s) down the first column and add scope="row" to them
 * //columnHeaders[1] will set the columnHeaders (<th />'s) across the second row and add scope="col" to them
 *   options = {
 *       header: {
 *           iOS: false,
 *           rowHeaders: [0],
 *           columnHeaders: [1]
 *       },
 *       aria: false
 *   };
 * ```
 *
 * @api public
 * @name markup.dataTable
 * @param {Mixed} element Selector, element reference or array-like object with element references
 * @param {Object} options Options object
 * @author  david.sturley@deque.com
 * @author  matt.schikore@deque.com
 * @author  harris.schneiderman@deque.com
 */
exports = module.exports = function (selector, options) {
  var index,
    elements = queryAll(selector),
    length = elements.length;

  for (index = 0; index < length; index++) {
    dataTable(elements[index], options);
  }
};

exports.defaults = {
  header : {
    rowHeaders: [],
    columnHeaders: [],
    iOS: navigator.userAgent.match(/iP(hone|od|ad)/i) !== null
  },
  ignore: {
    columns: [],
    rows: []
  },
  caption: null,
  aria: navigator.userAgent.match(/Mac OS/i) !== null
};

exports.defaults.emptyText = 'empty';
