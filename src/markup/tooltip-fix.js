'use strict';

var events = require('events'),
	selector = require('selector'),
	collections = require('collections'),
	rndid = require('rndid');

/**
 * Fixes a existing tooltip
 * - Gives the tooltip an id if it doesn't already have one
 * - Sets aria-describedby=tooltip.id attribute to the target
 * - Toggles display by default for options.show and options.hide
 *
 * Options object:
 *
 * - 'show' `Function` the display callback function taking the target, the tooltip and focusEvent as parameters.
 * Example:
 *
 * ```js
 * options.show: function (tooltip, target, focusEvent) {
 *   // tooltip = the tooltip
 *   // target = the element which invokes the tooltip
 *   // focusEvent = the event which invoked the tooltip
 *   tooltip.style.display = 'block';
 * }
 * ```
 *
 * - 'hide' `Function` the hide callback function taking the target, the tooltip and focusEvent as parameters
 *
 * ```js
 * options.hide: function (tooltip, target, focusEvent) {
 *     // tooltip = the tooltip
 *     // target = the element which invokes the tooltip
 *     // focusEvent = the event which invoked the tooltip
 *     tooltip.style.display = 'none';
 * }
 * ```
 *
 * Example implementation:
 *
 * ```js
 * var fixTips = require('lib/markup/tooltip').fix;
 *
 * fixTips('#tool-target', '#tooltipper');
 *
 * //or if you want to apply the fix to an array of elements with a common tooltip:
 * var markup = require('lib/markup'),
 *     targets = document.querySelectorAll('.target-input');
 * for (var i = targets.length -1; i >= 0; i--) {
 *     markup.tooltip.fix(targets[i], '#common');
 * }
 * ```
 *
 * @api public
 * @name markup.tooltip.fix
 * @param {String|HTMLElement} target Target of the tooltip
 * @param {String|HTMLElement} tips Tooltip element
 * @param {Object} [options] Options object
 */

exports = module.exports = function (target, tip, options) {

	options = collections.extend({
		show: function (tooltip) {
			//focus target -> display tooltip by default
			tooltip.style.display = 'block';
		},
		hide: function (tooltip) {
			//blur target -> dont display tooltip by default
			tooltip.style.display = 'none';
		}
	}, options || {});

	//check if target is a string
	if (typeof target === 'string') {
		//it is a string so query for it
		target = selector.query(target);
	}

	//check if tip is a string
	if (typeof tip === 'string') {
		tip = selector.query(tip);
	}

	//Check for a tooltip id
	//which will be used for aria described by on the target

	var id = tip.id || rndid(),
		described;

	tip.id = id;

	described = target.getAttribute('aria-describedby');

	//set aria-describedby attribute to the target
	//associating it with the tooltip
	if (!described) {
		target.setAttribute('aria-describedby', id);
	} else if (described.indexOf(id) === -1) {
		target.setAttribute('aria-describedby', described + ' ' + id);
	}

	//options.show
	events.on(target, 'focus', function (focusEvent) {
		options.show(tip, target, focusEvent);
	});
	//options.hide
	events.on(target, 'blur', function (blurEvent) {
		options.hide(tip, target, blurEvent);
	});
};
