
'use strict';

/**
 * Sets or replaces the document's language `<html lang="foo"></html>`
 *
 * @api public
 * @name markup.documentLanguage
 * @param {String} lang The language to set
 * @param {Boolean} [overwrite] False to not overwrite the existing langauge
 */
module.exports = function (lang, overwrite) {

	// defaulting lang to en
	lang = lang || 'en';
	//defaulting overwrite to true
	overwrite = overwrite === undefined ? true : overwrite;

	var doc = document.documentElement,
		currentLang = doc.getAttribute('lang');

	//if there is a language already defined and overwrite is false
	if (currentLang && !overwrite) {
		return;
	}

	//setting the language to what was provided in options
	doc.setAttribute('lang', lang);
};
