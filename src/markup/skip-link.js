
'use strict';

var selector = require('selector'),
    dom = require('dom'),
    events = require('events'),
    css = require('css');

//jshint maxstatements: 19

var skipLinkStylesAdded = false;


/**
 * Checks if the argument is a string, and if so passed is it to `selector.query`
 *
 * @api private
 * @param  {Mixed} thing    Selector or HTMLElement
 * @return {HTMLElement}    The element reference
 */
function unselectorify(thing) {
  if (typeof thing === 'string') {
    thing = selector.query(thing);
  }
  return thing;
}

/**
 * Calculates position based on oridinal directions (ne, nw, se, sw) or explicit top/left
 *
 * @api private
 * @param {HTMLElement} skipLink The skip link
 * @param {HTMLElement} to       The element to position the skip link to
 * @param {Mixed}       align    String of oridinal position (ne, nw, se, sw) or object of top/left
 */
function setPosition(skipLink, to, align) {

  align = align || '';

  var top, left, index;

  if (typeof align === 'object') {
    for (index in align) {
      if (align.hasOwnProperty(index)) {
        skipLink.style[index] = align[index];
      }
    }
    return;
  }

  align = align.toLowerCase();
  if (align.indexOf('s') !== -1) {
    top = (to.offsetTop + to.offsetHeight - skipLink.offsetHeight) + 'px';
  } else {
    top = to.offsetTop + 'px';
  }

  if (align.indexOf('e') !== -1) {
    left = (to.offsetLeft + to.offsetWidth - skipLink.offsetWidth) + 'px';
  } else {
    left = to.offsetLeft + 'px';
  }

  skipLink.style.top = top;
  skipLink.style.left = left;
}

/**
 * Listener for each type of skip link
 *
 * @api private
 * @param  {HTMLElement} target  The target of the skip link
 */
function skipLinkListener(target) {
  return function (event) {
    if (!dom.isFocusable(target)) {
      target.setAttribute('tabindex', '-1');
    }
    target.focus();

    event.preventDefault();
    // @todo stopProp?
  };
}

/**
 * Adds a skiplink
 *
 * Options:
 *
 * - text `String` Text of the skip link, defaults to 'Skip to main content'
 * - align `String|Object` Either an option specifying styles or a cardinal direction string
 *   - Accepted strings:
 *     - `n` Align to the "North"
 *     - `s` Align to the "South"
 *     - `e` Align to the "East"
 *     - `w` Align to the "West"
 *     - `ne` Align to the "North East"
 *     - `nw` Align to the "North West"
 *     - `se` Align to the "South East"
 *     - `sw` Align to the "South West"
 * - class `String` Additional class to add to the link
 *
 * Example Options:
 *
 *     {
 *       text: 'Jump to navigation',
 *       align: 'nw',
 *       class: 'my-skip-link'
 *     }
 *
 *     {
 *       align: {
 *         left: '10px',
 *         top: '20px'
 *       }
 *     }
 *
 * Example Usage:
 *
 * ```js
 * var skipLink = require('lib/markup').skipLink;
 *
 * // add as the **first** child of the `<body />`
 * skipLink.add(document.body.firstChild, '#main-content', {
 *   align: {
 *     left: '30px',
 *     top: '100px'
 *   }
 * });
 * ```
 *
 *
 * @api public
 * @name markup.skipLink.add
 * @param {HTMLElement|String} insertionPoint HTMLElement or selector to insertBefore the skip link
 * @param {HTMLElement|String} target HTMLElement or selector of the target of the skip link
 * @param {Object} options Options object
 */
exports.add = function (insertionPoint, target, options) {
  options = options || {};

  insertionPoint = unselectorify(insertionPoint);
  target = unselectorify(target);

  if (!skipLinkStylesAdded) {
    css('.amaze-skip-link {' +
      'position: absolute;' +
      'top: -9999px;' +
      'left: -9999px;' +
      'background: #fff;' +
      'color: #00f;' +
    '}');
    skipLinkStylesAdded = true;
  }

  var skipLink = document.createElement('a');

  skipLink.href = '#';

  if (target.id) {
    skipLink.href += target.id;
  }

  skipLink.className = 'amaze-skip-link';

  if (options['class']) {
    skipLink.className += ' ' + options['class'];
  }

  skipLink.appendChild(document.createTextNode(options.text || 'Skip to main content'));

  insertionPoint.parentNode.insertBefore(skipLink, insertionPoint);

  events.on(skipLink, 'focus', function () {
    setPosition(this, insertionPoint, options.align);
  });
  events.on(skipLink, 'blur', function () {
    this.style.left = '';
    this.style.top = '';
  });

  events.on(skipLink, 'click', skipLinkListener(target));

  return skipLink;
};

function resolveTarget(id) {
  if (id) {
    var target = document.getElementById(id);
    if (target) {
      return target;
    }

    target = document.getElementsByName(id);
    if (target.length) {
      return target[0];
    }
  }
}


/**
 * Fixes a skiplink by adding a click listener that will set focus programmatically
 *
 * Target object:
 *
 * Target of the skip link.  If the target of the skip link is
 * defined in the href attribute, this parameter can be safely
 * omitted.
 *
 * @api public
 * @name markup.skipLink.fix
 * @param {HTMLElement|String} element Element reference or selector of the skiplink anchor
 * @param {HTMLElement|String} [target] Target object
 */
exports.fix = function (element, target) {
  element = unselectorify(element);

  if (!target) {
    var hash = element.hash;
    target = resolveTarget(hash.substr(1));
    if (!target) {
      throw new Error('Cannot find skipLink target.  You must specify `target`' +
        ' if the skip link does not have a valid interal reference.');
    }
  } else if (typeof target === 'string') {
    target = selector.query(target);
  }

  events.on(element, 'click', skipLinkListener(target));
};
