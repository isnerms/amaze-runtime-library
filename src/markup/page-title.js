
'use strict';

/**
 * Adds/replaces or appends a new page title to the document
 *
 * @api public
 * @name markup.pageTitle
 * @param {String} title Title can be a string or part of the str object
 * @param {Boolean} [append] Append the title provided to the original title
 * @return {title} The newly set page title
 */
module.exports = function (title, append) {
	//if append is "true", add the provided title to the existing title
	//otherwise, replace the original title
	title = append ? document.title + ' ' + title : title;
	//set the document's title
	document.title = title;

	return title;
};
