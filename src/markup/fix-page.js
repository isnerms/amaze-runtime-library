
'use strict';

var skipLink = require('./skip-link'),
		landmarksModal = require('./landmarks-modal'),
		pageTitle = require('./page-title'),
		documentLanguage = require('./document-language');

/**
 * Easily fix a page by running the following methods against it:
 *
 * - `markup.skipLink.add` for adding a skip link
 * - `markup.documentLanguage` for adding a `lang="..."` attribute to `<html />`
 * - `markup.pageTitle` for adding (or appending to) the document's title
 * - `markup.skipModal` for easy keyboard navigation
 *
 * #### Options object:
 *
 * - skipLink `Object` Arguments provided to `markup.skipLink.add()`
 * - title `Object|String` Arguments provided to `markup.pageTitle()`.  When
 *   provided a string, the document's page title will be added as that string.
 * - lang `Object|String` Arguments provided to `markup.documentLanguage()`.
 *   When provided a string, the document's language will be set to it.
 * - landmark `Object` Arguments provided to `markup.skipModal`
 *
 * @api public
 * @name markup.fixPage
 * @param {Object} options pass in options to the options object
 */
module.exports = function (options) {
	/**
	 * example:
	 * title: {
	 *    str: 'foo',
	 *    append: true
	 * }
	 * -------or-------
	 * title: 'foo'
	 *
	 * @api private
	 * @param {String|Object} option
	 */
	function stringOption(option) {
		if (typeof option === 'string') {
			return option;
		}
		return option.str;
	}

	// exception prevention
	options = options || {};

	function execute() {
		var title, append = false, lang, overwrite;
		//If skipLink option is provided
		if (options.skipLink) {
			skipLink.add(options.skipLink.insertionPoint, options.skipLink.target, options.skipLink.options);
		}
		//If title option is provided
		if (options.title) {
			//decide whether a string has been provided or str
			title = stringOption(options.title);
			append = options.title.append;
			//call the pageTitle library function
			pageTitle(title, append);
		}
		//If lang option is provided
		if (options.lang) {
			//decide whether a string has been provided or str
			lang = stringOption(options.lang);
			overwrite = options.lang.overwrite;
			documentLanguage(lang, overwrite);
		}
		//just pass in the landmark's objects
		//to the landmarksModal library function
		if (options.landmark) {
			landmarksModal(options.landmark);
		}
	}

	if (typeof options.delay === 'number') {
		setTimeout(execute, options.delay);
	} else {
		execute();
	}

};
