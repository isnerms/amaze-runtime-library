
'use strict';

var selector = require('selector'),
    each = require('collections').each;

/**
 * Marks an element as presentational with
 * `role="presentation"` and `alt=""` for images.
 *
 * @api public
 * @name markup.presentation
 * @param {Mixed} elements Selector, element reference or array-like object containing element references
 */
module.exports = function (elements) {
	elements = selector.queryAll(elements);

	each(elements, function (element) {
		element.setAttribute('role', 'presentation');

		if (element.nodeName === 'IMG') {
			element.setAttribute('alt', '');
		}
	});

};
