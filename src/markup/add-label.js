
'use strict';

var selector = require('selector'),
    collections = require('collections');

var labels = 0;

/**
 * Append attribute value to element if that value is not already present
 *
 * @api private
 * @param {HTMLElement} element The element to add the attribute to
 * @param {String} name The attribute name; ie "aria-labelledby"
 * @param {String} value The attribute value; ie "my-id"
 */
function addDelimitedAttribute(element, name, value) {
  var newValue = '',
    oldValue = element.getAttribute(name);

  // don't add it if its already there
  if (!oldValue || (' ' + oldValue + ' ').indexOf(' ' + value + ' ') === -1) {

    if (oldValue && oldValue !== value) {
      newValue += oldValue + ' ';
    }
    newValue += value;

    element.setAttribute(name, newValue);
  }
}

var labels = 0;

/**
 * Adds an explicit `aria-label` or associates an already
 * on-screen label with an input via `aria-labelledby`.
 *
 * @api public
 * @name markup.addLabel
 * @param {Mixed} elements Selector, element reference or array-like object containing element references
 * @param {Mixed} label Either an element reference or literal text. **Will not accept a selector**
 * @param {String} [describedBy] Appends to `aria-describedby`
 */
module.exports = function (elements, label, describedBy) {
  elements = selector.queryAll(elements);

  var iterator;

  // if plain text, add aria-label
  if (typeof label === 'string') {

    iterator = function (element) {
      element.setAttribute('aria-label', label);
      if (describedBy) {
        addDelimitedAttribute(element, 'aria-describedby', describedBy);
      }
    };

  // TODO dom.isNode?
  } else if (label.nodeType && label.nodeType === 1) {

    if (!label.id) {
      label.id = 'amaze_label_' + labels++;
    }

    iterator = function (element) {

      addDelimitedAttribute(element, 'aria-labelledby', label.id);

      if (describedBy) {
        addDelimitedAttribute(element, 'aria-describedby', describedBy);
      }

    };
  } else {
    throw new Error('Bad parameter for `label`, you must pass literal text or an element reference');
  }

  collections.each(elements, iterator);
};
