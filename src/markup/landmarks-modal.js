
'use strict';

var array = require('array'),
    selector = require('selector'),
    events = require('events'),
    css = require('css'),
    SkipModal = require('./skip-modal');

module.exports = landmarksModal;

/**
 * Merges to objects together, favoring the second
 * object's properties.
 *
 * #### Details:
 *
 * overwrites `first` properties with `second`
 * properties and adds `second` properties if
 * missing in `first`
 *
 * TODO
 *
 * the `collections/extend` method is *horrible* and
 * should be replaced with a non-modifier method.
 *
 * @api private
 * @param {Object} first
 * @param {Object} second
 * @return {Object} The merged objects
 */
function deepExtend(first, second) {
  var prop, value,
      result = {};

  // mirror the first object
  for (prop in first) {
    if (first.hasOwnProperty(prop)) {
      result[prop] = first[prop];
    }
  }

  for (prop in second) {
    if (second.hasOwnProperty(prop)) {
      value = second[prop];
      // deep extend an object's objects, but not arrays (when deep is truthy)
      if (typeof value === 'object' && !array.isArray(value)) {
        result[prop] = deepExtend(result[prop], second[prop]);
      } else {
        result[prop] = second[prop];
      }
    }
  }

  return result;
}

/**
 * Simplified skip link (`markup.skipLink.add`) for the modal.  The
 * existing skiplink functions won't work in the case as the `target`
 * is added/removed from the DOM based on users' behavior.
 *
 * @api private
 * @param {Object} options
 * @param {SkipModal} modal
 * @return {HTMLElement}
 */
function landmarkSkipLink(options, modal) {
  var link = document.createElement('a'),
      entry = options.skipLink.entry || document.body.firstChild;

  link.className = 'amaze-skip-link';
  link.id = options.skipLink.id;
  link.href = '#';
  link.appendChild(document.createTextNode(options.skipLink.text || 'Skip to landmarks popup'));

  // handle selector
  if (typeof entry === 'string') {
    entry = selector.query(entry);
  }

  entry.parentNode.insertBefore(link, entry);

  // focus the title on clicks
  events.on(link, 'click', function () {
    if (!modal.isOpen()) {
      modal.show();
    }
    modal._titleBar.focus();
  });

  // focus the modal when ESC is hit
  modal.on('escape', function () {
    link.focus();
  });

  // remove the link when the modal is destroyed
  modal.on('destroyed', function () {
    try {
      entry.removeChild(link);
    } catch (err) {}
  });

  css([
      '#' + link.id + '.amaze-skip-link' + ' {',
      '  position: absolute;',
      '  top: -9999px;',
      '  left: -9999px;',
      '  background: #fff;',
      '  color: #00f;',
      '}',
      '  #' + link.id + '.amaze-skip-link' + ':focus {',
      '    position: relative;',
      '    top: 0;',
      '    left: 0;',
      '  }'
    ].join(''));

  return link;
}

/**
 * Create a `SkipModal` with the given `options`
 *
 * #### Options:
 *
 * - selector `String` A selector for all elements to
 *   add; defaults to `"[role],h1,h2,h3,h4,h5,h6"`
 * - excluded `Array` An array of selectors which no
 *   element should match; defaults to ignoring the roles:
 *   - _menuitem_ (`[role="menuitem"]`)
 *   - _listitem_ (`[role="listitem"]`)
 *   - _row_ (`[role="row"]`)
 * - id `String` The id to give the modal; defaults
 *   to `"amaze-navigation-modal"`
 * - title `String` The title of the modal; defaults
 *   to `"Navigation Modal"`
 * - openClass `String` The class to add/remove the modal's
 *   visibility state; defaults to `"open"`
 * - addStyles `Boolean` Should the default CSS be applied;
 *   defaults to `true`
 * - allowDuplicates `Boolean` Should duplicate elements be
 *   added to the modal; defaults to `false`
 * - escape `Boolean` Should the modal close when "ESCAPE" is
 *   pressed; defaults to `true`
 * - hideOnSkip `Boolean` Should the modal close when a user
 *   clicks one of its links; defaults to `true`
 * - autoHide `Boolean` Should the modal start hidden; defaults
 *   to `true`
 * - maxTextLength `Number` Max length a contained element's
 *   text; defaults to `30`
 * - trapFocus `Boolean` Should focus be kept within the
 *   modal; defaults to `true`
 * - skipLink `Boolean|Object` `false` if no skip link should
 *   be added, or an object containing the properties:
 *   - _entry_ `String|HTMLElement` An element or selector
 *     where the skip link should be added; defaults to the
 *     first child of the body
 *   - _text_ `String` The text the skip link should contain;
 *     defaults to `"Skip to landmarks popup"`
 *   - _id_ `String` The skip link's ID; defaults to
 *     `"amaze-skip-to-landmarks"`
 *
 * Examples:
 *
 * ```js
 *
 * var modal = landmarksModal({
 *   id: 'my-modal',
 *   title: 'My Awesome Modal',
 *   maxTextLength: 100
 * });
 * modal.show();
 *
 * // all anchors and landmarks
 * landmarksModal({
 *     selector: 'a,[role]'
 *   })
 *   // also the first level-1 heading
 *   .add(selector.query('h1'))
 *   // set foo=bar
 *   .set('foo', 'bar')
 *   .show(function (modal) {
 *     // the modal is showing :)
 *   })
 *   .on('skip', function (landmark) {
 *     console.log('The user skipped to', landmark);
 *   })
 *
 * ```
 *
 * @api public
 * @name markup.landmarksModal
 * @param {Object} options The described options
 * @return {SkipModal}
 */
function landmarksModal(options) {
  options = deepExtend(landmarksModal.defaults, options || {});
  var modal, landmarks,
      roles = options.includedRoles,
      // everything with a `role`
      elements = selector.queryAll('[role]');

  // matching roles
  landmarks = array.filter(elements, function (element) {
    return array.inArray(roles, (element.getAttribute('role') || '').toLowerCase());
  });

  // all included headings
  elements = selector.queryAll(options.includedHeadings.join(','));
  // landmarks = headings + landmarks
  landmarks = elements.concat(landmarks);
  // create modal given the options and landmarks
  modal = new SkipModal(landmarks, options);

  // add skiplink when applicable
  if (options.skipLink) {
    landmarkSkipLink(options, modal);
  }

  return modal;
}

landmarksModal.defaults = {
  // standard landmark roles
  includedRoles: [
    'search', 'main', 'contentinfo', 'footer',
    'form', 'navigation', 'banner', 'complimentary'
  ],
  // all headings
  includedHeadings: [
    'h1', 'h2', 'h3', 'h4',
    'h5', 'h6'
  ],
  skipLink: {
    // omitting "entry" suggests the first child of the body
    text: 'Landmarks Modal',
    id: 'amaze-skip-to-landmarks',
    'class': 'landmarks-modal'
  },
  autoHide: true
};
