
'use strict';

//
// TODO
//
//   - overly complicated, extremely difficult to
//     understand and unmaintainable.  needs a refactor.
//   - test coverage is severaly lacking
//   - documentation is lacking
//   - duplicate logic everywhere
//

var selector = require('selector'),
    queryAll = selector.queryAll,
    query = selector.query,
    events = require('events'),
    on = events.on,
    delegate = events.delegate,
    fire = events.fire,
    collections = require('collections'),
    each = collections.each,
    extend = collections.extend,
    indexOf = require('array').indexOf,
    classList = require('dom').classList;


/**
 * Set `display: #{style}` on the given `element`
 *
 * @api private
 * @param {HTMLElement} element
 * @param {String} style
 */
function display(element, style) {
  if (element && element.style) {
    element.style.display = style;
  }
}

/**
 * markup relationship between tab and tab panel
 *
 * @api private
 * @param {HTMLNode} node                   the DOM element that is the tab
 * @param {String}   ownsSelector           the selector string to use to find the tab panel for this tab
 * @param {Object}   options                the options
 * @param {Function} panelKeyBoardHandler   the callback for the panel keyboard events
 * @param {Number}   ourIndex               which tab are we in the list of tabs (0-indexed)
 */
function handleOwnsRelationship(node, ownsSelector, options, panelKeyBoardHandler, ourIndex) {
  //jshint maxstatements: 23
  var context, tabPanel;

  if (typeof options.indexGetter === 'function') {
    ourIndex = options.indexGetter(node);
  }

  if (ownsSelector.indexOf('{index}') !== -1) {
    ownsSelector = ownsSelector.replace('{index}', ourIndex);
  }

  context = node;
  if (options.preselection) {
    context = query(options.preselection);
  }

  tabPanel = queryAll(ownsSelector, context)[0];
  tabPanel.setAttribute('role', 'tabpanel');
  if (tabPanel.id) {
    node.setAttribute('aria-owns', tabPanel.id);
    on(tabPanel, 'keydown', panelKeyBoardHandler);
  }

  if (ourIndex === 0) {
    // the first tab should be selected
    node.setAttribute('aria-selected', true);
    classList(node).add(options.selectedClass);
    display(tabPanel, 'block');
  } else {
    // hide all others
    node.setAttribute('aria-selected', false);
    classList(node).remove(options.selectedClass);
    display(tabPanel, 'none');
  }
}

/**
 * Actual implementation function to markup a single container of tabs using the supplied options
 *
 * @api private
 * @param {HTMLNode} container  the DOM element that is the parent container of the tabs
 * @param {Object}   options    the options
 */

function markupTabs(container, options) {
  // jshint maxstatements: 17
  var children, anchors;

  function changeActiveTabTo(me) {
    each(children, function (value) {
      var ariaSelected = value.getAttribute('aria-selected');

      if (ariaSelected !== 'false') {
        value.setAttribute('aria-selected', 'false');
        classList(value).remove(options.selectedClass); // remove selectedClass

        tabHide(value); // hide the inactive panels
      }
    });
    me.setAttribute('aria-selected', true);
    classList(me).add(options.selectedClass); // add selectedClass

    tabDisplay(me); // configure panel display
  }


  /**
   * Get the *panel* associated with the given
   * `tab` via its `aria-owns` attribute
   *
   * TODO:
   *
   *   create a `map` for caching these lookups
   *
   * @api private
   * @param {HTMLElement} tab
   * @return {HTMLElement}
   */
  function getPanel(tab) {
    return selector.query('#' + tab.getAttribute('aria-owns'));
  }

  /**
   * Get the *tab* associated with the given `panel`
   *
   * TODO:
   *
   *   create a `map` for caching these lookups
   *
   * @api private
   * @param {HTMLElement} panel
   * @return {HTMLElement}
   */
  function getTab(panel) {
    var index, length, tab,
        id = panel.id;

    for (index = 0, length = children.length; index < length; index++) {
      tab = children[index];
      if (tab.getAttribute('aria-owns') === id) {
        return tab;
      }
    }
    return null;
  }

  function panelKeyBoardHandler(keydownEvent) {
    //jshint maxcomplexity:20
    var keyCode = keydownEvent.which,
        stop = false,
        ourTab, ourIndex;

    if (!keydownEvent.ctrlKey || keydownEvent.shiftKey || keydownEvent.altKey || keydownEvent.metaKey) {
      return;
    }

    switch (keyCode) {
    case 37: // CTRL-LEFT
    case 38: // CTRL-UP
      stop = true;
      ourTab = getTab(keydownEvent.target);
      ourTab.focus();
      break;
    case 33: // CTRL-page up
      stop = true;
      ourTab = getTab(keydownEvent.target);
      ourIndex = indexOf(children, ourTab) - 1;
      if (ourIndex < 0) {
        ourIndex = children.length - 1;
      }
      children[ourIndex].focus();
      break;
    case 34: // CTRL-page down
      stop = true;
      ourTab = getTab(keydownEvent.target);
      ourIndex = indexOf(children, ourTab) + 1;
      if (ourIndex >= children.length) {
        ourIndex = 0;
      }
      children[ourIndex].focus();
      break;
    default:
      return;
    }

    keydownEvent.stopPropagation();
    keydownEvent.preventDefault();
  }

  function tabClickHandler(e) {
    changeActiveTabTo(e.target);
    if (typeof options.callback === 'function') {
      options.callback(e.target);
    }
    e.stopPropagation();
    e.preventDefault();
  }

  function tabKeyBoardHandler(e) {
    //jshint maxcomplexity:29
    var keyCode = e.which,
      stop = false,
      ourIndex = indexOf(children, e.target),
      target;

    switch (keyCode) {
    case 13: // ENTER
    case 32: // SPACE

      if (e.ctrlKey || e.shiftKey || e.altKey || e.metaKey) {
        return;
      }
      stop = true;
      if (options.eventSelector) {
        target = queryAll(options.eventSelector, e.target)[0];
      } else {
        target = e.target;
      }
      if (target) {
        fire(target, 'click');
      }
      // this is a bit redundant but leaving here in case under some circumstances click doesn't fire
      changeActiveTabTo(e.target);
      break;
    case 37: // LEFT
    case 38: // UP
      if (e.ctrlKey || e.shiftKey || e.altKey || e.metaKey) {
        return;
      }
      ourIndex -= 1;
      if (ourIndex < 0) {
        ourIndex = children.length - 1;
      }

      e.target.tabIndex = -1;
      children[ourIndex].tabIndex = 0;
      children[ourIndex].focus();
      stop = true;
      break;
    case 27: // ESC
      if (e.ctrlKey || e.shiftKey || e.altKey || e.metaKey) {
        return;
      }
      stop = true;
      break;
    case 40: // DOWN
    case 39: // RIGHT
      if (e.ctrlKey || e.shiftKey || e.altKey || e.metaKey) {
        return;
      }
      ourIndex += 1;
      if (ourIndex >= children.length) {
        ourIndex = 0;
      }
      e.target.tabIndex = -1;
      children[ourIndex].tabIndex = 0;
      children[ourIndex].focus();
      stop = true;
      break;
    }
    if (stop) {
      e.stopPropagation();
      e.preventDefault();
    }
  }

  // first make all anchors within the container non-tab focussable so tab through works
  anchors = queryAll('a', container);
  each(anchors, function (value) {
    value.tabIndex = -1;
  });

  // set role on the container
  container.setAttribute('role', 'tablist');

  children = queryAll(options.childSelector, container);

  each(children, function (value, index) {
    var classes = classList(value),
        stepChild = query(options.eventSelector, value);

    if (options.ownsSelector) {
      handleOwnsRelationship(value, options.ownsSelector, options, panelKeyBoardHandler, index, container);
    }
    value.tabIndex = -1; // make focussable by JavaScript
    if (classes.has(options.selectedClass)) {
      value.setAttribute('aria-selected', true);
    } else {
      value.setAttribute('aria-selected', false);
    }
    value.setAttribute('role', 'tab');

    if (stepChild) {
      events.on(stepChild, 'click', function (clickEvent) {
        events.fire(value, 'click');
        clickEvent.preventDefault();
      });
    }
  });

  delegate(container, options.childSelector, 'keydown', tabKeyBoardHandler);
  delegate(container, options.childSelector, options.event, tabClickHandler);
  children[0].tabIndex = 0;


  //ACTIVE TABS ON ENTER SHOULD DISPLAY, OTHERWISE DISPLAY NONE ON THEM (if hideTabs is true)

  /**
   * Show the panel associated with the given `tab`
   *
   * @api private
   * @param {HTMLElement} tab
   */
  function tabDisplay(tab) {
    display(getPanel(tab), 'block');
  }

  /**
   * Hide the panel associated with the given `tab`
   *
   * @api private
   * @param {HTMLElement} tab
   */
  function tabHide(tab) {
    display(getPanel(tab), 'none');
  }

}

/**
 * function to add accessibile keyboard handling and markup to a tab list that is usable only with a mouse
 *
 * - `childSelector` _String_ the selector to be used to find the individual tab elements within the container.
 *    Default is '>li'
 * - `eventSelector` _String_ the selector used to find the element within the tab element which is to be
 *    the target of the click event that is fired when the keyboard user presses ENTER or SPACE. To send the
 *    to the tab element itself, set this to `null` or `undefined`. Default is '>a'
 * - `selectedClass` _String_ the class to be used on initialization to determine which tab is the currently
 *    selected tab. Default is 'selected'
 * - `ownsSelector` _String_ the selector to use to find the tab panel associated with a tab. If the tabs are
 *    static tabs (i.e. they cause full page refreshes), setting this to `null` or `undefined` will disable the
 *    tab panel functionality. This is necessary because when enabled, the implementation expects to be able to
 *    find a tab panel for every tab and will throw an exception if this condition does not hold true. Default
 *    value is 'tabpanel{index}'. If present in the value, the `{index}` string will be replaced with the index
 *    of the tab (0-indexed) before being evaluated. See the `indexGetter` option for ways to influence the value
 *    of the index. Prior to evaluating the resulting `ownsSelector` selector, the implementation will evaluate the
 *    `preselection` option (see below) to manipulate the evaluation context of the selector.
 * - `preselection` _String_ the selector that is evaluated prior to evaluating the `ownsSelector` selector. If a value
 *    of `undefined` or `null` is provided, this will result in the `ownsSelector` being evaluated in the context of
 *    the tab element itself. The `preselection` selector is always evaluated in the context of the whole `document`.
 *    The default value is 'body'
 * - `indexGetter` _Function_ a function that is executed to determine the value to be applied to the `{index}`
 *    substring in the `ownsSelector` to be able to locate the tab panel for the current tab. It is provided a single
 *    argument which is the tab HTMLNode itself.
 * -  `hidePanels` _Boolean_ if set to false, the first panel will be displayed by default.  If it is set to true,
 *     all panels will be concealed
 *
 *
 *```javascript
 * // This example works for static tabs by setting the ownsSeelctor to null
 * // It also shows how to change selectedClass to ensure that the starting state is set correctly for the current tab
 *
 * var tabs = require( 'lib/markup/ariaTabs'),
 * tabs('#topnav>ul', {
 *   selectedClass : 'active',
 *   ownsSelector : null
 * });
 *```
 *
 *
 *
 *```javascript
 * // This example works for dynamic tabs that have no anchor tag inside the list items. This is done by setting
 * // eventSelector to null
 * // It also shows how to use the indexGetter callback and a custom ownsSelector to extract the href value as the
 * // selector to find the tab panelKeyBoardHandler
 *
 * var tabs = require( 'lib/markup/ariaTabs'),
 * tabs('#topnav>ul', {
 *   ownsSelector : '{index}',
 *   eventSelector : null,
 *   indexGetter : function (tab) {
 *     var href = tab.getAttribute('href'); // do not want the expanded URL, do not use tab.href
 *     return href;
 *   });
 *```
 *
 * @api public
 * @name markup.ariaTabs
 * @param {String}    container  the selector for the container DOM elements that conatins the tabs
 * @param {Object}    options    the options used to initialize the a11yfied tabs
 */
module.exports = function (containers, options) {
  var defaultOptions = {
    childSelector: '>li',
    eventSelector: '>a',
    selectedClass: 'selected',
    ownsSelector: '#tabpanel{index}',
    preselection: 'body',
    hidePanels: true,
    callback: function () {},
    event: 'click'
  },
  elements = queryAll(containers);

  if (options) {
    extend(defaultOptions, options);
  }

  each(elements, function (value) {
    markupTabs(value, defaultOptions);
  });
};
