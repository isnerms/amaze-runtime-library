'use strict';

var selector = require('selector'),
    dom = require('dom'),
    isFocusable = dom.isFocusable,
    events = require('events');

var noop = function () {};

/**
 * Make a modal dialog keyboard operable. fixModal triggers off
 * the click event on the launcher element. When the modal launchers
 * the modal container will receive focus so that reading order of
 * the modal content is maintained. After an initial focus change,
 * the focus will subsequently rotate through the tab focusable
 * elements in the modal. If no tab focusable elements exist, focus
 * will be maintained on the modal container itself (unless moved
 * using an AT's document navigation functionality) if focus stays
 * in the modal, it will be dismissable either through the closer
 * element or by pressing the ESC key. If the close element is not
 * keyboard navigable it will get a tabindex of 0.
 *
 * **Options object**:
 *
 * - `modalSelector` _String_ the selector with which the modal
 *    container can be located once the launch event triggers.  This
 *    selector is global to the document
 * - `closer` _String|HTMLElement_ the element or the selector that
 *    identifies the button that closes the modal. The selector is
 *    evaluated within the scope of the `modalSelector`'s result
 * - `[callback]` _Function_ **Optional** a callback that is called
 *    when the modal is found, that can be used to sanitize the modal
 *    it receives one parameter - the `HTMLElement` representing the
 *    result of the `modalSelector`
 * - `[delay]` _Integer_ **Optional** the delay in milliseconds from
 *    when the launch event is triggered before the rest of the logic
 *    is exected. This is to handle animation and delayed or asynchronous
 *    drawing of the modal. The default is to execute the logic
 *    immediately without yielding.
 * - `[openEvent]` _String_ **Optional** the event that causes the
 *    launcher to open the dialog. Defaults to 'click'. If this is set
 *    to a keyboard event, then only SPACE and ENTER will trigger the
 *    event.  They will also only trigger when no modifier key is
 *    pressed simultaneusly.
 * - `[closeEvent]` _String_ **Optional** the event that causes the
 *    closer to close the dialog. Defaults to 'click'.
 *
 * @api public
 * @name markup.fixModal
 * @param {HTMLElement} launcher the element which, when clicked, will trigger the modal to be displayed
 * @param {Object} options the options for the modal fixer
 */
module.exports = function (launcher, options) {
  var modalSelector = options.modalSelector,
    closer = options.closer,
    callback = options.callback,
    openEvent = options.openEvent || 'click',
    closeEvent = options.closeEvent || 'click',
    delay = options.delay,
    clickHandler;

  clickHandler = function () {
    // jshint maxstatements:22

    var FOCUSSABLESELECTOR = 'a, input, [tabindex], select, button, textarea, area',
      modal = selector.query(modalSelector),
      focusable, first, last, close, firstIndex, lastIndex;

    if (!modal) {
      throw new Error('Could not find a modal for selector "' + modalSelector + '"');
    }

    focusable = selector.queryAll(FOCUSSABLESELECTOR, modal);

    if (typeof closer === 'string') {
      close = selector.query(closer, modal);
    } else {
      close = closer;
    }

    function getFirstAndLast() {
      //jshint maxstatements: 20
      if (!focusable || focusable.length === 0) {
        first = last = modal;
      } else {
        firstIndex = 0;
        lastIndex = focusable.length - 1;
        first = focusable[firstIndex];
        while (firstIndex <= lastIndex - 1) {
          first = focusable[firstIndex];
          if (isFocusable(first) && first.getAttribute('tabindex') !== '-1') {
            break;
          }
          firstIndex += 1;
        }
        last = focusable[lastIndex];
        while (lastIndex >= firstIndex + 1) {
          last = focusable[lastIndex];
          if (isFocusable(last) && last.getAttribute('tabindex') !== '-1') {
            break;
          }
          lastIndex -= 1;
        }
      }
    }

    if (!modal.getAttribute('tabindex')) {
      modal.tabIndex = -1;
      events.on(modal, 'blur', function () {
        modal.removeAttribute('tabindex');
      });
    }
    callback(launcher, modal, close);

    events.off(modal, 'keydown');

    events.once(close, closeEvent, function () {
      launcher.focus();
    });
    if (close && close.nodeName !== 'A' && close.nodeName !== 'INPUT' &&
      close.nodeName !== 'BUTTON' && !close.getAttribute('tabindex')) {
      close.tabIndex = 0;
    }

    events.on(modal, 'keydown', function (e) {
      //jshint maxstatements: 18, maxcomplexity: 8
      var target = e.target,
        which = e.which;

      if (focusable.length < 2) {
        focusable = selector.queryAll(FOCUSSABLESELECTOR, modal);
      }
      getFirstAndLast();
      if (which === 9) {
        if (e.shiftKey && (target === first || target === modal)) {
          last.focus();
          e.preventDefault();
          e.stopPropagation();
        } else if (!e.shiftKey && (target === last || target === modal)) {
          first.focus();
          e.preventDefault();
          e.stopPropagation();
        }
      } else if (which === 27) {
        close.click();
        launcher.focus();
        e.preventDefault();
        e.stopPropagation();
      }
    });

    // Set the focus to the first focussable element in the modal
    // This is required in case the modal is disjoint from the trigger
    getFirstAndLast();
    modal.focus();
  };
  callback = callback || noop;

  events.on(launcher, openEvent, function (e) {
    // jshint maxcomplexity: 8
    var keyCode;
    if (openEvent.indexOf('key') !== -1) {
      // need to make sure this is the ENTER or the SPACE key only
      keyCode = e.which || e.keyCode;
      if (e.ctrlKey || e.shiftKey || e.altKey) {
        return;
      }
      if (keyCode !== 13 && keyCode !== 32) {
        return;
      }
    }
    if (delay) {
      setTimeout(clickHandler, delay);
    } else {
      clickHandler();
    }
  });
};
