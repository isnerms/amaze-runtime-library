'use strict';
/**
 * Make all elements in the list aria buttons by adding:
 * role="button"
 * event handlers for keyCodes 13 and 32 (Enter and Spacebar)
 * excludes input w/ type='button' && button tags
 *
 *
 * Examples of Usage:
 *
 * ```js
 * amaze('a.blue.button').button();
 *
 * var button = require('lib/markup').button;
 * button('a.blue.button');
 *
 * var button = require('lib/markup').button;
 * var elems = [
 *   document.getElementById('first_name'),
 *   selector.query('a.submit')
 * ];
 * button(elems);
 *
 * var button = require('lib/markup').button;
 * button(document.getElementById('searchFormSubmit'));
 *
 * var buttons = [];
 * buttons.push(selector.query('a.submit'));
 * buttons.push(document.getElementById('first_name'));
 * var blueBtns = selector('a.blue.button');
 * if (blueBtns.length) {
 *   buttons.concat(blueBtns);
 * }
 * require('lib/markup').button(buttons);
 * ```
 *
 * @api public
 * @name markup.button
 * @param  {Mixed} elements - the collection of elements that will be made a button
 */

var each = require('collections').each;
var array = require('array');
var events = require('events');
var selector = require('selector');
var dom = require('dom');

module.exports = function button(elements) {

  if (elements === undefined) {
    throw new TypeError('elements is undefined');
  }

  if (typeof elements === 'string') {
    elements = selector.queryAll(elements);
  }

  if (elements.length && typeof elements === 'object' && !array.isArray(elements)) {
    elements = array.toArray(elements);
  }

  if (array.isArray(elements)) {
    each(elements, function (element) {
      makeButton(element);
    });
  } else {
    // elements must be a single node in this case
    makeButton(elements);
  }

};

/**
 * Add the role and keyboard event listener(s)
 * If it is a <button> or <input type="button"> It is not applicable.
 * If it is a <a> w/o href, event listeners and tabIndex are added.
 * If it is a <a> w/ href, only spacebar support is added.  It already has Enter key support and is inside the tabindex.
 * If it is any other element, event listeners and tabIndex are added.
 * The role="button" is added to all elements that are applicable.
 *
 * @api private
 * @param {Object} elem
 */
function makeButton(elem) {
  if (!elem) {
    throw new TypeError('elem is undefined');
  }

  var tagName = elem.tagName;

  if (tagName === 'BUTTON' || (tagName === 'INPUT' && elem.type === 'button')) {
    // just skip these because they do not need any treatment
    return;
  }

  var isAnchor = tagName === 'A';
  var isLink = isAnchor && elem.hasAttribute('href');
  var hasTabIndex = elem.hasAttribute('tabindex');

  if (!isLink && !hasTabIndex) {
    elem.tabIndex = 0;
  }

  elem.setAttribute('role', 'button');
  dom.addClass(elem, 'amaze-button');

  events.on(elem, 'keydown', function (evt) {

    var shouldClick = evt.which === 32 || evt.which === 13;
    var skipClick = isLink && evt.which === 13;
    if (shouldClick  && !skipClick) {
      elem.click();
    }

  });

}
