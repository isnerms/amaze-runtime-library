
'use strict';

exports.addLabel = require('./add-label');

exports.ariaTabs = require('./aria-tabs');

exports.dataTable = require('./data-table');

exports.documentLanguage = require('./document-language');

exports.fixModal = require('./fix-modal');

exports.fixPage = require('./fix-page');

exports.landmarksModal = require('./landmarks-modal');

exports.pageTitle = require('./page-title');

exports.presentation = require('./presentation');

exports.skipLink = require('./skip-link');

exports.SkipModal = require('./skip-modal');

exports.tooltip = require('./tooltip');

exports.button = require('./button');
