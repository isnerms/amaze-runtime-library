
'use strict';

//
// TODO
//
//  - this thing is huge.  break it up
//

var dom = require('dom'),
		collections = require('collections'),
		each = collections.each,
		extend = collections.extend,
		selector = require('selector'),
		events = require('events'),
		array = require('array'),
		css = require('css');

// publish api
module.exports = SkipModal;

////////////////////////////
// Private helper methods //
////////////////////////////

function noop() {}

function createElement(type) {
	return document.createElement(type);
}

/**
 * Simple text truncation
 *
 * #### Example:
 *
 *     truncate('1234567890', 4);
 *     //=> "1234..."
 *
 * @api private
 * @param {String} text
 * @param {Number} max
 * @return {String}
 */
function truncate(text, max) {
	text = text || '';
	if (max < text.length) {
		return text.substring(0, max - 3) + '...';
	}
	return text;
}

/**
 * Setup an element for the skip modal.  Will ensure the
 * element has an ID and can be focused.
 *
 * @api private
 * @param {HTMLElement} element
 * @return {HTMLElement}
 */
function prepareElement(element) {
	var id = element.id || 'amaze-' + Math.floor(Math.random() * 0x0deadbeef);

	element.id = id;
	if (!dom.isFocusable(element)) {
		element.tabIndex = -1;
	}

	return element;
}

/**
 * Create an `a` pointing to the given
 * `element` which is wrapped in an `li`
 *
 * @api private
 * @param {SkipModal} modal
 * @param {HTMLElement} element
 * @return {HTMLElement}
 */
function _skipLink(modal, element) {
	var li = createElement('li'),
		a = createElement('a');

	prepareElement(element);

	a.className = 'amaze-skip-to-element-link';
	a.href = '#' + element.id;
	a.setAttribute('data-skip-to', '#' + element.id);
	a.innerHTML = truncate(dom.getText(element),
		modal.get('maxTextLength'));

	li.className = 'item';
	li.appendChild(a);

	return li;
}

/**
 * Turn the provided `elements` into an array
 *
 * @api private
 * @param {String|Array|NodeList|...} elements
 * @return {Array}
 */
function castElements(elements) {
	if (!elements) {
		// nothing provided?  start with an empty array
		elements = [];
	} else if (typeof elements === 'string') {
		// provided a selector?
		elements = selector.queryAll(elements);
	} else if (!array.isArray(elements)) {
		// force NodeLists and stuff into Arrays
		elements = array.toArray(elements);
	}

	return elements;
}

/**
 * Add the modal styles to the page.
 *
 * Will not add the same styles twice.
 *
 * @api private
 * @param {Object} options Modal options
 */
function addStyles(options) {
	var stylesheet,
		id = 'amaze-skipmodal-' + options.id + '-' + options.openClass;

	// ensure we're not adding the same styles twice
	if (document.getElementById(id)) {
		return;
	}

	stylesheet = css(defaultCSS
		// #id
		.replace(/\$\{id\}/g, options.id)
		// .open
		.replace(/\$\{open\}/g, options.openClass)
		// uneeded whitespace
		.replace(/\s+/g, ' '));

	// save the id we don't add it again
	stylesheet.id = id;
}

// default css
var defaultCSS = [
	'#${id} {',
	'  margin: 0 auto;',
	'  border: 3px solid #fff;',
	'  border-radius: 12px;',
	'  background-color: #000;',
	'  width: 330px;',
	'  color: #fff;',
	'  padding: 12px;',
	'  -moz-box-shadow: 3px 3px 5px 6px #ccc;',
	'  -webkit-box-shadow: 3px 3px 5px 6px #ccc;',
	'  box-shadow: 3px 3px 5px 6px #ccc;',
	'  display: none;',
	'  font-family: "Lucida Grande", "Lucida Sans Unicode", ',
	'   Helvetica, Arial, Verdana, sans-serif',
	'}',
	'  #${id}.${open} {',
	'    display: block;',
	'  }',
	'  #${id} h2 {',
	'    font-size: 18px;',
	'    text-align: center;',
	'  }',
	'  #${id} ul {',
	'    margin: 0;',
	'    margin-left: 12px;',
	'    padding: 0;',
	'    font-size: 16px;',
	'  }',
	'    #${id} ul li {',
	'      list-style: none;',
	'      padding: 0;',
	'      margin: 0;',
	'      margin-bottom: 6px;',
	'    }',
	'      #${id} ul a.amaze-skip-to-element-link {',
	'        color: #fff;',
	'      }'
].join('');

/**
 * The `SkipModal` class
 *
 * #### Events
 *
 * ##### escape
 *
 * The user pressed `ESC` while the modal was in focus.  Callbacks
 * are provided the instance of the modal.
 *
 * ```js
 * modal.on('escapse', function (_modal) {
 *   assert.equal(_modal, modal);
 * });
 * ```
 *
 * ##### skip
 *
 * The user "skipped" to an element by clicking one if the modal's
 * links.  Callbacks are provided the element the user has
 * skipped to.
 *
 * ```js
 * modal.on('skip', function (element) {
 *   assert.equal(document.activeElement, element);
 * });
 * ```
 *
 * ##### close
 *
 * The modal has been closed (is hidden).  Callbacks are provided
 * the instance of the model.
 *
 * ```js
 * modal.on('close', function (_modal) {
 *   assert.equal(_modal, modal);
 * });
 * ```
 *
 * ##### destroyed
 *
 * The modal has been completely removed from the DOM and can no
 * longer be used.  Callbacks are provided nothing.
 *
 * ```js
 * modal.on('destroyed', function () {
 *   alert('no more modal!');
 * });
 * ```
 *
 * ##### open
 *
 * The modal has been opened (is visible).  Callbacks are provided
 * the instance of the model.
 *
 * ```js
 * modal.on('close', function (_modal) {
 *   assert.equal(_modal, modal);
 * });
 * ```
 *
 * @api public
 * @name markup.SkipModal
 */
function SkipModal(elements, options) {
	// jshint maxstatements:16
	var container = this.container = createElement('div');

	// allow instantiation without `new`
	if (!(this instanceof SkipModal)) {
		return new SkipModal(elements, options);
	}

	// emit/on
	this._listeners = {};

	// get options
	this.options = extend(SkipModal.defaults, options || {});

	// don't re-create the same modal
	if (document.getElementById(this.options.id)) {
		throw new Error('An element matching "#' + this.options.id + '"' +
			' already exists.  Use `SkipModal#add` / `SkipModal#remove` to edit' +
			' its links.');
	}

	// add styles when necessary
	if (this.get('addStyles')) {
		addStyles(this.options);
	}

	if (this.get('accessKey')) {
		container.setAttribute('accesskey', this.get('accessKey'));
	}

	this.elements = castElements(elements);

	container.id = this.options.id;
	// add modal as the first child of the body
	document.body.insertBefore(container, document.body.firstChild);

	if (!this.options.autoHide) {
		this._render().show();
	}
}

/**
 * Create the `title` of a `SkipModal`
 *
 * @api private
 * @param {Object} options
 * @return {HTMLElement}
 */
function _makeSkipModalTitle(options) {
	var h2 = document.createElement('h2');
	h2.tabIndex = -1;
	h2.appendChild(document.createTextNode(options.title));
	return h2;
}

/**
 * Render the `modal`
 *
 * @api private
 * @return {SkipModal}
 */
SkipModal.prototype._render = function () {
	var links, first, last,
		self = this,
		title = this._titleBar = _makeSkipModalTitle(this.options),
		content = createElement('ul');

	each(this.elements, function (element) {
		if (dom.isNode(element) && element.parentNode) {
			content.appendChild(_skipLink(self, element));
		}
	});

	// get all links contained in the modal
	links = selector.queryAll('a.amaze-skip-to-element-link', content);
	// first & last are used for tabbing
	first = links[0];
	last = links[links.length - 1];

	dom.empty(this.container);
	this.container.appendChild(title);
	this.container.appendChild(content);

	/**
	 * Get a `link`'s skip-to `target`
	 *
	 * @api private
	 * @param {HTMLElement} link
	 * @return {HTMLElement|null}
	 */
	function getTarget(link) {
		var target = link.getAttribute('data-skip-to');
		if (!target) {
			return null;
		}
		target = selector.query(target);
		return target;
	}

	// capture all clicks and focus their target
	events.delegate(content, 'a.amaze-skip-to-element-link', 'click', function (clickEvent) {
		var target = getTarget(clickEvent.target);
		// don't throw on broken links
		if (!target || !target.focus) {
			return;
		}

		target.focus();
		clickEvent.preventDefault();

		if (self.get('hideOnSkip')) {
			self.hide();
		}

		self.emit('skip', target);
	});

	// remove old keyboard handler
	events.off(this.container, 'keydown');

	// keyboard handler:
	//  - pressing `ESC` (assuming `options.escape==true`) will close the modal
	//  - users may not tab out of the modal (given `trapFocus==true`)
	events.on(this.container, 'keydown', function (keyboardEvent) {

		var element = keyboardEvent.target,
			which = keyboardEvent.which;

		// user presses `ESC` within the modal
		// will close the modal if `options.escape == true`
		if (which === 27 && self.get('escape')) {
			self.emit('escape', self);
			self.hide();
		}

		// handle tabbing
		if (which === 9 && self.get('trapFocus')) {
			if (keyboardEvent.shiftKey) {
				// first element or title
				if (element === first || element === title) {
					keyboardEvent.preventDefault();
					last.focus();
				}
			} else {
				if (element === last) {
					keyboardEvent.preventDefault();
					first.focus();
				}
			}
		}
	});

	// auto-focus the title
	title.focus();

	return this;
};

/**
 * Add an element to the modal
 *
 * Will not add a duplicate element unless
 * `options.allowDuplicates` is `true`.
 *
 * TODO:
 *
 * - support adding an array of elements
 * - support adding element by selector
 *
 * #### Example:
 *
 * ```js
 * var modal = new SkipModal(…);
 *
 * modal
 *   // add #foo
 *   .add(document.getElementById('foo'))
 *   // add #bar
 *   .add(document.getElementById('bar'))
 *   // show/render the modal
 *   .show();
 * ```
 *
 * @api public
 * @param {HTMLElement} element
 * @return {SkipModal}
 */
SkipModal.prototype.add = function (element) {
	if (!this.get('allowDuplicates') && array.inArray(this.elements, element)) {
		return this;
	}

	this.elements.push(element);
	return this;
};

/**
 * Remove an element from the modal
 *
 * #### Example:
 *
 * ```js
 * var modal = new SkipModal(…);
 *
 * modal
 *   // remove #foo
 *   .remove(document.getElementById('foo'))
 *   // remove the 3rd element
 *   .remove(3)
 *   // show/render the modal
 *   .show();
 * ```
 *
 * @api public
 * @param {HTMLElement|Number} index The element, or the element's index
 * @return {SkipModal}
 */
SkipModal.prototype.remove = function (index) {
	// HTMLElement
	if (typeof index === 'object') {
		index = array.indexOf(this.elements, index);
	}

	this.elements.splice(index, 1);
	return this;
};

/**
 * Remove all elements from the modal
 *
 * #### Example:
 *
 * ```js
 * var modal = new SkipModal(…);
 *
 * // using callback
 * modal
 *   // remove all
 *   .empty(function (modal) {
 *     // render/show the modal
 *     modal.show();
 *   });
 *
 * // using callback
 * modal
 *   // remove all
 *   .empty()
 *   // display
 *   .show();
 * ```
 *
 * @api public
 * @param {Function} [cb]
 * @return {SkipModal}
 */
SkipModal.prototype.empty = function (cb) {
	cb = cb || noop;
	this.elements = [];
	cb(this);
	return this;
};

/**
 * Replace an existing element with another
 *
 * #### Example:
 *
 * ```js
 * var modal = new SkipModal(…);
 *
 * modal
 *   // replace #foo with #bar
 *   .replace(document.getElementById('foo'), document.getElementById('bar'))
 *   // show/render the modal
 *   .show();
 * ```
 *
 * @api public
 * @param {HTMLElement|Number} previous The element to replace
 * @param {HTMLElement} element The element to add
 * @return {SkipModal}
 */
SkipModal.prototype.replace = function (previous, element) {
	// HTMLElement
	if (typeof previous === 'object') {
		previous = array.indexOf(this.elements, previous);
	}

	this.elements.splice(previous, 1, element);
	return this;
};

/**
 * Set an option
 *
 * #### Example:
 *
 * ```js
 * var modal = new SkipModal(…);
 *
 * modal
 *   // stop trapping focus
 *   .set('trapFocus', false)
 *   // show/render the modal
 *   .show();
 * ```
 *
 * @api public
 * @param {String} key
 * @param {Mixed|Any} value
 */
SkipModal.prototype.set = function (key, value) {
	this.options[key] = value;
	return this;
};

/**
 * Get an option
 *
 * #### Example:
 *
 * ```js
 * var modal = new SkipModal(…);
 * var isTrappingFocus = modal.get('trapFocus');
 * ```
 *
 * @api public
 * @param {String} key
 * @return {Mixed|Any}
 */
SkipModal.prototype.get = function (key) {
	return this.options[key];
};

/**
 * Check if the modal is currently open
 *
 * #### Example:
 *
 * ```js
 * var modal = new SkipModal(…);
 * if (modal.isOpen()) {
 *   // the modal is open
 * } else {
 *   // the modal is closed
 * }
 * ```
 *
 * @api public
 * @return {Boolean}
 */
SkipModal.prototype.isOpen = function () {
	return dom
		.classList(this.container)
		.has(this.get('openClass'));
};

/**
 * Display the modal.  Will emit an "_open_" event.
 *
 * #### Example:
 *
 * ```js
 *
 * var modal = new SkipModal(…);
 *
 * modal.show();
 *
 * // with callback
 * modal.show(function () {
 *   // the modal is visible
 * });
 * ```
 *
 * @api public
 * @param {Function} [cb] Callback
 * @return {SkipModal}
 */
SkipModal.prototype.show = function (cb) {
	cb = cb || noop;

	var cl = dom.classList(this.container);
	cl.add(this.get('openClass'));
	this._render();
	this.emit('open', this);
	cb(this);
	return this;
};

/**
 * Toggle the modal's visibility.  Will emit either
 * an "_open_" or "_close_" event, depending on the
 * modal's current state.
 *
 * #### Example:
 *
 * ```js
 *
 * var modal = new SkipModal(…);
 *
 * modal
 *   // hide the modal
 *   .hide()
 *   // show the modal
 *   .toggle();
 *
 * modal
 *   // show the modal
 *   .show()
 *   .toggle(function () {
 *    // modal is now hidden
 *   });
 *
 * ```
 *
 * @api public
 * @param {Function} [cb] Callback
 * @return {SkipModal}
 */
SkipModal.prototype.toggle = function (cb) {
	return this[this.isOpen() ? 'hide' : 'show'](cb || noop);
};

/**
 * Hide the modal.  Will emit a "_close_" event.
 *
 * #### Example:
 *
 * ```js
 *
 * var modal = new SkipModal(…);
 *
 * modal.hide();
 *
 * // with callback
 * modal.hide(function () {
 *   // the modal is not visible
 * });
 * ```
 *
 * @api public
 * @param {Function} [cb] Callback
 * @return {SkipModal}
 */
SkipModal.prototype.hide = function (cb) {
	cb = cb || noop;

	var cl = dom.classList(this.container);
	cl.remove(this.get('openClass'));
	this.emit('close', this);
	cb(this);
	return this;
};

/**
 * Remove the modal entirely from the DOM
 *
 * @param {Function} [cb] Callback
 * @return {SkipModal}
 */
SkipModal.prototype.destroy = function (cb) {
	cb = cb || noop;

	document.body.removeChild(this.container);
	this.emit('destroyed');
	// remove all listeners (after emitting "destroyed")
	this._listeners = {};
	cb(this);
	return this;
};


/*!
 * TODO:
 *
 * - inherit from EventEmitter?
 *
 */

/**
 * Add a `listener` for an `event`
 *
 * @api public
 * @param {String} event
 * @param {Function} listener
 * @return {SkipModal}
 */
SkipModal.prototype.on = function (event, listener) {
	this._listeners[event] = this._listeners[event] || [];

	if (!array.inArray(listener, this._listeners[event])) {
		this._listeners[event].push(listener);
	}
	return this;
};

/**
 * `emit` one or more `events`, providing their listeners `data`
 *
 * @api private
 * @param {String|Array} events
 * @param {Mixed|Any} data
 * @return {SkipModal}
 */
SkipModal.prototype.emit = function (events, data) {
	var self = this;

	if (!array.isArray(events)) {
		events = events.split(',');
	}

	each(events, function (event) {
		event = event.replace(/\s/g, '');
		self._listeners[event] = self._listeners[event] || [];

		each(self._listeners[event], function (fn) {
			fn(data);
		});
	});

	return this;
};

/**
 * Default SkipModal options
 *
 * @api private
 * @type {Object}
 */
SkipModal.defaults = {
	id: 'amaze-navigation-modal',
	title: 'Navigation Modal',
	openClass: 'open',
	allowDuplicates: false,
	escape: true,
	hideOnSkip: true,
	autoHide: true,
	maxTextLength: 30,
	trapFocus: true,
	addStyles: true,
	accessKey: null
};
