
//jshint maxlen: 200
//@todo style generator
//most likely horribly broken in IE7; go away

'use strict';

var dom = require('dom'),
    array = require('array'),
    selector = require('selector'),
    events = require('events'),
    css = require('css'),
    extend = require('extend');

var numberOfTooltips = 0,
  TOOLTIP_CLASSNAME = 'amaze-tooltip',
  DEFAULT_BACKGROUND_COLOR = '#ffffca',
  DEFAULT_COLOR = '#000',
  DEFAULT_BORDER_COLOR = DEFAULT_COLOR,

  DEFAULT_TOOLTIP_STYLES = '.amaze-tooltip {' +
      'position: absolute;' +
      'left: -9999px;' +
      'top: -9999px;' +
      'width: 230px;' +
      'z-index: 999;' +
      'border: 1px solid ' + DEFAULT_BORDER_COLOR + ';' +
      'color: ' + DEFAULT_COLOR + ';' +
      'background: ' + DEFAULT_BACKGROUND_COLOR + ';' +
      'padding: 10px;' +
      'border-radius: 2px;' +
      'box-shadow: 0 1px 3px rgba(0, 0, 0, 0.45);' +
    '}' +

    '.amaze-tooltip:after, .amaze-tooltip:before {' +
      'height: 0;' +
      'width: 0;' +
      'content: " ";' +
      'position: absolute;' +
      'border: 5px solid transparent;' +
      'pointer-events: none;' +
      'overflow: hidden;' +
    '}' +

    // after === background, before === border
    '.amaze-tooltip.y:after, .amaze-tooltip.y:before {' +
      'left: 50%;' +
      'margin-left: -5px;' +
    '}' +

    '.amaze-tooltip.y.s:after {' +
      'top: -10px;' +
      'border-bottom-color: ' + DEFAULT_BACKGROUND_COLOR + ';' +
    '}' +

    '.amaze-tooltip.y.s:before {' +
      'border-bottom-color: ' + DEFAULT_BORDER_COLOR + ';' +
      'top: -11px;' +
    '}' +

    '.amaze-tooltip.y.n:after {' +
      'border-top-color: ' + DEFAULT_BACKGROUND_COLOR + ';' +
      'bottom: -10px;' +
    '}' +

    '.amaze-tooltip.y.n:before {' +
      'border-top-color: ' + DEFAULT_BORDER_COLOR + ';' +
      'bottom: -11px;' +
    '}' +

    '.amaze-tooltip.y.w:before, .amaze-tooltip.y.w:after {' +
      'left: auto;' +
      'margin-left: 0;' +
      'right: 5px;' +
    '}' +

    '.amaze-tooltip.y.e:before, .amaze-tooltip.y.e:after {' +
      'left: 5px;' +
      'margin-left: 0;' +
    '}' +

    /* x axis arrows */
    '.amaze-tooltip.x:after, .amaze-tooltip.x:before {' +
      'top: 50%;' +
      'margin-top: -5px;' +
    '}' +

    '.amaze-tooltip.x.w:after {' +
      'right: -10px;' +
      'border-left-color: ' + DEFAULT_BACKGROUND_COLOR + ';' +
    '}' +

    '.amaze-tooltip.x.w:before {' +
      'border-left-color: ' + DEFAULT_BORDER_COLOR + ';' +
      'right: -11px;' +
    '}' +

    '.amaze-tooltip.x.e:after {' +
      'border-right-color: ' + DEFAULT_BACKGROUND_COLOR + ';' +
      'left: -10px;' +
    '}' +

    '.amaze-tooltip.x.e:before {' +
      'border-right-color: ' + DEFAULT_BORDER_COLOR + ';' +
      'left: -11px;' +
    '}' +

    '.amaze-tooltip.x.n:before, .amaze-tooltip.x.n:after {' +
      'top: 5px;' +
      'margin-top: 0;' +
    '}' +

    '.amaze-tooltip.x.s:before, .amaze-tooltip.x.s:after {' +
      'top: auto;' +
      'margin-top: 0;' +
      'bottom: 5px;' +
    '}';

/**
 * Gets the width and height of the viewport; used to calculate the right and bottom boundaries of the viewable area.
 *
 * @api private
 * @return {Object}  Object with the `width` and `height` of the viewport
 */
function getViewportSize() {
  var body,
    docElement = document.documentElement;

  if (window.innerWidth) {
    return {
      width: window.innerWidth,
      height: window.innerHeight
    };
  }

  if (docElement) {
    return {
      width: docElement.clientWidth,
      height: docElement.clientHeight
    };

  }

  body = document.body;

  return {
    width: body.clientWidth,
    height: body.clientHeight
  };
}

/**
 * Removes all positioning classes then adds the split contents of `align` and `axis`.  Used to position the arrow via CSS.
 *
 * @api private
 * @param {HTMLElement} tooltip  The tooltip
 * @param {String}      align    Oridinal/cardinal direction of the tooltip (e.g. n, ne)
 * @param {String}      axis     Axis of the tooltip (e.g. x or y)
 */
function addTooltipClasses(tooltip, align, axis) {
  var index,
    length = align.length,
    classes = dom.classList(tooltip);

  classes
    .remove('n')
    .remove('e')
    .remove('s')
    .remove('w')
    .remove('x')
    .remove('y');

  if (axis) {
    classes.add(axis);
  }

  for (index = 0; index < length; index++) {
    classes.add(align.substr(index, 1));
  }

}


/**
 * Calculates positions on the X axis
 *
 * @api private
 * @param  {Object} targetBox  The target's boundingClientRect
 * @param  {String} tooltipBox The tooltip's boundingClientRect
 * @param  {String} align      The align to start with
 * @param  {Number} offset     Number of pixels to offset the tooltip by
 * @return {Object} The result, top & left
 */
function calculatePositionX(targetBox, tooltipBox, align, offset) {
  var dir, index, length,
    result = {};

  // e / w should go first
  align = align.split('').reverse();

  for (index = 0, length = align.length; index < length; index++) {
    dir = align[index];
    switch (dir) {
    case 'e':
      result.top = targetBox.top + (targetBox.height / 2) - (tooltipBox.height / 2);
      result.left = targetBox.right + offset;
      break;
    case 'w':
      result.top = targetBox.top + (targetBox.height / 2) - (tooltipBox.height / 2);
      result.left = targetBox.left - tooltipBox.width - offset;
      break;
    case 'n':
      result.top = targetBox.top;
      break;
    case 's':
      result.top = targetBox.bottom - tooltipBox.height;
      break;
    }
  }

  return result;
}

/**
 * Calculates positions on the Y axis
 *
 * @api private
 * @param  {Object} targetBox  The target's boundingClientRect
 * @param  {String} tooltipBox The tooltip's boundingClientRect
 * @param  {String} align      The align to start with
 * @param  {Number} offset     Number of pixels to offset the tooltip by
 * @return {Object} The result, top & left
 */
function calculatePositionY(targetBox, tooltipBox, align, offset) {
  var dir, index, length,
    result = {};

  align = align.split('');

  for (index = 0, length = align.length; index < length; index++) {
    dir = align[index];
    switch (dir) {
    case 'n':
      result.top = targetBox.top - tooltipBox.height - offset;
      result.left = targetBox.left + (targetBox.width / 2) - (tooltipBox.width / 2);
      break;
    case 's':
      result.top = targetBox.top + targetBox.height + offset;
      result.left = targetBox.left + (targetBox.width / 2) - (tooltipBox.width / 2);
      break;
    case 'e':
      result.left = targetBox.left;
      break;
    case 'w':
      result.left = targetBox.right - tooltipBox.width;
      break;
    }
  }

  return result;
}

/**
 * Router function. Calls `calculatePositionX` or `calculatePositionY` depending on the value of `axis`
 *
 * @api private
 * @param  {Object} boxes   Object of tooltip, target, scroll and viewport "boxes" or dimensions
 * @param  {String} axis    The axis to calculate positions of
 * @param  {String} align   The align to start with
 * @param  {Number} offset  Number of pixels to offset the tooltip by
 * @return {Object} The result
 */
function calculatePosition(boxes, axis, align, offset) {
  var result;

  if (axis === 'y') {
    result = calculatePositionY(boxes.target, boxes.tooltip, align, offset);
  } else {
    result = calculatePositionX(boxes.target, boxes.tooltip, align, offset);
  }
  result.axis = axis;
  result.align = align;

  return result;
}

/**
 * Determines if a `candidate` will fit on screen
 *
 * @api private
 * @param  {Object} candidate Object with top/left positions, calculated by `calculatePosition`
 * @param  {Object} boxes     Object of tooltip, target, scroll and viewport "boxes" or dimensions
 * @return {Boolean}          Does the candidate fit on screen
 */
function fits(candidate, boxes) {
  var tooltipBox = boxes.tooltip,
    scrollOffset = boxes.scroll,
    viewportSize = boxes.viewport,
    top = candidate.top,
    left = candidate.left;

  return top >= scrollOffset.y && // top
    top + tooltipBox.height <= scrollOffset.y + viewportSize.height && // bottom
    left >= scrollOffset.x && // left
    left + tooltipBox.width <= scrollOffset.x + viewportSize.width; // right
}

/**
 * Loops over possible positions until it finds one that will fit on screen.
 *
 * @api private
 * @param  {Object} boxes   Object of tooltip, target, scroll and viewport "boxes" or dimensions
 * @param  {String} axis    The axis to calculate positions of
 * @param  {String} align   The align to start with
 * @param  {Number} offset  Number of pixels to offset the tooltip by
 * @return {Object}         Object with top/left integers, axis and align.
 */
function checkPositions(boxes, axis, align, offset) {
  var count, candidate,
    dirs = {
      x: ['ne', 'e', 'se', 'nw', 'w', 'sw'],
      y: ['ne', 'n', 'nw', 'se', 's', 'sw']
    },
    dir = dirs[axis],
    index = array.indexOf(dir, align),
    length = dir.length;

  index = index === -1 ? 0 : index;

  for (count = 0; count < length; index++, count++) {
    if (index >= length) {
      index = 0;
    }
    align = dir[index];
    candidate = calculatePosition(boxes, axis, align, offset);
    if (fits(candidate, boxes)) {
      return candidate;
    }
  }

}

/**
 * Gets the position of the tooltip; will swap axis if it cannot fit on the requested axis
 *
 * @api private
 * @param  {HTMLElement} target  The target as specified by the constructor
 * @param  {HTMLElement} tooltip The tooltip created by `createTooltip`
 * @param  {Object} params       Additional parameters: axis, align, offset
 * @return {Object}              Object with top/left integers, axis and align.
 */
function getPosition(target, tooltip, params) {
  var candidate,
    boxes = {
      scroll: dom.getScrollOffset(),
      viewport: getViewportSize(),
      tooltip: dom.getElementCoordinates(tooltip),
      target: dom.getElementCoordinates(target)
    },
    axis = params.axis,
    align = params.align,
    offset = params.offset,
    defaultPosition = calculatePosition(boxes, axis, align, offset);

  if (!params.fit) {
    return defaultPosition;
  }

  candidate = checkPositions(boxes, axis, align, offset);

  if (candidate) {
    return candidate;
  }

  // swap axis
  axis = axis === 'x' ? 'y' : 'x';
  candidate = checkPositions(boxes, axis, null, offset);

  if (candidate) {
    return candidate;
  }

  return defaultPosition;

}

/**
 * Creates the tooltip HTMLElement and binds mouse events to it.
 *
 * @api private
 * @param  {Tooltip} tip [description]
 */
function createTooltip(tip) {
  if (tip.tooltip) {
    return;
  }

  var options = tip.options,
    tooltip = document.createElement('div');

  tooltip.className = TOOLTIP_CLASSNAME;
  if (options['class']) {
    tooltip.className += ' ' + options['class'];
  }

  tooltip.innerHTML = tip.content;
  tooltip.id = tip.id;

  document.body.appendChild(tooltip);

  tooltip.setAttribute('aria-hidden', 'true');

  if (options.mouseEvents) {
    events.on(tooltip, 'mouseout', function () {
      tip.mouseTimeoutID = setTimeout(function () {
        tip.hide();
      }, tip.options.delay);
    });

    events.on(tooltip, 'mouseover', function () {
      if (tip.mouseTimeoutID) {
        tip.mouseTimeoutID = clearTimeout(tip.mouseTimeoutID);
      }
    });
  }

  tip.tooltip = tooltip;
}

/**
 * Creates a tooltip object
 *
 * @api private
 * @param  {Mixed}  target    HTMLElement or selector of the target of the tooltip
 * @param  {String} content   The content of the tooltip
 * @param  {Object} [options] Options object
 */
function Tooltip(target, content, options) {
  var focusTimeoutID,
    self = this;

  this.tooltip = null;
  this.target = target;
  this.content = content;
  this.options = options;
  this.id = TOOLTIP_CLASSNAME + numberOfTooltips++;

  // @todo do not clobber
  target.setAttribute('aria-describedby', this.id);

  events.on(target, 'focus', function () {
    // allow the browser to scroll first
    focusTimeoutID = setTimeout(function () {
      self.show();
    }, 0);
  });

  events.on(target, 'blur', function () {
    self.hide();
    if (focusTimeoutID) {
      clearTimeout(focusTimeoutID);
      focusTimeoutID = null;
    }
  });

  if (options.mouseEvents) {
    events.on(target, 'mouseover', function () {
      self.show();
      if (self.mouseTimeoutID) {
        self.mouseTimeoutID = clearTimeout(self.mouseTimeoutID);
      }
    });

    events.on(target, 'mouseout', function () {
      self.mouseTimeoutID = setTimeout(function () {
        self.hide();
      }, options.delay);
    });
  }
}

/**
 * Displays the tooltip.
 *
 * @api private
 */
Tooltip.prototype.show = function () {
  if (!this.tooltip) {
    createTooltip(this);
  }

  var options = this.options;

  this.position(options.axis, options.align, options.fit);

};

/**
 * Hides the tooltip
 *
 * @api private
 */
Tooltip.prototype.hide = function () {
  if (document.activeElement === this.target) {
    return;
  }

  var tip = this.tooltip;

  if (tip) {
    if (this.options.mouseEvents) {
      events.off(tip, 'mouseover');
      events.off(tip, 'mouseout');
    }
    tip.parentNode.removeChild(tip);
  }

  this.tooltip = null;
};

/**
 * Positions and shows the tooltip.  If a parameter is not specified, or null, it will take the value specifed by the constructor.
 *
 * @api private
 * @param  {String}  [axis]  What direction the tooltip should be rendered on (`'x'` for horizontal, `'y'` for vertical).
 * @param  {String}  [align] Oridinal and/or cardinal direction the tooltip should default to (e.g. `'ne'`, `'s'`). Defaults to `'ne'`.
 * @param  {Boolean} [fit]   Whether to auto-fit the tooltip on-screen.
 */
Tooltip.prototype.position = function (axis, align, fit) {
  if (!this.tooltip) {
    createTooltip(this);
  }


  var position,
    params = {},
    tooltip = this.tooltip,
    tooltipStyle = tooltip.style,
    options = this.options;

  //jshint eqnull: true
  params.axis = axis != null ? axis: options.axis;
  params.align = align != null ? align: options.align;
  params.fit = fit != null ? fit : options.fit;

  params.offset = options.offset;

  position = getPosition(this.target, tooltip, params);

  addTooltipClasses(tooltip, position.align, position.axis);

  tooltipStyle.top = position.top + 'px';
  tooltipStyle.left = position.left + 'px';

};



/**
 * Creates a keyboard accessible tooltip.
 *
 * Options object:
 *
 * - 'axis' `String`  What direction the tooltip should be rendered
 *   on (`'x'` for horizontal, `'y'` for vertical). Defaults to `'y'`.
 *
 * - 'align' `String`  What oridinal and/or cardinal direction the
 *   tooltip should default to (e.g. `'ne'`, `'s'`). Defaults to `'ne'`.
 *
 * - 'fit' `Boolean`  Whether to attempt to fit the tooltip on screen.
 *   Defaults to `true`.
 *
 *    **NOTE**: this will clobber your alignment settings.
 *
 * - 'mouseEvents' `Boolean` Bind mouse events? Defaults to `true`.
 *
 * - 'delay' `Number` Time in ms to delay before hiding the tooltip on
 *   mouseout. Defaults to `500`.
 *
 * - 'offset' `Number`  Number of pixels to offset the tooltip away from
 *   the target.
 *
 * @api public
 * @name markup.tooltip
 * @param {Mixed} target HTMLElement or selector of the target of the tooltip
 * @param {String} content The content of the tooltip
 * @param {Object} [options] Options object
 * @return {Tooltip} Constructed tooltip object
 */
exports = module.exports = function (target, content, options) {
  var opts = extend({}, exports.defaults, options || {});

  if (typeof target === 'string') {
    target = selector.query(target);
  }

  if (!numberOfTooltips) {
    css(DEFAULT_TOOLTIP_STYLES);
  }

  return new Tooltip(target, content, opts);
};

/**
 * Expose the default tooltip options
 *
 * @type {Object}
 * @api private
 */
exports.defaults = {
  axis: 'y',
  align: 'ne',
  fit: true,
  mouseEvents: true,
  delay: 500,
  offset: 4
};

/**
 * Expose `tooltip.fix`
 *
 * @type {Function}
 * @api private
 */
exports.fix = require('./tooltip-fix.js');
