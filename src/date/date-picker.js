
/*jslint maxstatements: 20, maxcomplexity:15, maxdepth: 5*/

// Next two overrides due to the switch statement in the keyboard handler

'use strict';

var selector = require('selector'),
    query = selector.query,
    queryAll = selector.queryAll,
    events = require('events'),
    on = events.on,
    off = events.off,
    fire = events.fire,
    dom = require('dom'),
    addClass = dom.addClass,
    removeClass = dom.removeClass,
    liveregions = require('live-regions'),
    assertive = liveregions.assertive,
    array = require('array'),
    inArray = array.inArray,
    collections = require('collections'),
    each = collections.each,
    extend = collections.extend,
    select = require('set-selection-range');

/**
 * function to add accessibility information to a datpicker object attached to an input field. The function is written
 * such that if called on a standard jQuery UI single month date picker, it should work without any options. However,
 * the options can be used to attach it to any datepicker implementation, including jQuery multi-month implementations.
 *
 * **NOTE :** In IE, the datePicker will set the value of the field while the user is moving around the datePicker.
 * This is because JAWS does not correctly handle the keyboard events on the input element. This can be turned off by
 * setting `fixJAWS` to false in the options passed in
 *
 *```javascript
 * // This example shows the default options for the jQuery UI single month
 * {
 *    container: {
 *      selector: 'body'
 *    },
 *    months: {
 *      selector: '#ui-datepicker-div',
 *      prev: {
 *        selector: 'a.ui-datepicker-prev',
 *        event: 'click'
 *      },
 *      next: {
 *        selector: 'a.ui-datepicker-next',
 *        event: 'click'
 *      },
 *      monthName: '.ui-datepicker-month',
 *      yearName: '.ui-datepicker-year',
 *      daysOfTheWeek: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
 *    },
 *    days: {
 *      selector: '#ui-datepicker-div>table>tbody>tr>td>a',
 *      selectedClass: 'ui-state-highlight',
 *      event: 'click',
 *      eventSelector: null,
 *      unselectable: null
 *    },
 *    fixJAWS: true, // only in IE
 *    removeJQueryHandlers: true
 * }
 *
 * // This example shows how to modify the options to support the jQuery UI datepicker with multi-month mode
 * // it also shows overriding the selectedClass and setting the prev and next event handlers to null to prevent
 * // the implementation from trying to advance the datepicker outside of the 60 day constraint set on the 3 month
 * // datepicker
 *
 *var datePicker = require( 'lib/date').datePicker,
 *  input = '#myIdofContainerDiv>input',
 *  multiMonthOptions = {
 *    container: {
 *      selector: '#ui-datepicker-div'
 *    },
 *    months: {
 *      selector: '.ui-datepicker-group',
 *      prev: {
 *        event: null
 *      },
 *      next: {
 *        event: null
 *      }
 *    },
 *    days: {
 *      selector: '>table>tbody>tr>td>a',
 *      selectedClass: 'ui-state-active'
 *    }
 *  };
 *datePicker(input, multiMonthOptions);
 *```
 *
 * @api public
 * @name date.datePicker
 * @param {HTMLNode}  inputs     the DOM input elements that have the calendar functionality attached to them
 * @param {Object}    options    the options used to initialize the a11yfied datepicker
 */
module.exports = (function () {

  /**
   * determines the index of the node in the layout table and uses this index to retrieve the text description
   * for that day from the language strings passed in
   *
   * @api private
   * @param {HTMLNode}  node     the element that is the potential parent
   * @param {Array}     days     array of string descriptions for the days
   * @return {String}
   */
  function getDayOfWeekFor(node, days) {
    var firstTd;
    firstTd = node;
    while (firstTd.nodeName !== 'TD' && firstTd.nodeName !== 'TH') {
      firstTd = firstTd.parentNode;
      if (firstTd.nodeName === 'BODY') {
        return days[0];
      }
    }
    return days[firstTd.cellIndex];
  }

  /**
   * call the initialization function on focus in the input field using the options passed-in
   *
   * @api private
   * @param {HTMLNode}  input      the DOM input element that will be the recipient of the selected date
   * @param {Object}    options    the options used to initialize the a11yfied datepicker
   * @return undefined
   */
  function makeDatePicker(input, options) {
    var container, currentVal;
    function restoreCurrentVal(e) {
      var $input;

      if (currentVal !== undefined) {
        e.target.value = currentVal;
        currentVal = undefined;
      }
      if (window.jQuery && window.jQuery.fn.datepicker) {
        $input = window.jQuery(input);
        if ($input.hasClass('hasDatepicker')) {
          $input.datepicker('hide');
        }
      }
    }

    /**
     * called by the keyboard event handler - implements the real keyboard event handler functionality
     * for the a11yfied datepicker
     *
     * @api private
     * @param {Event}     e          the keyboard event object
     * @param {HTMLNode}  container  the DOM node that contains all of the calendar UI elements
     * @param {HTMLNode}  input      the DOM input element that will be the recipient of the selected date
     * @param {Object}    months     the internal state of the a11yfied datepicker implementation
     * @param {Object}    options    the options used to initialize the a11yfied datepicker
     * @return {undefined}
     */
    function executeDatePickerKeyboardEvent(e, container, input, months, options) {
      var keyCode = e.which || e.keyCode,
        ourKeys = [33, 34, 35, 36, 37, 38, 39, 40, 13],
        currentDay, days, nextDay, currentMonth, currentMonthIndex,
        currentDayIndex, nextDayIndex, direction, target,
        monthName, yearName, announcement, els;

      /**
       * private function to move the currently highlighted day into the past by the designated amount.
       * Uses the scope chain for all of its interaction with the caller.
       * Side effects of this function are that the nextDay and nextDayIndex variables will be updated
       *
       * Possible side effects are:
       * 1)       the event for moving the current monthmay fire causing the UI to update
       * 2)       the date picker may be re-initialized after the event firing in case the
       *          calendar deleted and added UI elements
       * 3)       the currentMonth and currentMonthIndex variables might be modified
       *
       * @api private
       */
      function moveDayUp() {
        if (currentMonthIndex === 0) {
          if (months.prev) {
            fire(months.prev.node, months.prev.event);
            initDatePicker(container, input, options);
            currentMonth = months.nodes[currentMonthIndex];
            days = queryAll(months.days.selector, currentMonth);
            nextDayIndex = days.length + nextDayIndex;
            if (nextDayIndex < 0) {
              nextDayIndex = 0;
            }
            nextDay = days[nextDayIndex];
          } else if (currentDayIndex > 0) {
            nextDayIndex = 0;
            nextDay = days[nextDayIndex];
          }
        } else {
          currentMonthIndex -= 1;
          currentMonth = months.nodes[currentMonthIndex];
          days = queryAll(months.days.selector, currentMonth);
          nextDayIndex = days.length + nextDayIndex;
          if (nextDayIndex < 0) {
            nextDayIndex = 0;
          }
          nextDay = days[nextDayIndex];
        }
      }

      /**
       * Move the currently highlighted day into the future by the designated amount.
       * Uses the scope chain for all of its interaction with the caller.
       * Side effects of this function are that the nextDay and nextDayIndex variables will be updated
       *
       * Possible side effects are:
       * 1)       the event for moving the current month may fire causing the UI to update
       * 2)       the date picker may be re-initialized after the event firing in case the
       *          calendar deleted and added UI elements
       * 3)       the currentMonth and currentMonthIndex variables might be modified
       *
       * @api private
       */
      function moveDayDown() {
        if (currentMonthIndex === (months.count - 1)) {
          if (months.next) {
            fire(months.next.node, months.next.event);
            initDatePicker(container, input, options);
            currentMonth = months.nodes[currentMonthIndex];
            nextDayIndex = nextDayIndex - days.length; // figure out how many days we need to skip
            days = queryAll(months.days.selector, currentMonth);
            if (nextDayIndex >= days.length) {
              nextDayIndex = days.length - 1;
            }
            nextDay = days[nextDayIndex];
          } else if (currentDayIndex < days.length - 1) {
            nextDayIndex = days.length - 1;
            nextDay = days[nextDayIndex];
          }
        } else {
          currentMonthIndex += 1;
          currentMonth = months.nodes[currentMonthIndex];
          nextDayIndex = nextDayIndex - days.length; // figure out how many days we need to skip
          days = queryAll(months.days.selector, currentMonth);
          if (nextDayIndex >= days.length) {
            nextDayIndex = days.length - 1;
          }
          nextDay = days[nextDayIndex];
        }
      }

      /**
       * Modify the classes on the calendar UI to highlight the new current day
       * Uses the scope chain for all of its interaction with the caller.
       * Side effects of this function are that the classes in the celandar UI wil be modified and the content of the
       * aria-live region will be updated to announce the human understandable description of the newly highlighte date
       *
       * @api private
       */
      function changeVisualIndicator() {
        removeClass(currentDay, months.days.selectedClass);
        els = queryAll('.' + months.days.selectedClass, currentMonth);
        each(els, function (value) {
          removeClass(value, months.days.selectedClass);
        });
        addClass(nextDay, months.days.selectedClass);
        monthName = query(months.monthName, currentMonth);
        yearName = query(months.yearName, currentMonth);
        announcement = getDayOfWeekFor(nextDay, months.daysOfTheWeek) + ', ' + nextDay.innerHTML;
        if (monthName) {
          announcement += ' ' + monthName.innerHTML;
        }
        if (yearName) {
          announcement += ', ' + yearName.innerHTML;
        }
        assertive(announcement);
        if (options.fixJAWS) {
          input.value = announcement;
          select(input, announcement.length, announcement.length);
        }
      }

      /**
       * Move the currently highlighted day to the new position based on the keyboard event
       * Uses the scope chain for all of its interaction with the caller.
       * Side effects of this function are documented in the functions called by this function
       *
       * @api private
       */
      function moveDay() {
        if (direction) {
          if (nextDayIndex >= 0 && nextDayIndex < days.length) {
            nextDay = days[nextDayIndex];
            if (months.days.unselectable && nextDay.className.indexOf(months.days.unselectable) !== -1) {
              nextDay = null;
              if (direction === 'up') {
                nextDayIndex = days.length;
              } else {
                nextDayIndex = -1;
              }
            }
          }
          if (!nextDay) {
            if (nextDayIndex < 0) {
              moveDayUp();
            } else if (nextDayIndex >= days.length) {
              moveDayDown();
            }
          }
          if (nextDay) {
            changeVisualIndicator();
          }
        }
      }

      /**
       * Determine which day is the currently highlighted day in the calendar UI and
       * set the currentDay variable to point to this element. If no day is currently highlighted, the function
       * will highlight the first selectable day in the first month.
       * Uses the scope chain for all of its interaction with the caller.
       * Side effects of this function are that the currentDay, currentMonth, currentDayIndex and currentMonthIndex
       * variables will all be modified
       *
       * Throws some custom exceptions for various error conditions, exception messages describe the reason
       *
       * @api private
       */
      function getCurrentDay() {
        var i, _ilen;
        currentDay = query('.' + months.days.selectedClass, container);
        if (!currentDay) {
          // we are going to set this guy to the first day
          days = queryAll(months.days.selector, container);
          for (i = 0, _ilen = days.length; i < _ilen; i++) {
            if (!months.days.unselectable || days[i].className.indexOf(months.days.unselectable) === -1) {
              break;
            }
          }
          if (i === _ilen) {
            // could not find any selectable days, fail
            throw 'Could not find any selectable days on the calendar';
          }
          currentDay = days[i];
          addClass(currentDay, months.days.selectedClass);
        }
        for (i = 0, _ilen = months.count; i < _ilen; i++) {

          if (months.nodes[i].contains(currentDay)) {
            break;
          }
        }
        if (i === _ilen) {
          throw 'Could not find the month owner of the current day';
        }
        currentMonthIndex = i;
        currentMonth = months.nodes[currentMonthIndex];
        days = queryAll(months.days.selector, currentMonth);
        currentDayIndex = array.indexOf(days, currentDay);
      }

      if (inArray(ourKeys, keyCode)) {
        // find the month and the current day
        getCurrentDay();
        switch (keyCode) {
        case 33: //page up
          nextDayIndex = currentDayIndex - 28;
          direction = 'up';
          break;
        case 34: //page down
          nextDayIndex = currentDayIndex + 28;
          direction = 'down';
          break;
        case 35: //end
          nextDayIndex = days.length - 1;
          direction = 'down';
          break;
        case 36: //home
          nextDayIndex = 0;
          direction = 'up';
          break;
        case 37: //left
          nextDayIndex = currentDayIndex - 1;
          direction = 'up';
          break;
        case 38: //up
          nextDayIndex = currentDayIndex - 7;
          direction = 'up';
          break;
        case 39: //right
          nextDayIndex = currentDayIndex + 1;
          direction = 'down';
          break;
        case 40: //down
          nextDayIndex = currentDayIndex + 7;
          direction = 'down';
          break;
        case 13: //enter
          target = currentDay;
          if (months.days.eventSelector) {
            target = query(months.days.eventSelector, currentDay);
          }
          if (target) {
            currentVal = undefined;
            fire(target, months.days.event);
          }
          break;
        }
        moveDay();
        e.preventDefault();
        e.stopPropagation();
      }
    }
    /**
     * initialize the internal state of the a11yfied datepicker. It is designed to be
     * called multiple times whenever the implementation believes that the calendar UI could have been re-drawn
     *
     * @api private
     * @param {HTMLNode}  container  the DOM node that contains all of the calendar UI elements
     * @param {HTMLNode}  input      the DOM input element that will be the recipient of the selected date
     * @param {Object}    options    the options used to initialize the a11yfied datepicker
     * @return {Object}   months     the internal state object or null if the initialization failed
     */
    function initDatePicker(container, input, options) {
      // remember the current value
      if (currentVal === undefined) {
        currentVal = input.value;
      }
      function datePickerKeyboardHandler(e) {
        executeDatePickerKeyboardEvent(e, container, input, months, options);
      }
      var prev, next, monthEls, months;
      if (!options.months || !options.months.selector) {
        return null;
      }
      monthEls = queryAll(options.months.selector, container);
      if (!monthEls || monthEls.length === 0) {
        return null;
      }
      if (options.months.prev && options.months.prev.event) {
        prev = {
          node: query(options.months.prev.selector, container),
          event: options.months.prev.event
        };
      }
      if (options.months.next && options.months.next.event) {
        next = {
          node: query(options.months.next.selector, container),
          event: options.months.next.event
        };
      }
      months = {
        count: monthEls.length,
        nodes: monthEls,
        monthName: options.months.monthName,
        yearName: options.months.yearName,
        daysOfTheWeek: options.months.daysOfTheWeek,
        prev: prev,
        next: next,
        days: options.days
      };
      off(input, 'keydown'); // remove prior keyboard handler
      on(input, 'keydown', datePickerKeyboardHandler);
      off(input, 'blur', restoreCurrentVal);
      on(input, 'blur', restoreCurrentVal);
      return months;
    }

    function recurse() {
      // this gives the UI one chance to show before giving up
      initDatePicker(container, input, options);
    }

    if (options.container && options.container.selector) {
      container = query(options.container.selector);
    }
    on(input, 'focus', function () {
      if (!initDatePicker(container, input, options)) {
        setTimeout(recurse, 0);
      }
    });

  }

  /**
   * exported implementation function, extends the default option using the options passed in and initializes the
   * implementation for each of the matching inputs. See documentation above
   *
   * @api private
   * @param {HTMLNode}  input      the DOM input element that will be the recipient of the selected date
   * @param {Object}    options    the options used to initialize the a11yfied datepicker
   */
  function entry(inputs, options) {
    var defaultOptions = {
      container: {
        selector: 'body'
      },
      months: {
        selector: '#ui-datepicker-div',
        prev: {
          selector: 'a.ui-datepicker-prev',
          event: 'click'
        },
        next: {
          selector: 'a.ui-datepicker-next',
          event: 'click'
        },
        monthName: '.ui-datepicker-month',
        yearName: '.ui-datepicker-year',
        daysOfTheWeek: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
      },
      days: {
        selector: '#ui-datepicker-div>table>tbody>tr>td>a',
        selectedClass: 'ui-state-highlight',
        event: 'click',
        eventSelector: null,
        unselectable: null
      },
      fixJAWS: false,
      removeJQueryHandlers: true
    },
    elements = queryAll(inputs),
    toplevel;

    if (navigator.userAgent.indexOf('Windows') !== -1 && navigator.userAgent.indexOf('IE') !== -1) {
      // if operating in IE, then set do some selectivity magic
      defaultOptions.fixJAWS = true;
    }
    if (options) {
      for (toplevel in defaultOptions) {
        if (options.hasOwnProperty(toplevel)) {
          if (typeof defaultOptions[toplevel] === 'object') {
            extend(defaultOptions[toplevel], options[toplevel]);
          } else {
            defaultOptions[toplevel] = options[toplevel];
          }
        }
      }
    }
    each(elements, function (value) {
      makeDatePicker(value, defaultOptions);
      if (defaultOptions.removeJQueryHandlers && window.jQuery) {
        /*
         * if jQuery is defined and the flag is set, remove the standard jQuery UI keyboard handlers
         * which otherwise cause havoc
         */
        window.jQuery(value).unbind('keydown').unbind('keypress').bind('keydown', function (e) {
          var keyCode = e.which || e.keyCode;

          if ((keyCode >= 33 && keyCode <= 40) || keyCode === 13) {
            e.preventDefault();
            e.stopPropagation();
            if (e.stopImmediatePropagation) {
              e.stopImmediatePropagation();
            }
          }
        });
      }
    });
  }
  return entry;
}());
