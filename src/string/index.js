
'use strict';

/**
 * Export truncate
 *
 * @api private
 */
exports.truncate = require('./truncate');


/**
 * Trim leading and trailing whitespace on the given `str`
 *
 * #### Example
 *
 * ```js
 * var string = require('lib/string');
 * var str = '  \t hello world   ';
 *
 * assert(string.trim(str) === 'hello world');
 * ```
 *
 * @api public
 * @name string.trim
 * @param {String} str
 * @return {String}
 */
exports.trim = require('trim');

/**
 * Trim leading whitespace on the given `str`
 *
 * #### Example
 *
 * ```js
 * var string = require('lib/string');
 * var str = '  \t hello world   ';
 *
 * assert(string.trim.left(str) === 'hello world   ');
 * ```
 *
 * @api public
 * @name string.trim.left
 * @param {String} str
 * @return {String}
 */

/**
 * Trim trailing whitespace on the given `str`
 *
 * #### Example
 *
 * ```js
 * var string = require('lib/string');
 * var str = '  \t hello world   ';
 *
 * assert(string.trim.right(str) === '  \t hello world');
 * ```
 *
 * @api public
 * @name string.trim.right
 * @param {String} str
 * @return {String}
 */
