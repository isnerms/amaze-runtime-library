
'use strict';

/**
 * Truncate the given string in less or equal length
 * to the given characters starting from **LEFT** side
 *
 * Example
 *
 * ```js
 * var string = require('lib/string');
 *
 * var killbill = 'The lead character, called "The Bride", was a '
 *              + 'member of the Deadly Viper Assassination Squad, '
 *              + 'lead by her lover "Bill".';
 *
 * assert(string.truncate(killbill, 18) == 'The lead character');
 *
 * assert(string.truncate(killbill, 18, ' ...') == 'The lead character ...');
 * ```
 *
 * @api public
 * @name string.truncate
 * @param {String} str
 * @param {Number} len Max number of characters
 * @param {String} [suffix] String to add to summary
 * @return {String}
 */
exports = module.exports = function (str, len, suffix) {
  suffix = suffix || '';

  return (len < str.length) ? str.substring(0, len) + suffix : str;
};


/**
 * See `string.truncate`
 *
 * Example
 *
 * ```js
 * var string = require('lib/string');
 *
 * var killbill = 'The lead character, called "The Bride", was a '
 *              + 'member of the Deadly Viper Assassination Squad, '
 *              + 'lead by her lover "Bill".';
 *
 * assert(string.truncate.left(killbill, 18) == 'The lead character');
 * ```
 *
 * @api public
 * @name string.truncate.left
 */
exports.left = exports;


/**
 * Like `string.truncate()`, but from the **RIGHT**
 *
 * Example
 *
 * ```js
 * var string = require('lib/string');
 *
 * var killbill = 'The lead character, called "The Bride", was a '
 *              + 'member of the Deadly Viper Assassination Squad, '
 *              + 'lead by her lover "Bill".';
 *
 * assert(string.truncate.right(killbill, 20) == 'by her lover "Bill".');
 * assert(string.truncate.right(killbill, 20, '... ') == '... by her lover "Bill"..');
 * ```
 *
 * @api public
 * @name string.truncate.right
 * @param {String} str
 * @param {Number} len
 * @param {String} [prefix]
 * @return {String}
 */
exports.right = function (str, len, prefix) {
  prefix = prefix || '';

  var length = str.length;

  return (len < length) ? prefix + str.substring(length - len, length) : str;
};
