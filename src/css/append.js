'use strict';

/**
 * Updates an existing Amaze style tag, or adds one if one does
 * not already exist, then adds new rules to the style tag.
 *
 * Example:
 *
 * ```js
 * css(' #content { height: 75px; } ');
 * ```
 *
 * @api public
 * @name css
 * @param {String} rules A string of a new css rules
 * @return {HTMLElement} the <style> element with the new styles added
 */

var style = null;
var head = document.head || document.getElementsByTagName('head')[0];

module.exports = function (rules) {

  if (!style) {
    style = document.createElement('style');
    style.type = 'text/css';
    style.id = 'amaze-css-module';
    head.appendChild(style);
  }

  if (style.styleSheet === undefined) { // Not old IE
    style.textContent += rules;
  } else {
    style.styleSheet.cssText += rules;
  }

  return style;
};
