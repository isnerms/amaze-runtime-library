
'use strict';

var append = require('./append');
var css = require('css');

exports = module.exports = cssWrapper;
exports.append = append;

/**
 * Get the current value of a CSS `property` of an `element`.
 *
 * @api public
 * @name css.get
 * @param {HTMLElement} element
 * @param {String} property
 */

exports.get = css;

/**
 * Set the `value` of a CSS `property` on an `element`.
 *
 * @api public
 * @name css.set
 * @param {HTMLElement} element
 * @param {String} property
 * @param {String|Number} value
 */

exports.set = css;

/**
 * A convenience method for CSS manipulation.
 *
 * @api private
 * @param {Mixed} ...
 * @return {Mixed}
 */

function cssWrapper(element, property, value) {
  var arity = arguments.length;

  if (arity === 1) {
    return append(arguments[0]);
  }

  if (arity === 2) {
    return css(element, property);
  }

  if (arity > 3) {
    throw new Error('Invalid number of arguments');
  }

  css(element, property, value);
}
