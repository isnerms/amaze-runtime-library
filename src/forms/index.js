
/**
 * Expose the forms stuff
 *
 * @api private
 */

exports.generic = require('./generic');

exports.Validator = require('./validator');
