
'use strict';

//
// TODO
//
//   actual `Emitter` inhertance
//

var collections = require('collections'),
    each = collections.each,
    array = require('array');

/**
 * A generic form validator
 *
 * Example:
 *
 * ```js
 * var forms = require('lib/forms')
 *
 * var form = document.getElementById('my-form')
 *
 * var validator = forms.Validator(form)
 *
 * // Evented
 *
 * validator
 *   .on('invalid', function () {
 *     alert('form is not valid')
 *   })
 *   .on('valid', function () {
 *     alert('form is valid')
 *   })
 *   .on('error', function () {
 *     alert('could not validate form')
 *   })
 *   .validate()
 *
 * // Callback
 *
 * validator.validate(function (err, report) {
 *
 *   if (err) {
 *     return alert('could not validate form')
 *   }
 *
 *   each(report, function (item) {
 *     if (item.isValid) {
 *       console.log(item.element, 'is valid')
 *     } else {
 *       console.log(item.element, 'is invalid')
 *     }
 *   })
 *
 * })
 *
 * ```
 *
 * @name forms.Validator
 * @param {HTMLElement} form
 */
var Validator = module.exports = function (form) {
  // support invocation without `new`
  if (!(this instanceof Validator)) {
    return new Validator(form);
  }

  this._listeners = {};
  this.form = form;
};

/**
 * Add an `input` to the form validator
 *
 * Example:
 *
 * ```js
 * var validator = forms.Validator(…)
 * validator
 *   .addInput(selector.query('#foo'))
 *   .addInput(selector.query('#bar'))
 *   .addInput(selector.query('.foobar'))
 *   .addInput(selector.query('div'))
 *
 * ```
 *
 * @api public
 * @param {HTMLElement} input
 * @return {Validator}
 */
Validator.prototype.addInput = function (input) {
  (this._inputs = this._inputs || []).push(input);
  return this;
};

/**
 * Validate the form.
 *
 * Example using emitted events:
 *
 * ```js
 * var forms = require('lib/forms')
 *   , form = new forms.Validator(…)
 *
 * form
 *   // invalid listeners accept a `report`
 *   .on('invalid', function (report) {
 *     each(report, function (input) {
 *       console.log(input.element, 'is invalid')
 *     })
 *   })
 *   // valid listeners accept a `report`
 *   .on('valid', function (report) {
 *     console.log('all inputs are valid')
 *   })
 *   // error listeners accept an `err`
 *   .on('error', function (err) {
 *     throw err
 *   })
 *   .validate()
 * ```
 *
 * Example using callback:
 *
 * ```js
 * var form = new forms.Validator(...)
 *
 * form.validate(function (err, report) {
 *   // unable to validate form
 *   if (err) {
 *     throw err;
 *   }
 *
 *   if (report.isValid) {
 *     alert('valid!');
 *   } else {
 *     alert('invalid!');
 *   }
 * });
 * ```
 *
 * @api public
 * @param {Function} [cb] Callback function
 * @return {Validator}
 */
Validator.prototype.validate = function (cb) {
  cb = cb || function () {};

  var err,
    inputs = this._inputs || [],
    isValid = true,
    report = {};

  if (!inputs || !inputs.length) {
    err = new Error('No inputs to validate');
    cb(err);
    return this.emit('error', err);
  }

  each(inputs, function (input) {
    var hasValue,
        rep = {};

    rep.element = input;

    hasValue = input.type.toLowerCase() === 'checkbox'
            // a required checkbox must be checked
            ? input.checked
            // all other required inputs must have value
            : input.value;

    //instead of checking property, check the attribute (ie8)
    if (hasValue || !input.hasAttribute('required')) {
      rep.skipped = true;
      rep.isValid = true;
    } else {
      rep.isValid = false;
      isValid = false;
    }

    report[input.id] = rep;
  });

  cb(null, report);
  return this.emit(isValid ? 'valid' : 'invalid', report);
};

/**
 * Add a `listener` for an `event`
 *
 * Example:
 *
 * ```js
 * Validator(…)
 *   .on('foo', function () {
 *     alert('foo happened')
 *   })
 *   .on('bar', function () {
 *     alert('bar happened')
 *   })
 * ```
 *
 * @api public
 * @param {String} event
 * @param {Function} listener
 * @return {Validator}
 */
Validator.prototype.on = function (event, listener) {
  this._listeners[event] = this._listeners[event] || [];

  if (!array.inArray(listener, this._listeners[event])) {
    this._listeners[event].push(listener);
  }
  return this;
};

/**
 * `emit` one or more `events`, providing their listeners `data`
 *
 * @api private
 * @param {String|Array} events
 * @param {Mixed|Any} data
 * @return {Validator}
 */
Validator.prototype.emit = function (events, data) {
  var self = this;

  if (!array.isArray(events)) {
    events = events.split(',');
  }

  each(events, function (event) {
    event = event.replace(/\s/g, '');
    self._listeners[event] = self._listeners[event] || [];

    each(self._listeners[event], function (fn) {
      fn(data);
    });
  });

  return this;
};
