
'use strict';

var selector = require('selector'),
    collections = require('collections'),
    each = collections.each,
    extend = require('extend'),
    markup = require('markup'),
    tooltip = markup.tooltip,
    events = require('events'),
    Validator = require('./validator'),
    rndid = require('rndid');


/**
 * A simple, generic (evented) form fixer.
 *
 * #### Options:
 *
 * - message `String` The tooltip message; defaults to `"Please
 *   complete this mandatory field"`
 *
 * - fieldSelector `String` A selector for finding form _fields_;
 *   defaults to `"div.input"`
 *
 * - ariaRequired `Boolean` Should `aria-required="true"` be added
 *   to required inputs; defaults to `true`
 *
 * - onSubmit `Boolean` Should the form's `onsubmit` callback be
 *   removed then fired when all inputs are valid; defaults to
 *   `true`
 *
 * - noValidate `Boolean` Should the form be marked as HTML5
 *   `novalidate`.  Defaults to true.
 *
 * - singleTip `Boolean` Should only a single tooltip be displayed
 *   on a bad submission
 *
 * - isRequired `Function` A function to deterimine if a
 *   `field` is required
 *
 *    Example:
 *
 *    ```js
 *     isRequired: function (field) {
 *       if (selector.query('.required', field) {
 *         return true
 *       })
 *       return false
 *     }
 *    ```
 *
 * - getLabel `Function` A function to get a _field's_ `<label />`
 *
 *    Example:
 *
 *    ```js
 *     getLabel: function (field) {
 *       return selector.query('label', field)
 *     }
 *    ```
 *
 * - getInput `Function` A function to get a _field's_ `<input />`
 *
 *    Example:
 *
 *    ```js
 *     getInput: function (field) {
 *       // input or textarea
 *       return selector.query('input,textarea', field)
 *     }
 *    ```
 *
 * - preInput `Function` A function to execute on each `<input />`
 *
 *    Example:
 *
 *    ```js
 *     preInput: function (input) {
 *       // remove all jQuery listeners
 *       jQuery(input).unbind()
 *     }
 *    ```
 * - tooltips `Object` Options passed into `markup.tooltip`.
 *   See `markup.tooltip`.
 *
 *
 * @api public
 * @name forms.generic
 * @param {String|HTMLElement} element
 * @param {Object} [options]
 * @return {forms.Validator}
 */
exports = module.exports = function (element, options) {
  // jshint onevar:false

  // support selectors
  if (typeof element === 'string') {
    element = selector.query(element);
  }

  if (!element) {
    throw new TypeError('A <form> element must be provided');
  }


  var opts = extend({}, exports.defaults, options || {}),
      form = new Validator(element),
      // tooltip cache so we don't create more than one
      tooltips = {},
      // all fields
      fields = selector.queryAll(opts.fieldSelector, element),
      // possible onsubmit value
      onsubmit = null;

  // AM-217
  if (opts.noValidate) {
    element.setAttribute('novalidate', true);
  }

  // handle form onsumbit
  if (opts.onSubmit && element.onsubmit) {
    onsubmit = element.onsubmit;
    element.onsubmit = null;
  }

  // check each field
  each(fields, function (field) {
    var label = opts.getLabel(field),
      input = opts.getInput(field);

    // must have a label and an input
    if (!label || !input) {
      return;
    }

    // run the `preInput` function on each input
    if (opts.preInput) {
      opts.preInput(input);
    }

    // add the input to the form validator
    form.addInput(input);

    // give the input an id if it doesn't already have one
    input.id = input.id || rndid();

    // associate the label
    label.htmlFor = input.id;

    // mark the input as required when applicable
    if (opts.isRequired(field)) {
      // optionally add `aria-required="true"`
      if (opts.ariaRequired) {
        input.setAttribute('aria-required', true);
      }
      // always add HTML5 `required="true"`
      // setting attribute because ie8...
      input.setAttribute('required', true);
    }
  });

  // capture submission events
  events.on(element, 'submit', function (submissionEvent) {
    var setFocus = false,
        shownTooltip = false;

    /**
     * Invalid form submission callback.  Will create
     * tooltips for invalid entries.
     *
     * @api private
     * @param {Object} report
     */
    function onInvalid(report) {
      // stop the submission if the form isn't valid
      submissionEvent.preventDefault();
      submissionEvent.stopPropagation();

      // check each element in the report
      each(report, function (input, id) {
        var tip;

        // skip valid inputs
        if (input.isValid) {
          return;
        }

        // set focus on first bad input
        if (!setFocus) {
          input.element.focus();
          setFocus = true;
        }

        // grab the existing toolip
        tip = tooltips[id];
        if (!tip) {
          // don't re-create the same tooltip
          tip = tooltips[id] = tooltip(input.element,
              opts.message,
              opts.tooltips);
        }

        // show the tip
        // but only show one if `singleTip` is set
        if (opts.singleTip) {
          if (!shownTooltip) {
            tip.show();
            shownTooltip = true;
          }
        } else {
          tip.show();
        }
      });
    }

    /**
     * Error callback; throws the given `err`
     *
     * @api private
     * @param {Error} err
     */
    function onError(err) {
      throw err;
    }

    /**
     * Valid form submission callback.  When applicable, will fire `onsubmit`.
     *
     * @api private
     */
    function onValid() {
      if (opts.onSubmit && onsubmit) {
        onsubmit.call(element, submissionEvent);
        onsubmit = null;
      }
    }


    form
      // invalid listener
      .on('invalid', onInvalid)
      .on('error', onError)
      .on('valid', onValid)
      // validate the form
      .validate();
  });

  return form;
};

/**
 * Default options
 *
 * @api private
 * @type {Object}
 */
exports.defaults = {
  message: 'Please complete this mandatory field',
  fieldSelector: 'div.input',
  ariaRequired: true,
  onSubmit: true,
  noValidate: true,
  singleTip: false,
  isRequired: function (field) {
    // find the label
    var label = this.getLabel(field);

    // no label, not required
    if (!label) {
      return false;
    }

    // if there is a span contained in the label,
    // mark the field as required
    return selector.query('span', label);
  },
  getLabel: function (field) {
    return selector.query('label', field);
  },
  getInput: function (field) {
    return selector.query('input, textarea', field);
  },
  preInput: function (input) {
    var jQuery = window.jQuery;

    if (jQuery) {
      // remove any jQuery listeners
      jQuery(input).unbind();
    }
  }
};
