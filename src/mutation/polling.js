'use strict';

var INTERVAL = 500,
  array = require('array'),
  selector = require('selector'),
  inArray = array.inArray,
  toArray = array.toArray,
  query = selector.query,
  matchesSelector = selector.matches,
  types = [
    'inserted',
    'removed',
    'attribute'
  ];

/**
 * Get all child nodes of an HTMLElement
 *
 * @api private
 * @param {HTMLElement} element
 * @return {Array}
 */
function decendants(element) {
  return toArray(element.getElementsByTagName('*'));
}

/**
 * Poll an HTMLElement's children, waiting for a child
 * which matches a selector to be removed.
 *
 * @api private
 * @param {HTMLElement} element
 * @param {String} selector
 * @param {Function} callback
 */
function removed(element, selector, callback, persistent) {
  var interval,
    children = decendants(element);

  if (!query(selector, element)) {
    throw new Error('no element maching "' + selector +
      '" exists in the provided context');
  }

  interval = setInterval(function () {
    //jshint maxcomplexity:8
    var index, length, child, nodeType,
      mutations = [],
      newChildren = decendants(element);

    // walk the new subset of decendants
    for (index = 0, length = children.length; index < length; index += 1) {
      child = children[index];
      nodeType = child.nodeType;
      // no text nodes
      if (nodeType !== 3 || nodeType !== 8) {
        // if the child is not in the new Array, it was removed
        if (!inArray(newChildren, child)) {
          // does the removed child match?
          if (matchesSelector(child, selector)) {
            // add to stack
            mutations.push(child);
          }
        }
      }
    }

    // matching removed nodes?
    if (mutations.length) {
      if (!persistent) {
        clearInterval(interval);
      }
      return callback(mutations);
    }

    // no need to recheck the same child
    children = newChildren;
  }, INTERVAL);
}

/**
 * Poll an HTMLElement's children, waiting for a child
 * which matches a selector to be added.
 *
 * @api private
 * @param {HTMLElement} element
 * @param {String} selector
 * @param {Function} callback
 */
function inserted(element, selector, callback, persistent) {
  var interval,
    children = decendants(element);

  interval = setInterval(function () {
    var index, length, child,
      mutations = [],
      newChildren = decendants(element);

    // walk the children
    for (index = 0, length = newChildren.length; index < length; index += 1) {
      child = newChildren[index];
      // if it's a newly added child
      if (!inArray(children, child)) {
        // if it matches the selector
        if (matchesSelector(child, selector)) {
          // add to the array
          mutations.push(child);
        }
      }
    }

    // matching mutations?
    if (mutations.length) {
      if (!persistent) {
        // stop polling and provide the mutations
        clearInterval(interval);
      }
      return callback(mutations);
    }

    // update the children array; we don't need to keep
    // checking the same nodes over-and-over again
    children = newChildren;
  }, INTERVAL);
}

/**
 * Poll an HTMLElement's children, waiting for a child
 * which matches a selector's attributes to be modified.
 *
 * @api private
 * @param {HTMLElement} element
 * @param {String} selector
 * @param {Function} callback
 */
function attribute(element, selector, callback, persistent) {
  var interval,
    property = '__previous_attributes_hash';

  /**
   * Calculate a "hash" of an Element's attributes.
   *
   * @api private
   * @param {NamedNodeMap} attributes
   * @return {Number}
   */
  function getAttributeHash(attributes) {
    var index, length, attribute, name, value,
      hash = 0;

    // walk each attribute
    for (index = 0, length = attributes.length; index < length; index += 1) {
      attribute = attributes[index];
      // make sure it's valid
      if (attribute.specified) {
        name = attribute.name;
        value = attribute.value;
        // verify the attribute has a name and a value
        // do not count our own property (ie) to avoid infinite recursion
        if (name && value && name !== property) {
          // add name a value
          hash += name.length;
          hash += value.length;
        }
      }
    }

    return hash;
  }

  /**
   * Check an HTMLElement's current attributes vs its previous attributes
   *
   * @api private
   * @param {HTMLElement} child
   * @return {null|HTMLElement}
   */
  function checkChild(child) {
    var actual = getAttributeHash(child.attributes),
      expected = child[property];

    // first iteration with this element?
    if (expected === undefined) {
      // set the property and leave
      child[property] = actual;
      return;
    }

    // same as last interation?
    if (expected === actual) {
      return;
    }

    // update the property and return the child
    child[property] = actual;

    // matches the selector?
    if (matchesSelector(child, selector)) {
      // provide it
      return child;
    }

    return;
  }

  interval = setInterval(function () {
    var index, length, mutation,
      mutations = [],
      children = decendants(element);

    // walk the children
    for (index = 0, length = children.length; index < length; index += 1) {
      // attribute change on the correct element?
      mutation = checkChild(children[index]);
      if (mutation) {
        // add to list
        mutations.push(mutation);
      }
    }

    // found mutations matching?
    if (mutations.length) {
      if (!persistent) {
        // stop polling and provide the mutations
        clearInterval(interval);
      }
      // provide mutations to callback
      return callback(mutations);
    }
  }, INTERVAL);
}

/**
 * Mutations for IE7+
 *
 * @api private
 * Poll the DOM, waiting for changes
 */
exports = module.exports = function (element, selector, mutationType, callback, persistent) {

  if (typeof element === 'string') {
    element = query(element);
  }

  // must be an HTMLElement
  if (element.nodeType !== 1) {
    throw new TypeError('element must be inherit from HTMLElement');
  }

  // must be a valid mutation type
  if (!inArray(types, mutationType)) {
    throw new TypeError('unsupported type "' +  mutationType + '"');
  }

  if (mutationType === 'attribute') {
    return attribute(element, selector, callback, persistent);
  }

  if (mutationType === 'inserted') {
    return inserted(element, selector, callback, persistent);
  }

  return removed(element, selector, callback, persistent);
};

exports.mode = 'polling';
