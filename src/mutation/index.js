// jshint maxlen:400

'use strict';

var mo = require('mutation-observer'),
    observer = require('./observer'),
    events = require('./events'),
    polling = require('./polling');

/**
 * Wait for a specific type of DOM mutation to occur on a child of
 * an HTMLElement which matches a selector.
 *
 * You may *not* use decendant selectors when triggering off node
 * removal.  It's not possible as a removed node has no parent node.
 *
 * #### Supported mutation types:
 *
 * - _attribute_: attribute modifications within the context element
 * - _inserted_: node inserted into the context element
 * - _removed_: node removed from the context element
 *
 * Examples:
 *
 * ```js
 * var mutation = require('lib/mutation'),
 *   container = document.getElementById('container');
 *
 * mutation(container, 'div.foo.bar', 'inserted', function (elements) {
 *   console.log('%d elements were inserted', elements);
 * });
 *
 * mutation('#container', 'div.foo.bar', 'inserted', function (elements) {
 *   console.log('%d elements were inserted', elements);
 * });
 * ```
 *
 * @api public
 * @name mutation
 * @param {HTMLElement|String} element The context element or selector for one
 * @param {String} selector A CSS-style selector for targeted nodes
 * @param {String} mutationType
 * @param {Function} callback Callback function: `function (matches)`
 * @param {Boolean} [persistent] how many times will the callback be called. Default is false, setting this to true will allow the mutation listener to live forever. This is useful in situations where there is no user event that can be used to register a mutation observer (e.g. chat, live feeds etc.)
 */

exports = module.exports = mo
  // DOM4
  ? observer
  // DOM3
  : window.addEventListener
    ? events
    // IE
    : polling;

exports.observer = observer;
exports.events = events;
exports.polling = polling;
