'use strict';

var selector = require('selector'),
  events = require('events'),
  array = require('array'),
  query = selector.query,
  inArray = array.inArray,
  types = [
    'inserted',
    'removed',
    'attribute'
  ];

/**
 * DOM3 mutations
 *
 * @api private
 */
exports = module.exports = function (element, selector, mutationType, callback, persistent) {
  /**
   * Mutation callback
   *
   * @api private
   * @param {MutationEvent} mutationEvent
   */
  function onMutation(mutationEvent) {
    // add the added|removed|modified node to the array of mutations
    mutations.push(mutationEvent.target);

    if (!persistent) {
      // remove the listener if we're not persistent
      events.undelegate(element, eventType, onMutation);
    }
    callback(mutations);
  }

  var eventType, mutations;

  if (typeof element === 'string') {
    element = query(element);
  }

  // must be an HTMLElement
  if (element.nodeType !== 1) {
    throw new TypeError('element must be inherit from HTMLElement');
  }

  // must be a valid mutation type
  if (!inArray(types, mutationType)) {
    throw new TypeError('unsupported type "' +  mutationType + '"');
  }

  mutations = [];

  if (mutationType === 'attribute') {
    eventType = 'DOMAttrModified';
  } else if (mutationType === 'inserted') {
    eventType = 'DOMNodeInserted';
  } else {
    eventType = 'DOMNodeRemoved';
  }

  return events.delegate(element, selector, eventType, onMutation);
};

exports.mode = 'events';
