'use strict';

var Observer = require('mutation-observer'),
  selector = require('selector'),
  array = require('array'),
  query = selector.query,
  matchesSelector = selector.matches,
  inArray = array.inArray,
  toArray = array.toArray,
  types = [
    'inserted',
    'removed',
    'attribute'
  ];

/**
 * Iterate through an Array of MutationRecords, checking of any of
 * their `addedNodes`s match a provided selector.  Any matching node
 * will be added to an Array and returned.
 *
 * @api private
 * @param {Array} records An Array of MutationRecords
 * @param {String} selector A CSS selector
 * @return {Array} Any matching nodes
 */
function filter(records, selector, type) {
  var index, node,
    length = records.length,
    mutations = [];

  for (index = 0; index < length; index++) {
    if (type) {
      mutations = mutations.concat(filterType(records[index], selector, type));
    } else {
      node = records[index].target;
      if (matchesSelector(node, selector)) {
        mutations.push(node);
      }
    }
  }

  return mutations;
}

function filterType(record, selector, type) {
  var index, node,
    nodes = toArray(record[type]),
    length = nodes.length,
    result = [];


  for (index = 0; index < length; index++) {
    node = nodes[index];
    if (matchesSelector(node, selector)) {
      result.push(node);
    }
  }

  return result;
}

/**
 * Mutation events with the DOM4 MutationObserver
 *
 * @api private
 */
exports = module.exports = function (element, selector, mutationType, callback, persistent) {

  var init, type;

  if (typeof element === 'string') {
    element = query(element);
  }

  // must be an HTMLElement
  if (element.nodeType !== 1) {
    throw new TypeError('element must be inherit from HTMLElement');
  }

  // must be a valid mutation type
  if (!inArray(types, mutationType)) {
    throw new TypeError('unsupported type "' +  mutationType + '"');
  }

  init = {
    // monitor attribues (attribute)
    'attributes': false,
    // no need to record previous attribute values
    'attributeOldValue': false,
    // monitor decendants
    'subtree': true,
    // monitor children (insertion/removal)
    'childList': false,
    // record characterData changes
    'characterData': false,
    // keep previous characterData values
    'characterDataOldValue': false
  };

  if (mutationType === 'attribute') {
    // attribue change
    init.attributes = true;
  } else if (mutationType === 'inserted') {
    // node inserted
    init.childList = true;
    type = 'addedNodes';
  } else {
    // node removed
    init.childList = true;
    type = 'removedNodes';
  }

  new Observer(function (records) {
    var mutations = filter(records, selector, type);
    if (mutations.length) {
      if (!persistent) {
        this.disconnect();
      }
      return callback(mutations);
    }
  }).observe(element, init);
};

exports.mode = 'observer';
