
'use strict';

var docs = require('docs'),
    path = require('path'),
    globs = require('globs'),
    fs = require('fs'),
    mkdir = require('mkdirp'),
    pkg = require('./package.json'),
    log = require('component').utils.log;

var dir = path.join(__dirname, 'docs');

globs('src/**/component.json', function (err, components) {
  if (err) {
    throw err;
  }

  var scripts = [];

  components
    .filter(function (component) {
      return component.indexOf('/components/') === -1;
    })
    .forEach(function (component) {
      var root = path.dirname(component),
          json = fs.readFileSync(component).toString();

      json = JSON.parse(json);

      scripts = scripts.concat(json.scripts.map(function (script) {
        return path.join(__dirname, root, script);
      }));
    });

  mkdir('./docs', function (err) {
    if (err) {
      throw err;
    }

    var output = path.join(dir, 'amaze-' + pkg.version + '.html'),
        title = 'Amaze Runtime Docs (v' + pkg.version + ')';

    docs.generate(scripts, title, function (err, html) {
      if (err) {
        throw err;
      }

      fs.writeFileSync(output, html);
      log('wrote', title);
    });
  });
});
