/* globals assert:true */
'use strict';

var runtime = require('../..'),
    pkg = require('../../package.json'),
    assert = require('assert');

describe('require("runtime-library")', function () {
  it('should export the version number', function () {
    assert.equal(pkg.version, runtime.version);
  });

  it('should export its dox', function () {
    assert(typeof runtime.docs === 'string');
  });

  it('should export its code', function () {
    assert(typeof runtime.code === 'string');
  });

  it('should export a copyright', function () {
    assert(typeof runtime.copyright === 'string');
  });
});
