
describe('string.truncate', function () {
  'use strict';


  var string = require('string');
  var killbill = 'The lead character, called "The Bride", was a '
               + 'member of the Deadly Viper Assassination Squad, '
               + 'lead by her lover "Bill".';

  describe('truncate', function () {
    describe('truncate(str, len)', function () {
      it('should return the first "len" characters of the string', function () {
        var res = string.truncate(killbill, 18);

        assert.equal(18, res.length);
        assert.equal('The lead character', res);
      });
    });

    describe('truncate(str, len, suffix)', function () {
      it('should return the first "len" characters of the string and add the suffix', function () {
        assert.equal(string.truncate(killbill, 18, ' ...'), 'The lead character ...');
      });
    });
  });

  describe('truncate.left', function () {
    describe('truncate.left(str, len)', function () {
      it('should return the first "len" characters of the string', function () {
        var res = string.truncate.left(killbill, 18);

        assert.equal(18, res.length);
        assert.equal('The lead character', res);
      });
    });

    describe('truncate.left(str, len, suffix)', function () {
      it('should return the first "len" characters of the string and add the suffix', function () {
        assert.equal(string.truncate(killbill, 18, ' ...'),
          'The lead character ...');
      });
    });
  });

  describe('truncate.right', function () {
    describe('truncate.right(str, len)', function () {
      it('should return the last "len" characters of the string', function () {
        var res = string.truncate.right(killbill, 20);

        assert.equal(20, res.length);
        assert.equal('by her lover "Bill".', res);
      });
    });

    describe('truncate.right(str, len, prefix)', function () {
      it('should return the last "len" characters of the string and add the prefix', function () {
        assert.equal('... by her lover "Bill".', string.truncate.right(killbill, 20, '... '));
      });
    });
  });
});
