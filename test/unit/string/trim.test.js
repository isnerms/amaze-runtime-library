
describe('string.trim', function () {
  'use strict';

  var string = require('string');

  describe('trim(str)', function () {
    it('should remove whitespace from the beginning of a string', function () {
      assert.equal('hello world', string.trim('  \t hello world   '));
    });
  });

  describe('trim.left(str)', function () {
    it('should remove leading whitespace', function () {
      assert.equal('hello world   ', string.trim.left('  \t hello world   '));
    });
  });

  describe('trim.right(str)', function () {
    it('should remove trailing whitespace', function () {
      assert.equal('  \t hello world', string.trim.right('  \t hello world   '));
    });
  });
});
