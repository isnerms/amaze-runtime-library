
describe('css', function () {
  'use strict';

  var css = require('css');
  var append = css.append;
  var tagCount;
  var element;

  beforeEach(function () {
    tagCount = document.getElementsByTagName('style').length;
    element = document.createElement('div');
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  // Tests

  it('should maintain the current api', function () {
    var style = css('#hello { world: 40; }');
    assert.equal(style.type, 'text/css');
  });

  it('should get when given 2 arguments', function () {
    element.style.display = 'block';
    assert.equal('block', css(element, 'display'));
  });

  it('should set when given an element and an object', function () {
    css(element, { position: 'absolute', display: 'block' });
    assert.equal('absolute', css(element, 'position'));
    assert.equal('block', css(element, 'display'));
  });

  it('should set when given 3 arguments', function () {
    css(element, 'display', 'block');
    assert.equal('block', css(element, 'display'));
  });

  it('should throw when given 4 arguments', function () {
    var err = null;
    try {
      css(element, 'display', 'block', 'ninjas');
    } catch (e) {
      err = e;
    }
    assert.ok(err);
  });

  describe('.set', function () {
    it('should set a CSS property', function () {
      css(element, 'display', 'block');
      assert.equal('block', css(element, 'display'));
    });

    it('should automatically add "px"', function () {
      css(element, 'height', 12);
      assert.equal('12px', css(element, 'height'));
    });
  });

  describe('.get', function () {
    it('should get a CSS property', function () {
      element.style.display = 'block';
      assert.equal('block', css(element, 'display'));
    });
  });

  describe('.append', function () {
    it('should be a function', function () {
      assert.isFunction(append);
    });

    it('should insert a <style> tag into the document', function () {
      var s = append('#fixture { display: none; }');
      assert.equal('string', typeof s.id);
    });

    it('should operate on the same <style> tag, if available', function () {
      var res1 = append('#fixture { height: 100px; }');
      var res2 = append('#fixture { background-color: #669; }');
      assert.strictEqual(res1, res2);
    });

    it('should add rules', function () {
      var tag = append('#fixture { border: 3px solid #000; }');
      var content = (tag.styleSheet === undefined)
          ? tag.textContent
          : tag.styleSheet.cssText;
      assert.includes(content, '#fixture { border: 3px solid #000; }');
    });

    it('should not clobber existing rules', function () {
      var tag = append('#fixture { color: #FFF; }');
      var content = (tag.styleSheet === undefined)
          ? tag.textContent
          : tag.styleSheet.cssText;
      assert.includes(content, '#fixture { border: 3px solid #000; }');
    });
  });
});
