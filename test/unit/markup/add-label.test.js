describe('markup.addLabel', function () {
  'use strict';


  var collections = require('collections');
  var selector = require('selector');
  var addLabel = require('markup').addLabel;
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    fixture.id = 'fixture';
    document.body.appendChild(fixture);
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('sanity', function () {
    assert.equal(typeof addLabel, 'function');
  });

  it('literal text', function () {
    var meow,
      fixture = document.getElementById('fixture'),
      label = 'cats';

    fixture.innerHTML = '<input id="meow" class="cats" type="text"><input class="cats" type="text">';
    meow = document.getElementById('meow');


    // selector
    addLabel('.cats', label);
    collections.each(selector.queryAll('.cats'), function (element) {
      assert.equal(element.getAttribute('aria-label'), label,
        '`aria-label` should be added to all elements matching the selector');
    });

    // single element reference
    addLabel(meow, 'meow');
    assert.equal(meow.getAttribute('aria-label'), 'meow', 'Previous value should be overidden');
  });

  it('label as an element reference', function () {
    var input, label,
      fixture = document.getElementById('fixture');

    fixture.innerHTML =
      '<span id="label">Cats</span>' +
      '<b>Thing</b>' +
      '<input id="input1" type="text">' +
      '<input id="input2" type="text" aria-labelledby="label">' +
      '<input id="input3" type="text" aria-labelledby="meow">' +
      '<input id="input4" type="text" aria-labelledby="alabel">' +
      '<input id="input5" type="text" aria-labelledby="alabelb">' +
      '<input id="input6" type="text">' +
      '<input id="input7" type="text">';

    label = document.getElementById('label');


    input = document.getElementById('input1');
    addLabel(input, label);
    assert.equal(input.getAttribute('aria-labelledby'), label.id,
      'Label reference added');

    addLabel(input, label);
    assert.equal(input.getAttribute('aria-labelledby'), label.id,
      'Should not appened the same ID more than once');

    input = document.getElementById('input2');
    addLabel(input, label);
    assert.equal(input.getAttribute('aria-labelledby'), label.id,
      'Should not appened the same ID more than once');

    input = document.getElementById('input3');
    addLabel(input, label);
    assert.equal(input.getAttribute('aria-labelledby'), 'meow ' + label.id,
      'Label reference added');

    input = document.getElementById('input4');
    addLabel(input, label);
    assert.equal(input.getAttribute('aria-labelledby'), 'alabel ' + label.id,
      'No false positives on substring matches');

    input = document.getElementById('input5');
    addLabel(input, label);
    assert.equal(input.getAttribute('aria-labelledby'), 'alabelb ' + label.id,
      'No false positives on substring matches');

    input = document.getElementById('input6');
    label = fixture.getElementsByTagName('b')[0];
    addLabel(input, label);
    assert.equal(label.id, 'amaze_label_0');
    assert.equal(input.getAttribute('aria-labelledby'), 'amaze_label_0',
      'Should generate an ID');

    label.removeAttribute('id'); // ie is stupid
    input = document.getElementById('input7');
    addLabel(input, label);
    assert.equal(label.id, 'amaze_label_1');
    assert.equal(input.getAttribute('aria-labelledby'), 'amaze_label_1',
      'Generated ID should increment');

  });
  it('aria-describedby', function () {
    var input,
      fixture = document.getElementById('fixture');

    fixture.innerHTML =
      '<span id="db">Cats</span>' +
      '<b id="label">Thing</b>' +
      '<input id="input1" type="text">' +
      '<input id="input2" aria-describedby="label" type="text">';

    input = document.getElementById('input1');
    addLabel(input, 'label', 'db');
    // make sure "db" is added to aria-describedby
    assert.equal(input.getAttribute('aria-describedby'), 'db',
      'Describedby reference added');

    addLabel(input, 'label', 'db');
    // make sure we're not adding the "db" twice
    assert.equal(input.getAttribute('aria-describedby'), 'db',
      'Should not add the same attribute twice');


    input = document.getElementById('input2');
    addLabel(input, 'label', 'db');
    // make sure the db describedby is added
    assert.equal(input.getAttribute('aria-describedby'), 'label db',
      'Describedby reference added');
    //make sure 'db' isn't added twice
    addLabel(input, 'label', 'db');
    assert.equal(input.getAttribute('aria-describedby'), 'label db',
      'Should not add the same attribute twice');

  });
  //aria-labelledby (AM-32)
  it('aria-labelledby', function () {
    var input, db,
      fixture = document.getElementById('fixture');

    fixture.innerHTML =
      '<span id="db">Cats</span>' +
      '<b id="label">Thing</b>' +
      '<input id="input1" type="text">' +
      '<input id="input2" aria-labelledby="label" type="text">';

    // adding aria-labelledby="db" to the input1
    input = document.getElementById('input1');
    db = document.getElementById('db');
    addLabel(input, db);
    assert.equal(input.getAttribute('aria-labelledby'), 'db',
      'labelledby reference added');

    // ensure that db is not added twice into the aria-labelledby
    addLabel(input, db);
    assert.equal(input.getAttribute('aria-labelledby'), 'db',
      'Should not add the same attribute twice');


    // adding db to the aria-labelledby which already contains label
    // result looks like: aria-labelledby="label db"
    input = document.getElementById('input2');
    db = document.getElementById('db');
    addLabel(input, db);
    assert.equal(input.getAttribute('aria-labelledby'), 'label db',
      'labelledby reference added');

    // Making sure db is not added twice
    addLabel(input, db);
    assert.equal(input.getAttribute('aria-labelledby'), 'label db',
      'Should not add the same attribute twice');

  });

  it('bad parameters', function () {

    assert.throws(function () {
        addLabel('.cats', []);
      }, Error,
      'Should throw if second parameter is not text or element reference');

    assert.throws(function () {
        addLabel('.cats', null);
      }, Error,
      'Should throw if second parameter is not text or element reference');

    assert.throws(function () {
        addLabel('.cats', undefined);
      }, Error,
      'Should throw if second parameter is not text or element reference');

    assert.throws(function () {
        addLabel('.cats', false);
      }, Error,
      'Should throw if second parameter is not text or element reference');

    assert.throws(function () {
        addLabel('.cats', {});
      }, Error,
      'Should throw if second parameter is not text or element reference');

  });

});
