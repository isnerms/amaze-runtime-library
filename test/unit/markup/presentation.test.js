describe('markup.presentation', function () {
  'use strict';

  var each = require('collections').each;
  var markup = require('markup');
  var selector = require('selector');

  afterEach(function () {
    document.getElementById('fixture').innerHTML = '';
  });


  it('sanity', function () {
    assert.equal(typeof markup.presentation, 'function');
  });

  it('selector', function () {
    var elements,
      fixture = document.getElementById('fixture');

    fixture.innerHTML = '<table><tr><td></td></tr></table><img src="about:blank">' +
      '<table><tr><td></td></tr></table><img src="about:blank">' +
      '<table><tr><td></td></tr></table><img src="about:blank">' +
      '<table><tr><td></td></tr></table><img src="about:blank">';

    markup.presentation('table, img');

    elements = fixture.getElementsByTagName('table');
    each(elements, function (element) {
      assert.equal(element.getAttribute('role'), 'presentation');
    });

    elements = fixture.getElementsByTagName('img');
    each(elements, function (element) {
      assert.equal(element.getAttribute('role'), 'presentation');
      assert.strictEqual(element.getAttribute('alt'), '');
    });
  });

  it('NodeList', function () {
    var elements,
      fixture = document.getElementById('fixture');

    if (window.navigator.userAgent.indexOf('PhantomJS') !== -1) {
      assert.ok(true, 'typeof NodeList === "function"?');
      return;
    }

    fixture.innerHTML = '<table><tr><td></td></tr></table><img src="about:blank">' +
      '<table><tr><td></td></tr></table><img src="about:blank">' +
      '<table><tr><td></td></tr></table><img src="about:blank">' +
      '<table><tr><td></td></tr></table><img src="about:blank">';

    elements = fixture.getElementsByTagName('table');
    markup.presentation(elements);

    each(elements, function (element) {
      assert.equal(element.getAttribute('role'), 'presentation');
    });

    elements = fixture.getElementsByTagName('img');
    markup.presentation(elements);

    each(elements, function (element) {
      assert.equal(element.getAttribute('role'), 'presentation');
      assert.strictEqual(element.getAttribute('alt'), '');
    });
  });


  it('Array', function () {
    var elements,
      fixture = document.getElementById('fixture');

    fixture.innerHTML = '<table><tr><td></td></tr></table><img src="about:blank">' +
      '<table><tr><td></td></tr></table><img src="about:blank">' +
      '<table><tr><td></td></tr></table><img src="about:blank">' +
      '<table><tr><td></td></tr></table><img src="about:blank">';

    elements = selector.queryAll('table', fixture);
    markup.presentation(elements);

    each(elements, function (element) {
      assert.equal(element.getAttribute('role'), 'presentation');
    });

    elements = selector.queryAll('img', fixture);
    markup.presentation(elements);

    each(elements, function (element) {
      assert.equal(element.getAttribute('role'), 'presentation');
      assert.strictEqual(element.getAttribute('alt'), '');
    });
  });

  it('Single Node Reference', function () {
    var element,
      fixture = document.getElementById('fixture');

    fixture.innerHTML = '<table id="table-cat"><tr><td></td></tr></table><img id="img-cat" src="about:blank">' +
      '<table><tr><td></td></tr></table><img src="about:blank">' +
      '<table><tr><td></td></tr></table><img src="about:blank">' +
      '<table><tr><td></td></tr></table><img src="about:blank">';

    element = document.getElementById('table-cat');

    markup.presentation(element);
    assert.equal(element.getAttribute('role'), 'presentation');

    element = document.getElementById('img-cat');

    markup.presentation(element);
    assert.equal(element.getAttribute('role'), 'presentation');
    assert.strictEqual(element.getAttribute('alt'), '');

  });

});
