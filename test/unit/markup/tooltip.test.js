// TODO
//
//   - fix this text
//   - all skips should be rewritten
//   - don't modify implied globals for the sake of testing, n00bs
//

/*global createTooltip, getViewportSize, getPosition,
  addTooltipClasses, fits, checkPositions, calculatePosition,
  calculatePositionX, calculatePositionY */
describe('markup.tooltip', function () {
  'use strict';


  function closeTo(actual, expected, difference, msg) {
    return assert.ok(actual + difference >= expected && actual - difference <= expected, msg);
  }

  var markup = require('markup');
  var tooltip = markup.tooltip;
  var events = require('events');
  var array = require('array');
  var dom = require('dom');
  var collections = require('collections');
  var selector = require('selector');

  var fixture = document.getElementById('fixture');

  beforeEach(function () {
    fixture.innerHTML = '<a href="#" id="target">Target</a>';
  });

  afterEach(function () {
    fixture.innerHTML = '';
    collections.each(selector.queryAll('.amaze-tooltip'), function (tip) {
      tip.parentNode.removeChild(tip);
    });
  });

  describe('options', function () {
    it('should set defaults', function () {

      var tip = tooltip('#target', 'This is a tooltip');
      assert.deepEqual(tip.options, {
        axis: 'y',
        align: 'ne',
        fit: true,
        mouseEvents: true,
        delay: 500,
        offset: 4
      });

    });
    it('should should override defaults', function () {
      var tip,
        expected = {
          axis: 'x',
          align: 'se',
          fit: false,
          mouseEvents: false,
          delay: 250,
          offset: 27
        };

      tip = tooltip('#target', 'This is a tooltip', expected);
      assert.deepEqual(tip.options, expected);

    });

  });

  describe('keyboard events', function () {
    it('should set a 0ms timeout on focus', function () {
      var target, tip,
        called = 0,
        original = {
          setTimeout: window.setTimeout
        };

      window.setTimeout = function (fn, ms) {
        assert.equal(ms, 0);
        fn();
      };

      target = document.getElementById('target');

      tip = tooltip(target, 'This is a tooltip');
      tip.show = function () {
        called++;
      };

      events.fire('#target', 'focus');


      assert.equal(called, 1, 'showTooltip should have been called');

      window.setTimeout = original.setTimeout;
    });

    it('should hide the tooltip on blur and clear focus timeouts', function () {

      var target, tip, timeoutID,
        called = 0,
        original = {
          setTimeout: window.setTimeout,
          clearTimeout: window.clearTimeout
        };

      window.setTimeout = function (fn, ms) {
        if (original.setTimeout.call) {
          timeoutID = original.setTimeout.call(window, function () {
            assert.ok(false);
          }, ms);
        } else {
          timeoutID = original.setTimeout(function () {
            assert.ok(false);
          }, ms);
        }
        return timeoutID;
      };

      window.clearTimeout = function (actualTimeoutID) {
        assert.equal(actualTimeoutID, timeoutID);
        called++;
        if (original.clearTimeout.call) {
          original.clearTimeout.call(window, timeoutID);
        } else {
          original.clearTimeout(timeoutID);
        }
      };

      target = document.getElementById('target');

      tip = tooltip(target, 'This is a tooltip');

      tip.hide = function () {
        called++;
      };

      events.fire(target, 'focus');
      events.fire(target, 'blur');


      assert.equal(called, 2, 'hideTooltip and clearTimeout should have been called');

      window.setTimeout = original.setTimeout;
      window.clearTimeout = original.clearTimeout;
    });

  });

  describe('mouse events', function () {

    it('should not be bound if `options.mouseEvent === false`', function () {
      var called = 0,
        original = {
          eventsOn: events.on
        };

      events.on = function (element, type) {
        called++;

        if (type.indexOf('mouse') === 0) {
          return assert.ok(false, 'Should not bind mouse events');
        }

        return false;
      };

      tooltip('#target', 'foo', {
        mouseEvents: false
      });


      assert.equal(called, 2, 'on should be called for keyboard events only');

      events.on = original.eventsOn;
    });

    it('should show the tooltip on mouseover of the target and clear any mouse timeouts', function () {
      var tip,
        target = document.getElementById('target'),
        called = 0;


      tip = tooltip(target, 'stuff');
      tip.show = function () {
        called++;
      };
      events.fire(target, 'mouseover');

      assert.equal(called, 1);

    });

    it('should set a timeout on mouseout of the target', function () {
      var tip,
        timeoutID = 0,
        target = document.getElementById('target'),
        called = 0,
        original = {
          setTimeout: window.setTimeout
        };

      window.setTimeout = function (fn) {
        called++;
        fn();
        return timeoutID++;
      };

      tip = tooltip(target, 'stuff');

      tip.hide = function () {
        called++;
      };

      events.fire('#target', 'mouseout');

      assert.equal(called, 2);

      window.setTimeout = original.setTimeout;
    });

    it.skip('should set a timeout on mouseout of the tooltip', function () {
      var tip,
        timeoutID = 0,
        target = document.getElementById('target'),
        called = 0,
        original = {
          setTimeout: window.setTimeout
        };

      window.setTimeout = function (fn) {
        called++;
        fn();
        return timeoutID++;
      };

      tip = tooltip(target, 'stuff');
      createTooltip(tip);

      tip.hide = function () {
        called++;
      };

      events.fire(tip.tooltip, 'mouseout');

      assert.equal(called, 2);


      window.setTimeout = original.setTimeout;
    });

    it('should clear the timeout on mouseover of the target', function () {
      var timeoutID, tip,
        target = document.getElementById('target'),
        called = 0,
        original = {
          setTimeout: window.setTimeout,
          clearTimeout: window.clearTimeout
        };

      window.setTimeout = function (fn, ms) {
        if (original.setTimeout.call) {
          timeoutID = original.setTimeout.call(window, function () {
            assert.ok(false);
          }, ms);
        } else {
          timeoutID = original.setTimeout(function () {
            assert.ok(false);
          }, ms);

        }
        return timeoutID;
      };

      window.clearTimeout = function (actualTimeoutID) {
        assert.equal(actualTimeoutID, timeoutID);
        called++;
        if (original.clearTimeout.call) {
          original.clearTimeout.call(window, actualTimeoutID);
        } else {
          original.clearTimeout(actualTimeoutID);
        }
      };

      tip = tooltip(target, 'stuff');

      tip.show = function () {
        called++;
      };

      events.fire(target, 'mouseout');
      events.fire(target, 'mouseover');

      assert.equal(called, 2);


      window.setTimeout = original.setTimeout;
      window.clearTimeout = original.clearTimeout;
    });

    it.skip('should clear the timeout on mouseover of the tooltip', function () {
      var timeoutID, tip,
        target = document.getElementById('target'),
        called = 0,
        original = {
          setTimeout: window.setTimeout,
          clearTimeout: window.clearTimeout
        };

      window.setTimeout = function (fn, ms) {
        if (original.setTimeout.call) {
          timeoutID = original.setTimeout.call(window, function () {
            assert.ok(false);
          }, ms);
        } else {
          timeoutID = original.setTimeout(function () {
            assert.ok(false);
          }, ms);

        }
        return timeoutID;
      };

      window.clearTimeout = function (actualTimeoutID) {
        assert.equal(actualTimeoutID, timeoutID);
        called++;
        if (original.clearTimeout.call) {
          original.clearTimeout.call(window, actualTimeoutID);
        } else {
          original.clearTimeout(actualTimeoutID);
        }
      };

      tip = tooltip(target, 'stuff');
      createTooltip(tip);

      tip.show = function () {
        assert.ok(false);
      };

      events.fire(target, 'mouseout');
      events.fire(tip.tooltip, 'mouseover');

      assert.equal(called, 1);


      window.setTimeout = original.setTimeout;
      window.clearTimeout = original.clearTimeout;
    });

  });


  describe('Tooltip#show', function () {
    it.skip('should call createTooltip', function () {

      var called = 0,
        original = window.createTooltip,
        target = document.getElementById('target'),
        tip = tooltip(target, 'stuff');

      window.createTooltip = function (obj) {
        assert.equal(obj, tip);
        called++;
        original(obj);
      };

      tip.show();
      assert.equal(called, 1);
      window.createTooltip = original;
    });

    it('should call Tooltip#position', function () {

      var tip,
        called = 0,
        target = document.getElementById('target');

      tip = tooltip(target, 'stuff');
      tip._position = tip.position;
      tip.position = function (axis, align, fit) {
        assert.equal(axis, tip.options.axis);
        assert.equal(align, tip.options.align);
        assert.equal(fit, tip.options.fit);
        called++;

      };

      tip.show();
      assert.equal(called, 1);

    });

  });

  describe('Tooltip#hide', function () {

    it.skip('should destroy the tooltip element', function () {

      var el,
        target = document.getElementById('target'),
        tip = tooltip(target, 'stuff');

      createTooltip(tip);
      el = tip.tooltip;

      tip.hide();

      assert.equal(tip.tooltip, null);

      // can't check if parentNode is null, because IE is stupid and says the
      // parent node is [HTMLDocument]
      assert.equal(document.documentElement.contains(el), false);

      el = null;

    });

    it('should not error if the tooltip has not been created yet', function () {

      assert.doesNotThrow(function () {
        tooltip('#target', 'content').hide();
      });
    });
  });

  describe('Tooltip#position', function () {
    it('should call createTooltip if the tooltip does not exist', function () {
      var tip = tooltip('#target', 'hello');
      assert.equal(tip.tooltip, null);

      tip.position();

      assert.notEqual(tip.tooltip, null);

    });

    it('should default to options if parameter is not specified', function () {
      var tip,
        called = 0,
        options = {
          axis: 'x',
          align: 'e',
          fit: false,
          offset: 4
        },
        original = {
          getPosition: window.getPosition,
          addTooltipClasses: window.addTooltipClasses
        };

      window.getPosition = function (_, __, actualOptions) {
        assert.deepEqual(actualOptions, options);
        called++;
        return {
          top: 1,
          left: 1
        };
      };

      window.addTooltipClasses = function () {};

      tip = tooltip('#target', 'hello', options);
      tip.position();


      window.getPosition = original.getPosition;
      window.addTooltipClasses = original.addTooltipClasses;
    });

    it('should default to options if parameter is null', function () {
      var tip,
        called = 0,
        options = {
          axis: 'x',
          align: 'e',
          fit: false,
          offset: 4
        },
        original = {
          getPosition: window.getPosition,
          addTooltipClasses: window.addTooltipClasses
        };

      window.getPosition = function (_, __, actualOptions) {
        assert.deepEqual(actualOptions, options);
        called++;
        return {
          top: 1,
          left: 1
        };
      };

      window.addTooltipClasses = function () {};

      tip = tooltip('#target', 'hello', options);
      tip.position(null, null, null);


      window.getPosition = original.getPosition;
      window.addTooltipClasses = original.addTooltipClasses;
    });

    it.skip('should construct params object and pass it to getPosition', function () {

      var tip,
        called = 0,
        original = {
          getPosition: window.getPosition,
          addTooltipClasses: window.addTooltipClasses
        };

      window.getPosition = function (target, tooltip, params) {
        assert.deepEqual(params, {
          offset: 4,
          axis: 'x',
          align: 'se',
          fit: false
        });
        called++;
        return {
          top: 1,
          left: 1
        };
      };

      window.addTooltipClasses = function () {};

      tip = tooltip('#target', 'hello');
      tip.position('x', 'se', false);

      assert.equal(called, 1);

      window.getPosition = original.getPosition;
      window.addTooltipClasses = original.addTooltipClasses;
    });

    it.skip('should call addTooltipClasses', function () {
      var tip,
        original = {
          getPosition: window.getPosition,
          addTooltipClasses: window.addTooltipClasses
        };

      window.getPosition = function () {
        return {
          top: 1,
          left: 2,
          dir: 'nw',
          axis: 'x'
        };
      };

      window.addTooltipClasses = function () {};

      tip = tooltip('#target', 'hello');
      tip.position();

      assert.equal(tip.tooltip.style.left, '2px');
      assert.equal(tip.tooltip.style.top, '1px');


      window.getPosition = original.getPosition;
      window.addTooltipClasses = original.addTooltipClasses;

    });

    it('should set top and left', function () {
      var tip,
        called = 0,
        original = {
          getPosition: window.getPosition,
          addTooltipClasses: window.addTooltipClasses
        };

      window.getPosition = function () {
        return {
          top: 1,
          left: 1,
          align: 'nw',
          axis: 'x'
        };
      };

      window.addTooltipClasses = function (tooltip, align, axis) {
        assert.equal(tooltip, tip.tooltip);
        assert.equal(align, 'nw');
        assert.equal(axis, 'x');
        called++;
      };

      tip = tooltip('#target', 'hello');
      tip.position();


      window.getPosition = original.getPosition;
      window.addTooltipClasses = original.addTooltipClasses;
    });
  });

  describe('createTooltip', function () {
    it.skip('should create a div, assign properties and bind mouse events', function () {
      var tip = tooltip('#target', 'stuff'),
        events = [],
        original = events.on;

      events.on = function (target, type) {
        events.push({
          target: target,
          type: type
        });
      };

      createTooltip(tip);

      assert.equal(tip.tooltip.className, 'amaze-tooltip');
      assert.equal(tip.tooltip.id, tip.id);
      assert.equal(tip.tooltip.innerHTML, 'stuff');
      assert.equal(tip.tooltip.getAttribute('aria-hidden'), 'true');

      assert.deepEqual(events, [{
          type: 'mouseout',
          target: tip.tooltip
        }, {
          type: 'mouseover',
          target: tip.tooltip
        }]);

      events.on = original;
    });

    it.skip('should append options.class to tooltips class', function () {
      var tip = tooltip('#target', 'stuff', {
        'class': 'things'
      });

      createTooltip(tip);

      assert.equal(tip.tooltip.className, 'amaze-tooltip things');
    });

    it.skip('should optionally disable mouse events', function () {
      var called = 0,
        tip = tooltip('#target', 'stuff', {
          mouseEvents: false
        }),
        original = events.on;

      events.on = function () {
        called++;
        assert.ok(false);
      };

      createTooltip(tip);

      assert.equal(called, 0);

      events.on = original;
    });
  });

  describe('getPosition', function () {
    it.skip('should use the specified position if !options.fit', function () {
      var tip = tooltip('#target', 'stuff'),
        position = getPosition(tip.target, tip.tooltip, {
          axis: 'y',
          align: 'ne',
          offset: 5,
          fit: false
        });

      assert.equal(position.axis, 'y');
      assert.equal(position.align, 'ne');
    });

    it.skip('should attempt to position using the default or specified position/axis first', function () {
      var tip, position,
        original = window.fits;

      window.fits = function () {
        return true;
      };
      tip = tooltip('#target', 'stuff');
      position = getPosition(tip.target, tip.tooltip, {
        axis: 'y',
        align: 'ne',
        offset: 5,
        fit: true
      });

      assert.equal(position.axis, 'y');
      assert.equal(position.align, 'ne');


      window.fits = original;
    });

    it.skip('should use the default position nothing can fit', function () {
      var tip, position,
        original = window.fits;

      window.fits = function () {
        return false;
      };
      tip = tooltip('#target', 'stuff');
      position = getPosition(tip.target, tip.tooltip, {
        axis: 'y',
        align: 'ne',
        offset: 5,
        fit: true
      });

      assert.equal(position.axis, 'y');
      assert.equal(position.align, 'ne');


      window.fits = original;
    });

    it.skip('should call checkPositions if default does not fit', function () {
      var tip, position,
        called = 0,
        original = window.checkPositions;

      window.checkPositions = function (_, axis, align, offset) {
        assert.equal(axis, 'y');
        assert.equal(align, 'se');
        assert.equal(offset, 5);
        called++;
        return 'truthy';
      };

      tip = tooltip('#target', 'stuff');
      position = getPosition(tip.target, tip.tooltip, {
        axis: 'y',
        align: 'se',
        offset: 5,
        fit: true
      });

      assert.equal(called, 1);

      window.checkPositions = original;
    });

    it.skip('should swap axis is the cannot fit with the default axis', function () {
      var tip, position,
        called = 0,
        original = window.checkPositions;

      window.checkPositions = function (_, axis, align, offset) {
        if (axis === 'x') {
          return false;
        }
        assert.equal(axis, 'y');
        assert.equal(align, null);
        assert.equal(offset, 5);
        called++;
        return 'truthy';
      };

      tip = tooltip('#target', 'stuff');
      position = getPosition(tip.target, tip.tooltip, {
        axis: 'x',
        align: 'se',
        offset: 5,
        fit: true
      });

      assert.equal(called, 1);

      window.checkPositions = original;
    });

  });

  describe('calculatePosition', function () {
    it.skip('should call calculatePositionX if axis === x', function () {
      var result,
        called = 0,
        boxes = {
          tooltip: 'stuff',
          target: 'other stuff'
        },
        original = window.calculatePositionX;

      window.calculatePositionX = function (target, tooltip, align, offset) {
        assert.equal(target, boxes.target);
        assert.equal(tooltip, boxes.tooltip);
        assert.equal(align, 'whatever');
        assert.equal(offset, 42);
        called++;
        return {};
      };

      result = calculatePosition(boxes, 'x', 'whatever', 42);
      assert.equal(called, 1);

      assert.equal(result.axis, 'x');
      assert.equal(result.align, 'whatever');

      window.calculatePositionX = original;
    });

    it.skip('should call calculatePositionY if axis === y', function () {
      var result,
        called = 0,
        boxes = {
          tooltip: 'stuff',
          target: 'other stuff'
        },
        original = window.calculatePositionY;

      window.calculatePositionY = function (target, tooltip, align, offset) {
        assert.equal(target, boxes.target);
        assert.equal(tooltip, boxes.tooltip);
        assert.equal(align, 'whatevertoo');
        assert.equal(offset, 12);
        called++;
        return {};
      };

      result = calculatePosition(boxes, 'y', 'whatevertoo', 12);
      assert.equal(called, 1);

      assert.equal(result.axis, 'y');
      assert.equal(result.align, 'whatevertoo');

      window.calculatePositionY = original;
    });
  });
  describe('checkPositions', function () {
    it.skip('should loop over each direction until it finds open that fits', function () {
      var original = {
          fits: window.fits,
          calculatePosition: window.calculatePosition
        },
        possible = {
          x: ['ne', 'e', 'se', 'nw', 'w', 'sw'],
          y: ['ne', 'n', 'nw', 'se', 's', 'sw']
        };

      window.fits = function () {
        return false;
      };

      window.calculatePosition = function (_, axis, align) {
        var index = array.indexOf(possible[axis], align);
        possible[axis].splice(index, 1);
      };

      checkPositions({}, 'y');
      assert.equal(possible.y.length, 0);

      checkPositions({}, 'x');
      assert.equal(possible.x.length, 0);

      window.fits = original.fits;
      window.calculatePosition = original.calculatePosition;
    });

    it.skip('should start at the indexOf `align`', function () {
      var called = 0,
        original = {
          fits: window.fits,
          calculatePosition: window.calculatePosition
        };

      window.fits = function () {
        return true;
      };

      window.calculatePosition = function (_, axis, align) {
        assert.equal(axis, 'x');
        assert.equal(align, 'w');
        called++;
      };

      checkPositions({}, 'x', 'w');

      assert.equal(called, 1);

      window.fits = original.fits;
      window.calculatePosition = original.calculatePosition;
    });
  });

  // values are tested by integration tests
  describe('calculatePositionX', function () {
    it.skip('should return an object with top and left properties', function () {
      function assertions(align) {
        var result = calculatePositionX(mockTarget, mockTip, align, 0);
        assert.isTypeOf(result.top, 'number');
        assert.isTypeOf(result.left, 'number');
      }

      var mockTarget = {
          top: 10,
          left: 10,
          width: 10,
          height: 10
        },
        mockTip = {
          width: 10,
          height: 10
        };

      assertions('ne');
      assertions('e');
      assertions('se');
      assertions('nw');
      assertions('w');
      assertions('sw');
    });
  });

  // values are tested by integration tests
  describe('calculatePositionY', function () {
    it.skip('should return an object with top and left properties', function () {
      function assertions(align) {
        var result = calculatePositionY(mockTarget, mockTip, align, 0);
        assert.isTypeOf(result.top, 'number');
        assert.isTypeOf(result.left, 'number');
      }

      var mockTarget = {
          top: 10,
          left: 10,
          width: 10,
          height: 10
        },
        mockTip = {
          width: 10,
          height: 10
        };

      assertions('n');
      assertions('ne');
      assertions('nw');
      assertions('s');
      assertions('se');
      assertions('sw');
    });
  });


  describe('addTooltipClasses', function () {

    it.skip('should remove the classes `n s e w x y`', function () {
      var tip = document.createElement('div');
      tip.className = 'n s e w x y';

      addTooltipClasses(tip, '', '');
      assert.equal(tip.className, '');
    });

    it.skip('should split second paramter and add each character to the className', function () {

      var tip = document.createElement('div');
      tip.className = 'n s e w x y';

      addTooltipClasses(tip, 'thing', '');
      assert.equal(tip.className, 't h i n g');
    });

    it.skip('should add third paramter to className if truthy', function () {

      var tip = document.createElement('div');
      tip.className = 'n s e w x y';

      addTooltipClasses(tip, '', 'x');
      assert.equal(tip.className.replace(/^\s+|\s+/, ''), 'x');
    });
  });

  describe('getViewportSize', function () {
    it.skip('should return an object with width and height', function () {
      var size = getViewportSize();
      assert.isTypeOf(size.width, 'number');
      assert.isTypeOf(size.height, 'number');
    });
  });

  describe('fits', function () {
    it.skip('should return false if the top edge is off-screen', function () {
      var boxes = {
          scroll: {
            x: 50,
            y: 100
          },
          viewport: {
            width: 500,
            height: 500
          },
          tooltip: {
            width: 100,
            height: 100
          }
        },
        candidate = {
          top: 99,
          left: 50
        };

      assert.ok(!fits(candidate, boxes));
    });
    it.skip('should return false if the left edge is off-screen', function () {
      var boxes = {
          scroll: {
            x: 50,
            y: 100
          },
          viewport: {
            width: 500,
            height: 500
          },
          tooltip: {
            width: 100,
            height: 100
          }
        },
        candidate = {
          top: 100,
          left: 49
        };

      assert.ok(!fits(candidate, boxes));
    });
    it.skip('should return false if the right edge is off-screen', function () {
      var boxes = {
          scroll: {
            x: 50,
            y: 100
          },
          viewport: {
            width: 500,
            height: 500
          },
          tooltip: {
            width: 100,
            height: 100
          }
        },
        candidate = {
          top: 100,
          left: 451
        };

      assert.ok(!fits(candidate, boxes));
    });
    it.skip('should return false if the bottom edge is off-screen', function () {
      var boxes = {
          scroll: {
            x: 50,
            y: 100
          },
          viewport: {
            width: 500,
            height: 500
          },
          tooltip: {
            width: 100,
            height: 100
          }
        },
        candidate = {
          top: 501,
          left: 450
        };

      assert.ok(!fits(candidate, boxes));
    });
    it.skip('should return true if all edges are on-screen', function () {
      var boxes = {
          scroll: {
            x: 100,
            y: 100
          },
          viewport: {
            width: 500,
            height: 500
          },
          tooltip: {
            width: 300,
            height: 300
          }
        },
        candidate = {
          top: 200,
          left: 200
        };

      assert.ok(fits(candidate, boxes));
    });
  });

  describe('integration - no auto-fit', function () {

    describe('y axis', function () {
      describe('align ne', function () {
        it('should align the tooltip to the upper left corner', function () {
          var targetBox, tooltipBox,
            tip = tooltip('#target', 'things', {
              fit: false,
              axis: 'y',
              align: 'ne',
              offset: 27
            });

          tip.show();
          targetBox = dom.getElementCoordinates(tip.target);
          tooltipBox = dom.getElementCoordinates(tip.tooltip);

          closeTo(tooltipBox.left, targetBox.left, 0.02);
          closeTo(tooltipBox.bottom, targetBox.top - 27, 0.02);
        });
      });

      describe('align n', function () {
        it('should align the tooltip to the top center', function () {
          var targetBox, tooltipBox, targetCenter, tooltipCenter,
            tip = tooltip('#target', 'things', {
              fit: false,
              axis: 'y',
              align: 'n',
              offset: 12
            });

          tip.show();
          targetBox = dom.getElementCoordinates(tip.target);
          tooltipBox = dom.getElementCoordinates(tip.tooltip);

          targetCenter = (targetBox.left + (targetBox.width / 2));
          tooltipCenter = (tooltipBox.left + (tooltipBox.width / 2));

          closeTo(tooltipCenter, targetCenter, 1);
          closeTo(tooltipBox.bottom, targetBox.top - 12, 0.02);
        });
      });

      describe('align nw', function () {
        it('should align the tooltip to the upper right corner', function () {
          var targetBox, tooltipBox,
            tip = tooltip('#target', 'things', {
              fit: false,
              axis: 'y',
              align: 'nw',
              offset: 19
            });

          tip.show();
          targetBox = dom.getElementCoordinates(tip.target);
          tooltipBox = dom.getElementCoordinates(tip.tooltip);

          closeTo(tooltipBox.right, targetBox.right, 0.02);
          closeTo(tooltipBox.bottom, targetBox.top - 19, 0.02);
        });
      });

      describe('align se', function () {
        it('should align the tooltip to the lower left corner', function () {
          var targetBox, tooltipBox,
            tip = tooltip('#target', 'things', {
              fit: false,
              axis: 'y',
              align: 'se',
              offset: 13
            });

          tip.show();
          targetBox = dom.getElementCoordinates(tip.target);
          tooltipBox = dom.getElementCoordinates(tip.tooltip);

          closeTo(tooltipBox.left, targetBox.left, 0.02);
          closeTo(tooltipBox.top, targetBox.bottom + 13, 0.02);
        });
      });

      describe('align s', function () {
        it('should align the tooltip to the bottom center', function () {

          var targetBox, tooltipBox, targetCenter, tooltipCenter,
            tip = tooltip('#target', 'things', {
              fit: false,
              axis: 'y',
              align: 's',
              offset: 76
            });

          tip.show();
          targetBox = dom.getElementCoordinates(tip.target);
          tooltipBox = dom.getElementCoordinates(tip.tooltip);

          targetCenter = (targetBox.left + (targetBox.width / 2));
          tooltipCenter = (tooltipBox.left + (tooltipBox.width / 2));

          closeTo(tooltipCenter, targetCenter, 1);
          closeTo(tooltipBox.top, targetBox.bottom + 76, 0.02);
        });
      });

      describe('align sw', function () {
        it('should align the tooltip to the lower right corner', function () {
          var targetBox, tooltipBox,
            tip = tooltip('#target', 'things', {
              fit: false,
              axis: 'y',
              align: 'sw',
              offset: 13
            });

          tip.show();
          targetBox = dom.getElementCoordinates(tip.target);
          tooltipBox = dom.getElementCoordinates(tip.tooltip);

          closeTo(tooltipBox.right, targetBox.right, 0.02);
          closeTo(tooltipBox.top, targetBox.bottom + 13, 0.02);
        });
      });
    });

    describe('x axis', function () {
      describe('align ne', function () {
        it('should align the tooltip to the upper left corner', function () {
          var targetBox, tooltipBox,
            tip = tooltip('#target', 'things', {
              fit: false,
              axis: 'x',
              align: 'ne',
              offset: 41
            });

          tip.show();
          targetBox = dom.getElementCoordinates(tip.target);
          tooltipBox = dom.getElementCoordinates(tip.tooltip);

          closeTo(tooltipBox.left, targetBox.right + 41, 0.02);
          closeTo(tooltipBox.top, targetBox.top, 0.02);
        });
      });

      describe('align nw', function () {
        it('should align the tooltip to the upper right corner', function () {
          var targetBox, tooltipBox,
            tip = tooltip('#target', 'things', {
              fit: false,
              axis: 'x',
              align: 'nw',
              offset: 41
            });

          tip.show();
          targetBox = dom.getElementCoordinates(tip.target);
          tooltipBox = dom.getElementCoordinates(tip.tooltip);

          closeTo(tooltipBox.right, targetBox.left - 41, 0.02);
          closeTo(tooltipBox.top, targetBox.top, 0.02);
        });
      });
      describe('align se', function () {
        it('should align the tooltip to the lower left corner', function () {
          var targetBox, tooltipBox,
            tip = tooltip('#target', 'things', {
              fit: false,
              axis: 'x',
              align: 'se',
              offset: 9
            });

          tip.show();
          targetBox = dom.getElementCoordinates(tip.target);
          tooltipBox = dom.getElementCoordinates(tip.tooltip);

          closeTo(tooltipBox.left, targetBox.right + 9, 0.02);
          closeTo(tooltipBox.bottom, targetBox.bottom, 0.02);
        });
      });

      describe('align sw', function () {
        it('should align the tooltip to the lower right corner', function () {
          var targetBox, tooltipBox,
            tip = tooltip('#target', 'things', {
              fit: false,
              axis: 'x',
              align: 'sw',
              offset: 8
            });

          tip.show();
          targetBox = dom.getElementCoordinates(tip.target);
          tooltipBox = dom.getElementCoordinates(tip.tooltip);

          closeTo(tooltipBox.right, targetBox.left - 8, 0.1);
          closeTo(tooltipBox.bottom, targetBox.bottom, 0.1);
        });
      });

      describe('align w', function () {
        it('should align the tooltip to the left center', function () {
          var targetBox, tooltipBox, targetCenter, tooltipCenter,
            tip = tooltip('#target', 'things', {
              fit: false,
              axis: 'x',
              align: 'w',
              offset: 3
            });

          tip.show();
          targetBox = dom.getElementCoordinates(tip.target);
          tooltipBox = dom.getElementCoordinates(tip.tooltip);

          targetCenter = (targetBox.top + (targetBox.height / 2));
          tooltipCenter = (tooltipBox.top + (tooltipBox.height / 2));

          closeTo(tooltipBox.right, targetBox.left - 3, 0.02);
          closeTo(tooltipCenter, targetCenter, 1);
        });
      });

      describe('align e', function () {
        it('should align the tooltip to the right center', function () {
          var targetBox, tooltipBox, targetCenter, tooltipCenter,
            tip = tooltip('#target', 'things', {
              fit: false,
              axis: 'x',
              align: 'e',
              offset: 7
            });

          tip.show();
          targetBox = dom.getElementCoordinates(tip.target);
          tooltipBox = dom.getElementCoordinates(tip.tooltip);

          targetCenter = (targetBox.top + (targetBox.height / 2));
          tooltipCenter = (tooltipBox.top + (tooltipBox.height / 2));

          closeTo(tooltipBox.left, targetBox.right + 7, 0.02);
          closeTo(tooltipCenter, targetCenter, 1);
        });
      });

    });

  });

  describe('integration - auto-fit', function () {
    var target;
    beforeEach(function () {
      target = document.getElementById('target');
      target.style.position = 'absolute';

      // make sure the fixture is on screen, so we don't fail
      // for no reason
      var fixture = document.getElementById('fixture');
      fixture.style.position = 'fixed';
      fixture.style.top = '0';
      fixture.style.left = '0';
      fixture.style.width = '100%';
      fixture.style.height = '100%';
    });
    afterEach(function () {
      target.style.position = '';
      target.style.left = '';
      target.style.top = '';
      target.style.right = '';
      target.style.bottom = '';
      target.style.width = '';
      target.style.height = '';

      var fixture = document.getElementById('fixture');
      fixture.style.position = '';
      fixture.style.top = '';
      fixture.style.left = '';
      fixture.style.width = '';
      fixture.style.height = '';
    });

    describe('y axis', function () {

      it('nw', function () {
        target.style.bottom = '0';
        target.style.right = '0';

        var tip = tooltip('#target', 'stuff', {
          axis: 'y',
          align: 'se'
        });

        tip.show();

        assert.ok(dom.hasClass(tip.tooltip, 'n'));
        assert.ok(dom.hasClass(tip.tooltip, 'w'));
        assert.ok(dom.hasClass(tip.tooltip, 'y'));
      });
      it('ne', function () {
        target.style.bottom = '0';
        target.style.left = '0';

        var tip = tooltip('#target', 'stuff', {
          axis: 'y',
          align: 'n'
        });

        tip.show();

        assert.ok(dom.hasClass(tip.tooltip, 'n'));
        assert.ok(dom.hasClass(tip.tooltip, 'e'));
        assert.ok(dom.hasClass(tip.tooltip, 'y'));
      });
      it('n', function () {
        target.style.bottom = '0';
        target.style.left = '-10px';
        target.style.width = '110%';

        var tip = tooltip('#target', 'stuff', {
          axis: 'y',
          align: 'nw'
        });

        tip.show();

        assert.ok(dom.hasClass(tip.tooltip, 'n'));
        assert.ok(!dom.hasClass(tip.tooltip, 'e'));
        assert.ok(!dom.hasClass(tip.tooltip, 'w'));
        assert.ok(dom.hasClass(tip.tooltip, 'y'));
      });

      it('sw', function () {
        target.style.top = '0';
        target.style.right = '0';

        var tip = tooltip('#target', 'stuff', {
          axis: 'y',
          align: 'ne'
        });

        tip.show();

        assert.ok(dom.hasClass(tip.tooltip, 's'));
        assert.ok(dom.hasClass(tip.tooltip, 'w'));
        assert.ok(dom.hasClass(tip.tooltip, 'y'));
      });
      it('se', function () {
        target.style.top = '0';
        target.style.left = '0';

        var tip = tooltip('#target', 'stuff', {
          axis: 'y',
          align: 's'
        });

        tip.show();

        assert.ok(dom.hasClass(tip.tooltip, 's'));
        assert.ok(dom.hasClass(tip.tooltip, 'e'));
        assert.ok(dom.hasClass(tip.tooltip, 'y'));
      });
      it('s', function () {
        target.style.top = '0';
        target.style.left = '-10px';
        target.style.width = '110%';

        var tip = tooltip('#target', 'stuff', {
          axis: 'y',
          align: 'sw'
        });

        tip.show();

        assert.ok(dom.hasClass(tip.tooltip, 's'));
        assert.ok(!dom.hasClass(tip.tooltip, 'e'));
        assert.ok(!dom.hasClass(tip.tooltip, 'w'));
        assert.ok(dom.hasClass(tip.tooltip, 'y'));
      });

    });

    describe('x axis', function () {

      it('nw', function () {
        target.style.top = '0';
        target.style.right = '0';

        var tip = tooltip('#target', 'stuff', {
          axis: 'x',
          align: 'se'
        });

        tip.show();

        assert.ok(dom.hasClass(tip.tooltip, 'n'));
        assert.ok(dom.hasClass(tip.tooltip, 'w'));
        assert.ok(dom.hasClass(tip.tooltip, 'x'));
      });
      it('w', function () {
        target.style.top = '-10px';
        target.style.right = '0';
        target.style.height = '110%';

        var tip = tooltip('#target', 'stuff', {
          axis: 'x',
          align: 'sw'
        });

        tip.show();

        assert.ok(dom.hasClass(tip.tooltip, 'w'), 'should have w class');
        assert.ok(!dom.hasClass(tip.tooltip, 'n'), 'should not have n class');
        assert.ok(!dom.hasClass(tip.tooltip, 's'), 'should not have s class');
        assert.ok(!dom.hasClass(tip.tooltip, 'e'), 'should not have e class');
        assert.ok(dom.hasClass(tip.tooltip, 'x'));
      });

      it('sw', function () {
        target.style.bottom = '0';
        target.style.right = '0';

        var tip = tooltip('#target', 'stuff', {
          axis: 'x',
          align: 'ne'
        });

        tip.show();

        assert.ok(dom.hasClass(tip.tooltip, 's'));
        assert.ok(dom.hasClass(tip.tooltip, 'w'));
        assert.ok(dom.hasClass(tip.tooltip, 'x'));
      });
      it('ne', function () {
        target.style.top = '0';
        target.style.left = '0';

        var tip = tooltip('#target', 'stuff', {
          axis: 'x',
          align: 'e'
        });

        tip.show();

        assert.ok(dom.hasClass(tip.tooltip, 'n'));
        assert.ok(dom.hasClass(tip.tooltip, 'e'));
        assert.ok(dom.hasClass(tip.tooltip, 'x'));
      });
      it('se', function () {
        target.style.bottom = '0';
        target.style.left = '0';

        var tip = tooltip('#target', 'stuff', {
          axis: 'x',
          align: 's'
        });

        tip.show();

        assert.ok(dom.hasClass(tip.tooltip, 's'));
        assert.ok(dom.hasClass(tip.tooltip, 'e'));
        assert.ok(dom.hasClass(tip.tooltip, 'x'));
      });
      it('e', function () {
        target.style.top = '-10px';
        target.style.left = '-10px';
        target.style.height = '110%';

        var tip = tooltip('#target', 'stuff', {
          axis: 'x',
          align: 'se'
        });

        tip.show();

        assert.ok(dom.hasClass(tip.tooltip, 'e'));
        assert.ok(!dom.hasClass(tip.tooltip, 'w'));
        assert.ok(!dom.hasClass(tip.tooltip, 'n'));
        assert.ok(!dom.hasClass(tip.tooltip, 's'));
        assert.ok(dom.hasClass(tip.tooltip, 'x'));
      });
    });
  });

  describe('AM-225', function () {
    beforeEach(function () {
      fixture.innerHTML = [
        '<div style="width:700px;height:700px">',
        '  <a href="#" id="link" style="display:block;margin:100px;">link</a>',
        '</div>'
      ].join('');
    });

    it('should expose its defaults', function () {
      assert.isObject(tooltip.defaults);
      assert.isString(tooltip.defaults.axis);
    });

    it('should not alter its defaults', function () {
      tooltip('#link', 'hi', { axis: 'x' });
      assert.equal('y', tooltip.defaults.axis);

      tooltip('#link', 'hi', { align: 'w' });
      assert.equal('ne', tooltip.defaults.align);
    });

    it('should respect "align:w"', function (done) {
      var tip = tooltip('#link', 'hi', {
        align: 'w',
        axis: 'x',
        fit: false
      });

      tip.show();

      setTimeout(function () {
        var classes = dom.classList(tip.tooltip);
        assert.isTrue(classes.has('w'));
        done();
      }, 10);
    });
  });

});
