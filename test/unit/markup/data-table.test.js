(function () {
  'use strict';

  function isIOS() {
    return (navigator.userAgent.indexOf('AppleWebKit') !== -1 &&
              navigator.userAgent.indexOf('Mobile') !== -1 &&
              navigator.userAgent.indexOf('Safari') !== -1);
  }

  function isOSX() {
    return (navigator.userAgent.indexOf('AppleWebKit') !== -1 &&
              navigator.userAgent.indexOf('Mac OS X') !== -1 &&
              navigator.userAgent.indexOf('Safari') !== -1);
  }

  var array = require('array'),
      inArray = array.inArray,
      selector = require('selector'),
      queryAll = selector.queryAll,
      query = selector.query,
      collections = require('collections'),
      each = collections.each,
      markup = require('markup'),
      dataTable = markup.dataTable,
      dom = require('dom'),
      getVisibleText = dom.getVisibleText;

  describe('markup.dataTable', function () {

    beforeEach(function () {
      var fixture = document.getElementById('fixture');
      fixture.innerHTML = '<h3>Basic table; no explicit tbodies</h3>' +
        '<table id="basic">' +
        ' <tr class="caption">' +
        '   <td colspan="4">Cats</td>' +
        ' </tr>' +
        ' <tr class="header">' +
        '   <td>Name</td>' +
        '   <td>Age</td>' +
        '   <td>Gender</td>' +
        '   <td>Is A Jerk?</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Fred</td>' +
        '   <td>4</td>' +
        '   <td>Male</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Bob</td>' +
        '   <td>2</td>' +
        '   <td>Male</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Sally</td>' +
        '   <td>6</td>' +
        '   <td>Female</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Bob</td>' +
        '   <td>9</td>' +
        '   <td>Female</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        '</table>' +
        '<h3>Basic table; colspans</h3>' +
        '<table id="colspans">' +
        ' <tr class="caption">' +
        '   <td colspan="4">Cats</td>' +
        ' </tr>' +
        ' <tr class="header">' +
        '   <td>Name</td>' +
        '   <td>Age</td>' +
        '   <td>Gender</td>' +
        '   <td>Is A Jerk?</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td colspan="2">Fred</td>' +
        '   <td>Male</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Bob</td>' +
        '   <td>2</td>' +
        '   <td>Male</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td colspan="2">Sally</td>' +
        '   <td>Female</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Bob</td>' +
        '   <td>9</td>' +
        '   <td>Female</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        '</table>' +
        '<h3>Basic table; explicit tbody</h3>' +
        '<table id="basic-tbody">' +
        ' <tbody>' +
        '   <tr class="caption">' +
        '     <td colspan="4">Cats</td>' +
        '   </tr>' +
        '   <tr class="header">' +
        '     <td>Name</td>' +
        '     <td>Age</td>' +
        '     <td>Gender</td>' +
        '     <td>Is A Jerk?</td>' +
        '   </tr>' +
        '   <tr>' +
        '     <td>Fred</td>' +
        '     <td>4</td>' +
        '     <td>Male</td>' +
        '     <td>Yes</td>' +
        '   </tr>' +
        '   <tr>' +
        '     <td>Bob</td>' +
        '     <td>2</td>' +
        '     <td>Male</td>' +
        '     <td>No</td>' +
        '   </tr>' +
        '   <tr>' +
        '     <td>Sally</td>' +
        '     <td>6</td>' +
        '     <td>Female</td>' +
        '     <td>Yes</td>' +
        '   </tr>' +
        '   <tr>' +
        '     <td>Billy</td>' +
        '     <td>9</td>' +
        '     <td>Female</td>' +
        '     <td>No</td>' +
        '   </tr>' +
        ' </tbody>' +
        '</table>' +
        '<h3>Multiple tbodies</h3>' +
        '<table id="multi-tbody">' +
        ' <tbody>' +
        '   <tr class="caption">' +
        '     <td colspan="4">Cats</td>' +
        '   </tr>' +
        '   <tr class="header">' +
        '     <td>Name</td>' +
        '     <td>Age</td>' +
        '     <td>Gender</td>' +
        '     <td>Is A Jerk?</td>' +
        '   </tr>' +
        ' </tbody>' +
        ' <tbody>' +
        '   <tr>' +
        '     <td>Fred</td>' +
        '     <td>4</td>' +
        '     <td>Male</td>' +
        '     <td>Yes</td>' +
        '   </tr>' +
        '   <tr>' +
        '     <td>Bob</td>' +
        '     <td>2</td>' +
        '     <td>Male</td>' +
        '     <td>No</td>' +
        '   </tr>' +
        '   <tr>' +
        '     <td>Sally</td>' +
        '     <td>6</td>' +
        '     <td>Female</td>' +
        '     <td>Yes</td>' +
        '   </tr>' +
        '   <tr>' +
        '     <td>Billy</td>' +
        '     <td>9</td>' +
        '     <td>Female</td>' +
        '     <td>No</td>' +
        '   </tr>' +
        ' </tbody>' +
        '</table>' +
        '<h3>Explicit thead</h3>' +
        '<table id="thead">' +
        ' <thead>' +
        '   <tr class="caption">' +
        '     <td colspan="4">Cats</td>' +
        '   </tr>' +
        '   <tr class="header">' +
        '     <td>Name</td>' +
        '     <td>Age</td>' +
        '     <td>Gender</td>' +
        '     <td>Is A Jerk?</td>' +
        '   </tr>' +
        ' </thead>' +
        ' <tbody>' +
        '   <tr>' +
        '     <td>Fred</td>' +
        '     <td>4</td>' +
        '     <td>Male</td>' +
        '     <td>Yes</td>' +
        '   </tr>' +
        '   <tr>' +
        '     <td>Bob</td>' +
        '     <td>2</td>' +
        '     <td>Male</td>' +
        '     <td>No</td>' +
        '   </tr>' +
        '   <tr>' +
        '     <td>Sally</td>' +
        '     <td>6</td>' +
        '     <td>Female</td>' +
        '     <td>Yes</td>' +
        '   </tr>' +
        '   <tr>' +
        '     <td>Billy</td>' +
        '     <td>9</td>' +
        '     <td>Female</td>' +
        '     <td>No</td>' +
        '   </tr>' +
        ' </tbody>' +
        '</table>' +
        '<h3>Explicit thead; multiple tbodies</h3>' +
        '<table id="thead-multi-tbody">' +
        ' <thead>' +
        '   <tr class="caption">' +
        '     <td colspan="4">Cats</td>' +
        '   </tr>' +
        '   <tr class="header">' +
        '     <td>Name</td>' +
        '     <td>Age</td>' +
        '     <td>Gender</td>' +
        '     <td>Is A Jerk?</td>' +
        '   </tr>' +
        ' </thead>' +
        ' <tbody>' +
        '   <tr>' +
        '     <td>Fred</td>' +
        '     <td>4</td>' +
        '     <td>Male</td>' +
        '     <td>Yes</td>' +
        '   </tr>' +
        '   <tr>' +
        '     <td>Bob</td>' +
        '     <td>2</td>' +
        '     <td>Male</td>' +
        '     <td>No</td>' +
        '   </tr>' +
        ' </tbody>' +
        ' <tbody>' +
        '   <tr>' +
        '     <td>Sally</td>' +
        '     <td>6</td>' +
        '     <td>Female</td>' +
        '     <td>Yes</td>' +
        '   </tr>' +
        '   <tr>' +
        '     <td>Billy</td>' +
        '     <td>9</td>' +
        '     <td>Female</td>' +
        '     <td>No</td>' +
        '   </tr>' +
        ' </tbody>' +
        '</table>' +
        '<h3>Bad th</h3>' +
        '<table id="badth">' +
        ' <tr class="caption">' +
        '   <th colspan="4">Cats</th>' +
        ' </tr>' +
        ' <tr class="header">' +
        '   <td>Name</td>' +
        '   <td>Age</td>' +
        '   <td>Gender</td>' +
        '   <td>Is A Jerk?</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <th>Fred</th>' +
        '   <td>4</td>' +
        '   <td>Male</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <th>Bob</th>' +
        '   <td>2</td>' +
        '   <td>Male</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <th>Sally</th>' +
        '   <td>6</td>' +
        '   <td>Female</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <th>Bob</th>' +
        '   <td>9</td>' +
        '   <td>Female</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        ' <tr></tr>' +
        '</table>' +
        '<h3>Invalid markup</h3>' +
        '<table id="invalid">' +
        ' <tr class="caption">' +
        '   <td colspan="4">Cats</td>' +
        ' </tr>' +
        ' <tr></tr>' +
        ' <tr class="header">' +
        '   <td>Name</td>' +
        '   <td>Age</td>' +
        '   <td>Gender</td>' +
        '   <td>Is A Jerk?</td>' +
        ' </tr>' +
        ' <tr></tr>' +
        ' <tr>' +
        '   <td>Fred</td>' +
        '   <td>4</td>' +
        '   <td>Male</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Bob</td>' +
        '   <td>2</td>' +
        '   <td>Male</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Sally</td>' +
        '   <td>6</td>' +
        '   <td>Female</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Bob</td>' +
        '   <td>9</td>' +
        '   <td>Female</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        ' <tr></tr>' +
        '</table>' +
        '<h3>Presentation + Data in same table</h3>' +
        '<table id="presentation-data">' +
        ' <tr class="notdata">' +
        '   <td colspan="4">This is not data.</td>' +
        ' </tr>' +
        ' <tr class="caption">' +
        '   <td colspan="4">Cats</td>' +
        ' </tr>' +
        ' <tr class="header">' +
        '   <td>Name</td>' +
        '   <td>Age</td>' +
        '   <td>Gender</td>' +
        '   <td>Is A Jerk?</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Fred</td>' +
        '   <td>4</td>' +
        '   <td>Male</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Bob</td>' +
        '   <td>2</td>' +
        '   <td>Male</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Sally</td>' +
        '   <td>6</td>' +
        '   <td>Female</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Bob</td>' +
        '   <td>9</td>' +
        '   <td>Female</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        ' <tr class="notdata">' +
        '   <td colspan="4">This is not data.</td>' +
        ' </tr>' +
        ' <tr class="notdata alt">' +
        '   <td colspan="4">This is not data.</td>' +
        ' </tr>' +
        '</table>' +
        '<h3>Basic table; no tbody, no caption</h3>' +
        '<table id="nocaption">' +
        ' <tr class="header">' +
        '   <td>Name</td>' +
        '   <td>Age</td>' +
        '   <td>Gender</td>' +
        '   <td>Is A Jerk?</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Fred</td>' +
        '   <td>4</td>' +
        '   <td>Male</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Bob</td>' +
        '   <td>2</td>' +
        '   <td>Male</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Sally</td>' +
        '   <td>6</td>' +
        '   <td>Female</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Bob</td>' +
        '   <td>9</td>' +
        '   <td>Female</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        '</table>' +
        '<h3>Proper Markup</h3>' +
        '<table id="expected">' +
        ' <caption>Cats</caption>' +
        ' <tr>' +
        '   <th scope="col">Name</th>' +
        '   <th scope="col">Age</th>' +
        '   <th scope="col">Gender</th>' +
        '   <th scope="col">Is A Jerk?</th>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Fred</td>' +
        '   <td>4</td>' +
        '   <td>Male</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Bob</td>' +
        '   <td>2</td>' +
        '   <td>Male</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Sally</td>' +
        '   <td>6</td>' +
        '   <td>Female</td>' +
        '   <td>Yes</td>' +
        ' </tr>' +
        ' <tr>' +
        '   <td>Billy</td>' +
        '   <td>9</td>' +
        '   <td>Female</td>' +
        '   <td>No</td>' +
        ' </tr>' +
        '</table>';
    });

    afterEach(function () {
      var fixture = document.getElementById('fixture');
      fixture.innerHTML = '';
    });

    it('Bad Headers', function () {
      var table = document.getElementById('badth'),
        noheader = document.getElementById('basic');

      dataTable(table);
      dataTable(noheader);

      assert.equal(table.getElementsByTagName('th').length, 0, 'Should have no headers');
      assert.equal(noheader.getElementsByTagName('th').length, 0, 'Should have no headers');


    });

    it('Basic', function () {
      var index, row, cell, caption, expectedCaption,
        table = document.getElementById('basic');

      expectedCaption = table.rows[0].cells[0].innerHTML;

      dataTable(table, {
        caption: 0,
        header: {
          iOS: false,
          columnHeaders: [1]
        }
      });

      row = table.rows[0];
      for (index = 0; index < row.cells.length; index++) {
        cell = row.cells[index];
        assert.equal(cell.nodeName, 'TH', 'Should be a TH');
        assert.equal(cell.getAttribute('scope'), 'col', 'Should have scope="col"');
      }

      // caption
      caption = table.getElementsByTagName('caption');
      assert.equal(caption.length, 1, 'There should be 1 caption');
      assert.equal(caption[0].innerHTML, expectedCaption, 'Caption should come from the first row');

    });

    it('Multiple Column Headers', function () {
      var index, row, cell, caption, expectedCaption, rowIndex,
        table = document.getElementById('basic');

      expectedCaption = table.rows[0].cells[0].innerHTML;
      dataTable(table, {
        caption: 0,
        header: {
          iOS: false,
          columnHeaders: [1, 2]
        }
      });

      for (rowIndex = 0; rowIndex < 2; rowIndex++) {
        row = table.rows[rowIndex];
        for (index = 0; index < row.cells.length; index++) {
          cell = row.cells[index];
          assert.equal(cell.nodeName, 'TH', 'Should be a TH');
          assert.equal(cell.getAttribute('scope'), 'col', 'Should have scope="col"');
        }
      }

      // caption
      caption = table.getElementsByTagName('caption');
      assert.equal(caption.length, 1, 'There should be 1 caption');
      assert.equal(caption[0].innerHTML, expectedCaption, 'Caption should come from the first row');


    });

    it('Row Headers', function () {

      var cellIndex, row, cell, caption, expectedCaption, rowIndex,
        table = document.getElementById('basic');

      expectedCaption = table.rows[0].cells[0].innerHTML;

      dataTable(table, {
        caption: 0,
        header: {
          iOS: false,
          rowHeaders: [0]
        }
      });

      for (rowIndex = 0; rowIndex < table.rows.length; rowIndex++) {
        row = table.rows[rowIndex];
        for (cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
          cell = row.cells[cellIndex];
          if (cellIndex === 0) {
            assert.equal(cell.nodeName, 'TH', 'Should be a TH');
            assert.equal(cell.getAttribute('scope'), 'row', 'Should have scope="col"');

          } else {
            assert.equal(cell.nodeName, 'TD', 'Should be a TH');
            assert.equal(cell.getAttribute('scope'), null, 'Should have scope="col"');

          }
        }
      }

      // caption
      caption = table.getElementsByTagName('caption');
      assert.equal(caption.length, 1, 'There should be 1 caption');
      assert.equal(caption[0].innerHTML, expectedCaption, 'Caption should come from the first row');

    });

    it('Row Headers and Ignore', function () {

      var cellIndex, row, cell, rowIndex,
        table = document.getElementById('basic');

      dataTable(table, {
        header: {
          iOS: false,
          rowHeaders: [0, 2]
        },
        ignore: {
          rows: [0, 2, 3],
          columns: [2]
        }
      });

      for (rowIndex = 0; rowIndex < table.rows.length; rowIndex++) {
        row = table.rows[rowIndex];
        for (cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
          cell = row.cells[cellIndex];
          if (cellIndex === 0 && rowIndex !== 0 && rowIndex !== 2 && rowIndex !== 3) {
            assert.equal(cell.nodeName, 'TH', 'Should be a TH: ' + cellIndex + ',' + rowIndex);
            assert.equal(cell.getAttribute('scope'), 'row', 'Should have scope="row"');

          } else {
            assert.equal(cell.nodeName, 'TD', 'Should be a TD: ' + cellIndex + ',' + rowIndex);
            assert.equal(cell.getAttribute('scope'), null, 'Should not have scope');

          }
        }
      }

    });


    it('Row and Column Headers and Captions', function () {

      var cellIndex, row, cell, rowIndex, columnIndex,
        table = document.getElementById('basic'),
        config = {
          caption: 0,
          header: {
            iOS: false,
            rowHeaders: [0, 1],
            columnHeaders: [1]
          }
        };

      dataTable(table, config);

      for (rowIndex = 0; rowIndex < table.rows.length; rowIndex++) {
        row = table.rows[rowIndex];
        columnIndex = 0;
        for (cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
          cell = row.cells[cellIndex];

          if (inArray(config.header.columnHeaders, (rowIndex + 1))) {
            assert.equal(cell.nodeName, 'TH', 'Should be a TH: ' + cellIndex + ',' + rowIndex);
            assert.equal(cell.getAttribute('scope'), 'col', 'Should have scope="col"');

          } else if (inArray(config.header.rowHeaders, columnIndex)) {
            assert.equal(cell.nodeName, 'TH', 'Should be a TH: ' + cellIndex + ',' + rowIndex);
            assert.equal(cell.getAttribute('scope'), 'row', 'Should have scope="row"');

          } else {
            assert.equal(cell.nodeName, 'TD', 'Should be a TD: ' + cellIndex + ',' + rowIndex);
            assert.equal(cell.getAttribute('scope'), null, 'Should not have scope');
          }
          columnIndex += cell.colSpan;
        }
      }

    });

    it('Compatibility: Row and Column Headers and Captions', function () {
      // Same test as above, with x and y options instead of the new ones
      var cellIndex, row, cell, rowIndex, columnIndex,
        table = document.getElementById('basic'),
        config = {
          caption: 0,
          header: {
            iOS: false,
            x: [0, 1],
            y: [1]
          }
        };

      dataTable(table, config);
      for (rowIndex = 0; rowIndex < table.rows.length; rowIndex++) {
        row = table.rows[rowIndex];
        columnIndex = 0;

        for (cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
          cell = row.cells[cellIndex];
          if (inArray(config.header.columnHeaders, (rowIndex + 1))) {
            assert.equal(cell.nodeName, 'TH', 'Should be a TH: ' + cellIndex + ',' + rowIndex);
            assert.equal(cell.getAttribute('scope'), 'col', 'Should have scope="col"');

          } else if (inArray(config.header.rowHeaders, columnIndex)) {
            assert.equal(cell.nodeName, 'TH', 'Should be a TH: ' + cellIndex + ',' + rowIndex);
            assert.equal(cell.getAttribute('scope'), 'row', 'Should have scope="row"');

          } else {
            assert.equal(cell.nodeName, 'TD', 'Should be a TD: ' + cellIndex + ',' + rowIndex);
            assert.equal(cell.getAttribute('scope'), null, 'Should not have scope');
          }
          columnIndex += cell.colSpan;
        }
      }

    });

    it('Row, Column Headers, Captions and Colspans', function () {
      var cellIndex, row, cell, rowIndex, columnIndex,
        table = document.getElementById('colspans'),
        config = {
          caption: 0,
          header: {
            iOS: false,
            rowHeaders: [0, 2],
            columnHeaders: [1]
          }
        };

      dataTable(table, config);

      for (rowIndex = 0; rowIndex < table.rows.length; rowIndex++) {
        row = table.rows[rowIndex];
        columnIndex = 0;
        for (cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
          cell = row.cells[cellIndex];

          if (inArray(config.header.columnHeaders, (rowIndex + 1))) {
            assert.equal(cell.nodeName, 'TH', 'Should be a TH: ' + cellIndex + ',' + rowIndex);
            assert.equal(cell.getAttribute('scope'), 'col', 'Should have scope="col"');

          } else if (inArray(config.header.rowHeaders, columnIndex)) {
            assert.equal(cell.nodeName, 'TH', 'Should be a TH: ' + cellIndex + ',' + rowIndex);
            assert.equal(cell.getAttribute('scope'), 'row', 'Should have scope="row"');

          } else {
            assert.equal(cell.nodeName, 'TD', 'Should be a TD: ' + cellIndex + ',' + rowIndex);
            assert.equal(cell.getAttribute('scope'), null, 'Should not have scope');
          }
          columnIndex += cell.colSpan;
        }
      }

    });

    describe('on an iOS device', function () {
      it('Force iOS markup', function () {
        var cells,
          table = document.getElementById('basic'),
          config = {
            header: {
              rowHeaders: [0, 1],
              columnHeaders: [0, 1]
            }
          };
        if (!isIOS()) {
          // force iOS markup on other platforms
          config.header.iOS = true;
          config.header.aria = false;
        }

        dataTable(table, config);

        cells = queryAll('td', table);
        each(cells, function (cell) {
          var ospans = queryAll('span.amaze-offscreen', cell);

          if (cell.cellIndex > 1 && cell.parentNode.rowIndex > 1 && getVisibleText(cell) !== '') {
            assert.ok(ospans.length > 0, 'Should be at least one off screen span in each labelled cell');
          }
        });
      });
    });

    describe('on a Mac', function () {
      it('Should default to aria properties', function () {
        var row, cell, rowIndex, columnIndex,
          table = document.getElementById('basic'),
          config = {
            header: {
              iOS: false,
              rowHeaders: [0, 1],
              columnHeaders: [0, 1]
            }
          };

        if (!isOSX()) {
          // force aria markup on other platforms
          config.header.aria = true;
        }
        dataTable(table, config);

        for (rowIndex = table.rows.length; rowIndex--;) {
          row = table.rows[rowIndex];
          for (columnIndex = row.length; columnIndex--;) {
            cell = row[columnIndex];
            assert.equal(cell.headers, null);
            if (rowIndex >= 2 && columnIndex >= 2) {
              assert.notEqual(cell.getAttribute('aria-labelledby'), null);
            }
          }
        }
      });
    });
  });

  describe('markup.dataTable - more', function () {

    beforeEach(function () {
      var fixture = document.getElementById('fixture');
      fixture.innerHTML = '' +
        '<table id="shoppingCart">' +
          '<thead>' +
            '<tr class="header">' +
              '<th class="four_cols">Item</th>' +
              '<th class="four_cols">Description</th>' +
              '<th class="four_cols">Quantity</th>' +
              '<th class="four_cols">Price</th>' +
            '</tr>' +
          '</thead>' +
          '<tbody>' +
            '<tr style="border-bottom: 1px dashed #666;">' +
              '<td class="four_cols" tabindex="0"><img src="images/site/small174-3.jpg" alt="Fregatte 17"></td>' +
              '<td class="four_cols" tabindex="0">' +
                '<span style="font-weight: bold;">Fregatte 17</span>' +
                ' We had so many superlatives lined up to describe this classically-styled, ' +
                'ultra portable, budget-friendly, high performance machine that we couldn\'t contain ourselves.</td>' +
              '<td class="four_cols" style="text-align:center;" tabindex="0">1</td>' +
              '<td class="four_cols" style="text-align:center;" tabindex="0">$1234.56</td>' +
            '</tr><tr style="border-bottom: 1px dashed #666;">' +
              '<td class="four_cols" tabindex="0"><img src="images/site/small174-2.jpg" alt="Beschissen 750"></td>' +
              '<td class="four_cols" tabindex="0">' +
                '<span style="font-weight: bold;">Beschissen 750</span> ' +
                'You wanted a business minded PC at a home user price and we\'ve given it to you. ' +
                'There\'s no tedium quite like cube-farm tedium and this is the business class beige ' +
                'box made for just such a purpose. Beschissen: compute like it\'s 1989.' +
              '</td>' +
              '<td class="four_cols" style="text-align:center;" tabindex="0">1</td>' +
              '<td class="four_cols" style="text-align:center;" tabindex="0">$1234.56</td>' +
            '</tr>' +
            '<tr style="border-bottom: 1px dashed #666;">' +
              '<td class="four_cols" tabindex="0"><img src="images/site/small174-1.jpg" alt="Verrottet 875"></td>' +
              '<td class="four_cols" tabindex="0">' +
                '<span style="font-weight: bold;">Verrottet 875</span> Share all your photos and home videos with' +
                ' all your friends, relatives, co-workers, and passersby with built-in photo and video software ' +
                'preinstalled.' +
              '</td>' +
              '<td class="four_cols" style="text-align:center;" tabindex="0">1</td>' +
              '<td class="four_cols" style="text-align:center;" tabindex="0">$1234.56</td>' +
            '</tr>' +
            '<tr style="border-bottom: 1px dashed #666;">' +
              '<td class="four_cols" tabindex="0"><img src="images/site/small174-1.jpg" alt="Verrottet 875"></td>' +
              '<td class="four_cols" tabindex="0">' +
              '</td>' +
              '<td class="four_cols" style="text-align:center;" tabindex="0"></td>' +
              '<td class="four_cols" style="text-align:center;" tabindex="0"></td>' +
            '</tr>' +
          '</tbody>' +
        '</table>';
    });

    describe('Proper ARIA markup', function () {
      it('Should have no elements from th up that do not have a role', function () {
        var ths, table;

        table = query('#shoppingCart');
        dataTable(table, {
            header: {
              iOS: false,
              columnHeaders: [0]
            },
            aria: true
          });
        ths = queryAll('th', table);
        each(ths, function (th) {
          var el = th;
          while (el.nodeName !== 'TABLE') {
            assert.ok(el.getAttribute('role'), 'All TH elements up to the table must have a role: ' + el.outerHTML);
            el = el.parentNode;
          }
        });
      });
      it('Should have no elements from td up that do not have a role', function () {
        var tds, table;

        table = query('#shoppingCart');
        dataTable(table, {
            header: {
              iOS: false,
              columnHeaders: [0]
            },
            aria: true
          });
        tds = queryAll('td', table);
        each(tds, function (td) {
          var el = td;
          while (el.nodeName !== 'TABLE') {
            assert.ok(el.getAttribute('role'), 'All TD elements up to the table must have a role' + el.outerHTML);
            el = el.parentNode;
          }
        });
      });
      it('All gridcell roles must be labelled by something', function () {
        var tds, table;

        table = query('#shoppingCart');
        dataTable(table, {
            header: {
              iOS: false,
              columnHeaders: [0]
            },
            aria: true
          });
        tds = queryAll('td', table);
        each(tds, function (td) {
          if (td.getAttribute('role') === 'gridcell') {
            assert.ok(td.getAttribute('aria-labelledby'),
              'All gridcell roles must be labelled by something' + td.outerHTML);
          }
        });
      });
      it('All empty cells should now have the text "empty" inside a span with class "amaze-offscreen"', function () {
        var tds, table;

        table = query('#shoppingCart');
        dataTable(table, {
            header: {
              iOS: false,
              columnHeaders: [0]
            },
            aria: true
          });
        tds = queryAll('td span.amaze-offscreen', table);
        assert.equal(tds.length, 3, 'There should be three spans');
        each(tds, function (td) {
          assert.equal(td.innerHTML, 'empty', 'The text should be "empty"');
        });
      });
    });

    describe('markup.dataTable - proper tests for ignore', function () {

      beforeEach(function () {
        var fixture = document.getElementById('fixture');

        // Simple three by three grid
        fixture.innerHTML = '' +
          '<table id="threeby3">\n' +
            '<thead>\n' +
              '<tr>\n' +
                '<th class="four_cols">0, 0</th>\n' +
                '<th class="four_cols">0, 1</th>\n' +
                '<th class="four_cols">0, 2</th>\n' +
              '</tr>\n' +
            '</thead>\n' +
            '<tbody>\n' +
              '<tr>\n' +
                '<th>1, 0</td>\n' +
                '<td>1, 1</td>\n' +
                '<td>1, 2</td>\n' +
              '</tr>\n' +
              '<tr>\n' +
                '<th>2, 0</td>\n' +
                '<td>2, 1</td>\n' +
                '<td>2, 2</td>\n' +
              '</tr>\n' +
            '</tbody>\n' +
          '</table>\n';
      });

      describe('Ignore rows', function () {
        it('Set row header on column 0 except row 0', function () {
          var trs, table;

          table = query('#threeby3');
          dataTable(table, {
              header: {
                iOS: false,
                rowHeaders: [0]
              },
              ignore : {
                rows : [0]
              },
              aria: true
            });
          trs = queryAll('tr', table);
          assert.equal(trs.length, 3, 'There should be three rows');
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (tr.rowIndex === 0 && child.cellIndex === 0) {
                assert.equal(child.nodeName, 'TD', 'first cell in first row must be <td>');
              } else if (child.cellIndex === 0) {
                assert.equal(child.nodeName, 'TH', 'first cell in other rows must be <th>');
              } else if (tr.rowIndex !== 0) {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'second and third rows must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex);
              } else {
                assert.ok(!child.getAttribute('aria-labelledby'),
                  'first rows second and third cells should have no label');
              }
            });
          });
        });

        it('Set row header on column 1 except row 1', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                rowHeaders: [1]
              },
              ignore : {
                rows : [1]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (child.cellIndex === options.header.rowHeaders[0] && tr.rowIndex === options.ignore.rows[0]) {
                assert.equal(child.nodeName, 'TD', 'This cell is excepted and should be a normal TD');
              } else if (child.cellIndex === options.header.rowHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In other rows it must be a <th>');
              } else if (tr.rowIndex !== options.ignore.rows[0]) {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled rows must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              } else {
                assert.ok(!child.getAttribute('aria-labelledby'), 'Excepted rows cells should have no label');
              }
            });
          });
        });

        it('Set row header on column 2 except row 2', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                rowHeaders: [2]
              },
              ignore : {
                rows : [2]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (child.cellIndex === options.header.rowHeaders[0] && tr.rowIndex === options.ignore.rows[0]) {
                assert.equal(child.nodeName, 'TD', 'This cell is excepted and should be a normal TD');
              } else if (child.cellIndex === options.header.rowHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In other rows it must be a <th>');
              } else if (tr.rowIndex !== options.ignore.rows[0]) {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled rows must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              } else {
                assert.ok(!child.getAttribute('aria-labelledby'), 'Excepted rows cells should have no label');
              }
            });
          });
        });
      });

      describe('Column and Row Headers', function () {
        it('Set column header on row 0 except column 0', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [0]
              },
              ignore : {
                columns : [0]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (child.cellIndex === options.ignore.columns[0] && tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TD', 'This cell is excepted and should be a normal TD');
              } else if (tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In other columns it must be a <th>');
              } else if (child.cellIndex !== options.ignore.columns[0]) {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled columns must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              } else {
                assert.ok(!child.getAttribute('aria-labelledby'), 'Excepted rows cells should have no label');
              }
            });
          });
        });

        it('Set column header on row 1 except column 1', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [1]
              },
              ignore : {
                columns : [1]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (child.cellIndex === options.ignore.columns[0] && tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TD', 'This cell is excepted and should be a normal TD');
              } else if (tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In other columns it must be a <th>');
              } else if (child.cellIndex !== options.ignore.columns[0]) {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled columns must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              } else {
                assert.ok(!child.getAttribute('aria-labelledby'), 'Excepted rows cells should have no label');
              }
            });
          });
        });

        it('Set column header on row 2 except column 2', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [2]
              },
              ignore : {
                columns : [2]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (child.cellIndex === options.ignore.columns[0] && tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TD', 'This cell is excepted and should be a normal TD');
              } else if (tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In other columns it must be a <th>');
              } else if (child.cellIndex !== options.ignore.columns[0]) {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled columns must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              } else {
                assert.ok(!child.getAttribute('aria-labelledby'), 'Excepted rows cells should have no label');
              }
            });
          });
        });
      });

      describe('Markup rows and columns', function () {
        it('Set column header on row 0 and row headers on column 0', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [0],
                rowHeaders: [0]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header row, all cells must be a <th>');
              } else if (child.cellIndex === options.header.rowHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header columns all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });

        it('Set column header on row 1 and row headers on column 1', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [1],
                rowHeaders: [1]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header row, all cells must be a <th>');
              } else if (child.cellIndex === options.header.rowHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header columns all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });

        it('Set column header on row 2 and row headers on column 2', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [2],
                rowHeaders: [2]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header row, all cells must be a <th>');
              } else if (child.cellIndex === options.header.rowHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header columns all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });

        it('Set column header on row 0 and row headers on column 1', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [0],
                rowHeaders: [1]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header row, all cells must be a <th>');
              } else if (child.cellIndex === options.header.rowHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header columns all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });

        it('Set column header on row 0 and row headers on column 2', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [0],
                rowHeaders: [2]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header row, all cells must be a <th>');
              } else if (child.cellIndex === options.header.rowHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header columns all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });

        it('Set column header on row 1 and row headers on column 0', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [1],
                rowHeaders: [0]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header row, all cells must be a <th>');
              } else if (child.cellIndex === options.header.rowHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header columns all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });

        it('Set column header on row 2 and row headers on column 0', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [2],
                rowHeaders: [0]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (tr.rowIndex === options.header.columnHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header row, all cells must be a <th>');
              } else if (child.cellIndex === options.header.rowHeaders[0]) {
                assert.equal(child.nodeName, 'TH', 'In the header columns all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });
      });

      describe('Multiple Column Headers', function () {
        it('Row 0 and row 1', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [0, 1]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (inArray(options.header.columnHeaders, tr.rowIndex)) {
                assert.equal(child.nodeName, 'TH', 'In the header rows, all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });

        it('Row 0 and row 2', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [0, 2]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (inArray(options.header.columnHeaders, tr.rowIndex)) {
                assert.equal(child.nodeName, 'TH', 'In the header rows, all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });

        it('Row 1 and row 2', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                columnHeaders: [1, 2]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (inArray(options.header.columnHeaders, tr.rowIndex)) {
                assert.equal(child.nodeName, 'TH', 'In the header rows, all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });
      });


      describe('Multiple Row Headers', function () {
        it('Column 0 and column 1', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                rowHeaders: [0, 1]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (inArray(options.header.rowHeaders, child.cellIndex)) {
                assert.equal(child.nodeName, 'TH', 'In the header columns, all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });

        it('Column 0 and column 2', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                rowHeaders: [0, 2]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (inArray(options.header.rowHeaders, child.cellIndex)) {
                assert.equal(child.nodeName, 'TH', 'In the header columns, all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });

        it('Column 2 and column 1', function () {
          var trs, table,
            options = {
              header: {
                iOS: false,
                rowHeaders: [2, 1]
              },
              aria: true
            };

          table = query('#threeby3');
          dataTable(table, options);
          trs = queryAll('tr', table);
          each(trs, function (tr) {
            each(tr.children, function (child) {
              if (inArray(options.header.rowHeaders, child.cellIndex)) {
                assert.equal(child.nodeName, 'TH', 'In the header columns, all cells must be a <th>');
              } else {
                assert.ok(child.getAttribute('aria-labelledby'),
                  'To be labelled cells must have aria-labelledby: tr.rowIndex: ' +
                  tr.rowIndex + ', child.cellIndex: ' + child.cellIndex +
                  table.innerHTML);
              }
            });
          });
        });
      });

    });

  });

  describe('markup.dataTable - regions', function () {

    beforeEach(function () {
      var fixture = document.getElementById('fixture');
      fixture.innerHTML = '' +
                          '<table id="regions" cellpadding="3" border="1">' +
                          ' <caption>' +
                          '   <p>Travel Expense Report</p>' +
                          ' </caption>' +
                          ' <tbody>' +
                          '   <tr>' +
                          '     <td></td>' +
                          '     <th id="c2">Meals</th>' +
                          '     <th id="c3">Hotels</th>' +
                          '     <th id="c">Transport</th>' +
                          '     <td id="c5">subtotals</td>' +
                          '   </tr>' +
                          '   <tr>' +
                          '     <th id="r2">San Jose</th>' +
                          '     <td></td>' +
                          '     <td></td>' +
                          '     <td></td>' +
                          '     <td></td>' +
                          '   </tr>' +
                          '   <tr>' +
                          '     <td id="r3">25-Aug-97</td>' +
                          '     <td class="yellow">37.74</td>' +
                          '     <td>112.00</td>' +
                          '     <td>45.00</td>' +
                          '     <td></td>' +
                          '   </tr>' +
                          '   <tr>' +
                          '     <td id="r4">26-Aug-97</td>' +
                          '     <td>27.28</td>' +
                          '     <td>112.00</td>' +
                          '     <td>45.00</td>' +
                          '     <td></td>' +
                          '   </tr>' +
                          '   <tr>' +
                          '     <td id="r5">subtotals</td>' +
                          '     <td>65.02</td>' +
                          '     <td>224.00</td>' +
                          '     <td>90.00</td>' +
                          '     <td>379.02</td>' +
                          '   </tr>' +
                          '   <tr>' +
                          '     <th id="r6">Seattle</th>' +
                          '     <td></td>' +
                          '     <td></td>' +
                          '     <td></td>' +
                          '     <td></td>' +
                          '   </tr>' +
                          '   <tr>' +
                          '     <td id="r7">27-Aug-97</td>' +
                          '     <td>96.25</td>' +
                          '     <td>109.00</td>' +
                          '     <td>36.00</td>' +
                          '     <td></td>' +
                          '   </tr>' +
                          '   <tr>' +
                          '     <td id="r8">28-Aug-97</td>' +
                          '     <td>35.00</td>' +
                          '     <td>109.00</td>' +
                          '     <td>36.00</td>' +
                          '     <td></td>' +
                          '   </tr>' +
                          '   <tr>' +
                          '     <td id="r9">subtotals</td>' +
                          '     <td>131.25</td>' +
                          '     <td>218.00</td>' +
                          '     <td>72.00</td>' +
                          '     <td>421.25</td>' +
                          '   </tr>' +
                          '   <tr>' +
                          '     <th id="r10">Totals</th>' +
                          '     <td>196.27</td>' +
                          '     <td>442.00</td>' +
                          '     <td>162.00</td>' +
                          '     <td>800.27</td>' +
                          '   </tr>' +
                          ' </tbody>' +
                          '</table>';
    });

    describe('San Jose - ARIA', function () {
      it('Should be the header for row 2 columns 0 thru 3, row 3 columns 0 thru 3 and row 4', function () {
        var trs, table,
          options = {
            header: {
              iOS: false
            },
            regions : [
              {
                headerCell : {
                  column : 0,
                  row : 1
                },
                targetCells : [
                  {
                    row : 2,
                    columns : [0, 3]
                  },
                  {
                    row : 3,
                    columns : [0, 3]
                  },
                  {
                    row : 4,
                    columns : [0, 0]
                  }
                ]
              }
            ],
            aria: true
          };

        table = query('#regions');
        dataTable(table, options);
        trs = queryAll('tr', table);
        each(trs, function (tr) {
          if (tr.rowIndex > 1 && tr.rowIndex < 5) {
            each(tr.children, function (child) {
              var att;
              if (child.cellIndex < 4 || tr.rowIndex === 4) {
                att = child.getAttribute('aria-labelledby');
                assert.ok(att.indexOf('r2') !== -1, 'The cell must be headed by San Jose');
              }
            });
          }
        });
      });
    });

    describe('San Jose - headers', function () {
      it('Should be the header for row 2 columns 0 thru 3, row 3 columns 0 thru 3 and row 4', function () {
        var trs, table,
          options = {
            header: {
              iOS: false
            },
            regions : [
              {
                headerCell : {
                  column : 0,
                  row : 1
                },
                targetCells : [
                  {
                    row : 2,
                    columns : [0, 3]
                  },
                  {
                    row : 3,
                    columns : [0, 3]
                  },
                  {
                    row : 4,
                    columns : [0, 0]
                  }
                ]
              }
            ],
            aria: false
          };

        table = query('#regions');
        dataTable(table, options);
        trs = queryAll('tr', table);
        each(trs, function (tr) {
          if (tr.rowIndex > 1 && tr.rowIndex < 5) {
            each(tr.children, function (child) {
              var att;
              if (child.cellIndex < 4 || tr.rowIndex === 4) {
                att = child.getAttribute('headers');
                assert.ok(att.indexOf('r2') !== -1, 'The cell must be headed by San Jose');
              }
            });
          }
        });
      });
    });
    describe('San Jose - iOS', function () {
      it('Should be the header for row 2 columns 0 thru 3, row 3 columns 0 thru 3 and row 4', function () {
        var trs, table,
          options = {
            header : {
              iOS : true
            },
            regions : [
              {
                headerCell : {
                  column : 0,
                  row : 1
                },
                targetCells : [
                  {
                    row : 2,
                    columns : [0, 3]
                  },
                  {
                    row : 3,
                    columns : [0, 3]
                  },
                  {
                    row : 4,
                    columns : [0, 0]
                  }
                ]
              }
            ],
            aria: false
          };

        table = query('#regions');
        dataTable(table, options);
        trs = queryAll('tr', table);
        each(trs, function (tr) {
          if (tr.rowIndex > 1 && tr.rowIndex < 5) {
            each(tr.children, function (child) {
              var spans;
              if (child.cellIndex < 4 || tr.rowIndex === 4) {
                spans = queryAll('span.amaze-offscreen', child);
                assert.ok(spans.length > 0, 'The cell must have an offscreen span');
                assert.equal(getVisibleText(spans[0]), 'San Jose', 'Text of off screen span must be "San Jose"');
              }
            });
          }
        });
      });
    });


    //@author Harris.Schneiderman@deque.com
    describe('AM-27: simple 2D tables', function () {

      function firstElementChild(element) {
        if (element.firstElementChild) {
          return element.firstElementChild;
        }

        for (var node = element.firstChild; node; node = node.nextSibling) {
          if (node.nodeType === 1) {
            return node;
          }
        }
      }

      beforeEach(function () {
        var fixture = document.getElementById('fixture');

        fixture.innerHTML = [
          '<br /><table border id="am-27-table">',
          '  <tr>',
          '    <td>Foo</td>',
          '    <td>Bar</td>',
          '    <td>Cat</td>',
          '  </tr>',
          '  <tr>',
          '    <td>Ninja</td>',
          '    <td>Elephant</td>',
          '    <td>Squirrell</td>',
          '  </tr>',
          '  <tr>',
          '    <td>Salsa</td>',
          '    <td>Cheese</td>',
          '    <td>Gefilte Fish</td>',
          '  </tr>',
          '</table>'
        ].join('');
      });

      ////////////////////////////////////////////////////////////////////////////////////////////////////
      //for COL HEADERS (just one)////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////////////////////////////

      var table;
      beforeEach(function () {
        table = selector.query('#am-27-table');
      });
      //HEADERS
      it('should set table headers on first row', function () {
        dataTable(table, {
          header: {
            columnHeaders: [ 0 ]
          }

        });
        var trs = selector.queryAll('tr', table),
          tr = trs[0],
          headers = selector.queryAll('th', tr);

        //Table headings
        assert.ok(headers.length === 3, 'all tds in the first row should become ths');

      });

      //CELL INDEX ON TH'S
      it('it should set proper cell index', function () {
        dataTable(table, {
          header: {
            columnHeaders: [ 0 ]
          }

        });

        var trs = selector.queryAll('tr', table),
          firstRow = firstElementChild(trs[0]),
          secondRow = firstElementChild(trs[1]),
          thirdRow = firstElementChild(trs[2]);

        //Cell index 0
        assert.equal(firstRow.cellIndex, 0, 'row 1 col 1 cellIndex of 0');
        assert.equal(secondRow.cellIndex, 0, 'row 1 col 1 cellIndex of 0');
        assert.equal(thirdRow.cellIndex, 0, 'row 1 col 1 cellIndex of 0');
      });

      //ARIA-LABELLEDBY
      it('nothing should have aria-labelledby', function () {
        dataTable(table, {
          header: {
            columnHeaders: [ 0 ]
          }

        });

        var tds = selector.queryAll('td', table);

        //aria-labelledby
        each(tds, function (td) {
          assert.isNull(td.getAttribute('aria-labelledby'), 'should not have aria-labelledby attribute');
        });

      });

      it('should set scope=col properly on first row', function () {
        dataTable(table, {
          header: {
            columnHeaders: [ 0 ]
          }
        });
        var trs = selector.queryAll('tr', table),
          tr = trs[0],
          ths = selector.queryAll('th', tr);

        //<th scope="col" />
        each(ths, function (td) {
          assert.equal(td.getAttribute('scope'), 'col', 'the first row should have col scope');
        });

      });

    ///////////////////////////////////////////////////////////////////////////////////////////
    //for ROW HEADERS (just one)///////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////

      //TABLE HEADINGS
      it('should set table headers on the first row and first col', function () {
        dataTable(table, {
          header: {
            rowHeaders: [ 0 ]
          }
        });
        var trs = selector.queryAll('tr', table),
          firstRow = firstElementChild(trs[0]),
          secondRow = firstElementChild(trs[1]),
          thirdRow = firstElementChild(trs[2]);

        //check for table headings (th)
        assert.equal(firstRow.tagName, 'TH', 'first column in the first row should be th');
        assert.equal(secondRow.tagName, 'TH', 'first column in the second row should be th');
        assert.equal(thirdRow.tagName, 'TH', 'first column in third row should be th');
      });

      //SCOPE
      it('should set scope=row properly on first col', function () {
        dataTable(table, {
          header: {
            rowHeaders: [ 0 ]
          }
        });
        var trs = selector.queryAll('tr', table),
          firstRow = firstElementChild(trs[0]),
          secondRow = firstElementChild(trs[1]),
          thirdRow = firstElementChild(trs[2]);
        //<td scope="row" />
        assert.equal(firstRow.getAttribute('scope'), 'row', 'first col in first row should have scope=row');
        assert.equal(secondRow.getAttribute('scope'), 'row', 'first col in second row should have scope=row');
        assert.equal(thirdRow.getAttribute('scope'), 'row', 'first col in third row should have scope=row');
      });

      //ARIA-LABELLEDBY
      it('should not have aria-labelledby', function () {

        dataTable(table, {
          header: {
            rowHeaders: [ 0 ]
          }
        });

        var tds = selector.queryAll('td', table);
        //check that there are 6 tds
        assert.equal(tds.length, 6, 'there should be 4 tds in the table');
        //check for aria-labelledby
        each(tds, function (td) {
          assert.isNull(td.getAttribute('aria-labelledby'), 'nothing should have aria-labelledby');
        });
      });

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //for COL HEADERS && ROW HEADERS (one of each)/////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

      //TABLE HEADINGS
      it('should set table headers on the first row and first col', function () {
        dataTable(table, {
          header: {
            rowHeaders: [ 0 ],
            columnHeaders: [ 0 ]
          }
        });
        var trs = selector.queryAll('tr', table),
          rowHeads = selector.queryAll('th', trs[0]),
          firstRow = firstElementChild(trs[0]),
          secondRow = firstElementChild(trs[1]),
          thirdRow = firstElementChild(trs[2]);


        //no aria-labelledby
        //check scope attributes on each cell
        assert.ok(rowHeads.length === 3, 'all tds in the first row should become ths');

        assert.equal(firstRow.tagName, 'TH', 'first column in the first row should be th');
        assert.equal(secondRow.tagName, 'TH', 'first column in the second row should be th');
        assert.equal(thirdRow.tagName, 'TH', 'first column in third row should be th');
      });

      //ARIA-LABELLEDBY
      it('should not have aria-labelledby anywhere', function () {
        dataTable(table, {
          header: {
            rowHeaders: [ 0 ],
            columnHeaders: [ 0 ]
          }
        });
        var tds = selector.queryAll('td', table);
        //make sure there are 4 tds
        assert.equal(tds.length, 4, 'there should be 4 tds in the table');

        each(tds, function (td) {
          assert.isNull(td.getAttribute('aria-labelledby'), 'nothing should have aria-labelledby');
        });
      });

      //SCOPE
      it('should have scope=col on the col headers, and have scope=row on the row headers', function () {
        dataTable(table, {
          header: {
            rowHeaders: [ 0 ],
            columnHeaders: [ 0 ]
          }
        });
        var trs = selector.queryAll('tr', table),
          first = firstElementChild(trs[0]),
          second = firstElementChild(trs[1]),
          third = firstElementChild(trs[2]),
          tr = trs[0],
          ths = selector.queryAll('th', tr);
        //<td scope="row" />
        assert.equal(second.getAttribute('scope'), 'row', 'first col in second row should have scope=row');
        assert.equal(third.getAttribute('scope'), 'row', 'first col in third row should have scope=row');
        //<td scope="col" />
        //row should not win
        assert.equal(first.getAttribute('scope'), 'col', 'first col in first row should have scope=row');
        assert.equal(ths[1].getAttribute('scope'), 'col', 'the second column in the first row should have scope=col');
        assert.equal(ths[2].getAttribute('scope'), 'col', 'the third column in the first row should have scope=col');
      });

      //make sure that when two headers are defined, that the normal non - "simpleTable" functionality runs
      //
      //two column headers
      it('should have an aria-labelledby on the non-headers when two colHeaders are set', function () {
        dataTable(table, {
          header: {
            columnHeaders: [ 0, 1 ]
          }
        });
        var trs = selector.queryAll('tr', table),
          thirdOne = trs[2],
          thirdTds = selector.queryAll('td', thirdOne),
          attr = navigator.userAgent.match(/Mac OS/i) !== null
                  ? 'aria-labelledby'
                  : 'headers';

        each(thirdTds, function (td) {
          assert.isNotNull(td.getAttribute(attr), 'it should add the ' + attr + ' attribute');
        });
      });
      //two row headers
      it('should have an aria-labelledby on the non-headers when two rowHeaders are defined', function () {
        dataTable(table, {
          header: {
            rowHeaders: [ 0, 1 ]
          }
        });
        var trs = selector.queryAll('tr', table),
          lastFirst = selector.query('td', trs[0]),
          lastSecond = selector.query('td', trs[1]),
          lastThird = selector.query('td', trs[2]),
          attr = navigator.userAgent.match(/Mac OS/i) !== null
                  ? 'aria-labelledby'
                  : 'headers';

        assert.isNotNull(lastFirst.getAttribute(attr), 'last col of 1st row should have ' + attr);
        assert.isNotNull(lastSecond.getAttribute(attr), 'last col of 2nd row should have ' + attr);
        assert.isNotNull(lastThird.getAttribute(attr), 'last col of 3rd row should have ' + attr);
      });
      //no headers defined (neither rowHeaders nor columnHeaders)
      it('should not have any th\'s in entire table when none have been set', function () {
        dataTable(table);

        var ths = selector.queryAll('th', table);

        assert.equal(ths.length, 0, 'there should not be any th in the table');
      });
    });

  });

}());
