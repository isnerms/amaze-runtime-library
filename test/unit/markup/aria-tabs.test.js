describe('markup.ariaTabs', function () {
  // jshint maxlen:400

  'use strict';

  var markup = require('markup');
  var selector = require('selector');
  var dom = require('dom');
  var each = require('collections').each;
  var events = require('events');
  var fixture = null;


  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    fixture.innerHTML = '' +
                        '<div id="tabs">;;<ul id="tablist1">' +
                        '<li id="tablist1tab1"><a href="#">Tab 1</a></li>' +
                        '<li id="tablist1tab2" class="selected"><a href="tab2.html">Tab 2</a></li>' +
                        '<li id="tablist1tab3"><a href="tab3.html">Tab 3</a></li>' +
                        '<li id="tablist1tab4"><a href="tab4.html">Tab 4</a></li>' +
                        '<li id="tablist1tab5"><a href="tab5.html">Tab 5</a></li>' +
                        '<li id="tablist1tab6"><a href="tab6.html">Tab 6</a></li>' +
                        '</ul>' +
                        '<div class="tabpanel" id="tabpanel1"><a href="#">First focussable element in tab panel 1</a></div>' +
                        '<div class="tabpanel" id="tabpanel2"><a href="#">First focussable element in tab panel 2</a></div>' +
                        '<div class="tabpanel" id="tabpanel3"><a href="#">First focussable element in tab panel 3</a></div>' +
                        '<div class="tabpanel" id="tabpanel4"><a href="#">First focussable element in tab panel 4</a></div>' +
                        '<div class="tabpanel" id="tabpanel5"><a href="#">First focussable element in tab panel 5</a></div>' +
                        '<div class="tabpanel" id="tabpanel6"><a href="#">First focussable element in tab panel 6</a></div>' +
                        '</div>' +
                        '<div id="tabs2"><ul id="tablist2">' +
                        '<li id="tl2tab1"><a href="#">Tab 1</a></li>' +
                        '<li id="tl2tab2" class="selected"><a href="tab2.html">Tab 2</a></li>' +
                        '<li id="tl2tab3"><a href="tab3.html">Tab 3</a></li>' +
                        '<li id="tl2tab4"><a href="tab4.html">Tab 4</a></li>' +
                        '<li id="tl2tab5"><a href="tab5.html">Tab 5</a></li>' +
                        '<li id="tl2tab6"><a href="tab6.html">Tab 6</a></li>' +
                        '</ul>' +
                        '<div class="custtabpanel" id="tp1"><a href="#">First focussable element in tab panel 1</a></div>' +
                        '<div class="custtabpanel" id="tp2"><a href="#">First focussable element in tab panel 2</a></div>' +
                        '<div class="custtabpanel" id="tp3"><a href="#">First focussable element in tab panel 3</a></div>' +
                        '<div class="custtabpanel" id="tp4"><a href="#">First focussable element in tab panel 4</a></div>' +
                        '<div class="custtabpanel" id="tp5"><a href="#">First focussable element in tab panel 5</a></div>' +
                        '<div class="custtabpanel" id="tp6"><a href="#">First focussable element in tab panel 6</a></div>' +
                        '</div>' +
                        '<div id="tabs3"><ul id="tablist3">' +
                        '<li id="tablist3tab0"><a href="#">Tab 1</a></li>' +
                        '<li id="tablist3tab1" class="selected"><a href="tab2.html">Tab 2</a></li>' +
                        '<li id="tablist3tab2"><a href="tab3.html">Tab 3</a></li>' +
                        '<li id="tablist3tab3"><a href="tab4.html">Tab 4</a></li>' +
                        '<li id="tablist3tab4"><a href="tab5.html">Tab 5</a></li>' +
                        '<li id="tablist3tab5"><a href="tab6.html">Tab 6</a></li>' +
                        '</ul>' +
                        '<div class="tabpanel" id="tabp0"><a href="#">First focussable element in tab panel 1</a></div>' +
                        '<div class="tabpanel" id="tabp1"><a href="#">First focussable element in tab panel 2</a></div>' +
                        '<div class="tabpanel" id="tabp2"><a href="#">First focussable element in tab panel 3</a></div>' +
                        '<div class="tabpanel" id="tabp3"><a href="#">First focussable element in tab panel 4</a></div>' +
                        '<div class="tabpanel" id="tabp4"><a href="#">First focussable element in tab panel 5</a></div>' +
                        '<div class="tabpanel" id="tabp5"><a href="#">First focussable element in tab panel 6</a></div>' +
                        '</div>' +
                        '';
  });
  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('Static tabs: markup - default indexGetter', function () {
    var anchors, ul, lis, container = '#tablist3';

    ul = selector.query(container);
    assert.isNotNull(ul, 'Should be able to find the container');
    anchors = selector.queryAll('a', ul);
    assert.equal(anchors.length, 6, 'There should be 6 anchors within the container');
    each(anchors, function (value) {
      assert.isUndefined(dom.getAttributes(value).tabindex, 'The anchors should have no tabindex until we add it');
    });

    assert.isUndefined(dom.getAttributes(ul).role, 'The container should have no role until we add it');

    // execute the function
    markup.ariaTabs(container, { ownsSelector: '#tabp{index}'});

    assert.equal(dom.getAttributes(ul).role, 'tablist', 'The container should have a role of tablist');

    lis = selector.queryAll('>li', ul);
    each(lis, function (value) {
      var classes = dom.classList(value),
        atts = dom.getAttributes(value);
      if (classes.has('selected')) {
        assert.equal(atts['aria-selected'], 'true',
          'The currently selected list item should have aria-selected of true');
      } else {
        assert.equal(atts['aria-selected'], 'false',
          'The unselected list items should have aria-selected of false');
      }
      assert.equal(atts.role, 'tab',
        'Each list item should have a role of tab');
      assert.isNotNull(atts['aria-owns'],
        'Each list item should have an aria-owns attribute');
    });

    each(anchors, function (value) {
      assert.equal(dom.getAttributes(value).tabindex, '-1', 'The anchors should have a tabindex of -1');
    });
  });

  it('Static tabs: tab panel markup - default indexGetter', function () {
    var tabpanels, container = '#tablist3';

    // execute the function
    tabpanels = selector.queryAll('#tabs3 .tabpanel');

    assert.equal(tabpanels.length, 6, 'Should be 6 tab panels');
    each(tabpanels, function (value) {
      var atts = dom.getAttributes(value);
      assert.isUndefined(atts.role, 'The tab panels should not have roles until we add them');
    });
    markup.ariaTabs(container, { ownsSelector: '#tabp{index}'});
    each(tabpanels, function (value) {
      var atts = dom.getAttributes(value);
      assert.equal(atts.role, 'tabpanel', 'The tab panels should have role of tabpanel');
    });
  });


  it('Static tabs: markup, custom indexGetter', function () {
    var anchors, ul, lis, container = '#tablist1';

    ul = selector.query(container);
    assert.isNotNull(ul, 'Should be able to find the container');
    anchors = selector.queryAll('a', ul);
    assert.equal(anchors.length, 6, 'There should be 6 anchors within the container');
    each(anchors, function (value) {
      assert.isUndefined(dom.getAttributes(value).tabindex, 'The anchors should have no tabindex until we add it');
    });

    assert.isUndefined(dom.getAttributes(ul).role, 'The container should have no role until we add it');

    // execute the function
    markup.ariaTabs(container, {
      indexGetter: function (tab) {
        var atts = dom.getAttributes(tab),
          id = atts.id;

        return id.substring(11); // will remove 'tablist1tab' from the string and return the rest
      }
    });

    assert.equal(dom.getAttributes(ul).role, 'tablist', 'The container should have a role of tablist');

    lis = selector.queryAll('>li', ul);
    each(lis, function (value) {
      var classes = dom.classList(value),
        atts = dom.getAttributes(value);
      if (classes.has('selected')) {
        assert.equal(atts['aria-selected'], 'true',
          'The currently selected list item should have aria-selected of true');
      } else {
        assert.equal(atts['aria-selected'], 'false',
          'The unselected list items should have aria-selected of false');
      }
      assert.equal(atts.role, 'tab',
        'Each list item should have a role of tab');
      assert.isNotNull(atts['aria-owns'],
        'Each list item should have an aria-owns attribute');
    });

    each(anchors, function (value) {
      assert.equal(dom.getAttributes(value).tabindex, '-1', 'The anchors should have a tabindex of -1');
    });
  });

  it('Static tabs: tab panel markup, custom indexGetter', function () {
    var tabpanels, container = '#tablist1';

    // execute the function
    tabpanels = selector.queryAll('#tabs .tabpanel');

    assert.equal(tabpanels.length, 6, 'Should be 6 tab panels');
    each(tabpanels, function (value) {
      var atts = dom.getAttributes(value);
      assert.isUndefined(atts.role, 'The tab panels should not have roles until we add them');
    });
    markup.ariaTabs(container, {
      indexGetter: function (tab) {
        var atts = dom.getAttributes(tab),
          id = atts.id;

        return id.substring(11); // will remove 'tablist1tab' from the string and return the rest
      }
    });
    each(tabpanels, function (value) {
      var atts = dom.getAttributes(value);
      assert.equal(atts.role, 'tabpanel', 'The tab panels should have role of tabpanel');
    });
  });

  it('Static tabs: tab panel keyboard - CTRL-left and CTRL-up', function (done) {
    markup.ariaTabs('#tablist1', {
      indexGetter: function (tab) {
        return tab.id.substring(11);
      }
    });

    /**
     * Send a LEFT keypress to the element
     * matching the given `selector`
     *
     * @api private
     * @param {String} selector
     */
    function left(selector) {
      setTimeout(function () {
        events.fire(selector, 'keydown', {
          which: 37,
          ctrlKey: true
        });
      }, 10);
    }

    /**
     * Send a UP keypress to the element
     * matching the given `selector`
     *
     * @api private
     * @param {String} selector
     */
    function up(selector) {
      setTimeout(function () {
        events.fire(selector, 'keydown', {
          which: 38,
          ctrlKey: true
        });
      }, 10);
    }

    // when #1 gets focus, send LEFT to #2
    events.once('#tablist1tab1', 'focus', function () {
      left('#tabpanel2');
    });

    // when #2 gets focus, send LEFT to #3
    events.once('#tablist1tab2', 'focus', function () {
      left('#tabpanel3');
    });

    // when #3 gets focus, send UP to #4
    events.once('#tablist1tab3', 'focus', function () {
      up('#tabpanel4');
    });

    // when #4 gets focus, send UP to #5
    events.once('#tablist1tab4', 'focus', function () {
      up('#tabpanel5');
    });

    // when #5 gets focus, we're done
    events.once('#tablist1tab5', 'focus', function () {
      done();
    });

    // entry point.  send LEFT to #1
    left('#tabpanel1');
  });

  it('Static tabs: tab panel keyboard - CTRL-pgUp and CTRL-pgDown', function (done) {
    markup.ariaTabs('#tablist1', {
      indexGetter: function (tab) {
        return tab.id.substring(11);
      }
    });

    /**
     * Send a PAGEUP keypress to the element
     * matching the given `selector`
     *
     * @api private
     * @param {String} selector
     */
    function pageUp(selector) {
      setTimeout(function () {
        events.fire(selector, 'keydown', {
          which: 33,
          ctrlKey: true
        });
      }, 10);
    }

    /**
     * Send a PAGEDOWN keypress to the element
     * matching the given `selector`
     *
     * @api private
     * @param {String} selector
     */
    function pageDown(selector) {
      setTimeout(function () {
        events.fire(selector, 'keydown', {
          which: 34,
          ctrlKey: true
        });
      }, 10);
    }

    // when #1 gets focus, send a PAGEDOWN to #2
    events.once('#tablist1tab1', 'focus', function () {
      pageDown('#tabpanel2');
    });


    // when #3 gets focus, we're done
    events.once('#tablist1tab3', 'focus', function () {
      done();
    });

    // entry point.  send PAGEUP to #2
    pageUp('#tabpanel2');
  });

  it('Static tabs: tab panel markup - custom', function () {
    var tabpanels, container = '#tablist2';

    // execute the function
    tabpanels = selector.queryAll('.custtabpanel');

    assert.equal(tabpanels.length, 6, 'Should be 6 tab panels');
    each(tabpanels, function (value) {
      var atts = dom.getAttributes(value);
      assert.isUndefined(atts.role, 'The tab panels should not have roles until we add them');
    });
    markup.ariaTabs(container, {
      ownsSelector: '#tp{index}',
      indexGetter: function (tab) {
        var atts = dom.getAttributes(tab),
          id = atts.id;

        return id.substring(6);
      },
      preselector: '#tabs2'
    });
    each(tabpanels, function (value) {
      var atts = dom.getAttributes(value);
      assert.equal(atts.role, 'tabpanel', 'The tab panels should have role of tabpanel');
    });
  });

  it('Static tabs: keyboard events and focus', function (done) {
    var ul, lis, container = '#tablist1', evented;

    ul = selector.query(container);

    // execute the function
    markup.ariaTabs(container, {
      indexGetter: function (tab) {
        var atts = dom.getAttributes(tab),
          id = atts.id;

        return id.substring(11); // will remove 'tablist1tab' from the string and return the rest
      }
    });

    lis = selector.queryAll('>li', ul);
    each(lis, function (value) {
      events.on(value, 'focus', function () {
        evented.push({id: dom.getAttributes(this).id, event: 'focus'});
      });
    });

    evented = [];
    events.fire(lis[0], 'focus');
    events.fire(lis[0], 'keydown', {which: 39}); //right
    events.fire(lis[1], 'keydown', {which: 37}); //left
    events.fire(lis[0], 'keydown', {which: 40}); //down
    events.fire(lis[1], 'keydown', {which: 38}); //up

    // Asynchronous test because node.focus() fires asynchronously on Windows
    setTimeout(function () {
      assert.equal(lis.length, 6, 'Should be 6 list elements');
      assert.deepEqual(evented, [
          { id: 'tablist1tab1', event: 'focus'},
          { id: 'tablist1tab2', event: 'focus'},
          { id: 'tablist1tab1', event: 'focus'},
          { id: 'tablist1tab2', event: 'focus'},
          { id: 'tablist1tab1', event: 'focus'}
        ],
        'Sequence of expected events does not match' /*+ JSON.stringify(evented)*/);
      assert.equal(dom.getAttributes(lis[0]).tabindex, '0', 'First tab should be the focssable one');
      done();
    }, 0);

  });

  it('Static tabs: keyboard events and state', function (done) {
    var ul, element, container = '#tablist1';

    ul = selector.query(container);
    assert.isNotNull(ul, 'Should be able to find the container');

    // execute the function
    markup.ariaTabs(container, {
      indexGetter: function (tab) {
        var atts = dom.getAttributes(tab),
          id = atts.id;

        return id.substring(11); // will remove 'tablist1tab' from the string and return the rest
      }
    });

    element = selector.query('>li', ul);

    events.fire(element, 'focus');

    // timeout, let the focus event fire
    setTimeout(function () {

      /**
       * Stop the given `event`
       *
       * @api private
       * @param {Event} event
       */
      function stop(event) {
        event.preventDefault();
        event.stopPropagation();
      }

      assert.equal(element.tabIndex, 0, 'First tab should be the focssable one');

      events.once(element, 'click', function (clickEvent) {
        stop(clickEvent);

        events.once(element, 'click', function (clickEvent) {
          stop(clickEvent);
          done();
        });

        events.fire(element, 'click');
      });

      events.fire(element, 'keydown', { which: 13, keyCode: 13 });
    }, 10);
  });

  it('Static tabs: keyboard events and state, no eventSelector', function () {
    var ul, lis, container = '#tablist1', evented;

    ul = selector.query(container);
    assert.isNotNull(ul, 'Should be able to find the container');

    // execute the function
    markup.ariaTabs(container, {
      eventSelector: undefined,
      indexGetter: function (tab) {
        var atts = dom.getAttributes(tab),
          id = atts.id;

        return id.substring(11); // will remove 'tablist1tab' from the string and return the rest
      }
    });
    lis = selector.queryAll('>li', ul);
    each(lis, function (value) {
      var a;
      events.on(value, 'click', function (e) {
        evented.push({id: dom.getAttributes(this).id, event: 'click'});
        e.preventDefault(); // prevent click from navigating away from the document
      });
      a = selector.queryAll('a', value)[0];
      events.on(a, 'click', function () {
        evented.push({ type: 'a', event: 'click'});
      });
    });

    evented = [];
    events.fire(lis[0], 'focus');
    assert.equal(dom.getAttributes(lis[0]).tabindex, '0', 'First tab should be the focssable one');
    events.fire(lis[0], 'keydown', {which: 13}); //enter
    assert.deepEqual(evented, [{ id: 'tablist1tab1', event: 'click'}],
      'Should have clicked the first tab');

    evented = [];
    events.fire(lis[0], 'keydown', {which: 32}); //space
    assert.deepEqual(evented, [{ id: 'tablist1tab1', event: 'click'}],
      'Should have clicked the first tab again');
  });

  it('Static tabs: keyboard events and aria-selected', function () {
    var ul, lis, container = '#tablist1';

    ul = selector.query(container);
    assert.isNotNull(ul, 'Should be able to find the container');

    // execute the function
    markup.ariaTabs(container, {
      indexGetter: function (tab) {
        var atts = dom.getAttributes(tab),
          id = atts.id;

        return id.substring(11); // will remove 'tablist1tab' from the string and return the rest
      }
    });

    lis = selector.queryAll('>li', ul);
    each(lis, function (value) {
      events.on(value, 'click', function (e) {
        e.preventDefault(); // prevent click from navigating away from the document
      });
    });
    events.fire(lis[0], 'focus');
    assert.equal(dom.getAttributes(lis[0])['aria-selected'], 'false',
      'The selected state of the tab should be false');
    events.fire(lis[0], 'keydown', {which: 13}); //enter
    assert.equal(dom.getAttributes(lis[0])['aria-selected'], 'true',
      'The selected state of the tab should now be set to true');
  });

  it('Static tabs: keyboard events and aria-selected, no eventSelector', function () {
    var ul, lis, container = '#tablist1';

    ul = selector.query(container);
    assert.isNotNull(ul, 'Should be able to find the container');

    // execute the function
    markup.ariaTabs(container, {
      eventSelector: undefined,
      indexGetter: function (tab) {
        var atts = dom.getAttributes(tab),
          id = atts.id;

        return id.substring(11); // will remove 'tablist1tab' from the string and return the rest
      }
    });
    lis = selector.queryAll('>li', ul);
    each(lis, function (value) {
      events.on(value, 'click', function (e) {
        e.preventDefault(); // prevent click from navigating away from the document
      });
    });
    events.fire(lis[0], 'focus');
    assert.equal(dom.getAttributes(lis[0])['aria-selected'], 'false',
      'The selected state of the tab should be false');
    events.fire(lis[0], 'keydown', {which: 13}); //enter
    assert.equal(dom.getAttributes(lis[0])['aria-selected'], 'true',
      'The selected state of the tab should now be set to true');
  });


  var genericFixture = [
    '<div id="tabs-container">',
    '<div id="topnav" role="navigation">',
    '<ul>',
    '<li><a href="#" id="home">Home</a></li>',
    '<li><a href="#" id="laptops">Laptops &amp; Notebooks</a></li>',
    '<li><a href="#" id="desktops">Desktops</a></li>',
    '</ul>',
    '<br /><br /><br />',
    '</div>',
    '<div class="tabber" id="tab0">',
    '<p>NinjaCatephants</p>',
    '</div>',
    '<div class="tabber" id="tab1">',
    '<h3>Bob Loblaw</h3>',
    '<p>',
    'Lorem ipsum dolor sit amet',
    '</p>',
    '</div>',
    '<div class="tabber" id="tab2">',
    '<p>lakdjsf lkasdjf lkjsdf isd</p>',
    '</div>'
  ].join('');

  describe('clicks on eventSelector (AM-81)', function () {

    beforeEach(function () {
      document.getElementById('fixture').innerHTML = genericFixture;
    });

    it('should display the proper panel and hide the others', function () {
      markup.ariaTabs('#topnav>ul', {
        ownsSelector: '#tab{index}'
      });
      var laptops = document.getElementById('laptops'),
        tab0 = document.getElementById('tab0'),
        tab1 = document.getElementById('tab1'),
        tab2 = document.getElementById('tab2');

      events.fire(laptops, 'click');
      events.on(laptops, 'click', function () {
        assert.equal(tab1.style.display, 'block');
        assert.equal(tab0.style.display, 'none');
        assert.equal(tab2.style.display, 'none');
      });
    });

    it('should add selected class to the li(parent of clicked eventSelector)', function () {
      markup.ariaTabs('#topnav>ul', {
        ownsSelector: '#tab{index}'
      });
      var laptops = document.getElementById('laptops'),
        parent = laptops.parentNode;

      events.fire(laptops, 'click');
      events.on(laptops, 'click', function () {
        //check for selected class on the selected tab's <li />
        assert.equal(parent.className, 'selected');

      });
    });

    it('should not have selected class on tab li\'s that are not selected', function () {
      markup.ariaTabs('#topnav>ul', {
        ownsSelector: '#tab{index}'
      });
      var laptops = document.getElementById('laptops'),
        homeParent = document.getElementById('home').parentNode,
        desktopsParent = document.getElementById('desktops').parentNode;

      events.fire(laptops, 'click');
      events.on(laptops, 'click', function () {
        assert.notEqual(homeParent.className, 'selected');
        assert.notEqual(desktopsParent.className, 'selected');
      });
    });

    it('should set aria-selected=true to the selected tab\'s <li />', function () {
      markup.ariaTabs('#topnav>ul', {
        ownsSelector: '#tab{index}'
      });
      var laptops = document.getElementById('laptops'),
        parent = laptops.parentNode;

      events.fire(laptops, 'click');
      events.on(laptops, 'click', function () {
        //check for aria-selected=true attribute on the selected tab's <li />
        assert.equal(parent.getAttribute('aria-selected'), true);
      });
    });

    it('should set aria-selected=false to the non-selected tab\'s <li />\'s', function () {
      markup.ariaTabs('#topnav>ul', {
        ownsSelector: '#tab{index}'
      });
      var laptops = document.getElementById('laptops'),
        homeParent = document.getElementById('home').parentNode,
        desktopsParent = document.getElementById('desktops').parentNode;

      events.fire(laptops, 'click');
      events.on(laptops, 'click', function () {
        assert.notEqual(homeParent.getAttribute('aria-selected'), false);
        assert.notEqual(desktopsParent.getAttribute('aria-selected'), false);
      });
    });
  });


  describe('AM-33', function () {

    beforeEach(function () {
      document.getElementById('fixture').innerHTML = genericFixture;
    });

    describe('hidePanels', function () {

      it('should display the first tab when truthy', function () {
        markup.ariaTabs('#topnav>ul', {
          ownsSelector: '#tab{index}',
          eventsSelector: null,
          hidePanels: true
        });

        var tabs = selector.queryAll('.tabber');

        each(tabs, function (tab, index) {
          if (index === 0) {
            assert.equal(tab.style.display, 'block');
          } else {
            assert.equal(tab.style.display, 'none', 'Tab #' + index + ' should be display none');
          }
        });
      });

      it('should display the first tab when omitted', function () {
        markup.ariaTabs('#topnav>ul', {
          ownsSelector: '#tab{index}',
          eventsSelector: null
        });

        var tabs = selector.queryAll('.tabber');

        each(tabs, function (tab, index) {
          if (index === 0) {
            assert.equal(tab.style.display, 'block');
          } else {
            assert.equal(tab.style.display, 'none', 'Tab #' + index + ' should be display none');
          }
        });
      });

      it('should display block on the first panel when falsey', function () {
        markup.ariaTabs('#topnav>ul', {
          ownsSelector: '#tab{index}',
          eventSelector: null,
          hidePanels: false
        });

        var tabs = selector.queryAll('.tabber'),
          first = tabs.shift();


        // first should be display:block
        assert.equal(first.style.display, 'block', 'first one should be display block');
        //and the rest should be display:none
        each(tabs, function (tab) {
          assert.equal(tab.style.display, 'none', 'the rest should have display none');
        });
      });

    });

    it('should add selectedClass to the active/selected tab', function (done) {
      markup.ariaTabs('#topnav>ul', {
        ownsSelector: '#tab{index}',
        eventsSelector: null,
        hidePanels: true
      });

      var firstItem = selector.query('#topnav li');

      events.fire(firstItem, 'click');

      setTimeout(function () {
        assert.isTrue(dom.classList(firstItem).has('selected'));
        done();
      }, 10);


    });
    it('should fire the provided callback function', function (done) {
      markup.ariaTabs('#topnav>ul', {
        ownsSelector: '#tab{index}',
        eventsSelector: null,
        hidePanels: true,
        callback: function () {
          done();
        }
      });

      var firstItem = selector.query('#topnav li');

      events.fire(firstItem, 'click');
    });
    it('should optionally use the provided event-type listener', function (done) {
      markup.ariaTabs('#topnav>ul', {
        ownsSelector: '#tab{index}',
        eventsSelector: null,
        hidePanels: true,
        callback: function () {
          done();
        },
        event: 'cats'
      });

      var firstItem = selector.query('#topnav li');

      // it's not possible to fire a pseudo "cats" event in IE8
      //
      //   Error: Invalid argument.
      //
      if (document.body.attachEvent && !document.body.addEventListener) {
        return done();
      }

      events.fire(firstItem, 'cats');
    });

  });
});
