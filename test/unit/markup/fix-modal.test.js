describe('markup.fixModal', function () {
  'use strict';

  var markup = require('markup');
  var selector = require('selector');
  var events = require('events');
  var dom = require('dom');
  var html = '<div id="div"><button id="launch">Launch</button></div>';
  var modalHtml =
      '    <div id="modal" style="display:none;">' +
      '      <p class="p">this is a paragraph</p>' +
      '      <input type="text" value="my text" id="text"/>' +
      '      <input class="close" id="close" type="image" src="blah.jpg" style="width:10px;height:10px;" alt="Close"/>' +
      '      <p class="p">this is a paragraph</p>' +
      '      <p class="p maybe">this is a paragraph</p>' +
      '      <input type="hidden" value="my text" id="hidden"/>' +
      '    </div>';
  var modalDivCloseHtml =
      '      <div id="modal" style="display:none;">' +
      '      <p class="p">this is a paragraph</p>' +
      '      <input type="text" value="my text" id="text"/>' +
      '      <div class="close" id="close">Close</div>' +
      '      <p class="p">this is a paragraph</p>' +
      '      <p class="p maybe">this is a paragraph</p>' +
      '      <input type="hidden" value="my text" id="hidden"/>' +
      '    </div>';
  var modalACloseHtml =
      '      <div id="modal" style="display:none;">' +
      '      <p class="p">this is a paragraph</p>' +
      '      <input type="text" value="my text" id="text"/>' +
      '      <a class="close" id="close" href="#">Close</a>' +
      '      <p class="p">this is a paragraph</p>' +
      '      <p class="p maybe">this is a paragraph</p>' +
      '      <input type="hidden" value="my text" id="hidden"/>' +
      '    </div>';
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    fixture.innerHTML = html;
    fixture.id = 'fixture';
    document.body.appendChild(fixture);
  });

  afterEach(function () {
    try {
      document.body.removeChild(fixture);
    } catch (e) {}
  });

  it('default: should set initial focus to the modal itself', function (done) {
    var div, launcher, modal;

    div = selector.query('#div');
    div.innerHTML += modalHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    events.on(launcher, 'click', function () {
      modal.style.display = 'block';
      setTimeout(function () {
        assert.equal(modal.getAttribute('tabindex'), '-1', 'modal container should have its tabindex set to -1');
        assert.ok(document.activeElement === modal, 'initial focus should be on the modal container itself');
        events.fire(modal, 'blur');
        setTimeout(function () {
          assert.ok(modal.getAttribute('tabindex') === '' ||
            modal.getAttribute('tabindex') === null, 'modal container should have no tabindex');
          done();
        }, 100);
      }, 100);
    });
    events.on(modal, 'focus', function () {
      assert.ok(true, 'container should receive the focus event');
    });
    markup.fixModal(launcher, {
      modalSelector : '#modal',
      closer : 'input.close',
      callback : function () {},
      delay : 1
    });
    events.fire(launcher, 'click');
  });

  it('mousedown: should set initial focus to the modal itself', function (done) {
    var div, launcher, modal;

    div = selector.query('#div');
    div.innerHTML += modalHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    events.on(launcher, 'mousedown', function () {
      modal.style.display = 'block';
      setTimeout(function () {
        assert.equal(modal.getAttribute('tabindex'), '-1', 'modal container should have its tabindex set to -1');
        assert.ok(document.activeElement === modal, 'initial focus should be on the modal container itself');
        events.fire(modal, 'blur');
        setTimeout(function () {
          assert.ok(modal.getAttribute('tabindex') === '' ||
            modal.getAttribute('tabindex') === null, 'modal container should have no tabindex');
          done();
        }, 100);
      }, 100);
    });
    events.on(modal, 'focus', function () {
      assert.ok(true, 'container should receive the focus event');
    });
    markup.fixModal(launcher, {
      modalSelector : '#modal',
      closer : 'input.close',
      callback : function () {},
      delay : 1,
      openEvent : 'mousedown'
    });
    events.fire(launcher, 'mousedown');
  });

  it('keydown(ENTER): should set initial focus to the modal itself', function (done) {
    var div, launcher, modal;

    div = selector.query('#div');
    div.innerHTML += modalHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    events.on(launcher, 'keydown', function () {
      modal.style.display = 'block';
      setTimeout(function () {
        assert.equal(modal.getAttribute('tabindex'), '-1', 'modal container should have its tabindex set to -1');
        assert.ok(document.activeElement === modal, 'initial focus should be on the modal container itself');
        events.fire(modal, 'blur');
        setTimeout(function () {
          assert.ok(modal.getAttribute('tabindex') === '' ||
            modal.getAttribute('tabindex') === null, 'modal container should have no tabindex');
          done();
        }, 100);
      }, 100);
    });
    events.on(modal, 'focus', function () {
      assert.ok(true, 'container should receive the focus event');
    });
    markup.fixModal(launcher, {
      modalSelector : '#modal',
      closer : 'input.close',
      callback : function () {},
      delay : 1,
      openEvent : 'keydown'
    });
    events.fire(launcher, 'keydown', { keyCode : 13});
  });

  it('keydown(SPACE): should set initial focus to the modal itself', function (done) {
    var div, launcher, modal;

    div = selector.query('#div');
    div.innerHTML += modalHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    events.on(launcher, 'keydown', function () {
      modal.style.display = 'block';
      setTimeout(function () {
        assert.equal(modal.getAttribute('tabindex'), '-1', 'modal container should have its tabindex set to -1');
        assert.ok(document.activeElement === modal, 'initial focus should be on the modal container itself');
        events.fire(modal, 'blur');
        setTimeout(function () {
          assert.ok(modal.getAttribute('tabindex') === '' ||
            modal.getAttribute('tabindex') === null, 'modal container should have no tabindex');
          done();
        }, 100);
      }, 100);
    });
    events.on(modal, 'focus', function () {
      assert.ok(true, 'container should receive the focus event');
    });
    markup.fixModal(launcher, {
      modalSelector : '#modal',
      closer : 'input.close',
      callback : function () {},
      delay : 1,
      openEvent : 'keydown'
    });
    events.fire(launcher, 'keydown', { keyCode : 32});
  });

  it('keydown(Ctrl-SPACE): should NOT set initial focus to the modal itself', function (done) {
    var div, launcher, modal;

    div = selector.query('#div');
    div.innerHTML += modalHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    events.on(launcher, 'keydown', function () {
      modal.style.display = 'block';
      setTimeout(function () {
        assert.notEqual(modal.getAttribute('tabindex'), '-1', 'modal container should NOT have its tabindex set to -1');
        done();
      }, 100);
    });
    markup.fixModal(launcher, {
      modalSelector : '#modal',
      closer : 'input.close',
      callback : function () {},
      delay : 1,
      openEvent : 'keydown'
    });
    events.fire(launcher, 'keydown', { keyCode : 32, ctrlKey : true});
  });

  it('keydown(Ctrl-ENTER): should NOT set initial focus to the modal itself', function (done) {
    var div, launcher, modal;

    div = selector.query('#div');
    div.innerHTML += modalHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    events.on(launcher, 'keydown', function () {
      modal.style.display = 'block';
      setTimeout(function () {
        assert.notEqual(modal.getAttribute('tabindex'), '-1', 'modal container should NOT have its tabindex set to -1');
        done();
      }, 100);
    });
    markup.fixModal(launcher, {
      modalSelector : '#modal',
      closer : 'input.close',
      callback : function () {},
      delay : 1,
      openEvent : 'keydown'
    });
    events.fire(launcher, 'keydown', { keyCode : 13, ctrlKey : true});
  });

  it('shift tab handling on first', function (done) {
    var div, launcher, modal, text, close;

    div = selector.query('#div');
    div.innerHTML += modalHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    text = selector.query('#text');
    close = selector.query('#close');
    events.on(launcher, 'click', function () {
      modal.setAttribute('style', 'display:block;');
      setTimeout(function () {
        events.fire(text, 'keydown', { keyCode : 9, which : 9, shiftKey : true });
      }, 500);
    });
    events.on(close, 'focus', function () {
      assert.ok(true, 'second input should receive the focus event');
      done();
    });
    markup.fixModal(launcher, {
      modalSelector : '#modal',
      closer : '.close',
      callback : function () {},
      delay : 100
    });
    events.fire(launcher, 'click');
  });

  it('shift tab handling on modal', function (done) {
    var div, launcher, modal, text, close;

    div = selector.query('#div');
    div.innerHTML += modalHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    text = selector.query('#text');
    close = selector.query('#close');
    events.on(launcher, 'click', function () {
      modal.setAttribute('style', 'display:block;');
      setTimeout(function () {
        events.fire(modal, 'keydown', { keyCode : 9, which : 9, shiftKey : true });
      }, 500);
    });
    events.on(close, 'focus', function () {
      assert.ok(true, 'second input should receive the focus event');
      done();
    });
    markup.fixModal(launcher, {
      modalSelector : '#modal',
      closer : '.close',
      callback : function () {},
      delay : 100
    });
    events.fire(launcher, 'click');
  });

  it('tab handling on last', function (done) {
    var div, launcher, modal, text, close;

    div = selector.query('#div');
    div.innerHTML += modalHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    text = selector.query('#text');
    close = selector.query('#close');
    events.on(launcher, 'click', function () {
      modal.setAttribute('style', 'display:block;');
      setTimeout(function () {
        events.fire(close, 'keydown', { keyCode : 9, which : 9 });
      }, 500);
    });
    events.on(text, 'focus', function () {
      assert.ok(true, 'first input should receive the focus event');
      done();
    });
    markup.fixModal(launcher, {
      modalSelector : '#modal',
      closer : '.close',
      callback : function () {},
      delay : 100
    });
    events.fire(launcher, 'click');
  });

  it('tab handling on modal', function (done) {
    var div, launcher, modal, text, close;

    div = selector.query('#div');
    div.innerHTML += modalHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    text = selector.query('#text');
    close = selector.query('#close');
    events.on(launcher, 'click', function () {
      modal.setAttribute('style', 'display:block;');
      setTimeout(function () {
        events.fire(modal, 'keydown', { keyCode : 9, which : 9 });
      }, 500);
    });
    events.on(text, 'focus', function () {
      assert.ok(true, 'first input should receive the focus event');
      done();
    });
    markup.fixModal(launcher, {
      modalSelector : '#modal',
      closer : '.close',
      callback : function () {},
      delay : 1
    });
    events.fire(launcher, 'click');
  });

  it('Close(selector) should get tabindex if simulated button', function (done) {
    var div, launcher, modal, close;

    div = selector.query('#div');
    div.innerHTML += modalDivCloseHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    close = selector.query('#close');
    events.on(launcher, 'click', function () {
      modal.setAttribute('style', 'display:block;');
      setTimeout(function () {
        assert.equal(close.getAttribute('tabindex'), '0', 'Close div should have its tabindex set to 0');
        assert.notEqual(close.getAttribute('role'), 'button', 'Close div should not have its role set to button');
        done();
      }, 100);
    });
    markup.fixModal(launcher, {
      modalSelector: '#modal',
      closer: '.close',
      callback: function () {},
      delay: 1
    });
    events.fire(launcher, 'click');
  });

  it('Close(element) should get tabindex if simulated button', function (done) {
    var div, launcher, modal, close;

    div = selector.query('#div');
    div.innerHTML += modalDivCloseHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    close = selector.query('#close');
    events.on(launcher, 'click', function () {
      modal.setAttribute('style', 'display:block;');
      setTimeout(function () {
        assert.equal(close.getAttribute('tabindex'), '0', 'Close div should have its tabindex set to 0');
        assert.notEqual(close.getAttribute('role'), 'button', 'Close div should not have its role set to button');
        done();
      }, 100);
    });
    markup.fixModal(launcher, {
      modalSelector: '#modal',
      closer: '.close',
      callback: function () {},
      delay: 1
    });
    events.fire(launcher, 'click');
  });

  it('Close(selector) should not get role if anchor simulated button', function (done) {
    var div, launcher, modal, close;

    div = selector.query('#div');
    div.innerHTML += modalACloseHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    close = selector.query('#close');
    events.on(launcher, 'click', function () {
      modal.setAttribute('style', 'display:block;');
      setTimeout(function () {
        assert.ok(close.getAttribute('tabindex') !== '0', 'Close anchor should NOT have its tabindex set to 0');
        assert.notEqual(close.getAttribute('role'), 'button', 'Close anchor should not have its role set to button');
        done();
      }, 100);
    });
    markup.fixModal(launcher, {
      modalSelector: '#modal',
      closer: '.close',
      callback: function () {},
      delay: 1
    });
    events.fire(launcher, 'click');
  });

  it('Close(element) should not get role if anchor simulated button', function (done) {
    var div, launcher, modal, close;

    div = selector.query('#div');
    div.innerHTML += modalACloseHtml;
    launcher = selector.query('#launch');

    modal = selector.query('#modal');
    close = selector.query('#close');
    events.on(launcher, 'click', function () {
      modal.setAttribute('style', 'display:block;');
      setTimeout(function () {
        assert.ok(close.getAttribute('tabindex') !== '0', 'Close anchor should NOT have its tabindex set to 0');
        assert.notEqual(close.getAttribute('role'), 'button', 'Close anchor should not have its role set to button');
        done();
      }, 100);
    });
    markup.fixModal(launcher, {
      modalSelector: '#modal',
      closer: '.close',
      callback: function () {},
      delay: 1
    });
    events.fire(launcher, 'click');
  });

  describe('AM-122: first & last "tabindex=-1"', function () {
    beforeEach(function () {
      document.getElementById('fixture').innerHTML = [
        '<a href="#" id="open">open modal</a>',
        '<div id="modal">',
        '  <div>',
        '    <h3>modal heading</h3>',
        '  </div>',
        '  <div>',
        // first
        '    <a href="#" id="close" tabindex="-1">close</a>',
        '  </div>',
        '  <ul>',
        '    <li><button>button1</button></li>',
        '    <li><button>button2</button></li>',
        '    <li><button>button3</button></li>',
        '    <li><button>button4</button></li>',
        '    <li><button>button5</button></li>',
        '    <li><button>button6</button></li>',
        '    <li><button>button7</button></li>',
        '    <li><button>button8</button></li>',
        // last
        '    <li><button tabindex="-1" id="last">button9</button></li>',
        '  </ul>',
        '</div>'
      ].join('');
    });

    it('should allow for backward-circular tabbing', function (done) {
      var open = selector.query('#open'),
          modal = selector.query('#modal'),
          count = 0;

      // make the actual assertion 100 times
      function next() {
        var active = document.activeElement,
            // lazy way to verify the active element is
            // contained within the modal: the modal must
            // be *somewhere* up the dom
            inModal = dom.findUp(active, '#modal');

        assert.ok(inModal, 'focus should stay in the modal');

        // do this 100 times
        count++;
        if (count === 100) {
          return done();
        }

        // fire SHIFT+TAB from the currently focused element
        events.fire(active, 'keydown', {
          which: 9,
          keyCode: 9,
          shiftKey: true
        });

        // let the keydown event fire, then do it again
        setTimeout(next, 0);
      }

      // callback from fixModal; starts the SHIFT+TAB chain
      function start() {
        // give everything a moment; the fixModal thing is delay based
        setTimeout(function () {
          events.fire(modal, 'keydown', {
            which: 9,
            keyCode: 9,
            shiftKey: true
          });

          // left the keydown fire, then start the assertions
          setTimeout(next, 0);
        }, 10);
      }

      markup.fixModal(open, {
        modalSelector: '#modal',
        closer: '#close',
        callback: start
      });

      events.fire(open, 'click');
    });

    it('should allow for forward-circular tabbing', function (done) {
      var open = selector.query('#open'),
          modal = selector.query('#modal'),
          count = 0;

      // make the actual assertion 100 times
      function next() {
        var active = document.activeElement,
            // lazy way to verify the active element is
            // contained within the modal: the modal must
            // be *somewhere* up the dom
            inModal = dom.findUp(active, '#modal');

        assert.ok(inModal, 'focus should stay in the modal');

        // do this 100 times
        count++;
        if (count === 100) {
          return done();
        }

        // fire SHIFT+TAB from the currently focused element
        events.fire(active, 'keydown', {
          which: 9,
          keyCode: 9,
          shiftKey: false
        });

        // let the keydown event fire, then do it again
        setTimeout(next, 0);
      }

      // callback from fixModal; starts the TAB chain
      function start() {
        // give everything a moment; the fixModal thing is delay based
        setTimeout(function () {
          events.fire(modal, 'keydown', {
            which: 9,
            keyCode: 9,
            shiftKey: false
          });

          // left the keydown fire, then start the assertions
          setTimeout(next, 0);
        }, 10);
      }

      markup.fixModal(open, {
        modalSelector: '#modal',
        closer: '#close',
        callback: start
      });

      events.fire(open, 'click');
    });
  });
});
