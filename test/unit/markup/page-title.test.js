//tests for page title
describe('markup.pageTitle', function () {
  'use strict';

  var oldTitle;
  var pageTitle = require('markup').pageTitle;

  // save the current title
  before(function () {
    oldTitle = document.title;
  });

  // before each of the tests, reset the title
  beforeEach(function () {
    document.title = oldTitle;
  });

  after(function () {
    document.title = oldTitle;
  });

  it('should return the new title', function () {
    assert.equal(pageTitle('hello'), 'hello');
  });

  describe('Append', function () {
    it('should append the title provided in options', function () {
      pageTitle('append test', true);

      var actual = document.title,
          // if there was no old title, we don't need the space
          expected = (oldTitle ? oldTitle + ' ' : '') + 'append test';

      assert.equal(actual, expected);
    });
  });
  describe('Reset', function () {
    it('should replace the title completely', function () {
      pageTitle('ninjas');

      assert.equal(document.title, 'ninjas');

    });
  });
});
