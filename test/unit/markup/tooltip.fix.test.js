'use strict';

describe('AM-81 markup.tooltip.fix', function () {

	var markup = require('markup');
	var fixTooltip = markup.tooltip.fix;
	var selector = require('selector');
	var fixture = document.getElementById('fixture');

	beforeEach(function () {
		fixture.innerHTML = '<a href="#" id="tipsy">Hi my name is tool</a>' +
							'<div class="tooltip" id="tooltrip">I do tippy fixy stuff</div>' +
							'<input type="text" class="input-tool">' +
							'<div class="tipster">Goodbye world</div>' +
							'<input type="text" id="cats" aria-describedby="error1">' +
							'<div class="tool" id="other">Im a tool</div>' +
							'<input type="text" id="salsa" aria-describedby="queso">' +
							'<div id="queso">Tacos</div>';
	});

	afterEach(function () {
		fixture.innerHTML = '';
	});

	it('should take string selectors', function () {
		fixTooltip('#tipsy', '.tooltip');
	});
	it('should take HTMLElement selectors', function () {
		var tool = document.getElementById('tipsy'),
			tip = selector.query('.tooltip');

		fixTooltip(tool, tip);
	});
	it('should generate an id for a tooltip lacking one', function () {
		var target = selector.query('.input-tool'),
			tooltip = selector.query('.tipster');

		fixTooltip(target, tooltip);

		assert.ok(tooltip.id);
	});
	it('should set aria-describedby attribute to the target if it does not already have a value', function () {
		var target = document.getElementById('tipsy'),
			tooltip = document.getElementById('tooltrip');

		fixTooltip(target, tooltip);

		assert.equal(target.getAttribute('aria-describedby'), 'tooltrip');

	});
	it('should add to the existing aria-describedby value if it is not already properly set', function () {
		var target = document.getElementById('cats'),
			tip = document.getElementById('other');

		fixTooltip(target, tip);
		assert.equal(target.getAttribute('aria-describedby'), 'error1 other');
	});
	it('should leave the value of aria-describedby if it is already properly set for the tooltip', function () {
		var target = document.getElementById('salsa'),
			tooltip = document.getElementById('queso');

		fixTooltip(target, tooltip);

		assert.equal(target.getAttribute('aria-describedby'), 'queso');
	});
	it('should display: block on focus (default function)', function (done) {
		var target = document.getElementById('tipsy'),
			tooltip = document.getElementById('tooltrip');

		fixTooltip(target, tooltip);

		target.focus();
		setTimeout(function () {
			assert.equal(tooltip.style.display, 'block');
			done();
		}, 10);

	});
	it('should display: none on blur (default function)', function (done) {
		var target = document.getElementById('tipsy'),
			tooltip = document.getElementById('tooltrip');

		fixTooltip(target, tooltip);

		target.focus();
		target.blur();
		setTimeout(function () {
			assert.equal(tooltip.style.display, 'none');
			done();
		}, 10);
	});
	it('should execute the options.show function', function (done) {
		var target = document.getElementById('tipsy'),
			tooltip = document.getElementById('tooltrip');

		fixTooltip(target, tooltip, {
			show: function (tooltip) {
				tooltip.id = 'hello';
			}
		});

		target.focus();
		setTimeout(function () {
			assert.equal(tooltip.id, 'hello');
			done();
		}, 10);
	});
	it('should execute the options.hide function', function (done) {
		var target = document.getElementById('tipsy'),
			tooltip = document.getElementById('tooltrip');

		fixTooltip(target, tooltip, {
			show: function () {
				target.blur();
			},
			hide: function (tooltip) {
				tooltip.id = 'hello';
			}
		});

		target.focus();
		setTimeout(function () {
			assert.equal(tooltip.id, 'hello');
			done();
		}, 10);
	});

});
