describe('markup.landmarksModal', function () {
  'use strict';

  var markup = require('markup');
  var selector = require('selector');
  var events = require('events');
  var each = require('collections').each;
  var SkipModal = markup.SkipModal;
  var landmarksModal = markup.landmarksModal;
  var fixture = null;

  var LANDMARKS_HTML = [
      '<div id="header" role="banner">A banner image and introductory title</div>',
      '<div role="banner">THis is another banner</div>',
      '<div id="sitelookup" role="search">....</div>',
      '<div id="nav" role="navigation" tabindex="0">',
      '  ...a list of links here ...',
      '</div>',
      '<div id="content" role="main">',
      '  ... Ottawa is the capital of Canada ...',
      '</div>',
      '<div id="rightsideadvert" role="complementary" tabindex="4">',
      '  ....an advertisement here...',
      '</div>',
      '<div id="footer" role="contentinfo">',
      '  &copy; The Freedom Company, 123 Freedom Way, Helpville, USA',
      '</div>',
      '<div id="leftnav" role="navigation" aria-labelledby="leftnavheading">',
      '  <h2 id="leftnavheading">Institutional Links</h2>',
      '  <ul><li>...a list of links here ...</li></ul>',
      '</div>',
      '<div id="rightnav" role="navigation" aria-labelledby="rightnavheading">',
      '  <h2 id="rightnavheading">Related topics</h2>',
      '  <ul><li>...a list of links here ...</li></ul>',
      '</div>',
      '<div role="region" aria-label="weather portlet">',
      '  Things are in here for you to enjoy.',
      '  <span>foo</span>',
      '</div>',
      '<div role="elephant">',
      '  <span>mouse</span>',
      '</div>'
    ].join('');
  var HEADINGS_HTML = [
      '<h1>hello</h1>',
      '<h2>hello</h2>',
      '<h3>hello</h3>',
      '<h4>hello</h4>',
      '<h5>hello</h5>',
      '<h6>hello</h6>',
      '<h1>hello</h1>',
      '<h2>hello</h2>',
      '<h3>hello</h3>',
      '<h4>hello</h4>',
      '<h5>hello</h5>',
      '<h6>hello</h6>'
    ].join('');
  var EXCLUDED_HTML = [
      '<div class="excluded" role="menuitem">menuitem</div>',
      '<div class="excluded" role="menuitem">menuitem</div>',
      '<div class="excluded" role="listitem">listitem</div>',
      '<div class="excluded" role="row">row</div>'
    ].join('');

  beforeEach(function () {
    fixture = document.createElement('div');
    fixture.id = 'fixture';
    document.body.appendChild(fixture);
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should be a function', function () {
    assert.isFunction(landmarksModal);
  });

  it('should have defaults', function () {
    assert.isObject(landmarksModal.defaults);
    assert.isArray(landmarksModal.defaults.includedRoles);
    assert.isArray(landmarksModal.defaults.includedHeadings);
  });

  it('should return a SkipModal', function () {
    var modal = landmarksModal();
    assert.isInstanceOf(modal, SkipModal);
    modal.destroy();
  });

  it('should pass options along', function () {
    var modal = landmarksModal({
      id: 'hello-world'
    });

    assert.equal(modal.get('id'), 'hello-world');
    modal.destroy();
  });

  it('should use all landmarks', function () {
    fixture.innerHTML = LANDMARKS_HTML;

    var sel = '[role="' +
          landmarksModal.defaults.includedRoles.join('"],[role="') +
          '"]',
      landmarks = selector.queryAll(sel),
      modal = landmarksModal({
        id: 'hello-world'
      });

    each(landmarks, function (landmark) {
      assert.includes(modal.elements, landmark);
    });

    modal.destroy();
  });

  it('should use all headings', function () {
    fixture.innerHTML = HEADINGS_HTML;

    var headings = fixture.childNodes,
      modal = landmarksModal({
        id: 'hello-world'
      });

    each(headings, function (landmark) {
      assert.includes(modal.elements, landmark);
    });

    modal.destroy();
  });

  it('should ignore the excluded selectors', function () {
    fixture.innerHTML = LANDMARKS_HTML + EXCLUDED_HTML;

    var excluded = selector.queryAll('#fixture .excluded'),
      modal = landmarksModal({
        id: 'hello-world'
      });

    each(excluded, function (landmark) {
      assert.doesNotInclude(modal.elements, landmark);
    });

    modal.destroy();
  });

  it('should add a skip link', function () {
    landmarksModal({
      id: 'hello-ninjas!'
    }).destroy();

    assert.ok(document.getElementById(landmarksModal.defaults.skipLink.id));
  });
  describe('the skip link', function () {
    var link, modal;

    before(function () {
      modal = landmarksModal({
        id: 'skip-link-test',
        skipLink: {
          id: 'landmarks-skipping-linky-thing'
        }
      });

      link = document.getElementById('landmarks-skipping-linky-thing');
    });

    after(function () {
      modal.destroy();
    });

    it('should open and focus the modal', function (done) {
      var isDone = false;

      modal.on('open', function () {
        if (isDone) {
          return;
        }

        assert.isTrue(modal.isOpen());
        assert.equal(document.activeElement, modal._titleBar);

        isDone = true;
        done();
      }, 0);

      events.fire(link, 'click');
    });
    it('should regain focus when closing the modal', function (done) {
      modal
        .on('escape', function () {
          assert.equal(link, document.activeElement);
          done();
        })
        .show(function () {
          events.fire(modal._titleBar, 'keydown', { which: 27 });
        });
    });
  });

});
