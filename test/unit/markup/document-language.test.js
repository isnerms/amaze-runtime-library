describe('markup.documentLanguage', function () {
  'use strict';

  var markup = require('markup');
  var documentLanguage = markup.documentLanguage;
  var oldLang = document.documentElement.getAttribute('lang');

  beforeEach(function () {
    //resetting document language after each test
    documentLanguage(oldLang, true);
  });

  after(function () {
    documentLanguage(oldLang, true);
  });

  it('should default to overwriting lang', function () {
    documentLanguage('hello', true);
    documentLanguage('hi');
    assert.equal(document.documentElement.getAttribute('lang'), 'hi');
  });

  it('should not overwrite when overwrite=false', function () {
    documentLanguage('hello', true);
    documentLanguage('hi', false);
    assert.equal(document.documentElement.getAttribute('lang'), 'hello');
  });

  it('should set the document language', function () {
    documentLanguage('hi');
    assert.equal(document.documentElement.getAttribute('lang'), 'hi');
  });

  describe('omitting "lang"', function () {
    it('should default to english ("en")', function () {
      documentLanguage();
      assert.equal(document.documentElement.getAttribute('lang'), 'en');
    });
  });

});
