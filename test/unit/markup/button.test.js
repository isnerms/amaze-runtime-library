describe('markup.button', function () {
  'use strict';

  // TODO: look at these tests and work out why they do not work
  // in phantom.js
  if (window.mochaPhantomJS) {
    this.pending = true;
  }

  var each = require('collections').each;
  var markup = require('markup');
  var selector = require('selector');
  var events = require('events');
  var dom = require('dom');

  var testHtml = ['<a href="#" class="blue button research">Research on Google</a>',
  '<a class="blue button search">Search</a>',
  '<i class="icon button map" id="icon-test"></i>',
  '<a href="#" id="submit-button-test" class="blue button reset">Reset</a>',
  '<span class="button login rounded green-theme">Login</span>',
  '<button class="register rounded blue-theme">Register</button>',
  '<input type="button" name="moreinfo" value="More Info"/>'].join('\n');

  var fixture = document.getElementById('fixture');

  afterEach(function () {
    fixture.innerHTML = '';
  });

  beforeEach(function () {
    fixture.innerHTML = testHtml;
  });


  it('should be a function', function () {
    assert.equal(typeof markup.button, 'function');
  });


  it('should work when given a Selector', function () {

    markup.button('a.blue.button');

    var elements = selector('a.blue.button', fixture);
    each(elements, function (element) {
      assert.equal(element.getAttribute('role'), 'button');
      assert.equal(dom.hasClass(element, 'amaze-button'), true);
    });

  });


  it('should work when given a NodeList', function () {

    if (window.navigator.userAgent.indexOf('PhantomJS') !== -1) {
      assert.ok(true, 'typeof NodeList === "function"?');
      return;
    }

    var elements = fixture.getElementsByTagName('a');

    markup.button(elements);

    each(elements, function (element) {
      assert.equal(element.getAttribute('role'), 'button');
      assert.equal(dom.hasClass(element, 'amaze-button'), true);
    });

  });


  it('should work when given an Array', function () {

    var elements = selector.queryAll('a', fixture);

    markup.button(elements);

    each(elements, function (element) {
      assert.equal(element.getAttribute('role'), 'button');
      assert.equal(dom.hasClass(element, 'amaze-button'), true);
    });

  });


  it('should work when given a Single Node Reference', function () {

    var element = document.getElementById('submit-button-test');

    markup.button(element);

    assert.equal(element.getAttribute('role'), 'button');
    assert.equal(dom.hasClass(element, 'amaze-button'), true);

  });

  it('should skip button elements', function () {

    markup.button(fixture.getElementsByTagName('*'));

    var element = selector.query('button', fixture);

    assert.notEqual(element.getAttribute('role'), 'button');
    assert.notEqual(dom.hasClass(element, 'amaze-button'), true);

  });


  it('should skip input w/ type ="button" elements', function () {

    markup.button(fixture.getElementsByTagName('*'));

    var element = selector.query('input', fixture);

    assert.notEqual(element.getAttribute('role'), 'button');
    assert.notEqual(dom.hasClass(element, 'amaze-button'), true);

  });


  it('should add spacebar key support for the element', function (done) {

    markup.button(fixture.getElementsByTagName('*'));

    var element = document.getElementById('icon-test');// this is an <i>

    events.on(element, 'click', function (e) {
      e.preventDefault();
      done();
    });

    events.fire(element, 'keydown', { which: 32 });

  });


  it('should add enter key support for the element', function (done) {

    markup.button(fixture.getElementsByTagName('*'));

    var element = document.getElementById('icon-test');// this is an <i>

    events.on(element, 'click', function (e) {
      e.preventDefault();
      done();
    });

    events.fire(element, 'keydown', { which: 13 });
  });


  it('should NOT add enter key support for an anchor with href', function (done) {

    markup.button(fixture.getElementsByTagName('*'));

    var element = document.getElementById('submit-button-test');// this is an <a href>

    events.on(element, 'click', function (e) {
      e.preventDefault();
      assert(false);
    });

    events.fire(element, 'keydown', { which: 13 });

    setTimeout(done, 500);
  });


  it('should add tabindex to elements that are not natively in the tabindex', function () {

    markup.button(fixture.getElementsByTagName('*'));

    var element = document.getElementById('icon-test');// this is an <i>

    assert.equal(element.hasAttribute('tabindex'), true);

  });


  it('should NOT add tabindex to elements that are natively in the tabindex', function () {

    markup.button(fixture.getElementsByTagName('*'));

    var element = document.getElementById('submit-button-test');// this is an <a href>

    assert.equal(element.hasAttribute('tabindex'), false);

  });


});
