describe('markup.SkipModal', function () {
  'use strict';

  var markup = require('markup');
  var SkipModal = markup.SkipModal;
  var selector = require('selector');
  var each = require('collections').each;
  var events = require('events');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    fixture.id = 'skip_modal_fixture';
    document.body.appendChild(fixture);
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should set default options', function () {
    var modal = new SkipModal();
    assert.deepEqual(modal.options, SkipModal.defaults);
    modal.destroy();
  });

  it('should add default CSS', function () {
    var id = 'amaze-skipmodal-' + SkipModal.defaults.id +
          '-' + SkipModal.defaults.openClass,
        style = document.getElementById(id),
        css = style.styleSheet
            ? style.styleSheet.cssText
            : style.innerHTML;

    assert.equal(style.tagName, 'STYLE');
    assert.notEqual(css.indexOf('#' + SkipModal.defaults.id), -1);
    assert.notEqual(css.indexOf('.' + SkipModal.defaults.openClass), -1);
  });

  it('should throw when adding duplicates', function () {
    var modal1, modal2;
    assert.throws(function () {
      modal1 = new SkipModal();
      modal2 = new SkipModal();
    });
    if (modal1) { modal1.destroy(); }
    if (modal2) { modal2.destroy(); }
  });

  it('should not throw when adding multiples', function () {
    assert.doesNotThrow(function () {
      return new SkipModal(null, { id: 'foo' }).destroy();
    });
    assert.doesNotThrow(function () {
      return new SkipModal(null, { id: 'bar' }).destroy();
    });
    assert.doesNotThrow(function () {
      return new SkipModal(null, { id: 'qax' }).destroy();
    });
  });

  it('should close on escape', function (done) {
    var modal = new SkipModal(null, { id: 'escape-test' });
    modal.set('openClass', 'modal-is-open');
    modal.show();

    // fire a keyboard event (ESCAPE)
    events.fire(modal.container, 'keydown', { which: 27 });

    // wait 10ms, then verify the modal is closed
    setTimeout(function () {
      assert.doesNotInclude(' ' + modal.container.className + ' ',
        ' modal-is-open ');
      modal.destroy();
      done();
    }, 10);
  });

  it('should emit "escape" when escape is pressed', function (done) {
    var modal = new SkipModal(null, { id: 'escape-test-emit' });
    modal
      .show()
      .on('escape', function () {
        modal.destroy();
        done();
      });
    // fire a keyboard event (ESCAPE)
    events.fire(modal.container, 'keydown', { which: 27 });
  });

  describe('#add()', function () {
    var modal;

    before(function () {
      modal = new SkipModal(null, { id: 'WHAM!'});
    });

    after(function (done) {
      modal.destroy(function () {
        done();
      });
    });

    afterEach(function (done) {
      modal.empty(function () {
        done();
      });
    });

    it('should chain', function () {
      var element = document.body.firstChild;
      modal
        .add(element)
        // just do something
        .remove(element);
    });
    it('should add to .elements', function () {
      var element = document.body.firstChild;
      assert.includes(modal.add(element).elements, element);
      // cleanup
      modal.remove(element);
    });
    it('should not add duplicates', function () {
      var expected,
        element = document.body.firstChild;

      modal.add(element);
      expected = modal.elements.length;

      modal.add(element);
      assert.equal(modal.elements.length, expected);
    });
    describe('given allowDuplicates == true', function () {
      it('should add duplicates', function () {
        var element = document.body.firstChild;

        modal
          .add(element)
          .set('allowDuplicates', true)
          .add(element);
        assert.equal(modal.elements.length, 2);
      });
    });
  });

  describe('#remove()', function () {

    var modal, element;

    before(function () {
      modal = new SkipModal(null, { id: 'WHAM!'});
      element = document.body.firstChild;
    });

    after(function () {
      modal.destroy();
    });

    afterEach(function (done) {
      modal.empty(function () {
        done();
      });
    });

    it('should remove by reference', function () {
      modal.add(element).remove(element);
      assert.doesNotInclude(modal.elements, element);
    });
    it('should remove by index', function () {
      modal
        .add(element)
        // first and only element
        .remove(0);
      assert.doesNotInclude(modal.elements, element);
    });
    it('should chain', function () {
      modal
        .add(element)
        .remove(element)
        // just do something
        .empty();
    });
    it('should not error when given a bad index', function () {
      modal.remove(293480324823);
    });
    it('should not error when given a bad reference', function () {
      modal.remove(document.body.lastChild);
    });
  });

  describe('#replace()', function () {
    var modal, oldElement, newElement;

    before(function () {
      modal = new SkipModal(null, { id: 'WHAM!'});
      oldElement = document.body.firstChild;
      newElement = document.body.lastChild;
    });

    afterEach(function () {
      modal.empty();
    });

    after(function () {
      modal.destroy();
    });

    it('should replace an element in the list', function () {
      modal.add(oldElement);
      assert.includes(modal.elements, oldElement);
      modal.replace(oldElement, newElement);
      assert.includes(modal.elements, newElement);
      assert.doesNotInclude(modal.elements, oldElement);
    });
    it('should replace by index', function () {
      modal.add(oldElement);
      assert.includes(modal.elements, oldElement);
      modal.replace(0, newElement);
      assert.includes(modal.elements, newElement);
      assert.doesNotInclude(modal.elements, oldElement);
    });
    it('should chain', function () {
      modal.add(oldElement).replace(oldElement, newElement).empty();
    });
    it('should not error when given a bad reference', function () {
      modal.replace(oldElement, newElement);
    });
  });

  describe('#set', function () {
    var modal;
    before(function () {
      modal = new SkipModal(null, {
        id: 'WHAM!' + Math.floor(Math.random() * 0x0deadbeef)
      });
    });
    after(function () {
      modal.destroy();
    });
    it('should set the given option', function () {
      modal.set('hello', 'world');
      assert.equal(modal.get('hello'), 'world');
    });
    it('should chain', function () {
      assert.equal(modal.set('goodbye', 'universe').get('goodbye'), 'universe');
    });
  });

  describe('#get', function () {
    it('should return the given key', function () {
      var modal = new SkipModal(null, { id: 'should-return-the-given-key'});
      assert.equal(modal.get('id'), 'should-return-the-given-key');
      modal.destroy();
    });
  });

  describe('#_render()', function () {
    var modal, spans;
    beforeEach(function () {
      fixture.innerHTML = [
        '<span>hi</span>', '<span>hi</span>',
        '<span>hi</span>', '<span>hi</span>',
        '<span>hi</span>', '<span>hi</span>',
        '<span>hi</span>', '<span>hi</span>',
        '<span>hi</span>', '<span>hi</span>',
        '<span>hi</span>', '<span>hi</span>',
        '<span>hi</span>', '<span>hi</span>',
        '<span>hi</span>', '<span>hi</span>'
      ].join('');

      spans = selector.queryAll('span', fixture);

      modal = new SkipModal('#skip_modal_fixture span', { id: 'modal-' + Math.random() });
    });
    afterEach(function () {
      modal.destroy();
    });
    it('should should add the title', function () {
      modal
        .set('title', 'Hello world!')
        ._render();

      var h2s = modal.container.getElementsByTagName('h2');

      assert.equal(h2s.length, 1);
      assert.includes(h2s[0].innerHTML, 'Hello world!');
    });
    it('should set tabindex=-1 on the title', function () {
      modal._render();

      var h2 = modal.container.getElementsByTagName('h2')[0];

      assert.equal(h2.tabIndex, -1);
      assert.equal(h2.getAttribute('tabindex'), '-1');

    });
    it('should add a link for each element', function () {
      modal._render();

      var links = selector.queryAll('ul li a.amaze-skip-to-element-link', modal.container);

      assert.equal(links.length, spans.length);
    });
    describe('given trapFocus==true', function () {
      it('should not let a user tab out of the modal');
      it('should not let a user shift+tab out of the modal');
    });
    describe('the added links', function () {
      it('should focus their respective element when clicked', function (done) {
        modal._render();

        // phantomjs sucks
        if (navigator.userAgent.indexOf('PhantomJS') !== -1) {
          return done();
        }

        var links = selector.queryAll('ul li a.amaze-skip-to-element-link',
            modal.container),
          pending = spans.length;

        each(links, function (link, index) {
          var span = spans[index];

          assert.equal(span.tabIndex, -1);

          // callback for focus
          span.onfocus = function () {
            pending--;
            // no more spans to focus?
            if (!pending) {
              // test passed :)
              done();
            }
          };

          link.click();
        });
      });
    });
  });

  describe('#show()', function () {
    var modal;
    beforeEach(function () {
      modal = new SkipModal();
    });
    afterEach(function () {
      modal.destroy();
    });
    it('should render the modal', function (done) {
      // clobber the existing SkipModal#_render
      modal._render = done;
      modal.show();
    });
    it('should add the "openClass"', function () {
      modal.set('openClass', 'OPENCLASS').show();
      assert.includes(' ' + modal.container.className + ' ', ' OPENCLASS ');
    });
    it('should chain', function () {
      modal.show().hide();
    });
    it('should fire the callback', function (done) {
      modal.show(function () {
        done();
      });
    });
    it('should set focus on the titlebar', function (done) {
      modal.show(function (modal) {
        var active = document.activeElement,
          titlebar = modal.container.getElementsByTagName('h2')[0];

        assert.equal(active, titlebar);
        done();
      });
    });
    it('should publish ._titleBar', function (done) {
      modal.show(function (modal) {
        var titlebar = modal.container.getElementsByTagName('h2')[0];

        assert.equal(titlebar, modal._titleBar);
        done();
      });
    });
    it('should emit "open"', function (done) {
      modal.on('open', function () {
        done();
      });
      modal.show();
    });
  });

  describe('#isOpen', function () {
    var modal;
    before(function () {
      modal = new SkipModal(null, {
        id: 'isOpenChecks'
      });
    });
    after(function () {
      modal.destroy();
    });
    it('should return "true" when the modal is open', function () {
      modal.show();
      assert.isTrue(modal.isOpen());
    });
    it('should return "false" when the modal is closed', function () {
      modal.hide();
      assert.isFalse(modal.isOpen());
    });
  });

  describe('#toggle', function () {
    var modal;
    beforeEach(function () {
      modal = new SkipModal();
    });
    afterEach(function () {
      modal.destroy();
    });
    it('should chain', function () {
      modal.toggle().show();
    });
    it('should fire the callback', function (done) {
      modal.toggle(function () {
        done();
      });
    });
    describe('a shown modal', function () {
      it('should remove the "openClass"', function () {
        modal
          .set('openClass', 'theopenclass')
          .show()
          .toggle();
        assert.doesNotInclude(' ' + modal.container.className + ' ',
          ' theopenclass ');
      });
      it('should emit "open"', function (done) {
        modal.on('open', function () {
          done();
        });
        modal.show();
      });
    });
    describe('a hidden modal', function () {
      it('should add the "openClass"', function () {
        modal
          .set('openClass', 'theopenclass')
          .hide()
          .toggle();
        assert.includes(' ' + modal.container.className + ' ',
          ' theopenclass ');
      });
      it('should emit "close"', function (done) {
        modal.on('close', function () {
          done();
        });
        modal.hide();
      });
    });
  });

  describe('#hide', function () {
    var modal;
    beforeEach(function () {
      modal = new SkipModal();
    });
    afterEach(function () {
      modal.destroy();
    });
    it('should chain', function () {
      modal.hide().show();
    });
    it('should remove the "openClass"', function () {
        modal
          .set('openClass', 'theopenclass')
          .show()
          .hide();
        assert.doesNotInclude(' ' + modal.container.className + ' ',
          ' theopenclass ');
      });
    it('should fire the callback', function (done) {
      modal.hide(function () {
        done();
      });
    });
    it('should emit "close"', function (done) {
      modal.on('close', function () {
        done();
      });
      modal.hide();
    });
  });

  describe('#destoy()', function () {
    it('should remove the modal form the DOM', function (done) {
      var id = 'skip_' + Math.floor(Math.random() * 0x0deadbeef);

      new SkipModal(null, { id: id }).show(function (modal) {
        assert.ok(document.getElementById(id));
        modal.destroy(function () {
          assert.isNull(document.getElementById(id));
          done();
        });
      });
    });
    it('should chain', function () {
      var id = 'skip_' + Math.floor(Math.random() * 0x0deadbeef);

      new SkipModal(null, { id: id })
        .destroy()
        // just do something
        .hide();
    });
    it('should fire the callback', function (done) {
      var id = 'skip_' + Math.floor(Math.random() * 0x0deadbeef);

      new SkipModal(null, { id: id }).destroy(function () {
        done();
      });
    });
    it('should emit "destroyed"', function (done) {
      new SkipModal(null, { id: 'destroy-emit-test'})
        .on('destroyed', done)
        .destroy();
    });
  });

  describe('#on()', function () {
    var modal;

    before(function () {
      modal = new SkipModal(null, { id: 'on-checks' });
    });
    after(function () {
      modal.destroy();
    });
    it('should add a listener for the provided event name', function (done) {
      modal.on('foo', done).emit('foo');
    });
  });

  describe('#emit()', function () {
    var modal;

    before(function () {
      modal = new SkipModal(null, { id: 'emit-checks' });
    });
    after(function () {
      modal.destroy();
    });

    it('should emit the event', function (done) {
      modal
        .on('foo', done)
        .emit('foo');
    });

    it('should fire all listeners', function (done) {
      var count = 0;
      modal
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .emit('bar');

      setTimeout(function () {
        assert.equal(count, 11);
        done();
      }, 0);
    });
    it('should be possible to emit more than one event at a time', function (done) {
      var firstFired = false;
      modal
        .on('first', function () {
          firstFired = true;
        })
        .on('second', function () {
          assert.isTrue(firstFired);
          done();
        })
        .emit('first, second');
    });
  });
});
