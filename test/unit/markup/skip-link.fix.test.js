describe('markup.skiplink.fix', function () {
  'use strict';

  var markup = require('markup');
  var events = require('events');

  afterEach(function () {
    document.getElementById('fixture').innerHTML = '';
  });


  function implicitTargetTests(html, target, skiplink) {
    var fixture = document.getElementById('fixture');

    fixture.innerHTML = html;

    skiplink = typeof skiplink === 'function' && skiplink() || skiplink ||
      document.getElementById('skiplink');

    target = typeof target === 'function' && target() || target ||
      document.getElementById('target');

    markup.skipLink.fix(skiplink);

    events.fire(skiplink, 'click');
    assert.equal(document.activeElement, target);
  }

  function explicitTargetTests(html, target, skiplink) {
    var fixture = document.getElementById('fixture');

    fixture.innerHTML = html;

    skiplink = typeof skiplink === 'function' && skiplink() ||
      document.getElementById('skiplink');

    target = typeof target === 'function' && target() ||
      document.getElementById('target');

    markup.skipLink.fix(skiplink, target);

    events.fire(skiplink, 'click');
    assert.equal(document.activeElement, target);
  }

  describe('implicit targets', function () {

    describe('not natively focusable', function () {
      it('should get the target from an ID reference in hash', function () {

        var html = '<a id="skiplink" href="#target">Skip</a>' +
          '<div id="target"></div>';

        implicitTargetTests(html);


      });

      it('should fallback to name if no matching ID is found', function () {

        var html = '<a id="skiplink" href="#target">Skip</a>' +
          '<img name="target" id="t" src="about:blank" />';

        implicitTargetTests(html, function () {
          return document.getElementById('t');
        });

      });

      it('should focus the first element with the name reference', function () {

        var html = '<a id="skiplink" href="#target">Skip</a>' +
          '<img name="target" id="t" src="about:blank">' +
          '<img name="target" src="about:blank"></div>';

        implicitTargetTests(html, function () {
          return document.getElementById('t');
        });

      });
    });

    describe('natively focusable', function () {
      it('should get the target from an ID reference in hash', function () {

        var html = '<a id="skiplink" href="#target">Skip</a>' +
          '<a id="target">hi</a>';

        implicitTargetTests(html);

      });

      it('should fallback to name if no matching ID is found', function () {

        var html = '<a id="skiplink" href="#target">Skip</a>' +
          '<a name="target" id="t">t</a>';

        implicitTargetTests(html, function () {
          return document.getElementById('t');
        });

      });

      it('should focus the first element with the name reference', function () {

        var html = '<a id="skiplink" href="#target">Skip</a>' +
          '<a name="target" id="t">t</a><a name="target">t</a>';

        implicitTargetTests(html, function () {
          return document.getElementById('t');
        });

      });


    });

    it('should accept a selector', function () {

      var skiplink, target,
        fixture = document.getElementById('fixture');

      fixture.innerHTML = '<a id="skiplink" href="#target">Skip</a>' +
        '<a name="target" id="t">t</a>';

      skiplink = document.getElementById('skiplink');
      target = document.getElementById('t');

      markup.skipLink.fix('#skiplink');

      events.fire(skiplink, 'click');
      assert.equal(document.activeElement, target);

    });
  });
  describe('explicit targets', function () {

    describe('not natively focusable', function () {


      // explicit target

      it('should make the target temporarily focusable', function () {

        var html = '<a id="skiplink" href="#target">Skip</a>' +
          '<div id="target"></div>';

        explicitTargetTests(html);

      });

      it('should accept selectors', function () {

        var html = '<a id="skiplink" href="#target">Skip</a>' +
          '<div id="target"></div>';

        explicitTargetTests(html, '#target', '#skiplink');

      });

    });

    describe('natively focusable', function () {


      it('should focus the target', function () {

        var html = '<a id="skiplink" href="#target">Skip</a>' +
          '<a id="target">hi</a>';

        explicitTargetTests(html);

      });

      it('should accept selectors', function () {

        var html = '<a id="skiplink" href="#target">Skip</a>' +
          '<a id="target">hi</a>';

        explicitTargetTests(html, '#target', '#skiplink');

      });

    });
  });

  describe('error', function () {

    it('should throw if it cannot find the target', function () {
      var fixture = document.getElementById('fixture');

      fixture.innerHTML = '<a id="skiplink" href="#target">Skip</a>';

      assert.throws(function () {
        markup.skipLink.fix('#skiplink');
      }, Error);

    });
  });

});
