describe('markup.skiplink.add', function () {
  'use strict';

  var markup = require('markup');
  var events = require('events');

  beforeEach(function () {
    document.getElementById('fixture').innerHTML = '<div id="insertionPoint"></div>' +
      '<div id="target">stuff</div>';
  });
  afterEach(function () {
    document.getElementById('fixture').innerHTML = '';
  });

  describe('basic usage', function () {

    it.skip('should add some styles on the first invocation', function () {
      var called = 0,
        original = window.amaze.css;

      window.amaze.css = function (css) {
        called++;
        original(css);
      };

      markup.skipLink.add('#insertionPoint', '#target');
      markup.skipLink.add('#insertionPoint', '#target');
      assert.equal(called, 1);

      window.amaze.css = original;
    });


    it('should focus the target when the skiplink is clicked', function (done) {

      var target = document.getElementById('target'),
        insertionPoint = document.getElementById('insertionPoint'),
        skiplink = markup.skipLink.add(insertionPoint, target);

      events.on(skiplink, 'click', function () {
        done();
      });

      events.fire(skiplink, 'click');
    });

      // explicit target

    it('should make the target focusable', function (done) {

      var target = document.getElementById('target'),
        insertionPoint = document.getElementById('insertionPoint'),
        skiplink = markup.skipLink.add(insertionPoint, target);

      events.on(skiplink, 'click', function () {
        done();
      });

      events.fire(skiplink, 'click');
    });

    it('should accept selectors', function (done) {
      var skiplink = markup.skipLink.add('#insertionPoint', '#target');

      events.on(skiplink, 'click', function () {
        done();
      });

      events.fire(skiplink, 'click');
    });

    it('should accept `options.text` and create a text node based on it', function () {

      var expected = 'Hello, I am a skip link, click me to skip stuff',
        skiplink = markup.skipLink.add('#insertionPoint', '#target', {
          text: expected
        });

      assert.equal(skiplink.firstChild.nodeValue, expected);

    });

    it('should accept `options.class` and append it to the skiplink\'s className', function () {

      var skiplink = markup.skipLink.add('#insertionPoint', '#target', {
          'class': 'hi'
        });

      assert.equal(skiplink.className, 'amaze-skip-link hi');

    });

    it('should preventDefault', function () {

      var skiplink = markup.skipLink.add('#insertionPoint', '#target');

      skiplink.href = '#should-not-be-set';

      events.fire(skiplink, 'click');

      assert.notEqual(window.location.has, skiplink.href);
    });

  });

  describe('position', function () {

    it('should initially be positioned offscreen', function () {

      var skiplink = markup.skipLink.add('#insertionPoint', '#target'),
        boundingRect = skiplink.getBoundingClientRect();

      assert.ok(boundingRect.left < 9000);
      assert.ok(boundingRect.top < 9000);

    });

    it('should default to NW on focus', function () {

      var insertionPoint = document.getElementById('insertionPoint'),
        skiplink = markup.skipLink.add(insertionPoint, '#target');

      events.fire(skiplink, 'focus');

      assert.equal(skiplink.style.top, insertionPoint.offsetTop + 'px');
      assert.equal(skiplink.style.left, insertionPoint.offsetLeft + 'px');
    });

    it('should clear position on blur', function () {

      var insertionPoint = document.getElementById('insertionPoint'),
        skiplink = markup.skipLink.add(insertionPoint, '#target');

      events.fire(skiplink, 'focus');
      events.fire(skiplink, 'blur');

      assert.equal(skiplink.style.left, '');
      assert.equal(skiplink.style.top, '');

    });

    it('should accept exact, explicit position', function () {

      var expected = {
          left: '10px',
          top: '20px'
        },
        skiplink = markup.skipLink.add('#insertionPoint', '#target', {
          align: expected
        });

      events.fire(skiplink, 'focus');
      assert.equal(skiplink.style.left, expected.left);
      assert.equal(skiplink.style.top, expected.top);

    });

    it('should accept `align: "nw"`', function () {


      var insertionPoint = document.getElementById('insertionPoint'),
        skiplink = markup.skipLink.add(insertionPoint, '#target');

      events.fire(skiplink, 'focus', {
        align: 'nw'
      });

      assert.equal(skiplink.style.top, insertionPoint.offsetTop + 'px');
      assert.equal(skiplink.style.left, insertionPoint.offsetLeft + 'px');
    });

    it('should accept `align: "ne"`', function () {


      var insertionPoint = document.getElementById('insertionPoint'),
        skiplink = markup.skipLink.add(insertionPoint, '#target', {
          align: 'ne'
        });

      events.fire(skiplink, 'focus');

      assert.equal(skiplink.style.top, insertionPoint.offsetTop + 'px');
      assert.equal(skiplink.style.left, (insertionPoint.offsetLeft +
        insertionPoint.offsetWidth - skiplink.offsetWidth) + 'px');
    });

    it('should accept `align: "sw"`', function () {


      var insertionPoint = document.getElementById('insertionPoint'),
        skiplink = markup.skipLink.add(insertionPoint, '#target', {
          align: 'sw'
        });

      events.fire(skiplink, 'focus');

      assert.equal(skiplink.style.top, (insertionPoint.offsetTop +
        insertionPoint.offsetHeight - skiplink.offsetHeight) + 'px');

      assert.equal(skiplink.style.left, insertionPoint.offsetLeft + 'px');
    });

    it('should accept `align: "se"`', function () {


      var insertionPoint = document.getElementById('insertionPoint'),
        skiplink = markup.skipLink.add(insertionPoint, '#target', {
          align: 'se'
        });

      events.fire(skiplink, 'focus');

      assert.equal(skiplink.style.top, (insertionPoint.offsetTop +
        insertionPoint.offsetHeight - skiplink.offsetHeight) + 'px');

      assert.equal(skiplink.style.left, (insertionPoint.offsetLeft +
        insertionPoint.offsetWidth - skiplink.offsetWidth) + 'px');
    });

    it('should be case-insenstive', function () {


      var insertionPoint = document.getElementById('insertionPoint'),
        skiplink = markup.skipLink.add(insertionPoint, '#target', {
          align: 'SE'
        });

      events.fire(skiplink, 'focus');

      assert.equal(skiplink.style.top, (insertionPoint.offsetTop +
        insertionPoint.offsetHeight - skiplink.offsetHeight) + 'px');

      assert.equal(skiplink.style.left, (insertionPoint.offsetLeft +
        insertionPoint.offsetWidth - skiplink.offsetWidth) + 'px');
    });
  });

});
