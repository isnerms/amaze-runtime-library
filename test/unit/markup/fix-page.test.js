
//
// TODO
//
//   - fix this test
//   - all skips are because commonjs
//
//
//

describe('markup.fixPage', function () {
  'use strict';

  var markup = require('markup');

  //LANG <html lang="en"></html>
  describe('lang', function () {

    /*
    var original;
    //grab the original document language
    before(function () {
      original = markup.documentLanguage;
    });
    //reset the document language to the original
    after(function () {
      markup.documentLanguage = original;
    });
    */

    it.skip('should handle a string', function (done) {
      var opts = {
        lang: 'hello'
      };
      //ensure that the lang has been set as the string
      markup.documentLanguage = function (lang) {
        assert.equal(lang, opts.lang);
        done();
      };

      markup.fixPage(opts);
    });

    it.skip('should handle an object', function (done) {
      var opts = {
        lang: {
          str: 'hello',
          overwrite: true
        }
      };
      //ensure that the lang has been set by the lang object
      markup.documentLanguage = function (lang, overwrite) {
        assert.equal(lang, opts.lang.str);
        assert.equal(overwrite, opts.lang.overwrite);
        done();
      };

      markup.fixPage(opts);
    });
    //make sure fixPage still works when lang is not provided
    it.skip('should not call "markup.documentLanguage" when "lang" is omitted', function () {
      markup.documentLanguage = function () {
        throw new Error('this should not be called');
      };
      markup.fixPage();
    });
  });
  //LANDMARK MODAL
  describe('landmark', function () {
    /*
    var original;
    before(function () {
      original = markup.landmarksModal;
    });

    after(function () {
      markup.landmarksModal = original;
    });
    */

    it.skip('should provide the correct options to landmarksModal', function (done) {
      var opts = {
        landmark: {
          foo: 'bar',
          baz: 'qax'
        }
      };

      markup.landmarksModal = function (options) {
        assert.deepEqual(options, opts.landmark);
        done();
      };

      markup.fixPage(opts);
    });

    it.skip('should not be called when "landmark" is omitted', function () {
      markup.landmarksModal = function () {
        throw new Error('generic error msg');
      };
      markup.fixPage({
        landmark: false
      });
    });

  });
  //PAGE TITLE <title>NinjaCatElephants</title>
  describe('title', function () {
    /*
    var original;
    before(function () {
      original = markup.pageTitle;
    });

    after(function () {
      markup.pageTitle = original;
    });
    */

    it.skip('should handle a string', function (done) {
      var options = {
        title: 'i am a string'
      };
      markup.pageTitle = function (title) {
        assert.equal(title, 'i am a string');
        done();
      };
      markup.fixPage(options);
    });

    it.skip('should handle an object', function (done) {
      var options = {
        title: {
          str: 'string',
          append: true
        }
      };
      markup.pageTitle = function (title, append) {
        assert.equal(title, 'string');
        assert.isTrue(append);
        done();
      };
      markup.fixPage(options);
    });
    it.skip('should not be called when "title" is omitted', function () {
      markup.pageTitle = function () {
        throw new Error('error!!');
      };
      markup.fixPage({
        title: false
      });
    });
  });
  //SKIP LINK
  describe('skipLink', function () {
    /*
    var original;
    before(function () {
      original = markup.skipLink.add;
    });

    after(function () {
      markup.skipLink.add = original;
    });
    */

    it.skip('should provided the correct arguments to skipLink', function (done) {
      var opts = {
        skipLink: {
          insertionPoint: 'the insertionPoint',
          target: 'the target',
          options: 'the options'
        }
      };
      markup.skipLink.add = function (insertionPoint, target, options) {
        assert.equal(insertionPoint, opts.skipLink.insertionPoint);
        assert.equal(target, opts.skipLink.target);
        assert.equal(options, opts.skipLink.options);
        done();
      };

      markup.fixPage(opts);
    });

    it.skip('should not be called when "skipLink" is omitted', function () {
      markup.skipLink.add = function () {
        throw new Error('erroneous');
      };
      markup.fixPage({
        skipLink: false
      });
    });
  });

  describe('delay', function () {
    /*
    var original;
    before(function () {
      original = markup.pageTitle;
    });

    after(function () {
      markup.pageTitle = original;
    });
    */

    it.skip('should wait "delay" ms before executing', function (done) {

      var start = new Date();

      markup.pageTitle = function () {
        var stop = new Date(),
          ms = stop - start;

        assert.ok(ms >= 5);
        done();
      };

      markup.fixPage({
        title: 'hi',
        delay: 10
      });
    });
  });

});
