describe('menu.ariaMenu', function () {
  'use strict';

  var menu = require('menu');
  var ariaMenu = menu.ariaMenu;
  var selector = require('selector');
  var query = selector.query;
  var queryAll = selector.queryAll;
  var each = require('collections').each;
  var events = require('events');
  var fixture = null;


  beforeEach(function () {
    /*jshint maxlen: 5000 */

    fixture = document.createElement('div');
    document.body.appendChild(fixture);

    fixture.innerHTML = '<h3>Click to open and close</h3>' +
      '<div id="tabs_navigation">' +
      ' <ul class="primaryNavigation" id="primaryNavigation">' +
      ' <li class="primary_tab">' +
      ' <a onclick="javascript:urchinTracker(\'/my_accounts\');" id="tab_My Accounts" class="on" onmouseover="document.getElementById(\'tab_My Accounts\').focus();" onfocus="document.getElementById(\'tab_My Accounts\').blur();">' +
      ' <span>My Accounts' +
      ' </span>' +
      ' </a>' +
      ' <div class="pixel"></div>' +
      ' <ul>' +
      ' <li class="first"><a href="/myaccount/banking/account_summary.vm" onclick="javascript:urchinTracker(\'/megadropdown/my_accounts/summary\');" id="tab_mega_AccountSummary" class="">Summary</a></li>' +
      ' <li><a href="/myaccount/banking/display_external_links.vm" onclick="javascript:urchinTracker(\'/megadropdown/my_accounts/external_accounts\');" id="tab_mega_ExternalLinks" class="">External Accounts</a></li>' +
      ' <li><a href="/myaccount/banking/automatic_savings_plan" onclick="javascript:urchinTracker(\'/megadropdown/my_accounts/automatic_savings_plan\');" id="tab_mega_AutomaticSavingsPlanTab" class="">Automatic Savings Plan</a></li>' +
      ' <li><a href="/myaccount/banking/automatic_savings_plan?tab=ASP.howItWorks" onclick="javascript:urchinTracker(\'/megadropdown/my_accounts/automatic_savings_plan/how_it_works_tab\');" id="tab_mega_ASP.howItWorks" class="third ">How It Works</a></li>' +
      ' <li><a href="/myaccount/banking/automatic_savings_plan?tab=ASP.savingsPlans" onclick="javascript:urchinTracker(\'/megadropdown/my_accounts/automatic_savings_plan/savings_plans_tab\');" id="tab_mega_ASP.savingsPlans" class="third ">Savings Plans</a></li>' +
      ' <li )=""><a href="/myaccount/banking/referAFriend" onclick="javascript:urchinTracker(\'/megadropdown/my_accounts/secondary_tab_refer_a_friend\');" id="tab_mega_Refer A Friend Webflow" class="">Refer a Friend</a></li>' +
      ' <li><a href="/myaccount/banking/referAFriend?tab=ReferralsInput" onclick="javascript:urchinTracker(\'/megadropdown/my_accounts/refer_a_friend/tertiary_tab_refer_a_friend\');" id="tab_mega_ReferralsInputWebflow" class="third ">Refer A Friend</a></li>' +
      ' <li><a href="/myaccount/banking/referAFriend?tab=ReferralsCompleted" onclick="javascript:urchinTracker(\'/megadropdown/my_accounts/refer_a_friend/tertiary_tab_completed\');" id="tab_mega_ReferralsCompletedWebflow" class="third ">Completed</a></li>' +
      ' <li )=""><a href="/myaccount/banking/goaltracker" onclick="javascript:urchinTracker(\'/megadropdown/my_accounts/my_savings_goals\');" id="tab_mega_SavingsGoals" class="lastm">My Savings Goals</a></li>' +
      ' <li class="last"></li>' +
      ' </ul>' +
      ' </li>' +
      ' <li class="primary_tab">' +
      ' <a onclick="javascript:urchinTracker(\'/make_transfer\');" id="tab_Transfer Money" onmouseover="document.getElementById(\'tab_Transfer Money\').focus();" onfocus="document.getElementById(\'tab_Transfer Money\').blur();">' +
      ' <span>Transfers &amp; Deposits' +
      ' </span>' +
      ' </a>' +
      ' <div class="pixel"></div>' +
      ' <ul>' +
      ' <li class="first"><a href="/myaccount/banking/deposit_transfer_input.vm?initialize=true" onclick="javascript:urchinTracker(\'/megadropdown/make_transfer/banking\');" id="tab_mega_BankToBankTransferMoney" class="">Transfers</a></li>' +
      ' <li )=""><a href="/myaccount/banking/deposit_checks.vm" onclick="javascript:urchinTracker(\'/megadropdown/make_transfer/deposit_checks\');" id="tab_mega_TransferMoneyCheckDeposit" class="">Deposit Checks</a></li>' +
      ' <li><a href="/myaccount/banking/checkdeposit_overview.vm" onclick="javascript:urchinTracker(\'/megadropdown/make_transfer/deposit_checks/overview\');" id="tab_mega_Overview" class="third ">Overview</a></li>' +
      ' <li><a href="/myaccount/banking/checkUpload" onclick="javascript:urchinTracker(\'/megadropdown/make_transfer/deposit_checks/upload_checks\');" id="tab_mega_UploadCheck" class="third ">Image Upload</a></li>' +
      ' <li><a href="/myaccount/banking/checkdeposit_mobile.vm" onclick="javascript:urchinTracker(\'/megadropdown/make_transfer/deposit_checks/mobile\');" id="tab_mega_Mobile" class="third lastm">Mobile</a></li>' +
      ' <li class="last"></li>' +
      ' </ul>' +
      ' </li>' +
      ' <li class="primary_tab">' +
      ' <a onclick="javascript:urchinTracker(\'/estatements\');" id="tab_eStatementsTab" onmouseover="document.getElementById(\'tab_eStatementsTab\').focus();" onfocus="document.getElementById(\'tab_eStatementsTab\').blur();">' +
      ' <span>eStatements' +
      ' </span>' +
      ' </a>' +
      ' <div class="pixel"></div>' +
      ' <ul>' +
      ' <li class="first"><a href="/myaccount/banking/estatements.vm" onclick="javascript:urchinTracker(\'/megadropdown/estatements/estatements\');" id="tab_mega_eStatements" class="">eStatements</a></li>' +
      ' <li><a href="/myaccount/banking/tax_forms_landing_page.vm" onclick="javascript:urchinTracker(\'/megadropdown/estatements/tax_forms\');" id="tab_mega_TaxForms" class="lastm">Tax Forms</a></li>' +
      ' <li class="last"></li>' +
      ' </ul>' +
      ' </li>' +
      ' <li class="primary_tab">' +
      ' <a onclick="javascript:urchinTracker(\'/my_info\');" id="tab_MyInfoTab" onmouseover="document.getElementById(\'tab_MyInfoTab\').focus();" onfocus="document.getElementById(\'tab_MyInfoTab\').blur();">' +
      ' <span>My Info' +
      ' </span>' +
      ' </a>' +
      ' <div class="pixel"></div>' +
      ' <ul>' +
      ' <li class="first"><a href="/myaccount/banking/contactinfo?" onclick="javascript:urchinTracker(\'/megadropdown/my_info/contact_info\');" id="tab_mega_ContactInfo" class="">Contact Info</a></li>' +
      ' <li )=""><a href="/myaccount/banking/signinoptions.vm" onclick="javascript:urchinTracker(\'/megadropdown/my_info/signin_options\');" id="tab_mega_SignInOptions" class="">Sign In Options</a></li>' +
      ' <li><a href="/myaccount/banking/signinoptions.vm" onclick="javascript:urchinTracker(\'/megadropdown/my_info/signin_options/pin_and_saver_id\');" id="tab_mega_PinAndSaverId" class="third ">PIN &amp; Saver ID </a></li>' +
      ' <li><a href="/myaccount/banking/securityImageAndPhrase?" onclick="javascript:urchinTracker(\'/megadropdown/my_info/signin_options/image_and_phrase\');" id="tab_mega_ImageAndPhrase" class="third ">Image &amp; Phrase</a></li>' +
      ' <li><a href="/myaccount/banking/securityQuestions?" onclick="javascript:urchinTracker(\'/megadropdown/my_info/signin_options/security_questions\');" id="tab_mega_SecurityQuestions" class="third ">Security Questions</a></li>' +
      ' <li><a href="/myaccount/banking/registeredComputers?" onclick="javascript:urchinTracker(\'/megadropdown/my_info/signin_options/registered_computers\');" id="tab_mega_RegisteredComputers" class="third ">Registered Computers</a></li>' +
      ' <li )=""><a href="/myaccount/banking/preferences_child_tab.vm" onclick="javascript:urchinTracker(\'/megadropdown/my_info/preferences\');" id="tab_mega_Preferences" class="">Preferences</a></li>' +
      ' <li><a href="/myaccount/banking/agreements?" onclick="javascript:urchinTracker(\'/megadropdown/my_info/preferences/agreements\');" id="tab_mega_Agreements" class="third ">Agreements</a></li>' +
      ' <li><a href="/myaccount/banking/privacy?" onclick="javascript:urchinTracker(\'/megadropdown/my_info/preferences/privacy\');" id="tab_mega_Privacy" class="third ">Privacy</a></li>' +
      ' <li><a href="/myaccount/banking/accessCode?" onclick="javascript:urchinTracker(\'/megadropdown/my_info/preferences/access_code\');" id="tab_mega_AccessCode" class="third ">Access Code</a></li>' +
      ' <li><a href="/myaccount/banking/photoid?" onclick="javascript:urchinTracker(\'/megadropdown/my_info/photoid\');" id="tab_mega_PhotoID" class="lastm">My State-Issued ID</a></li>' +
      ' <li class="last"></li>' +
      ' </ul>' +
      ' </li>' +
      ' <li class="primary_tab">' +
      ' <a onclick="javascript:urchinTracker(\'/alerts_and_messages\');" id="tab_Alerts and Messages" onmouseover="document.getElementById(\'tab_Alerts and Messages\').focus();" onfocus="document.getElementById(\'tab_Alerts and Messages\').blur();">' +
      ' <span>Alerts &amp; Messages' +
      ' <b id="icon_Alerts &amp; Messages"></b>' +
      ' </span>' +
      ' </a>' +
      ' <div class="pixel"></div>' +
      ' <ul>' +
      ' <li class="first"><a href="/myaccount/banking/deliveryOptions?" onclick="javascript:urchinTracker(\'/megadropdown/alerts_and_messages/delivery_options\');" id="tab_mega_DeliveryOptions" class="">Delivery Options</a></li>' +
      ' <li><a href="/myaccount/banking/cccAlerts?" onclick="javascript:urchinTracker(\'/megadropdown/alerts_and_messages/manage_alerts\');" id="tab_mega_Manage Alerts" class="">Manage Alerts</a></li>' +
      ' <li><a href="/myaccount/banking/cccAlerts?" onclick="javascript:urchinTracker(\'/megadropdown/alerts_and_messages/manage_alerts/current_alerts\');" id="tab_mega_Current Alerts" class="third ">Current Alerts</a></li>' +
      ' <li><a href="/myaccount/banking/textCommands?" onclick="javascript:urchinTracker(\'/megadropdown/alerts_and_messages/manage_alerts/text_commands\');" id="tab_mega_Text Commands" class="third ">Text Commands</a></li>' +
      ' <li><a href="/myaccount/banking/message_list.vm" onclick="javascript:urchinTracker(\'/megadropdown/alerts_and_messages/message_center\');" id="tab_mega_Message Center" class="lastm">Message Center (0)</a></li>' +
      ' <li class="last"></li>' +
      ' </ul>' +
      ' </li>' +
      ' </ul>' +
      ' <div class="secondary">' +
      ' <a href="/myaccount/banking/account_summary.vm" onclick="javascript:urchinTracker(\'/my_accounts/summary\');" id="tab_AccountSummary" class="  on ">' +
      ' <span>Summary</span>' +
      ' </a>' +
      ' <a href="/myaccount/banking/display_external_links.vm" onclick="javascript:urchinTracker(\'/my_accounts/external_accounts\');" id="tab_ExternalLinks" class=" ">' +
      ' <span>External Accounts</span>' +
      ' </a>' +
      ' <a href="/myaccount/banking/automatic_savings_plan" onclick="javascript:urchinTracker(\'/my_accounts/automatic_savings_plan\');" id="tab_AutomaticSavingsPlanTab" class=" ">' +
      ' <span>Automatic Savings Plan</span>' +
      ' </a>' +
      ' <a href="/myaccount/banking/referAFriend" onclick="javascript:urchinTracker(\'/my_accounts/secondary_tab_refer_a_friend\');" id="tab_Refer A Friend Webflow" class=" ">' +
      ' <span>Refer a Friend</span>' +
      ' </a>' +
      ' <a href="/myaccount/banking/goaltracker" onclick="javascript:urchinTracker(\'/my_accounts/my_savings_goals\');" id="tab_SavingsGoals" class=" ">' +
      ' <span>My Savings Goals</span>' +
      ' </a>' +
      ' </div>' +
      ' </div>' +
      ' <div id="primaryNav" style="-webkit-tap-highlight-color: rgba(26, 26, 26, 0.298039);">' +
      ' <h3>' +
      '   <a href="/index.asp?WT.svl=1">State Farm Insurance - Auto, Life, Fire</a>' +
      ' </h3>' +
      ' <ul>' +
      '   <li>' +
      '     <a href="/insurance/insurance.asp?WT.svl=2" id="navInsurance">Insurance: Auto, Home, Life &amp; More</a>' +
      '     <!-- Drop Down -->' +
      '     <ul class="insurance" >' +
      '       <li >' +
      '         <!-- Sub List One -->' +
      '         <ul class="one" >' +
      '           <li><a href="/insurance/auto_insurance/auto_insurance.asp?WT.svl=3">Auto</a></li>' +
      '           <li><a href="/insurance/homeowners/homeowners.asp?WT.svl=4">Homeowners</a></li>' +
      '           <li><a href="/insurance/renters/renters.asp?WT.svl=6">Renters</a></li>' +
      '           <li><a href="/insurance/condo_owners/condo_owners.asp?WT.svl=5">Condo Owners</a></li>' +
      '           <li><a href="/insurance/business/business.asp?WT.svl=8">Business</a></li>' +
      '           <li><a href="/insurance/life_annuity/life/life.asp?WT.svl=7">Life</a></li>' +
      '           <li><a href="/insurance/health/health.asp">Health</a></li>' +
      '           <li><a href="/insurance/other/other.asp?WT.svl=9">More Insurance...</a></li>' +
      '         </ul>' +
      '         <!-- Sub List One -->' +
      '         <!-- Sub List Two -->' +
      '         <ul class="two" >' +
      '           <li class="bold">Tools &amp; Advice</li>' +
      '           <li><a href="/insurance/identify_insurance.asp?WT.svl=10">Common Insurance Questions</a></li>' +
      '           <li><a href="/insurance/auto_insurance/discounts/auto-discounts.asp?WT.svl=11">Auto Insurance Discounts</a></li>' +
      '           <li><a href="http://learningcenter.statefarm.com?WT.svl=116">Learning Center</a></li>' +
      '           <li><a href="http://www.statefarm.com/homeindex/index.asp ">Home Inventory Checklist</a></li>' +
      '           <li><a href="/insurance/life_annuity/life/lifeNeedsCalc.asp">Life Insurance Calculator</a></li>' +
      '           <li><a href="/insurance/identityFraud.asp?WT.svl=187">Identity Protection</a></li>' +
      '         </ul>' +
      '         <!-- Sub List Two -->' +
      '         <!-- Sub List Three -->' +
      '         <ul class="three" >' +
      '           <li class="bold">Quick Links</li>' +
      '           <li><a href="/insurance/quote/quote.asp?WT.svl=16">Get Insurance Quotes</a></li>' +
      '           <li><a href="http://www.statefarm.com/agent/index.xhtml" onclick="dcsMultiTrack(\'DCS.dcsuri\', \'\', \'WT.svl\', \'17\');">' +
      '             Find an Agent</a></li>' +
      '           <li><a href="/account.htm?WT.svl=18">Manage Your Policy</a></li>' +
      '           <li><a href="/insurance/service_center/pickyourwaytopay.asp?WT.svl=19">Payment Options</a></li>' +
      '           <li><a href="/insurance/claim_center/claim_center.asp?WT.svl=20">Claims Center</a></li>' +
      '           <li><a href="http://www.statefarm.com/rflocatorinet/index.xhtml">Repair Facility' +
      '             Locator</a></li>' +
      '           <li><a href="/welcome/index.asp?WT.svl=131">Welcome Center</a></li>' +
      '         </ul>' +
      '         <!-- Sub List Three -->' +
      '         <div class="clear">' +
      '           <!--  -->' +
      '         </div>' +
      '       </li>' +
      '     </ul>' +
      '     <!-- Drop Down -->' +
      '   </li>' +
      '   <li><a href="/mutual/mutual.asp?WT.svl=21" id="navMutual">Mutual Funds: Save, Invest' +
      '     &amp; Plan</a>' +
      '     <!-- Drop Down -->' +
      '     <!-- mp_trans_disable_start -->' +
      '     <ul >' +
      '       <li >' +
      '         <!-- Sub List One -->' +
      '         <ul class="one" >' +
      '           <li><a href="/mutual-funds/start-planning/start-planning.asp?WT.svl=162">Start Planning</a></li>' +
      '           <li><a href="/mutual-funds/account-types/general-investing.asp?WT.svl=163">General Investing</a></li>' +
      '           <li><a href="/mutual-funds/account-types/education-savings-plans/education-savings-plans.asp?WT.svl=164">' +
      '             Education Savings</a></li>' +
      '           <li><a href="/mutual-funds/account-types/retirement/retirement.asp?WT.svl=165">Retirement' +
      '             Accounts</a></li>' +
      '           <li><a href="/mutual-funds/account-types/small-business/small-business.asp?WT.svl=166">' +
      '             Small Business Plans</a></li>' +
      '           <li><a href="/mutual-funds/rollovers-transfers/rollovers-transfers.asp?WT.svl=167">Rollovers' +
      '             &amp; Transfers</a></li>' +
      '         </ul>' +
      '         <!-- Sub List One -->' +
      '         <!-- Sub List Two -->' +
      '         <ul class="two" >' +
      '           <li class="bold">Fund Information</li>' +
      '           <li><a href="/mutual-funds/life-path-funds/life-path-funds.asp?WT.svl=168">Life Path<sup>Â®</sup>' +
      '             Funds</a></li>' +
      '           <li><a href="/mutual-funds/stock-funds/stock-funds-active-index.asp?WT.svl=169">Stock' +
      '             &amp; Index Funds</a></li>' +
      '           <li><a href="/mutual-funds/money-market-funds/money-market-funds.asp?WT.svl=170">Bond' +
      '             &amp; Money Market Funds</a></li>' +
      '           <li><a href="/mutual-funds/resources-tools/fund-performance/fund-performance.asp?WT.svl=171">' +
      '             Fund Performance</a></li>' +
      '           <li><a href="/mutual-funds/resources-tools/fund-performance/fund-prices.asp?WT.svl=172">' +
      '             Fund Prices</a></li>' +
      '           <li><a href="/mutual-funds/resources-tools/fund-selection-tool.asp?WT.svl=173">Fund' +
      '             Selection Tool</a></li>' +
      '         </ul>' +
      '         <!-- Sub List Two -->' +
      '         <!-- Sub List Two -->' +
      '         <!-- Sub List Three -->' +
      '         <ul class="three" >' +
      '           <li class="bold">Quick Links</li>' +
      '           <li><a href="/mutual-funds/about-mutual-funds/open-account.asp?WT.svl=174">Open an Account</a></li>' +
      '           <li><a href="/mutual-funds/account-help/manage-your-account/manage-your-account.asp?WT.svl=175">' +
      '             Manage Your Account</a></li>' +
      '           <li><a href="/mutual-funds/resources-tools/resources-tools.asp?WT.svl=176">Investing' +
      '             Resources</a></li>' +
      '           <li><a href="/mutual-funds/account-help/forms-downloads.asp">Forms &amp; Downloads</a></li>' +
      '           <li><a href="/mutual-funds/account-help/account-help.asp?WT.svl=177">Account Help</a></li>' +
      '           <li><a href="/mutual-funds/about-mutual-funds/contact-us.asp?WT.svl=178&amp;wt.sftrk=walkin#walkin">' +
      '             Find an Agent</a></li>' +
      '           <li><a href="/mutual-funds/about-mutual-funds/contact-us.asp?WT.svl=179">Contact Us</a></li>' +
      '         </ul>' +
      '         <!-- Sub List Three -->' +
      '         <div class="clear">' +
      '           <!--  -->' +
      '         </div>' +
      '       </li>' +
      '     </ul>' +
      '     <!-- mp_trans_disable_end -->' +
      '     <!-- Drop Down -->' +
      '   </li>' +
      '   <li>' +
      '     <a class="bank" href="/bank/bank.asp?WT.svl=142" id="navBank">State Farm BankÂ® Full Service Financial' +
      '     </a>' +
      '     <!-- Drop Down -->' +
      '     <ul >' +
      '       <li >' +
      '         <!-- Sub List One -->' +
      '         <ul class="one" >' +
      '           <li><a class="bank" href="/bank/checking/checking.asp?WT.svl=143">Checking Accounts</a></li>' +
      '           <li><a class="bank" href="/bank/savings/savings.asp?WT.svl=144">Savings Accounts &amp;' +
      '             CDs</a></li>' +
      '           <li><a class="bank" href="/bank/creditcard/credit-cards.asp?WT.svl=145">Credit Cards</a></li>' +
      '           <li><a class="bank" href="/bank/loans/loans.asp?WT.svl=146">Home &amp; Auto Loans</a></li>' +
      '           <li><a class="bank" href="/bank/giftcards/gift-cards.asp?WT.svl=147">Gift Cards</a></li>' +
      '         </ul>' +
      '         <!-- Sub List One -->' +
      '         <!-- Sub List Two -->' +
      '         <ul class="two" >' +
      '           <li class="bold">Tools &amp; Resources</li>' +
      '           <li><a class="bank" href="/bank/aboutbank/accessing-accounts.asp?WT.svl=148#online">' +
      '             Online Banking</a></li>' +
      '           <li><a class="bank" href="/bank/aboutbank/accessing-accounts.asp?WT.svl=149#mobile">' +
      '             Mobile Banking</a></li>' +
      '           <li><a class="bank" href="/bank/aboutbank/accessing-accounts.asp?WT.svl=150#automatedPhone">' +
      '             Banking By Phone</a></li>' +
      '           <li><a class="bank" href="http://online2.statefarm.com/bank/rates/index.xhtml">' +
      '             Rates</a></li>' +
      '           <li><a class="bank" href="/bank/resources/calcs.asp?WT.svl=151">Financial Calculators</a></li>' +
      '           <li><a class="bank" href="/bank/aboutbank/newsupdates.asp">News &amp;' +
      '             Updates</a></li>' +
      '           <li><a class="bank" href="/bank/aboutbank/security/securityinfo.asp?WT.svl=153">Security' +
      '             Information &amp; Alerts</a></li>' +
      '         </ul>' +
      '         <!-- Sub List Two -->' +
      '         <!-- Sub List Three -->' +
      '         <ul class="three" >' +
      '           <li class="bold">Quick Links</li>' +
      '           <li><a class="bank" href="/bank/aboutbank/accessing-accounts.asp?WT.svl=155#ATM">Find' +
      '             an ATM</a></li>' +
      '           <li><a class="bank" href="/account.htm">Manage Your Accounts</a></li>' +
      '           <li><a class="bank" href="/agent/index.xhtml">Find an Agent</a></li>' +
      '           <li><a class="bank" href="/bank/help/faqs.asp#what-do-if-debit-credit-card-stolen">' +
      '             Lost Card</a></li>' +
      '           <li><a class="bank" href="/bank/aboutbank/contact.asp?WT.svl=157">Contact Us</a></li>' +
      '         </ul>' +
      '         <!-- Sub List Three -->' +
      '         <div class="clear">' +
      '           <!--  -->' +
      '         </div>' +
      '       </li>' +
      '     </ul>' +
      '     <!-- Drop Down -->' +
      '   </li>' +
      ' </ul>' +
      '</div>' +
      '<div>' +
      ' <h3>Internals Test 1: Disable Focus on Links In SubTree</h3>' +
      ' <ul id="internalEntryPoint1">' +
      '   <a href="/someotherlink.html">My Menu Strange Link</a>' +
      '   <li id="one1" class="collapsed">One' +
      '     <a href="/someotherlink.html">Another Menu Strange Link</a>' +
      '     <ul id="oneSub">' +
      '       <li id="oneOne1"><a href="/somelink.html">One One</a>' +
      '       </li>' +
      '       <li id="oneTwo1"><a href="/somelink.html">One Two</a>' +
      '       </li>' +
      '       <li id="oneThree1"><a href="/somelink.html">One Three</a>' +
      '       </li>' +
      '     </ul>' +
      '   </li>' +
      ' </ul>' +
      '</div>' +
      '<div>' +
      ' <h3>Internals Test 2: events</h3>' +
      ' <ul id="internalEntryPoint2">' +
      '   <li id="one2" class="collapsed">One' +
      '     <ul id="oneSub2">' +
      '       <li id="oneOne2"><a href="#">One One</a>' +
      '         <ul>' +
      '           <li id="oneOneOne2"><a href="#">One One One</a>' +
      '           </li>' +
      '           <li id="oneOneTwo2"><a href="#">One One Two</a>' +
      '           </li>' +
      '           <li id="oneOneThree2"><a href="#">One One Three</a>' +
      '           </li>' +
      '         </ul>' +
      '       </li>' +
      '       <li id="oneTwo2"><a href="#">One Two</a>' +
      '       </li>' +
      '       <li id="oneThree2"><a href="#">One Three</a>' +
      '       </li>' +
      '     </ul>' +
      '   </li>' +
      '   <li id="two2" class="collapsed">Two' +
      '     <ul id="twoSub2">' +
      '       <li id="twoOne2">Two One' +
      '       </li>' +
      '       <li id="twoTwo2">Two Two' +
      '       </li>' +
      '       <li id="twoThree2">Two Three' +
      '       </li>' +
      '     </ul>' +
      '   </li>' +
      '   <li id="three2" class="collapsed">Three' +
      '     <ul id="threeSub2">' +
      '       <li id="threeOne2">Three One' +
      '       </li>' +
      '       <li id="threeTwo2">Three Two' +
      '       </li>' +
      '       <li id="threeThree2">Three Three' +
      '       </li>' +
      '     </ul>' +
      '   </li>' +
      ' </ul>' +
      '</div>' +
      '<div id="internalEntryPoint3">' +
      '<div id="nav-bar-inner">' +
      '<a id="" href="javascript:void(0)" class="nav_a nav-button-outer nav-menu-active nav-shop-all-button nav-button-outer-open" alt="Shop By Department" style="display: inline;" >' +
      '<span class="nav-button-mid nav-sprite">' +
      '<span class="nav-button-inner nav-sprite">' +
      '<span class="nav-button-title nav-button-line1">Shop by</span>' +
      '<span class="nav-button-title nav-button-line2">Department</span>' +
      '</span>' +
      '</span>' +
      '<span class="nav-down-arrow nav-sprite"></span>' +
      '</a>' +
      '</div>' +
      '<ul id="nav_cats" class="nav_browse_ul">' +
      '<li class="nav_first nav_pop_li nav_cat" id="nav_cat_0">Unlimited Instant Videos</li>' +
      '<li class="nav_taglined nav_pop_li nav_cat" id="nav_cat_1">MP3s &amp; Cloud Player<div class="nav_tag">20 million songs, play anywhere</div></li>' +
      '<li class="nav_taglined nav_pop_li nav_cat" id="nav_cat_2">Amazon Cloud Drive<div class="nav_tag">5 GB of free storage</div></li>' +
      '<li class="nav_pop_li nav_cat" id="nav_cat_3">Kindle</li>' +
      '<li class="nav_taglined nav_pop_li nav_cat" id="nav_cat_4">Appstore for Android<div class="nav_tag">Touch Donut Hockey! free today</div></li>' +
      '<li class="nav_pop_li nav_cat" id="nav_cat_5">Digital Games &amp; Software</li>' +
      '<li class="nav_pop_li nav_cat" id="nav_cat_6">Audible Audiobooks</li>' +
      '<li class="nav_pop_li nav_cat nav_divider_before" id="nav_cat_8">Books</li>' +
      '<li class="nav_pop_li nav_cat" id="nav_cat_9">Movies, Music &amp; Games</li>' +
      '<li class="nav_pop_li nav_cat" id="nav_cat_10">Electronics &amp; Computers</li>' +
      '<li class="nav_pop_li nav_cat nav_active" id="nav_cat_11">Home, Garden &amp; Tools</li>' +
      '</ul>' +
      '<div id="nav_subcats">' +
      '<div id="nav_subcats_0" data-nav-promo-id="instant-video" class="nav_browse_subcat nav_super_cat" style="display: block;"><img src="http://g-ecx.images-amazon.com/images/G/01/img12/digital/flyout/AIV_GNO-FlyoutFinal-MadMenS6-PassCTA._V371293829_.png" usemap="#nav_imgmap_instant-video" class="nav_browse_promo" style="bottom: -24px; right: -20px; width: 500px; height: 495px;"><ul class="nav_browse_ul nav_browse_cat_ul"><li class="nav_pop_li nav_browse_cat_head">Unlimited Instant Videos</li>' +
      '<li class="nav_first nav_taglined nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_aiv_piv?ie=UTF8&amp;node=2676882011" class="nav_a nav_item">Prime Instant Videos</a><div class="nav_tag">Unlimited streaming of thousands of<br>movies and TV shows with Amazon Prime</div></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/gp/prime/signup/videos/ref=sa_menu_aiv_prm?ie=UTF8&amp;redirectQueryParams=bm9kZT0yNjE1MjYwMDEx&amp;redirectURL=L2Iv" class="nav_a nav_item">Learn More About Amazon Prime</a></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li nav_divider_before"><a href="/Instant-Video/b/ref=sa_menu_aiv_vid?ie=UTF8&amp;node=2858778011" class="nav_a nav_item">Amazon Instant Video Store</a><div class="nav_tag">Rent or buy hit movies and TV shows<br>to stream or download</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/video/library/ref=sa_menu_aiv_yvl" class="nav_a nav_item">Your Video Library</a><div class="nav_tag">Your movies and TV shows<br>stored in the cloud</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/video/ontv/ontv/ref=sa_menu_aiv_wtv" class="nav_a nav_item">Watch Anywhere</a><div class="nav_tag">Watch instantly on your Kindle Fire,<br>TV, Blu-ray player, or set-top box</div></li>' +
      '</ul></div>' +
      '<div id="nav_subcats_1" data-nav-promo-id="mp3" class="nav_browse_subcat nav_super_cat" style="display: none;"><img src="http://g-ecx.images-amazon.com/images/G/01/music/2013/gno/3-25_disney-hop-to_flyout-4b._V371694951_.png" usemap="#nav_imgmap_mp3" class="nav_browse_promo" style="bottom: -39px; right: -30px; width: 767px; height: 612px;"><ul class="nav_browse_ul nav_browse_cat_ul"><li class="nav_pop_li nav_browse_cat_head">MP3s &amp; Cloud Player</li>' +
      '<li class="nav_first nav_taglined nav_subcat_link nav_pop_li"><a href="/MP3-Music-Download/b/ref=sa_menu_mp3_str?ie=UTF8&amp;node=163856011" class="nav_a nav_item">MP3 Music Store</a><div class="nav_tag">Shop over 20 million songs</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/feature.html/ref=sa_menu_fire_music?ie=UTF8&amp;docId=1000825251" class="nav_a nav_item">Music on Kindle Fire</a><div class="nav_tag">Discover how to play your music</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/dmusic/marketing/CloudPlayerLaunchPage/ref=sa_menu_mp3_acp" class="nav_a nav_item" target="_blank">Cloud Player for Web</a><div class="nav_tag">Play from any browser</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/feature.html/ref=sa_menu_mp3_and?ie=UTF8&amp;docId=1000454841" class="nav_a nav_item">Cloud Player for Android</a><div class="nav_tag">For Android phones and tablets</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/feature.html/ref=sa_menu_mp3_ios?ie=UTF8&amp;docId=1000776061" class="nav_a nav_item">Cloud Player for iOS</a><div class="nav_tag">For iPhone, iPad and iPod touch</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_mp3_hm?ie=UTF8&amp;node=2658409011" class="nav_a nav_item">Cloud Player for Home</a><div class="nav_tag">For Sonos and Roku</div></li>' +
      '</ul></div>' +
      '<div id="nav_subcats_2" data-nav-promo-id="cloud-drive" class="nav_browse_subcat nav_super_cat"><img src="http://g-ecx.images-amazon.com/images/G/01/digital/adrive/images/desktop/DesktopApp_GNO_YouAre._V371478196_.png" usemap="#nav_imgmap_cloud-drive" class="nav_browse_promo" style="bottom: -14px; right: 10px; width: 480px; height: 472px;"><ul class="nav_browse_ul nav_browse_cat_ul"><li class="nav_pop_li nav_browse_cat_head">Amazon Cloud Drive</li>' +
      '<li class="nav_first nav_taglined nav_subcat_link nav_pop_li"><a href="/clouddrive/ref=sa_menu_acd_urc" class="nav_a nav_item" target="_blank">Your Cloud Drive</a><div class="nav_tag">5 GB of free storage</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/feature.html/ref=sa_menu_acd_dsktopapp?ie=UTF8&amp;docId=1000796781" class="nav_a nav_item">Get the Desktop App</a><div class="nav_tag">For Windows and Mac</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/feature.html/ref=sa_menu_acd_photo?ie=UTF8&amp;docId=1000848741" class="nav_a nav_item">Cloud Drive Photos for Android</a><div class="nav_tag">For Android phones and tablets</div></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/gp/feature.html/ref=sa_menu_acd_lrn?ie=UTF8&amp;docId=1000796931" class="nav_a nav_item">Learn More About Cloud Drive</a></li>' +
      '</ul></div>' +
      '<div id="nav_subcats_3" data-nav-promo-id="kindle" class="nav_browse_subcat nav_super_cat" style="display: none;"><img src="http://g-ecx.images-amazon.com/images/G/01/gno/beacon/merch/browse/gno-family-440x151._V389693769_.png" usemap="#nav_imgmap_kindle" class="nav_browse_promo" style="bottom: -49px; right: 28px; width: 440px; height: 151px;"><ul class="nav_browse_ul nav_browse_cat_ul"><li class="nav_first nav_pop_li nav_browse_cat_head">Kindle E-readers</li>' +
      '<li class="nav_first nav_taglined nav_subcat_link nav_pop_li"><a href="/dp/B007HCCNJU/ref=sa_menu_kdptq" class="nav_a nav_item">Kindle</a><div class="nav_tag">Small, light, perfect for reading</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/dp/B007OZNZG0/ref=sa_menu_kdpclw" class="nav_a nav_item">Kindle Paperwhite</a><div class="nav_tag">World\'s most advanced e-reader</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/dp/B007OZNUCE/ref=sa_menu_kdpclwn" class="nav_a nav_item">Kindle Paperwhite 3G</a><div class="nav_tag">With free 3G wireless</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_kacce?ie=UTF8&amp;node=5916440011" class="nav_a nav_item">Kindle E-reader Accessories</a><div class="nav_tag">Covers, chargers, sleeves and more</div></li>' +
      '<li class="nav_pop_li nav_browse_cat_head nav_divider_before">Kindle Store</li>' +
      '<li class="nav_first nav_subcat_link nav_pop_li"><a href="/Kindle-eBooks/b/ref=sa_menu_kbo?ie=UTF8&amp;node=1286228011" class="nav_a nav_item">Kindle Books</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/gp/digital/fiona/redirect/newsstand/home/ref=sa_menu_knwstnd" class="nav_a nav_item">Newsstand</a></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/redirect.html/ref=sa_menu_kds?ie=UTF8&amp;location=%2Fkindlelendinglibrary" class="nav_a nav_item">Kindle Owners\' Lending Library</a><div class="nav_tag">With Prime, Kindle owners read for free</div></li>' +
      '</ul>' +
      '<ul class="nav_browse_ul nav_browse_cat2_ul">' +
      '<li class="nav_pop_li nav_browse_cat_head">Kindle Fire</li>' +
      '<li class="nav_first nav_taglined nav_subcat_link nav_pop_li"><a href="/dp/B0083Q04IQ/ref=sa_menu_kdpo2" class="nav_a nav_item">Fire</a><div class="nav_tag">All new--faster, twice the memory</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/dp/B0083PWAPW/ref=sa_menu_kdpt" class="nav_a nav_item">Fire HD</a><div class="nav_tag">7", Dolby audio, ultra-fast Wi-Fi</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/dp/B008GFRE5A/ref=sa_menu_kdpj" class="nav_a nav_item">Fire HD 8.9"</a><div class="nav_tag">8.9", Dolby audio, ultra-fast Wi-Fi</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/dp/B008GFRDL0/ref=sa_menu_kdpjw" class="nav_a nav_item">Fire HD 8.9" 4G</a><div class="nav_tag">With ultra-fast 4G LTE wireless</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_kaccf?ie=UTF8&amp;node=5916439011" class="nav_a nav_item">Kindle Fire Accessories</a><div class="nav_tag">Cases, chargers, sleeves and more</div></li>' +
      '<li class="nav_pop_li nav_browse_cat_head nav_divider_before">Kindle Apps &amp; Resources</li>' +
      '<li class="nav_first nav_taglined nav_subcat_link nav_pop_li"><a href="https://www.amazon.com:443/gp/redirect.html/ref=sa_menu_kcr?location=https://read.amazon.com/&amp;token=34AD60CFC4DCD7A97D4E2F4A4A7C4149FBEEF236" class="nav_a nav_item">Kindle Cloud Reader</a><div class="nav_tag">Read your Kindle books in a browser</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/feature.html/ref=sa_menu_karl?ie=UTF8&amp;docId=1000493771" class="nav_a nav_item">Free Kindle Reading Apps</a><div class="nav_tag">For PC, iPad, iPhone, Android, and more</div></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/gp/digital/fiona/manage/ref=sa_menu_myk" class="nav_a nav_item">Manage Your Kindle</a></li>' +
      '</ul></div>' +
      '<div id="nav_subcats_4" data-nav-promo-id="android" class="nav_browse_subcat" style="display: none;">   <a href="/gp/product/ref=nav_sap_mas_13_04_01?ie=UTF8&amp;ASIN=B00872HO8E" class="nav_asin_promo">     <img src="http://ecx.images-amazon.com/images/I/714HTEjuOLL._SS100_.png" class="nav_asin_promo_img">     <span class="nav_asin_promo_headline">Free App of the Day</span>     <span class="nav_asin_promo_info">       <span class="nav_asin_promo_title">Touch Donut Hockey!</span>       <span class="nav_asin_promo_title2">(List Price: $0.99)</span>       <span class="nav_asin_promo_price">FREE</span>     </span>     <span class="nav_asin_promo_button nav-sprite">Learn more</span>   </a> <ul class="nav_browse_ul nav_browse_cat_ul"><li class="nav_pop_li nav_browse_cat_head">Appstore for Android</li>' +
      '<li class="nav_first nav_subcat_link nav_pop_li"><a href="/mobile-apps/b/ref=sa_menu_adr_app?ie=UTF8&amp;node=2350149011" class="nav_a nav_item">Apps</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_adr_gam?ie=UTF8&amp;node=2478844011" class="nav_a nav_item">Games</a></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li nav_divider_before"><a href="/b/ref=sa_menu_adr_testd?ie=UTF8&amp;node=3071729011" class="nav_a nav_item">Test Drive Apps</a><div class="nav_tag">Try thousands of apps and games right now</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/feature.html/ref=sa_menu_adr_amz?ie=UTF8&amp;docId=1000645111" class="nav_a nav_item">Amazon Apps</a><div class="nav_tag">Kindle, mobile shopping, MP3, and more</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/mas/your-account/myapps/ref=sa_menu_adr_yad" class="nav_a nav_item">Your Apps and Devices</a><div class="nav_tag">View your apps and manage your devices</div></li>' +
      '</ul></div>' +
      '<div id="nav_subcats_5" data-nav-promo-id="digital-games-software" class="nav_browse_subcat nav_super_cat" style="display: none;"><img src="http://g-ecx.images-amazon.com/images/G/01/img13/digital-video-games/flyout/0325-bio-infinite-post-launch-flyout._V371364415_.png" usemap="#nav_imgmap_digital-games-software" class="nav_browse_promo" style="bottom: -14px; right: -28px; width: 512px; height: 472px;"><ul class="nav_browse_ul nav_browse_cat_ul"><li class="nav_pop_li nav_browse_cat_head">Digital Games &amp; Software</li>' +
      '<li class="nav_first nav_taglined nav_subcat_link nav_pop_li"><a href="/Game-Downloads/b/ref=sa_menu_dgs_gam?ie=UTF8&amp;node=979455011" class="nav_a nav_item">Game Downloads</a><div class="nav_tag">For PC and Mac</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_dgs_ftp?ie=UTF8&amp;node=5267605011" class="nav_a nav_item">Free-to-Play Games</a><div class="nav_tag">For PC and Mac</div></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/pc-mac-software-downloads/b/ref=sa_menu_dgs_sft?ie=UTF8&amp;node=1233514011" class="nav_a nav_item">Software Downloads</a><div class="nav_tag">For PC and Mac</div></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/gp/swvgdtt/your-account/manage-downloads.html/ref=sa_menu_dgs_gsl" class="nav_a nav_item">Your Games &amp; Software Library</a></li>' +
      '</ul></div>' +
      '<div id="nav_subcats_6" data-nav-promo-id="audible" class="nav_browse_subcat nav_super_cat" style="display: none;"><img src="http://g-ecx.images-amazon.com/images/G/01/Audible/en_US/images/creative/amazon/beacon/ADBLCRE-1968_beacon_MemoryofLight._V371600424_.png" usemap="#nav_imgmap_audible" class="nav_browse_promo" style="bottom: -14px; right: -19px; width: 479px; height: 470px;"><ul class="nav_browse_ul nav_browse_cat_ul"><li class="nav_pop_li nav_browse_cat_head">Audible Audiobooks</li>' +
      '<li class="nav_first nav_taglined nav_subcat_link nav_pop_li"><a href="/gp/audible/signup/display.html/ref=sa_menu_aud_mem" class="nav_a nav_item">Audible Membership</a><div class="nav_tag">Get to know Audible</div></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_aud_bks?ie=UTF8&amp;node=2402172011" class="nav_a nav_item">Audible Audiobooks &amp; More</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/gp/bestsellers/books/2402172011/ref=sa_menu_aud_bst" class="nav_a nav_item">Bestsellers</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_aud_new?ie=UTF8&amp;node=2669348011" class="nav_a nav_item">New &amp; Notable</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_aud_fav?ie=UTF8&amp;node=2669344011" class="nav_a nav_item">Listener Favorites</a></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_aud_wfv?ie=UTF8&amp;node=5744819011" class="nav_a nav_item">Whispersync for Voice</a><div class="nav_tag">Switch between reading and listening</div></li>' +
      '</ul></div>' +
      '<div id="nav_subcats_8" data-nav-promo-id="books" class="nav_browse_subcat nav_super_cat"><img src="http://g-ecx.images-amazon.com/images/G/01/img13/books/flyout/0220-adult-books-flyout._V375019574_.png" usemap="#nav_imgmap_books" class="nav_browse_promo" style="bottom: -14px; right: 0px; width: 499px; height: 410px;"><ul class="nav_browse_ul nav_browse_cat_ul"><li class="nav_first nav_pop_li nav_browse_cat_head">Books</li>' +
      '<li class="nav_first nav_subcat_link nav_pop_li"><a href="/books-used-books-textbooks/b/ref=sa_menu_bo?ie=UTF8&amp;node=283155" class="nav_a nav_item">Books</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Kindle-eBooks/b/ref=sa_menu_kbo?ie=UTF8&amp;node=1286228011" class="nav_a nav_item">Kindle Books</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Childrens-Books/b/ref=sa_menu_cbo?ie=UTF8&amp;node=4" class="nav_a nav_item">Children\'s Books</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/New-Used-Textbooks-Books/b/ref=sa_menu_tb?ie=UTF8&amp;node=465600" class="nav_a nav_item">Textbooks</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Audiobooks-Books/b/ref=sa_menu_ab?ie=UTF8&amp;node=368395011" class="nav_a nav_item">Audiobooks</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/magazines/b/ref=sa_menu_magazines?ie=UTF8&amp;node=599858" class="nav_a nav_item">Magazines</a></li>' +
      '</ul></div>' +
      '<div id="nav_subcats_9" data-nav-promo-id="movies-music-games" class="nav_browse_subcat" style="display: none;"><ul class="nav_browse_ul nav_browse_cat_ul"><li class="nav_pop_li nav_browse_cat_head">Movies, Music &amp; Games</li>' +
      '<li class="nav_first nav_subcat_link nav_pop_li"><a href="/movies-tv-dvd-bluray/b/ref=sa_menu_mov?ie=UTF8&amp;node=2625373011" class="nav_a nav_item">Movies &amp; TV</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/movies-tv-bluray-bluray3d/b/ref=sa_menu_blu?ie=UTF8&amp;node=2901953011" class="nav_a nav_item">Blu-ray</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Instant-Video/b/ref=sa_menu_atv?ie=UTF8&amp;node=2858778011" class="nav_a nav_item">Amazon Instant Video</a></li>' +
      '<li class="nav_subcat_link nav_pop_li nav_divider_before"><a href="/music-rock-classical-pop-jazz/b/ref=sa_menu_mu?ie=UTF8&amp;node=5174" class="nav_a nav_item">Music</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/MP3-Music-Download/b/ref=sa_menu_dmusic?ie=UTF8&amp;node=163856011" class="nav_a nav_item">MP3 Downloads</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/musical-instruments-accessories-sound-recording/b/ref=sa_menu_mi?ie=UTF8&amp;node=11091801" class="nav_a nav_item">Musical Instruments</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_entcol?ie=UTF8&amp;node=5088769011" class="nav_a nav_item">Entertainment Collectibles</a></li>' +
      '<li class="nav_subcat_link nav_pop_li nav_divider_before"><a href="/computer-video-games-hardware-accessories/b/ref=sa_menu_cvg?ie=UTF8&amp;node=468642" class="nav_a nav_item">Video Games</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Game-Downloads/b/ref=sa_menu_gdown?ie=UTF8&amp;node=979455011" class="nav_a nav_item">Game Downloads</a></li>' +
      '</ul></div>' +
      '<div id="nav_subcats_10" data-nav-promo-id="electronics-computers" class="nav_browse_subcat nav_super_cat" style="display: none;"><img src="http://g-ecx.images-amazon.com/images/G/01/img13/camera-photo/flyout/camera_flyout._V372573145_.png" usemap="#nav_imgmap_electronics-computers" class="nav_browse_promo" style="bottom: -14px; right: -21px; width: 516px; height: 472px;"><ul class="nav_browse_ul nav_browse_cat_ul"><li class="nav_first nav_pop_li nav_browse_cat_head">Electronics</li>' +
      '<li class="nav_first nav_subcat_link nav_pop_li"><a href="/Televisions-Video/b/ref=sa_menu_tv?ie=UTF8&amp;node=1266092011" class="nav_a nav_item">TV &amp; Video</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Home-Audio-Electronics/b/ref=sa_menu_hat?ie=UTF8&amp;node=667846011" class="nav_a nav_item">Home Audio &amp; Theater</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Camera-Photo-Film-Canon-Sony/b/ref=sa_menu_p?ie=UTF8&amp;node=502394" class="nav_a nav_item">Camera, Photo &amp; Video</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/cell-phones-service-plans-accessories/b/ref=sa_menu_wi?ie=UTF8&amp;node=2335752011" class="nav_a nav_item">Cell Phones &amp; Accessories</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/computer-video-games-hardware-accessories/b/ref=sa_menu_cvg?ie=UTF8&amp;node=468642" class="nav_a nav_item">Video Games</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/MP3-Players-Audio-Video/b/ref=sa_menu_mp3?ie=UTF8&amp;node=172630" class="nav_a nav_item">MP3 Players &amp; Accessories</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Car-Electronics/b/ref=sa_menu_gps?ie=UTF8&amp;node=1077068" class="nav_a nav_item">Car Electronics &amp; GPS</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Appliances/b/ref=sa_menu_ha?ie=UTF8&amp;node=2619525011" class="nav_a nav_item">Appliances</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/musical-instruments-accessories-sound-recording/b/ref=sa_menu_mi?ie=UTF8&amp;node=11091801" class="nav_a nav_item">Musical Instruments</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_elec_acc?ie=UTF8&amp;node=5745855011" class="nav_a nav_item">Electronics Accessories</a></li>' +
      '</ul>' +
      '<ul class="nav_browse_ul nav_browse_cat2_ul">' +
      '<li class="nav_pop_li nav_browse_cat_head">Computers</li>' +
      '<li class="nav_first nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_lapnet?ie=UTF8&amp;node=2956501011" class="nav_a nav_item">Laptops, Tablets &amp; Netbooks</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Desktops-Computers-Add-Ons/b/ref=sa_menu_deskserv?ie=UTF8&amp;node=565098" class="nav_a nav_item">Desktops &amp; Servers</a></li>' +
      '<li class="nav_taglined nav_subcat_link nav_pop_li"><a href="/b/ref=sa_menu_compaccess?ie=UTF8&amp;node=2956536011" class="nav_a nav_item">Computer Accessories &amp; Peripherals</a><div class="nav_tag">External drives, mice, networking &amp; more</div></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/PC-Components-Computer-Add-Ons-Computers/b/ref=sa_menu_components?ie=UTF8&amp;node=193870011" class="nav_a nav_item">Computer Parts &amp; Components</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/design-download-business-education-software/b/ref=sa_menu_sw?ie=UTF8&amp;node=229534" class="nav_a nav_item">Software</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/PC-Games/b/ref=sa_menu_pcgm?ie=UTF8&amp;node=229575" class="nav_a nav_item">PC Games</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Printers-Office-Electronics/b/ref=sa_menu_printers?ie=UTF8&amp;node=172635" class="nav_a nav_item">Printers &amp; Ink</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/office-products-supplies-electronics-furniture/b/ref=sa_menu_op?ie=UTF8&amp;node=1064954" class="nav_a nav_item">Office &amp; School Supplies</a></li>' +
      '</ul></div>' +
      '<div id="nav_subcats_11" data-nav-promo-id="home-garden-tools" class="nav_browse_subcat nav_super_cat" style="display: none;"><img src="http://g-ecx.images-amazon.com/images/G/01/img13/home/flyout/spring-cleaning_flyout._V373208916_.png" usemap="#nav_imgmap_home-garden-tools" class="nav_browse_promo" style="bottom: -14px; right: -20px; width: 515px; height: 308px;"><ul class="nav_browse_ul nav_browse_cat_ul"><li class="nav_first nav_pop_li nav_browse_cat_head">Home, Garden &amp; Pets</li>' +
      '<li class="nav_first nav_subcat_link nav_pop_li"><a href="/kitchen-dining-small-appliances-cookware/b/ref=sa_menu_ki?ie=UTF8&amp;node=284507" class="nav_a nav_item">Kitchen &amp; Dining</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/furniture-decor-rugs-lamps-beds-tv-stands/b/ref=sa_menu_fd?ie=UTF8&amp;node=1057794" class="nav_a nav_item">Furniture &amp; DÃ©cor</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/bedding-bath-sheets-towels/b/ref=sa_menu_bb?ie=UTF8&amp;node=1057792" class="nav_a nav_item">Bedding &amp; Bath</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Appliances/b/ref=sa_menu_ha?ie=UTF8&amp;node=2619525011" class="nav_a nav_item">Appliances</a></li>' +
      '<li class="nav_subcat_link nav_pop_li nav_divider_before"><a href="/Patio-Lawn-Garden/b/ref=sa_menu_lp?ie=UTF8&amp;node=2972638011" class="nav_a nav_item">Patio, Lawn &amp; Garden</a></li>' +
      '<li class="nav_subcat_link nav_pop_li nav_divider_before"><a href="/Arts-Crafts-Sewing/b/ref=sa_menu_sch?ie=UTF8&amp;node=2617941011" class="nav_a nav_item">Arts, Crafts &amp; Sewing</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/pet-supplies-dog-cat-food-bed-toy/b/ref=sa_menu_ps?ie=UTF8&amp;node=2619533011" class="nav_a nav_item">Pet Supplies</a></li>' +
      '</ul>' +
      '<ul class="nav_browse_ul nav_browse_cat2_ul">' +
      '<li class="nav_pop_li nav_browse_cat_head">Tools, Home Improvement</li>' +
      '<li class="nav_first nav_subcat_link nav_pop_li"><a href="/Tools-and-Home-Improvement/b/ref=sa_menu_hi2?ie=UTF8&amp;node=228013" class="nav_a nav_item">Home Improvement</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Power-Tools-and-Hand-Tools/b/ref=sa_menu_hi?ie=UTF8&amp;node=328182011" class="nav_a nav_item">Power &amp; Hand Tools</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Lighting-and-Ceiling-Fans/b/ref=sa_menu_llf?ie=UTF8&amp;node=495224" class="nav_a nav_item">Lamps &amp; Light Fixtures</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Kitchen-and-Bath-Fixtures/b/ref=sa_menu_kbf?ie=UTF8&amp;node=3754161" class="nav_a nav_item">Kitchen &amp; Bath Fixtures</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Hardware-Locks-and-Fasteners/b/ref=sa_menu_hdw?ie=UTF8&amp;node=511228" class="nav_a nav_item">Hardware</a></li>' +
      '<li class="nav_subcat_link nav_pop_li"><a href="/Building-Supply-and-Building-Materials/b/ref=sa_menu_bs?ie=UTF8&amp;node=551240" class="nav_a nav_item">Building Supplies</a></li>' +
      '</ul></div>' +
      '</div>' +
      '</div>';

  });
  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('Click to Open and Close', function () {
    var ul = '#primaryNavigation',
      childrenSelectors = [
        {
          selector: '>li',
          role: 'menuitem',
          sanitize: function () {
            this.removeAttribute('onfocus');
            this.removeAttribute('onclick');
          },
          expand: { event: 'click', target: 'self', selector: 'a>span'},
          clickSelector: 'a>span'
        }, {
          ownsSelector: null,
          selector: '>ul>li',
          collapse: { event: 'click', target: 'parent', selector: 'span'},
          role: 'menuitem',
          clickSelector: 'a'
        }
      ],
      container, lis, menuitems = [], menus = [], uls, menubars = [], els, pres = [];

    ariaMenu(ul, childrenSelectors);
    container = query(ul);
    lis = queryAll('li', container);
    each(lis, function (value) {
      var role;

      role = value.getAttribute('role');
      if (role === 'menuitem') {
        menuitems.push(value);
      }
    });
    assert.equal(menuitems.length, 42, 'should have 42 menuitems');

    uls = queryAll('ul');
    each(uls, function (value) {
      var role;
      role = value.getAttribute('role');
      if (role === 'menubar') {
        menubars.push(value);
      } else if (role === 'menu') {
        menus.push(value);
      }
    });
    assert.equal(menubars.length, 1, 'should have 1 menubar');
    assert.equal(menus.length, 5, 'should have 5 menus');

    els = container.getElementsByTagName('*');
    each(els, function (value) {
      var role;
      role = value.getAttribute('role');
      if (role === 'presentation') {
        pres.push(value);
      }
    });
    assert.equal(pres.length, 0, 'should have 0 presentational elements');

    //TODO: figure out how to test the keyboard stuff
  });

  it('CSS to expand and collapse', function () {
    var ul = '#primaryNav>ul',
    childrenSelectors = [
      {
        selector: '>li',
        role: 'menuitem',
        sanitize: function () {
        },
        expand: { addClass: 'amaze-statefarm-menu-expanded', target: 'self', selector: '>ul'},
        clickSelector: 'a>span'
      }, {
        selector: '>ul>li>ul>li',
        collapse: { removeClass: 'amaze-statefarm-menu-expanded', target: 'parent', selector: '>ul'},
        role: 'menuitem',
        clickSelector: 'a'
      }
    ],
    container, lis, menuitems = [], menus = [], uls, menubars = [], els, pres = [], anchors, violations = 0;

    ariaMenu(ul, childrenSelectors);
    container = query(ul);

    anchors = queryAll('a', container);
    each(anchors, function (value) {
      if (!value.getAttribute('tabindex')) {
        violations += 1;
      }
    });
    assert.equal(violations, 0, 'should have 0 anchors without tabindex');

    lis = queryAll('li', container);
    each(lis, function (value) {
      var role;

      role = value.getAttribute('role');
      if (role === 'menuitem') {
        menuitems.push(value);
      }
    });
    assert.equal(menuitems.length, 66, 'should have 66 menuitems');

    uls = queryAll('ul');
    each(uls, function (value) {
      var role;
      role = value.getAttribute('role');
      if (role === 'menubar') {
        menubars.push(value);
      } else if (role === 'menu') {
        menus.push(value);
      }
    });
    assert.equal(menus.length, 3, 'should have 3 menus');
    assert.equal(menubars.length, 1, 'should have 1 menubar');

    els = container.getElementsByTagName('*');
    each(els, function (value) {
      var role;
      role = value.getAttribute('role');
      if (role === 'presentation') {
        pres.push(value);
      }
    });
    assert.equal(pres.length, 12, 'should have 12 presentational elements');
  });

  it('test events', function () {

    function byId(id) {
      return document.getElementById(id);
    }

    var ul = '#internalEntryPoint2',
      childrenSelectors = [
        {
          selector: '>li',
          role: 'menuitem',
          expand: { event: 'click', target: 'self', selector: '', addClass : 'expanded', removeClass : 'collapsed'}
        }, {
          selector: '>ul>li',
          role: 'menuitem',
          collapse: { event: 'click', target: 'parent', addClass : 'collapsed', removeClass : 'expanded'},
          expand: { event: 'click', target: 'self', addClass : 'expanded', removeClass : 'collapsed'}
        }, {
          selector: '>ul>li',
          role: 'menuitem',
          collapse: { event: 'click', target: 'parent', addClass : 'collapsed', removeClass : 'expanded'}
        }
      ],
      container, lis, focussed = [],
      expected, li, clazz, parentMenu;

    ariaMenu(ul, childrenSelectors);
    container = query(ul);
    lis = queryAll('li', container);
    each(lis, function (value) {
      events.on(value, 'click', function (e) {
        focussed.push({ type : 'click', id : e.target.getAttribute('id')});
        e.stopPropagation();
        e.preventDefault();
      });
    });
    // Test right key on top menu
    events.fire('#one2', 'keydown', { keyCode : 39}); // RIGHT
    assert.equal(document.activeElement, byId('two2'), 'right key should trigger focus event on next element');

    events.fire('#two2', 'keydown', { keyCode : 39}); // RIGHT
    assert.equal(document.activeElement, byId('three2'), 'right key should trigger focus event on next element');

    // Test left key on top menu
    events.fire('#three2', 'keydown', { keyCode : 37}); // LEFT
    assert.equal(document.activeElement, byId('two2'), 'left key should trigger focus event on prev element');

    // Test cycle around from first to last
    events.fire('#two2', 'keydown', { keyCode : 37}); // LEFT
    assert.equal(document.activeElement, byId('one2'), 'left key should trigger focus event on last element');

    events.fire('#one2', 'keydown', { keyCode : 37}); // LEFT
    assert.equal(document.activeElement, byId('three2'), 'left key should trigger focus event on prev element');

    // Test enter key to open menu
    focussed = [];
    expected = [{ type: 'click', id: 'one2'}];
    li = query('#one2');
    events.fire(li, 'keydown', { keyCode : 13}); // ENTER

    clazz = li.className;
    assert.ok(clazz.indexOf('expanded') !== -1, 'expanded class should be on the newly expanded menu');
    parentMenu = li;
    assert.equal(document.activeElement, byId('oneOne2'));
    assert.deepEqual(focussed, expected, 'enter key should trigger focus event on first child element');

    // test ESC to close menu
    focussed = [];
    expected = [{ type: 'click', id: 'one2'}];
    li = query('#oneOne2');
    events.fire(li, 'keydown', { keyCode : 27}); // ESC

    clazz = parentMenu.className;
    assert.ok(clazz.indexOf('expanded') === -1, 'expanded class should be gone');
    assert.ok(clazz.indexOf('collapsed') !== -1, 'collapsed class should be on the newly collapsed menu');
    assert.equal(document.activeElement, byId('one2'));
    assert.deepEqual(focussed, expected, 'esc key should trigger focus event on parent element');

    // Test down key to open menu
    focussed = [];
    expected = [{ type: 'click', id: 'one2'}];
    li = query('#one2');
    events.fire(li, 'keydown', { keyCode : 40}); // DOWN
    assert.equal(document.activeElement, byId('oneOne2'));
    assert.deepEqual(focussed, expected, 'down key should trigger focus event on first child element');

    // test left key to close menu
    focussed = [];
    expected = [{ type: 'click', id: 'one2'}];
    li = query('#oneOne2');
    events.fire(li, 'keydown', { keyCode : 37}); // LEFT
    assert.equal(document.activeElement, byId('one2'));
    assert.deepEqual(focussed, expected, 'left key should trigger focus event on parent element');

    // test down key in menu
    events.fire('#oneOne2', 'keydown', { keyCode : 40}); // DOWN
    assert.equal(document.activeElement, byId('oneTwo2'), 'down key should trigger focus event on first child element');

    events.fire('#oneTwo2', 'keydown', { keyCode : 40}); // DOWN
    assert.equal(document.activeElement, byId('oneThree2'), 'down key should trigger focus event on next element');

    // test cycle from bottom back to top
    events.fire('#oneThree2', 'keydown', { keyCode : 40}); // DOWN
    assert.equal(document.activeElement, byId('oneOne2'), 'down key should trigger focus event on last element');

    // test up key cycle from first to last
    focussed = [];
    expected = [{ type: 'focus', id: 'oneThree2'}];
    li = query('#oneOne2');
    events.fire('#oneOne2', 'keydown', { keyCode : 38}); // UP
    assert.equal(document.activeElement, byId('oneThree2'), 'up key should trigger focus event on last element');

    // test up key in menu
    events.fire('#oneThree2', 'keydown', { keyCode : 38}); // UP
    assert.equal(document.activeElement, byId('oneTwo2'), 'up key should trigger focus event on prev element');

    events.fire('#oneTwo2', 'keydown', { keyCode : 38}); // UP
    assert.equal(document.activeElement, byId('oneOne2'), 'up key should trigger focus event on prev element');

    // test sub-menu keyboard focus
    // test right key opening sub-menu
    focussed = [];
    expected = [{ type: 'click', id: 'oneOne2'}];
    li = query('#oneOne2');
    events.fire(li, 'keydown', { keyCode : 39}); // RIGHT
    assert.equal(document.activeElement, byId('oneOneOne2'));

    clazz = li.className;
    assert.ok(clazz.indexOf('expanded') !== -1, 'expanded class should be on the newly expanded sub-menu');
    parentMenu = li;
    assert.deepEqual(focussed, expected, 'right key should trigger focus event on first child element');

    // test left key closing sub-menu
    focussed = [];
    expected = [{ type: 'click', id: 'oneOne2'}];
    li = query('#oneOneOne2');
    events.fire(li, 'keydown', { keyCode : 37}); // LEFT
    assert.equal(document.activeElement, byId('oneOne2'));

    clazz = parentMenu.className;
    assert.ok(clazz.indexOf('expanded') === -1, 'expanded class should be gone');
    assert.ok(clazz.indexOf('collapsed') !== -1, 'collapsed class should be on the newly collapsed sub-menu');

    assert.deepEqual(focussed, expected, 'left key should trigger a focus event on the parent menu');
  });

  it('test aria owns, preselector and indexGetter', function () {
    var fixtureSel = '#internalEntryPoint3',
      options, topmenuSelector, fixture, owned, lis, count, subs,
      topsel = 'a.nav-shop-all-button';

    topmenuSelector = 'nav-bar-inner';
    options = [
      {
        selector : topsel,
        role : 'menuitem'
      },
      {
        preselection : 'body',
        ownsSelector : '#nav_cats',
        selector : '#nav_cats>li',
        timeout : 100,
        clickSelector : '>a',
        role : 'menuitem'
      },
      {
        preselection : 'body',
        indexGetter : function (el) {
          var id = el.getAttribute('id');

          return id.substring(8);
        },
        ownsSelector : '#nav_subcats_{index}',
        selector : '#nav_subcats_{index}>ul>li',
        clickSelector : '>a',
        role : 'menuitem'
      }
    ];
    ariaMenu('#' + topmenuSelector, options);
    fixture = query(fixtureSel);
    lis = queryAll('#nav_cats li');
    owned = [];
    each(lis, function (value) {
      if (value.hasAttribute('aria-owns')) {
        owned.push(value);
      }
    });
    assert.equal(11, owned.length, 'Should be 11 menuitems that own a sub-menu');
    count = 0;
    each(owned, function (value) {
      if (value.hasAttribute('tabindex') && value.getAttribute('tabindex') === '-1' &&
        value.hasAttribute('role') && value.getAttribute('role') === 'menuitem' &&
        value.hasAttribute('aria-haspopup') && value.getAttribute('aria-haspopup') === 'true') {
        count += 1;
      }
    });
    assert.equal(11, count, 'All 11 menuitems should have the correct attributes');
    subs = [];
    each(owned, function (value) {
      var owns = document.getElementById(value.getAttribute('aria-owns'));
      if (owns) {
        subs.push(owns);
      }
    });
    assert.equal(11, subs.length, 'All 11 menuitems should own an element that exists');
  });
});
