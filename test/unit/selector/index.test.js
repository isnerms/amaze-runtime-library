describe('selector', function () {
  'use strict';

  var selector = require('selector');
  var each = require('collections').each;
  var dom = require('dom');

  beforeEach(function () {
    var div = document.createElement('div');
    div.id = 'fixture';
    div.innerHTML =
        '<p id=a class="want"></p>'
      + '<p id=b class="want"></p>'
      + '<p id=c class="want"></p>'
      + '<p id=d class="want"></p>'
      + '<p id=e class="dontwant"></p>'
      + '<div class="context">'
      + '  <p id=f class="dontwant"></p>'
      + '  <p id=g class="want insidecontext"></p>'
      + '  <p id=h class="want insidecontext"></p>'
      + '  <p id=i class="dontwant"></p>'
      + '</div>';
    document.body.appendChild(div);
  });

  afterEach(function () {
    var div = document.getElementById('fixture');
    document.body.removeChild(div);
  });

  describe('selector()', function () {
    it('should be a function', function () {
      assert.isFunction(selector);
    });

    it('should return an array', function () {
      assert.isArray(selector('span'));
    });
  });

  describe('selector.queryAll()', function () {
    it('should return an array', function () {
      assert.isArray(selector.queryAll());
      assert.isArray(selector.queryAll('span'));
    });

    it('should take a context element', function () {
      var spans = selector.queryAll('span', selector.query('#mocha'));

      each(spans, function (span) {
        assert.ok(dom.findUp(span, '#mocha'));
      });
    });

    it('should alias selector()', function () {
      assert.equal(selector, selector.queryAll);
    });

    it('should throw if given an invalid context', function () {
      assert.throws(function () {
        selector.queryAll('span', '#element-that-doesnt-exist');
      });
      assert.throws(function () {
        selector.queryAll('span', null);
      });
      assert.throws(function () {
        selector.queryAll('span', false);
      });
      assert.throws(function () {
        selector.queryAll('span', true);
      });
    });

    it('should support :not pseudos', function () {
      var g = selector.query('#g');
      var h = selector.query('#h');
      var context = selector.query('#fixture .context');
      var p = selector.queryAll('p:not([class=dontwant])', context);
      assert.deepEqual([g, h], p);
    });
  });

  describe('selector.query()', function () {
    it('should return a single node', function () {
      assert.isObject(selector.query('span'));
      assert.isTrue(dom.isNode(selector.query('div')));
    });

    it('should take a context element', function () {
      // phantomjs sucks and is throwing here for no reason:
      //
      //
      //     1) selector selector.query() should take a context element:
      //       AssertionError: null == true
      //           at fail (http://localhost:9876/node_modules/proclaim/lib/proclaim.js:224)
      //           at http://localhost:9876/node_modules/proclaim/lib/proclaim.js:240
      //           at http://localhost:9876/test/unit/library/selector.test.js:55
      //           at http://localhost:9876/node_modules/mocha/mocha.js:4106
      //           at http://localhost:9876/node_modules/mocha/mocha.js:4475
      //           at http://localhost:9876/node_modules/mocha/mocha.js:4534
      //           at next (http://localhost:9876/node_modules/mocha/mocha.js:4401)
      //           at http://localhost:9876/node_modules/mocha/mocha.js:4410
      //           at next (http://localhost:9876/node_modules/mocha/mocha.js:4354)
      //           at http://localhost:9876/node_modules/mocha/mocha.js:4378
      //           at timeslice (http://localhost:9876/node_modules/mocha/mocha.js:5361)
      //
      if (window.mochaPhantomJS) {
        return;
      }

      var span = selector.query('span', selector.query('#mocha'));

      assert.ok(dom.findUp(span, '#mocha'));
    });

    it('should throw if given an invalid context', function () {
      assert.throws(function () {
        selector.query('span', '#element-that-doesnt-exist');
      });
      assert.throws(function () {
        selector.query('span', null);
      });
      assert.throws(function () {
        selector.query('span', false);
      });
      assert.throws(function () {
        selector.query('span', true);
      });
    });

    it('should support :not pseudos', function () {
      var g = selector.query('#g');
      var context = selector.query('#fixture .context');
      var p = selector.query('p:not([class=dontwant])', context);
      assert.equal(g, p);
    });
  });

  describe('selector.matches()', function () {
    it('should return true when an element matches a selector', function () {
      assert.ok(selector.matches(document.body, 'body'));
    });

    it('should return false when an element does not match a selector', function () {
      assert.ok(!selector.matches(document.body, 'span.not-the-body'));
    });
  });

  describe('selector.unique()', function () {
    it('should generate and return a unique selector', function () {
      var mocha = selector.query('#mocha'),
        uniq = selector.unique(mocha),
        thing = selector.query(uniq);

      assert.strictEqual(mocha, thing);
    });

    it('should return a selector which only matches a single element', function () {
      var div = selector.query('#mocha'),
          uniq = selector.unique(div);

      assert.equal(selector.queryAll(uniq).length, 1);
    });
  });

});
