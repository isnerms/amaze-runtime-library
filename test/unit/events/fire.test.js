
describe('events.fire', function () {
  'use strict';

  var events = require('events');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    location.hash = '';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should fire an event', function (done) {
    fixture.innerHTML = '<div id=blah></div>';
    events.on('#blah', 'click', function () {
      done();
    });
    events.fire('#blah', 'click');
  });

  it('should add additional properties to the fired event', function (done) {
    fixture.innerHTML = '<div id=bar>yo</div>';
    events.on('#bar', 'keydown', function (e) {
      assert.equal(13, e.which);
      assert.equal(document.getElementById('bar'), e.target);
      done();
    });
    events.fire('#bar', 'keydown', { which: 13 });
  });

  it('should throw when provided properties it can\'t set', function () {
    assert.throws(function () {
      events.fire('body', 'click', { target: document });
    }, Error);
    assert.throws(function () {
      events.fire('body', 'click', { type: 'apples' });
    }, Error);
  });
});
