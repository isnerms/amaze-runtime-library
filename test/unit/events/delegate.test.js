
describe('events.delegate', function () {
  'use strict';

  var events = require('events');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    location.hash = '';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should bind a delegated event listener', function (done) {
    fixture.innerHTML =
        '<div id=root>'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '  <span class=blam>hi</span>'
      + '  <span>hi</span>'
      + '</div>';

    events.delegate('#root', '.blam', 'click', function (e) {
      assert.equal('blam', e.target.className);
      done();
    });

    events.fire('#root .blam', 'click');
  });

  it('should return the bound function', function () {
    var fn = events.delegate('body', 'div', 'mouseenter', function () {});
    assert.equal('function', typeof fn);
  });
});
