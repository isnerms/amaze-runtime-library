
describe('events.undelegate', function () {
  'use strict';

  var events =  require('events');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    location.hash = '';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should unbind a delegated event listener', function () {
    fixture.innerHTML =
        '<div id=root>'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '  <span class=blam>hi</span>'
      + '  <span>hi</span>'
      + '</div>';

    var fn = events.delegate('#root', '.blam', 'click', function () {
      throw new Error('fail');
    });

    events.undelegate('#root', 'click', fn);

    events.fire('#root .blam', 'click');
  });

  it('should unbind all delegated event listeners when not provided fn', function () {
    fixture.innerHTML =
        '<div id=root>'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '  <span class=blam>hi</span>'
      + '  <span>hi</span>'
      + '</div>';

    for (var i = 0; i < 1000; i++) {
      events.delegate('#root', 'span', 'click', onclick);
    }

    events.undelegate('#root', 'click');
    events.fire('#root .blam', 'click');

    function onclick() {
      throw new Error('fail');
    }
  });
});
