
describe('events.off', function () {
  'use strict';

  var events = require('events');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    location.hash = '';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should remove a bound event listener', function (done) {
    fixture.innerHTML = '<div id=foo>hi</div>';
    var div = document.getElementById('foo');
    var fn = events.on(div, 'click', function () {
      throw new Error('didnt unbind listener');
    });

    events.off(div, 'click', fn);
    events.fire(div, 'click');
    setTimeout(function () {
      done();
    }, 10);
  });

  it('should work with function declarations', function (done) {
    var clicks = 0;
    fixture.innerHTML = '<div id=foo>hi</div>';
    events.on('#foo', 'click', onclick);
    events.fire('#foo', 'click');
    events.off('#foo', 'click', onclick);
    events.fire('#foo', 'click');

    setTimeout(function () {
      assert.equal(1, clicks);
      done();
    }, 10);

    function onclick() {
      clicks++;
    }
  });

  it('should throw when not provided type', function () {
    assert.throws(function () {
      events.off('body');
    }, TypeError);
  });

  it('should remove all listeners of type when no fn is provided', function (done) {
    fixture.innerHTML = '<a href=# id=bar>google</a>';

    // unrealistic number of event listeners
    for (var i = 0; i < 1000; i++) {
      events.on('#bar', 'click', onclick);
    }

    events.off('#bar', 'click');

    for (var k = 0; k < 100; k++) {
      events.fire('#bar', 'click');
    }

    setTimeout(done, 100);

    function onclick() {
      throw new Error('listener not unbound');
    }
  });
});
