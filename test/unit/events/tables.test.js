
describe('events and tables', function () {
  'use strict';

  var fixture = null;
  var events = require('events');

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  describe('.delegate', function () {
    it('should allow using a table as root', function (done) {

      fixture.innerHTML =
          '<table>'
        + '  <tbody>'
        + '    <tr>'
        + '      <td>'
        + '        <a href="#" id="anchor">'
        + '          click me'
        + '        </a>'
        + '      </td>'
        + '    <tr>'
        + '  </tbody>'
        + '</table>';

      var table = fixture.firstChild;

      events.delegate(table, 'a', 'click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        done();
      });

      events.fire('#anchor', 'click');
    });
  });

  describe('.on', function () {
    it('should allow binding to a table', function (done) {

      fixture.innerHTML =
          '<table>'
        + '  <tbody>'
        + '    <tr>'
        + '      <td>'
        + '        <a href="#" id="anchor">'
        + '          click me'
        + '        </a>'
        + '      </td>'
        + '    <tr>'
        + '  </tbody>'
        + '</table>';

      var table = fixture.firstChild;

      events.on(table, 'click', function () {
        done();
      });

      events.fire(table, 'click');
    });
  });

});
