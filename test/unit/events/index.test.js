
describe('events', function () {
  'use strict';

  var events = require('events');

  // dynamically build sanity tests for each method

  var methods = [ 'on', 'off', 'fire', 'delegate', 'undelegate', 'once' ];

  for (var i = 0; methods[i]; i++) {
    it(should(methods[i]), test(methods[i]));
  }

  function should(name) {
    var spec = [ 'should have' ];
    spec.push(/^[aeiou]/.test(name) ? 'an' : 'a');
    spec.push('"' + name + '"');
    spec.push('method');
    return spec.join(' ');
  }

  function test(name) {
    return function () {
      assert.equal('function', typeof events[name]);
    };
  }
});
