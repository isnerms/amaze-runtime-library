
describe('events.on', function () {
  'use strict';

  var events = require('events');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    location.hash = '';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should bind an event listener to an HTMLElement', function (done) {
    fixture.innerHTML = '<div id=foo></div>';
    var div = document.getElementById('foo');
    events.on(div, 'click', function () {
      done();
    });
    events.fire(div, 'click');
  });

  it('should bind an event listener to an HTMLElement matching a selector', function (done) {
    fixture.innerHTML = '<div id=bar></div>';
    events.on('#bar', 'click', function () {
      done();
    });
    events.fire('#bar', 'click');
  });

  it('should normalize captured events', function (done) {
    fixture.innerHTML =
        '<div>'
      + '  <a id=baz href=#foo>'
      + '    this is an anchor'
      + '  </a>'
      + '</div>';

    var anchor = document.getElementById('baz');

    events.on('#baz', 'click', function (event) {
      assert.equal('function', typeof event.preventDefault);
      event.preventDefault();
      assert.equal('function', typeof event.stopPropagation);
      event.stopPropagation();
      assert.strictEqual(anchor, event.target);
      done();
    });

    events.fire('#baz', 'click');
  });

  it('should return the bound function', function (done) {
    fixture.innerHTML = '<div id=blah></div>';
    var fn = events.on('#blah', 'click', function () {
      assert.equal('function', typeof fn);
      done();
    });
    events.fire('#blah', 'click');
  });
});
