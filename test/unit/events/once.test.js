
describe('events.once', function () {
  'use strict';

  var events = require('events');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    location.hash = '';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should bind an event listener which removes itself', function (done) {
    var count = 0;
    fixture.innerHTML = '<div id=foo></div>';
    events.once('#foo', 'click', function () {
      count++;
    });
    for (var i = 0; i < 10; i++) {
      events.fire('#foo', 'click');
    }
    setTimeout(function () {
      assert.equal(1, count);
      done();
    }, 10);
  });
});
