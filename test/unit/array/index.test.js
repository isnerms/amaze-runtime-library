describe('array', function () {
  'use strict';

  var toString = Object.prototype.toString;
  var array = require('array');

  describe('.isArray', function () {
    it('valid Arrays', function () {
      var A = Array;
      assert.ok(array.isArray([]), 'Zero-length array');
      assert.ok(array.isArray([1, 2, 3]), '> zero-length array');
      assert.ok(array.isArray(new A()), 'zero-length array, constructor');
      assert.ok(array.isArray(new A(3)), '> zero-length array, constructor');
      assert.ok(array.isArray(new A(1, 2, 3)), '> zero-length array, constructor');
    });

    it('invalid (non) Arrays', function () {
      assert.ok(!array.isArray({}), 'An object is not an array');
      assert.ok(!array.isArray(null), 'null is not an array');
      assert.ok(!array.isArray('cats'), 'A string is not an array');
      assert.ok(!array.isArray(2), 'A number is not an array');
      assert.ok(!array.isArray(true), 'A boolean is not an array');
      assert.ok(!array.isArray({ length: 2 }),
        'An object with a numeric property called `length` is not an array');
    });

    it('Arguments', function () {
      function cats() {
        assert.equal(array.isArray(arguments), false);
      }
      cats(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    });

    it('NodeList', function () {
      var nodes = document.getElementsByTagName('*');
      assert.equal(array.isArray(nodes), false);
    });

    it('[]', function () {
      assert.equal(array.isArray([]), true);
      assert.equal(array.isArray([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]), true);
    });

    it('Array()', function () {
      var A = Array;
      assert.equal(array.isArray(new A()), true);
      assert.equal(array.isArray(new A(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)), true);
    });

    it('{}', function () {
      assert.equal(array.isArray({}), false);
      assert.equal(array.isArray({a: 1, b: 2, c: 3}), false);
    });
  });

  describe('.inArray', function () {
    it('inArray', function () {
      assert.ok(array.inArray([1, 2, 3], 3), 'Integer');
      assert.ok(array.inArray(['1', '2', '3'], '3'), 'String');
      assert.ok(!array.inArray([1, 2, '3'], 3), 'Should use strict-equal comparasion');
      assert.ok(!array.inArray([], 3), 'Empty array doesn\'t have anything');
      assert.ok(!array.inArray([1, 2, 3], 'cats'), 'Does not contain cats');
      assert.ok(!array.inArray(['1', '2', '3'], 'cats'), 'No Match');
      assert.ok(array.inArray([1, 2, 3], 1), 'Should return true for first element too');
      assert.ok(array.isArray(new Array(3), undefined), 'undefined');
    });
  });

  describe('.toArray', function () {
    it('toArray - NodeList', function () {
      var list = document.getElementsByTagName('*'),
          firstChild = list[0],
          childCount = list.length,
          result;
      result = array.toArray(list);
      assert.equal(toString.call(result), '[object Array]',
        'The liveList should be returned as an Array.');
      assert.ok(result.length,
        'The Array should have a length property.');
      assert.equal(result[0], firstChild,
        'The order of the list should not have changed.');
      assert.equal(result.length, childCount,
        'The number of Nodes should not change.');
    });

    it('toArray - array-like', function () {
      var firstChild, childCount, result,
          list = {};

      list[0] = 0;
      list[1] = 1;
      list[2] = 2;
      list[3] = 3;
      list.length = 4;
      childCount = list.length;
      firstChild = list[0];

      result = array.toArray(list);

      assert.equal(toString.call(result), '[object Array]',
        'The liveList should be returned as an Array.');
      assert.ok(result.length,
        'The Array should have a length property.');
      assert.equal(result[0], list[0],
        'The order of the list should not have changed.');
      assert.equal(result[1], list[1],
        'The order of the list should not have changed.');
      assert.equal(result[2], list[2],
        'The order of the list should not have changed.');
      assert.equal(result[3], list[3],
        'The order of the list should not have changed.');
      assert.equal(result.length, list.length,
        'The number of Nodes should not change.');
    });

    it('toArray - array-like, no indicies', function () {
      var list = { length: 5 },
          arr = array.toArray(list);

      assert.deepEqual(arr, new Array(5),
        'Should properly set length if there are no matches indicies');
    });

    it('toArray - Invalid objects', function () {
      var list = { length: 'cats' };

      assert.deepEqual(array.toArray(list), [],
        'Should return an empty array');

      list['0'] = 'meow';
      assert.deepEqual(array.toArray(list), [],
        'Should return an empty array');

      list = {
        length: 'cats'
      };

      list[0] = 'meow';
      assert.deepEqual(array.toArray(list), [],
        'Should return an empty array');
    });
  });

  describe('.filter', function () {
    it('stupid stuff', function () {
      array.filter([1], function (item, index, array) {
        assert.ok(index === undefined);
        assert.ok(array === undefined);
      });
    });

    it('even numbers', function () {
      var original = [1, 2, 3, 4, 5, 6],
          even = array.filter(original, function (item) {
            return (item % 2) === 0;
          });

      assert.deepEqual(even, [2, 4, 6]);
    });

    it('isBigEnough (from mdn)', function () {
      function isBigEnough(element) {
        return (element >= 10);
      }

      var filtered = array.filter([12, 5, 8, 130, 44], isBigEnough);
      assert.deepEqual(filtered, [12, 130, 44]);
    });
  });

  describe('.clone', function () {
    it('clone an array of integers', function () {
      var original = [1, 2, 4, 5, 6], copied;
      copied = array.clone(original);
      assert.deepEqual(original, copied);
    });

    it('clone an array of mixed values', function () {
      var original = [1, {'2': 2}, { abc : 'abc', a: function () {}}, 5, 6], copied;
      copied = array.clone(original);
      assert.deepEqual(original, copied);
    });

  });

});
