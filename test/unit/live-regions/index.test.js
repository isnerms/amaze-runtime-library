//
//  TODO
//   - support for `require('live-regions')(TTL)`
//

describe('live-regions', function () {
  'use strict';

  var liveregions = require('live-regions');
  var selector = require('selector');
  var each = require('collections').each;

  it('should have a "polite" method', function () {
    assert.isFunction(liveregions.polite);
  });

  it('should have a "assertive" method', function () {
    assert.isFunction(liveregions.assertive);
  });

  it('should insert the polite region into the DOM', function () {
    setTimeout(function () {
      assert.ok(selector.query('#amaze_liveregions_polite'));
    }, 0);
  });

  it('should insert the assertive region into the DOM', function () {
    setTimeout(function () {
      assert.ok(selector.query('#amaze_liveregions_assertive'));
    }, 0);
  });

  describe('liveregions.polite', function () {
    it('should add a paragraph into #amaze_liveregions_polite', function (done) {
      liveregions.polite('polite test');

      var paragraphs = selector.queryAll('#amaze_liveregions_polite p');

      assert.isNumber(paragraphs.length);

      each(paragraphs, function (p) {
        if (p.innerHTML === 'polite test') {
          done();
        }
      });
    });
  });

  describe('liveregions.assertive', function () {
    it('should add a paragraph into #amaze_liveregions_assertive', function (done) {
      liveregions.assertive('assertive test');

      var paragraphs = selector.queryAll('#amaze_liveregions_assertive p');

      assert.isNumber(paragraphs.length);

      each(paragraphs, function (p) {
        if (p.innerHTML === 'assertive test') {
          done();
        }
      });
    });
  });
});
