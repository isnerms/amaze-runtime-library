
describe('dom.children', function () {
  'use strict';

  var dom = require('dom');
  var children = dom.children;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.id = 'fixture';
    element.innerHTML =
        '<span class="first yo"></span>'
      + '<button class="first yo"></button>'
      + '<i class="first"></i>'
      + '<div class="first yo">'
      + '  <span class="second"></span>'
      + '  <button class="second"></button>'
      + '  <i class="second"></i>'
      + '  <div class="second">'
      + '    <span class="third"></span>'
      + '    <button class="third"></button>'
      + '    <i class="third"></i>'
      + '    <div class="third"></div>'
      + '  </div>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should return direct descendants of an element', function () {
    var descendants = children(element);
    assert.equal(4, descendants.length);
    for (var i = descendants.length - 1; i >= 0; i--) {
      assert.ok(dom.hasClass(descendants[i], 'first'));
    }
  });

  it('should handle an element selector', function () {
    var descendants = children('#fixture');
    assert.equal(4, descendants.length);
    for (var i = descendants.length - 1; i >= 0; i--) {
      assert.ok(dom.hasClass(descendants[i], 'first'));
    }
  });

  it('should filter descendants by a selector', function () {
    var descendants = children('#fixture', '.yo');
    assert.equal(3, descendants.length);
    for (var i = descendants.length - 1; i >= 0; i--) {
      assert.ok(dom.hasClass(descendants[i], 'first'));
      assert.ok(dom.hasClass(descendants[i], 'yo'));
    }
  });
});
