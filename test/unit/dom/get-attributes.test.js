
describe('dom.getAttributes', function () {
  'use strict';

  var dom = require('dom');
  var div = null;

  beforeEach(function () {
    div = document.createElement('div');
  });

  it('should support the "class" attribute', function () {
    div.className = 'hello goodbye';
    var attrs = dom.getAttributes(div);
    assert.equal('hello goodbye', attrs['class']);
  });

  it('should support the "id" attribute', function () {
    div.id = 'hello goodbye';
    var attrs = dom.getAttributes(div);
    assert.equal('hello goodbye', attrs.id);
  });

  it('should support the "onclick" attribute', function () {
    div.setAttribute('onclick', 'hi');
    var attrs = dom.getAttributes(div);
    assert.equal('hi', attrs.onclick);
  });

  it('should support the "tabindex" attribute', function () {
    div.setAttribute('tabindex', 500);
    var attrs = dom.getAttributes(div);
    assert.equal(500, attrs.tabindex);
  });

  it('should support the "disabled" attribute', function () {
    div.setAttribute('disabled', 'whatever');
    var attrs = dom.getAttributes(div);
    assert.ok(attrs.disabled);
  });
});
