
describe('dom.getScrollOffset', function () {
  'use strict';

  var dom = require('dom');

  beforeEach(function () {
    var fixture = document.createElement('div');
    fixture.id = 'fixture';
    document.body.appendChild(fixture);
  });

  afterEach(function () {
    var fixture = document.getElementById('fixture');
    document.body.removeChild(fixture);
  });

  it('should return scrollTop and scrollLeft for normal nodes', function () {
    var offset = dom.getScrollOffset({
      nodeType: 3,
      scrollTop: 42,
      scrollLeft: 98
    });
    assert.equal(offset.x, 98);
    assert.equal(offset.y, 42);
  });

  it('should get the scroll from the documentElement if a document is passed in', function () {
    var offset = dom.getScrollOffset({
      nodeType: 9,
      documentElement: {
        scrollTop: 42,
        scrollLeft: 98
      }
    });
    assert.equal(offset.x, 98);
    assert.equal(offset.y, 42);
  });

  it('should get the scroll from the document.body if a document is passed in and it has no documentElement', function () {
    var offset = dom.getScrollOffset({
      nodeType: 9,
      body: {
        scrollTop: 42,
        scrollLeft: 98
      }
    });
    assert.equal(offset.x, 98);
    assert.equal(offset.y, 42);
  });

  // @todo not sure how useful this test is as all it does it duplicate the logic
  // in dom.getScrollOffset
  it('should use an implied document if no element is passed', function () {
    var offset = dom.getScrollOffset();
    var left = (document.documentElement && document.documentElement.scrollLeft)
            || (document.body && document.body.scrollLeft) || 0;
    var top = (document.documentElement && document.documentElement.scrollTop)
            || (document.body && document.body.scrollTop) || 0;

    assert.equal(offset.x, left);
    assert.equal(offset.y, top);
  });

  it('should work with passed in elements', function () {
    var fixture = document.getElementById('fixture');
    fixture.innerHTML = '<div id="target" style="overflow: auto; width: 10px; height: 10px;">' +
    '<div style="width: 400px; height: 400px;"></div></div>';

    var target = document.getElementById('target');

    target.scrollLeft = 98;
    target.scrollTop = 42;

    var offset = dom.getScrollOffset(target);

    assert.equal(offset.x, 98);
    assert.equal(offset.y, 42);
  });

  it('should work with no parameters', function (done) {
    var fixture = document.getElementById('fixture');
    var original = dom.getScrollOffset();

    fixture.innerHTML = '<div id="target" style="height: 10000px; width: 10000px;"></div>';
    //give ie some time to process the height/width styles
    setTimeout(function () {
      window.scrollTo(98, 42);
      window.setTimeout(function () {
        var offset = dom.getScrollOffset();
        assert.equal(offset.y, 42);
        assert.equal(offset.x, 98);
        fixture.innerHTML = '';
        window.scrollTo(original.x, original.y);
        done();
      }, 100);
    }, 10);
  });
});
