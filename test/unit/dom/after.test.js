
describe('dom.after', function () {
  'use strict';

  var query = require('selector').query;
  var after = require('dom').after;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should handle an orphan target', function () {
    element.innerHTML =
        '<div id="foo">'
      + '  hi'
      + '</div>';

    after('#foo', '<p>yo</p>');
    var p = query('p', element);
    assert.equal('yo', p.innerHTML);
    assert.strictEqual(p, element.lastChild);
  });

  it('should throw an error when given a detached target', function () {
    var target = document.createElement('div');
    var err = null;
    var msg = 'Target element must be attached to the document';

    try {
      after(target, '<p>hi</p>');
    } catch (e) {
      err = e;
    }

    assert.equal(msg, err.message);
  });

  it('should insert an element after the target', function () {
    element.innerHTML =
        '<div id="a"></div>'
      + '<div id="b"></div>'
      + '<div id="c"></div>';

    var b = document.getElementById('b');
    var p = document.createElement('p');
    after(b, p);
    assert.strictEqual(p, b.nextSibling);
  });

  it('should insert a block of HTML after the target', function () {
    element.innerHTML =
        '<div id="a"></div>'
      + '<div id="b"></div>'
      + '<div id="c"></div>';

    var b = document.getElementById('b');
    after(b, '<p>yo</p>');

    var p = query('p', element);
    assert.equal('yo', p.innerHTML);
    assert.strictEqual(p, b.nextSibling);
  });

  it('should move nodes', function () {
    element.innerHTML =
        '<div id="a"></div>'
      + '<div id="b"></div>'
      + '<div id="c"></div>';

    var a = document.getElementById('a');
    var b = document.getElementById('b');
    after(b, a);
    assert.strictEqual(a, b.nextSibling);
  });
});
