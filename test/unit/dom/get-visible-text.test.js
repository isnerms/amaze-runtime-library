
describe('dom.getVisibleText', function () {
  'use strict';

  var dom = require('dom');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should ignore `display:none` text', function () {

    // for some reason this test fails on phantomjs and I cannot work
    // out why, so skip for now
    if (window.mochaPhantomJS) {
        return;
    }

    fixture.innerHTML =
        '<span style="display:none">hello</span>'
      + '<span>world</span>';
    var text = dom.getVisibleText(fixture);
    assert.equal(text, 'world');
  });


  it('should get the text of button', function () {
    fixture.innerHTML =
        '<div id="div">'
      + '<button>Hello</button>'
      + '</div>';
    var text = dom.getVisibleText(fixture);
    assert.equal(text, 'Hello');
  });

  it('should get the text of input type="button"', function () {
    fixture.innerHTML =
        '<div id="div">'
      + '<input type="button">Hello</input>'
      + '</div>';
    var text = dom.getVisibleText(fixture);
    assert.equal(text, 'Hello');
  });

  it('should get the text of input type="radio"', function () {
    fixture.innerHTML =
        '<div id="div">'
      + '<input type="radio" value="Hello" />'
      + '</div>';
    var text = dom.getVisibleText(fixture);
    assert.equal(text, 'Hello');
  });

  it('should get the text of <script> should be empty', function () {
    fixture.innerHTML =
        '<div id="div">'
      + '<script>function(){}();</script>'
      + '</div>';
    var text = dom.getVisibleText(fixture);
    assert.equal(text, '', 'script tags should be ignored');
  });

  it('should get the text of <style> should be empty', function () {
    fixture.innerHTML =
        '<div id="div">'
      + '<style>.hello { color : #fff; }</style>'
      + '</div>';
    var text = dom.getVisibleText(fixture);
    assert.equal(text, '');
  });

  it('should get the text of <legend>', function () {
    fixture.innerHTML =
        '<div id="div">'
      + '<legend>Hello</legend>'
      + '</div>';
    var text = dom.getVisibleText(fixture);
    assert.equal(text, 'Hello');
  });

  it('should get the text of compound structure', function () {
    fixture.innerHTML =
        '<div id="div">'
      + '<form>'
      + '<input type="button">Hello</input>'
      + '<button>Hello<span>Hello</span></button>'
      + '<input type="radio" value="Hello" />'
      + '</form>'
      + '</div>';
    var text = dom.getVisibleText(fixture);
    assert.equal(text, 'Hello Hello Hello Hello');
  });

  it('should get the text of compound structure with spurious white space', function () {
    fixture.innerHTML =
        '<div id="div">'
      + '<form>\n\r\t  \u00A0\t\t\t\t'
      + '<input type="button">Hello</input>'
      + '<button>Hello<span>Hello</span></button>'
      + '<input type="radio" value="Hello" />'
      + '</form>'
      + '</div>';
    var text = dom.getVisibleText(fixture);
    assert.equal(text, 'Hello Hello Hello Hello');
  });
});
