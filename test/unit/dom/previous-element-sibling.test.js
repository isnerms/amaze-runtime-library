
describe('dom.previousElementSibling', function () {
  'use strict';

  var dom = require('dom');
  var selector = require('selector');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    fixture.innerHTML = '<ul id="list">' +
              '<li id="first">Thing (#first)</li>' +
              '<li id="two">Thing</li>' +
              '<!-- Im a comment node -->' +
              '<!-- Im a comment node -->' +
              '<li id="three">Thing</li>' +
              '<li id="four">Thing</li>' +
              '<li id="mid">Thing (#mid)<a href="#">child</a></li>' +
              '<!-- Im a comment node -->' +
              '<li id="six">Thing</li>' +
              ' hi, I am just a simple text node' +
              '<li id="seven">Thing</li>' +
              '</ul>' +
              '<div>Im a div</div>';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should grab the element\'s previous sibing', function () {
    var two = document.getElementById('two');
    var one = document.getElementById('first');
    assert.strictEqual(dom.previousElementSibling(two), one);
  });

  it('should return null if there is no previous sibling', function () {
    var first = document.getElementById('first');
    assert.strictEqual(null, dom.previousElementSibling(first));
  });

  it('should not grab a child node', function () {
    var six = document.getElementById('six');
    var child = selector.query('#mid a');
    var mid = document.getElementById('mid');
    var prev = dom.previousElementSibling(six);
    assert.notStrictEqual(prev, child);
    assert.strictEqual(prev, mid);
  });

  it('should not grab comment or text nodes', function () {
    // there is a text node in between seven and six...
    var seven = document.getElementById('seven');
    var six = document.getElementById('six');
    var mid = document.getElementById('mid');
    assert.strictEqual(dom.previousElementSibling(seven), six); // text node check
    assert.strictEqual(dom.previousElementSibling(six), mid); // comment node check
  });
});


