
describe('dom.before', function () {
  'use strict';

  var query = require('selector').query;
  var before = require('dom').before;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should handle an orphan target', function () {
    element.innerHTML =
        '<div id="foo">'
      + '  hi'
      + '</div>';

    before('#foo', '<p>yo</p>');
    var p = query('p', element);
    assert.equal('yo', p.innerHTML);
    assert.strictEqual(p, element.firstChild);
  });

  it('should throw an error when given a detached target', function () {
    var target = document.createElement('div');
    var err = null;
    var msg = 'Target element must be attached to the document';

    try {
      before(target, '<p>hi</p>');
    } catch (e) {
      err = e;
    }

    assert.equal(msg, err.message);
  });

  it('should insert an element before the target', function () {
    element.innerHTML =
        '<div id="a"></div>'
      + '<div id="b"></div>'
      + '<div id="c"></div>';

    var b = document.getElementById('b');
    var p = document.createElement('p');
    before(b, p);
    assert.strictEqual(p, b.previousSibling);
  });

  it('should insert a block of HTML before the target', function () {
    element.innerHTML =
        '<div id="a"></div>'
      + '<div id="b"></div>'
      + '<div id="c"></div>';

    var b = document.getElementById('b');
    before(b, '<p>yo</p>');

    var p = query('p', element);
    assert.equal('yo', p.innerHTML);
    assert.strictEqual(p, b.previousSibling);
  });

  it('should move nodes', function () {
    element.innerHTML =
        '<div id="a"></div>'
      + '<div id="b"></div>'
      + '<div id="c"></div>';

    var c = document.getElementById('c');
    var b = document.getElementById('b');
    before(b, c);
    assert.strictEqual(c, b.previousSibling);
  });
});
