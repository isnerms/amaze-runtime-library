
describe('dom.isNode', function () {
  'use strict';

  var dom = require('dom');

  it('should return true when given an actual Node', function () {
    assert.equal(true, dom.isNode(document));
    assert.equal(true, dom.isNode(document.body));
    assert.equal(true, dom.isNode(document.body.firstChild));
  });

  it('should return false when given a non-node', function () {
    assert.equal(false, dom.isNode({}));
    assert.equal(false, dom.isNode(null));
    assert.equal(false, dom.isNode(window));
  });
});
