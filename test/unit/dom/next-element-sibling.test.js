
describe('dom.nextElementSibling', function () {
  'use strict';

  var dom = require('dom');
  var selector = require('selector');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    fixture.innerHTML = '<ul id="list">' +
              '<li id="first">Thing (#first)</li>' +
              '<li id="two">Thing</li>' +
              '<!-- Im a comment node -->' +
              '<!-- Im a comment node -->' +
              '<li id="three">Thing</li>' +
              '<li id="four">Thing</li>' +
              '<li id="mid">Thing (#mid)<a href="#">child</a></li>' +
              '<!-- Im a comment node -->' +
              '<li id="six">Thing</li>' +
              ' hi, I am just a simple text node' +
              '<li id="seven">Thing</li>' +
              '</ul>' +
              '<div>Im a div</div>';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should grab the element\'s next sibling', function () {
    var first = document.getElementById('first');
    var second = document.getElementById('two');
    assert.strictEqual(dom.nextElementSibling(first), second);
  });

  it('should not grab comment nodes', function () {
    var two = document.getElementById('two');
    var three = document.getElementById('three');
    assert.strictEqual(dom.nextElementSibling(two), three);
  });

  it('should not grab text nodes', function () {
    var six = document.getElementById('six');
    var seven = document.getElementById('seven');
    assert.strictEqual(dom.nextElementSibling(six), seven);
  });

  it('should return null if there is not a next sibling', function () {
    var seven = document.getElementById('seven');
    assert.strictEqual(null, dom.nextElementSibling(seven));
  });

  it('should not grab a child element', function () {
    var mid = document.getElementById('mid');
    var midsChild = selector.query('#mid a');
    assert.notStrictEqual(dom.nextElementSibling(mid), midsChild);
  });
});


