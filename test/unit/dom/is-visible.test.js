
describe('dom.isVisible', function () {
  'use strict';

  var dom = require('dom');
  var selector = require('selector');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    fixture.innerHTML =
        '<div id="div" style="display:block;position:absolute;top:0px;left:0px;">'
      + ' <span id="visibleToEveryone0">Hello Everyone</span>'
      + ' <span id="invisibleToEveryone0" style="display:none;">Hello Nobody</span>'
      + ' <span style="display:none;"><span id="invisibleToEveryone1">Hello Nobody</span></span>'
      + ' <span id="invisibleToEveryone2" style="visibility:hidden;">Hello Nobody</span>'
      + ' <span style="visibility:hidden;"><span id="invisibleToEveryone3">Hello Nobody</span></span>'
      + ' <span id="invisibleToScreenReaders0" aria-hidden="true">Hello Sighted Users</span>'
      + ' <span aria-hidden="true"><span id="invisibleToScreenReaders1">Hello Sighted Users</span></span>'
      + ' <span id="visibleToScreenReaders0" style="position:absolute;top:-999em;left:-999em;">'
      + '  Hello Screen Readers</span>'
      + ' <span style="position:absolute;top:-999em;left:-999em;">'
      + ' <span  id="visibleToScreenReaders1">Hello Screen Readers</span></span>'
      + ' <span style="text-indent:-999em;">'
      + ' <span  id="visibleToScreenReaders2">Hello Screen Readers</span></span>'
      + ' <span id="visibleToScreenReaders3" style="text-indent:-999em;">'
      + ' Hello Screen Readers</span>'
      + '</div>';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('isVisible', function () {
    var el;

    el = selector.query('#visibleToEveryone0');
    assert.equal(true, dom.isVisible(el), 'This element should be visible to everyone');

    el = selector.query('#invisibleToEveryone0');
    assert.equal(false, dom.isVisible(el), 'This element should be invisible to everyone');
    el = selector.query('#invisibleToEveryone1');
    assert.equal(false, dom.isVisible(el), 'This element should be invisible to everyone');
    el = selector.query('#invisibleToEveryone2');
    assert.equal(false, dom.isVisible(el), 'This element should be invisible to everyone');
    el = selector.query('#invisibleToEveryone3');
    assert.equal(false, dom.isVisible(el), 'This element should be invisible to everyone');

    el = selector.query('#invisibleToScreenReaders0');
    assert.equal(false, dom.isVisible(el, true), 'This element should be invisible to screen readers');
    el = selector.query('#invisibleToScreenReaders1');
    assert.equal(false, dom.isVisible(el, true), 'This element should be invisible to screen readers');

    el = selector.query('#visibleToScreenReaders0');
    assert.equal(true, dom.isVisible(el, true), 'This element should be visible to screen readers');
    el = selector.query('#visibleToScreenReaders1');
    assert.equal(true, dom.isVisible(el, true), 'This element should be visible to screen readers');
    el = selector.query('#visibleToScreenReaders2');
    assert.equal(true, dom.isVisible(el, true), 'This element should be visible to screen readers');
    el = selector.query('#visibleToScreenReaders3');
    assert.equal(true, dom.isVisible(el, true), 'This element should be visible to screen readers');
  });
});
