
describe('dom.getText', function () {
  'use strict';

  var dom = require('dom');
  var fixture = null;
  var COMMENT = 'comment';
  var TEXT = 'text';
  var HELLO_WORLD = /^hello[\s]*world$/;

  before(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    fixture.innerHTML = [
      '<p id="p">', TEXT, '</p>',
      '<div id="div">',
      '<!--', COMMENT, '-->',
      '</div>',
      '<span id="span">',
      '<p>hello</p>',
      '<span>world</span>',
      '</span>'
    ].join('');
  });

  after(function () {
    document.body.removeChild(fixture);
  });

  it('should handle comment nodes', function () {
    var comment = document.getElementById('div').firstChild;
    assert.equal(dom.getText(comment), COMMENT);
  });

  it('should handle element nodes', function () {
    var p = document.getElementById('p');
    assert.equal(dom.getText(p), TEXT);
  });

  it('should handle text nodes', function () {
    var text = document.getElementById('p').firstChild;
    assert.equal(dom.getText(text), TEXT);
  });

  it('should return all text within an element', function () {
    var span = document.getElementById('span');
    // AM-47
    // IE places whitespace between nodes in innerText
    // as there is not workaround for this, we're just updating the test
    assert.equal(true, HELLO_WORLD.test(dom.getText(span)));
  });
});
