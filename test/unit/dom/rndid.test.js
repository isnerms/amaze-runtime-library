
describe('dom.rndid', function () {
  'use strict';

  var dom = require('dom');
  var divs = [];

  after(function () {
    for (var i = 0; i < divs.length; i++) {
      document.body.removeChild(divs[i]);
    }
  });

  it('should generate a unique ID string', function () {
    for (var i = 0; i < 1000; i++) {
      var id = dom.rndid();
      assert.ok(id);
      assert.equal(null, document.getElementById(id));
      // add an element with the ID to ensure we don't get dupes
      div(id);
    }
  });

  describe('given a specific length', function () {
    it('should generate a unique ID of "n" characters', function () {
      for (var i = 0; i < 1000; i++) {
        var id = dom.rndid(16);
        assert.ok(id);
        assert.equal(16, id.length);
        assert.equal(null, document.getElementById(id));
        // add an element with the ID to ensure we don't get dupes
        div(id);
      }
    });
  });

  describe('given a prefix', function () {
    it('should generate a prefixed unique ID', function () {
      for (var i = 0; i < 1000; i++) {
        var id = dom.rndid('food');
        assert.ok(id);
        // verify prefix
        assert.equal('food', id.substr(0, 4));
        assert.equal(null, document.getElementById(id));
        // add an element with the ID to ensure we don't get dupes
        div(id);
      }
    });
  });

  describe('given a length and a prefix', function () {
    it('should generate a unique prefixed ID of n characters', function () {
      for (var i = 0; i < 1000; i++) {
        var id = dom.rndid('food', 16);
        assert.ok(id);
        // verify prefix
        assert.equal('food', id.substr(0, 4));
        // prefix + length
        assert.equal(20, id.length);
        assert.equal(null, document.getElementById(id));
        // add an element with the ID to ensure we don't get dupes
        div(id);
      }
    });
  });

  function div(id) {
    var el = document.createElement('div');
    el.id = id;
    document.body.appendChild(el);
    divs.push(el);
  }
});
