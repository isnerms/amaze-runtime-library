
describe('dom.moveChildNodes', function () {
  'use strict';

  var dom = require('dom');
  var selector = require('selector');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    fixture.innerHTML = [

      '<div id="source">',
      '  <div class="one">',
      '    <div class="two">',
      '      <div class="three">',
      '        <div class="four">',
      '          <p class="five">hello</p>',
      '          <p class="five">hello</p>',
      '          <p class="five">hello</p>',
      '          <p class="five">hello</p>',
      '          <p class="five">hello</p>',
      '        </div>',
      '      </div>',
      '    </div>',
      '  </div>',
      '</div>',
      '<div id="dest">',
      '</div>'
    ].join('');
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should move child nodes', function () {
    dom.moveChildNodes('#source', '#dest');
    assert.equal(5, selector('#dest .five').length);
    assert.equal(0, selector('#source .five').length);
  });
});
