
describe('dom.isFocusable', function () {
  'use strict';

  var dom = require('dom');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);

    fixture.innerHTML =
      // focusable things
        '<div id=focusable>'
      + '  <a href=#>a</a>'
      + '  <input type=text>'
      + '  <input type=radio>'
      + '  <input type=checkbox>'
      + '  <input type=button>'
      + '  <select></select>'
      + '  <span tabindex=0>span</span>'
      + '  <textarea></textarea>'
      + '  <button>button</button>'
      + '  <a tabindex=0>a</a>'
      + '</div>'
      + '<div id=not_focusable>'
      + '  <div></div>'
      + '  <span></span>'
      + '  <input type="hidden" />'
      + '  <a>test</a>'
      + '  <span tabindex="cats">hello</span>'
      + '  <input type="text" disabled="disabled" />'
      + '  <area></area>'
      + '  <input type="text" disabled>'
      + '  <input type="hidden" tabindex="0" />'
      + '  <a href="#" style="display:none;">link</a>'
      + '</div>';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should return true when given focusable elements', function () {
    var elements = document.querySelectorAll('#focusable > *');
    for (var i = 0; i < elements.length; i++) {
      assert.equal(true, dom.isFocusable(elements[i]));
    }
  });

  it('should return false when given non-focusable elements', function () {
    var elements = document.querySelectorAll('#not_focusable > *');
    for (var i = 0; i < elements.length; i++) {
      assert.equal(false, dom.isFocusable(elements[i]));
    }
  });

});
