
describe('dom', function () {
  'use strict';

  var dom = require('dom');

  // dynamically build sanity tests for each method

  var methods = [
    'addClass',
    'classList',
    'empty',
    'findUp',
    'getAttributes',
    'getScrollOffset',
    'getText',
    'getVisibleText',
    'hasClass',
    'isFocusable',
    'isNode',
    'isVisible',
    'moveChildNodes',
    'nextElementSibling',
    'previousElementSibling',
    'removeClass',
    'replaceTag',
    'setAttribute',
    'setAttributes',
    'siblings',
    'toggleClass',
    'unwrap',
    'wrap'
  ];

  for (var i = 0; methods[i]; i++) {
    it(should(methods[i]), test(methods[i]));
  }

  function should(name) {
    var spec = [ 'should have' ];
    spec.push(/^[aeiou]/.test(name) ? 'an' : 'a');
    spec.push('"' + name + '"');
    spec.push('method');
    return spec.join(' ');
  }

  function test(name) {
    return function () {
      assert.equal('function', typeof dom[name]);
    };
  }
});
