
describe('dom.classList', function () {
  'use strict';

  var classList = require('dom').classList;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.className = 'foo bar baz';
  });

  it('add', function () {
    var list;

    list = classList(element);
    list.add('hat');

    assert.equal(element.className, 'foo bar baz hat',
      'should add the class name');

    list.add('bat');
    list.add('bat');
    list.add('bat');
    list.add('bat');
    list.add('bat');
    list.add('bat');

    assert.equal(element.className, 'foo bar baz hat bat',
      'should not add the same class name twice');
  });

  it('remove from end', function () {
    var list = classList(element);
    list.remove('baz');
    assert.equal(element.className, 'foo bar',
      'should remove a class from the end');
  });

  it('remove from beginning', function () {
    var list = classList(element);
    list.remove('foo');
    assert.equal(element.className, 'bar baz',
      'should remove a class from the beginning');
  });

  it('remove from middle', function () {
    var list = classList(element);
    list.remove('bar');
    assert.equal(element.className, 'foo baz',
      'should remove a class from the middle');
  });

  it('remove', function () {
    var list = classList(element);
    list.remove('bar');
    list.remove('foo');
    list.remove('baz');
    assert.equal(element.className, '',
      'should remove all');
    list.remove('wac');
    assert.equal(element.className, '',
      'should not throw after removing all classes');
  });

  it('replace when present', function () {
    classList(element).replace('foo', 'hat');
    assert.deepEqual(element.className,
      'bar baz hat');
  });

  it('replace when missing', function () {
    classList(element).replace('bat', 'hat');
    assert.deepEqual(element.className,
      'foo bar baz hat');
  });

  it('toggle when present', function () {
    var list = classList(element);
    list.toggle('bar');
    assert.equal(element.className, 'foo baz',
      'should remove the class');
  });

  it('toggle when missing', function () {
    var list = classList(element);
    list.toggle('bah');
    assert.equal(element.className, 'foo bar baz bah',
      'should add the class');
  });

  it('has/contains', function () {
    assert.strictEqual(classList(element).has('baz'), true);
    assert.strictEqual(classList(element).has('bar'), true);
    assert.strictEqual(classList(element).has('foo'), true);
    assert.strictEqual(classList(element).has('hat'), false);

    assert.strictEqual(classList(element).contains('baz'), true);
    assert.strictEqual(classList(element).contains('bar'), true);
    assert.strictEqual(classList(element).contains('foo'), true);
    assert.strictEqual(classList(element).contains('hat'), false);
  });

  it('array', function () {
    var array = classList(element).toArray();

    assert.equal(array.length, 3);
    assert.equal(array[0], 'foo');
    assert.equal(array[1], 'bar');
    assert.equal(array[2], 'baz');
  });

  it('toString', function () {
    assert.strictEqual(classList(element).toString(),
      'foo bar baz');
  });

  it('chaining', function () {
    var result = classList(element)
      .add('bat')
      .remove('bat')
      .toggle('bar')
      .replace('foo', 'hat')
      .toArray();

    assert.deepEqual(result, ['baz', 'hat']);
  });

});
