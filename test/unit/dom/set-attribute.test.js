
describe('dom.setAttribute', function () {
  'use strict';

  var dom = require('dom');
  var div = null;

  beforeEach(function () {
    div = document.createElement('div');
  });

  it('should set legal attributes', function () {
    dom.setAttribute(div, 'id', 'whatever');
    assert.equal('whatever', div.id);
    dom.setAttribute(div, 'class', 'whatever');
    assert.equal('whatever', div.className);
  });

  it('should set the "for" attribute', function () {
    dom.setAttribute(div, 'for', 'whatever');
    assert.equal('whatever', div.htmlFor);
  });

  it('should set the "style" attribute', function () {
    dom.setAttribute(div, 'style', 'color:black');
    assert.equal('black', div.style.color);
  });
});
