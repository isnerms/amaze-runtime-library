
describe('dom.replaceTag', function () {
  'use strict';

  var dom = require('dom');
  var div = null;

  beforeEach(function () {
    div = document.createElement('div');
    div.style.display = 'none';
    document.body.appendChild(div);
  });

  it('should maintain attributes', function () {
    div.setAttribute('foo', 'bar');
    div.className = 'waaaaaaa';

    var span = dom.replaceTag(div, 'span');

    assert.equal('waaaaaaa', span.className);
    assert.equal('bar', span.getAttribute('foo'));
  });

  it('should add the given augment attributes', function () {
    var attrs = {
      'class': 'foo',
      'for': 'blah',
      foo: 'bar'
    };

    var label = dom.replaceTag(div, 'label', attrs);
    assert.equal('foo', label.className);
    assert.equal('blah', label.htmlFor);
    assert.equal('bar', label.getAttribute('foo'));
  });

  it('should keep text nodes', function () {
    div.innerHTML = 'hello world';
    var node = div.firstChild;

    var span = dom.replaceTag(div, 'span');
    assert.equal(node, span.firstChild);
  });

  it('should keep comment nodes', function () {
    div.innerHTML = '<!--hello world-->';
    var node = div.firstChild;

    var span = dom.replaceTag(div, 'span');
    assert.equal(node, span.firstChild);
  });

  it('should keep child nodes', function () {
    div.innerHTML = '<div><span><button>hi</button></span></div>';
    var node = div.firstChild;
    var span = dom.replaceTag(div, 'span');
    assert.equal(node, span.firstChild);
  });
});
