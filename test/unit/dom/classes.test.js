
describe('dom classes', function () {
  'use strict';

  var dom = require('dom');
  var div = null;

  beforeEach(function () {
    div = document.createElement('div');
    document.body.appendChild(div);
  });

  afterEach(function () {
    document.body.removeChild(div);
  });

  describe('dom.addClass', function () {
    it('should add a class to an element', function () {
      div.className = 'foo bar';
      dom.addClass(div, 'baz');
      assert.equal('foo bar baz', div.className);
    });

    it('should not add duplicates', function () {
      div.className = 'foo bar baz';
      dom.addClass(div, 'baz');
      assert.equal('foo bar baz', div.className);
    });
  });

  describe('dom.removeClass', function () {
    it('should remove a class from an element', function () {
      div.className = 'foo';
      dom.removeClass(div, 'foo');
      assert.equal('', div.className);
    });
  });

  describe('dom.toggleClass', function () {
    it('should toggle an element\'s class', function () {
      dom.toggleClass(div, 'foo');
      assert.equal('foo', div.className);
      dom.toggleClass(div, 'foo');
      assert.equal('', div.className);
    });
  });

  describe('dom.hasClass', function () {
    it('should check if an element has a class', function () {
      div.className = 'foo bar';
      assert.equal(true, dom.hasClass(div, 'foo'));
      assert.equal(false, dom.hasClass(div, 'baz'));
    });
  });
});
