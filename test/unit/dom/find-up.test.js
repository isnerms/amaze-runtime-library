
describe('dom.findUp', function () {
  'use strict';

  var dom = require('dom');
  var div = null;

  beforeEach(function () {
    div = document.createElement('div');
    document.body.appendChild(div);
  });

  afterEach(function () {
    document.body.removeChild(div);
  });

  it('should return NULL when no match is found', function () {
    assert.strictEqual(null, dom.findUp(div, 'boondoogle'));
  });

  it('should return the found match', function () {
    assert.equal(document.body, dom.findUp(div, 'body'));
    div.innerHTML =
        '<div>'
      + ' <div>'
      + '  <div>'
      + '   <div>'
      + '    <div>'
      + '     <div id=hi></div>'
      + '    </div>'
      + '   </div>'
      + '  </div>'
      + ' </div>'
      + '</div>';

    assert.equal(div, dom.findUp('#hi', 'body > div'));
  });
});
