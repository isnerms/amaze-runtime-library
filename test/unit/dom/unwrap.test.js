
describe('dom.unwrap', function () {
  'use strict';

  var dom = require('dom');
  var selector = require('selector');
  var array = require('array');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    fixture.innerHTML = '<div id="thing">' +
                'asdfasdfasdfasdf' +
                '<div id="cat">CAT</div>' +
                '<div id="dog">DOG<div class="sub-child">dogephant</div></div>' +
                '<!-- comment node -->' +
              '</div>';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should find the parent element and remove it', function () {
    var cat = document.getElementById('cat');
    dom.unwrap(cat);
    assert.ok(!document.getElementById('thing'));
  });

  it('should keep all children in the same order', function () {
    var fixChildren;
    var thing = document.getElementById('thing');
    var cat = document.getElementById('cat');
    var children = array.toArray(thing.childNodes);

    dom.unwrap(cat);

    fixChildren = array.toArray(fixture.childNodes);

    for (var i = fixChildren.length - 1; i >= 0; i--) {
      assert.equal(fixChildren[i], children[i]);
    }
  });

  it('should not remove any children (including text and comment nodes)', function () {
    var fixChildren;
    var cat = document.getElementById('cat');
    var thing = document.getElementById('thing');
    var thingChildren = array.toArray(thing.childNodes);

    dom.unwrap(cat);
    fixChildren = array.toArray(fixture.childNodes);
    assert.equal(thingChildren.length, fixChildren.length);
  });

  it('should move children into the place of their parent in the DOM', function () {
    fixture.innerHTML = '<div class="other-thing" id="one">adsf</div>' +
              '<div class="other-thing" id="two">adsf</div>' +
              '<div id="thing">' +
                '<div id="cat">CAT</div>' +
                'asdfasdfasdfasdf' +
                '<div id="dog">DOG<div class="sub-child">dogephant</div></div>' +
                '<!-- comment node -->' +
              '</div>' +
              '<div class="other-thing" id="three">adsf</div>';

    var fixChilds;
    var cat = document.getElementById('cat');

    dom.unwrap(cat);

    fixChilds = array.toArray(fixture.childNodes);

    assert.equal(fixChilds.length, 7); // after unwrap as taken place, there should be 7 child nodes
    assert.equal(fixChilds[0], document.getElementById('one'));
    assert.equal(fixChilds[1], document.getElementById('two'));
    assert.equal(fixChilds[2], cat);
    assert.equal(fixChilds[3].nodeType, 3); // text node
    assert.equal(fixChilds[4], document.getElementById('dog'));
    assert.equal(fixChilds[5].nodeType, 8); // comment node
    assert.equal(fixChilds[6], document.getElementById('three'));

    fixture.innerHTML = '';
  });

  it('should also move the children\'s children', function () {
    var subChild;
    var cat = document.getElementById('cat');

    dom.unwrap(cat);
    subChild = selector.query('#dog .sub-child');

    assert.ok(subChild);
    assert.equal(subChild.parentNode, document.getElementById('dog'));
  });
});
