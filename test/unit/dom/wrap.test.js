
describe('dom.wrap()', function () {
  'use strict';

  var dom = require('dom');
  var selector = require('selector');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    fixture.id = 'fixture';
    fixture.innerHTML =
        '<div id="cage">'
      + '  <p id="first">HELLO</p>'
      + '  <p id="two">HELLO</p>'
      + '  <p id="three">HELLO</p>'
      + '</div';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should return the wrapper element', function () {
    var paragraphs = selector.queryAll('#cage p');
    var wrapper = dom.wrap(paragraphs, 'div');

    wrapper.id = 'amaze-wrapper';

    assert.ok(document.getElementById('amaze-wrapper'));
  });

  it('should wrap all elements in an array within one tag (when array is passed in)', function () {
    var paragraphs = selector.queryAll('#cage p');
    var wrapper = dom.wrap(paragraphs, 'div');

    for (var i = paragraphs.length - 1; i >= 0; i--) {
      assert.strictEqual(paragraphs[i].parentNode, wrapper);
    }
  });

  it('should wrap a single element within one wrapping tag (when single element passed in)', function () {
    var p = selector.query('#cage p'); // just grabbing the first one
    var cage = document.getElementById('cage');
    var two = document.getElementById('two');
    var three = document.getElementById('three');
    var wrapper = dom.wrap(p);

    assert.strictEqual(p.parentNode, wrapper);
    assert.notStrictEqual(two.parentNode, wrapper);
    assert.notStrictEqual(three.parentNode, wrapper);
    assert.notStrictEqual(p.parentNode, cage);
    assert.strictEqual(two.parentNode, cage);
    assert.strictEqual(three.parentNode, cage);
  });

  it('should wrap only the element(s) passed in; NOT the parent(s)', function () {
    var paragraphs = selector.queryAll('#cage p');
    var cage = document.getElementById('cage');
    var wrapper = dom.wrap(paragraphs, 'span');
    // the #fixture should be the parent node of cage
    assert.strictEqual(cage.parentNode, fixture);
    // the wrapper (returned by wrap()) should not wrap #cage
    assert.notStrictEqual(cage.parentNode, wrapper);
  });

  it('should allow for any wrapper tagName to be passed in as second parameter', function () {
    var paragraphs = selector.queryAll('#cage p');
    var wrapper = dom.wrap(paragraphs, 'span');
    dom.addClass(wrapper, 'amaze-span');
    assert.strictEqual(wrapper.tagName, 'SPAN');
    assert.ok(selector.query('span.amaze-span'));
  });

  it('should default to a div as the wrapper tag when one is not declared', function () {
    var paragraphs = selector.queryAll('#cage p');
    var wrapper = dom.wrap(paragraphs); // not declaring 2nd param - tag
    assert.strictEqual(wrapper.tagName, 'DIV');
  });

  it('should return only one wrapper when wrap() is only called once', function () {
    var p = selector.query('#cage p');
    var wrapper = dom.wrap(p);
    dom.addClass(wrapper, 'amaze-rapper');
    assert.strictEqual(selector.queryAll('.amaze-rapper').length, 1);
  });

  it('should accept jQuery objects', function () {
    var jQuery = window.jQuery;
    if (!jQuery) {
      return;
    }
    var paragraphs = jQuery('#cage p');
    var wrapper = dom.wrap(paragraphs);

    for (var i = paragraphs.length - 1; i >= 0; i--) {
      assert.strictEqual(paragraphs[i].parentNode, wrapper);
    }
  });

  it('should accept NodeLists', function () {
    // live NodeList
    var paragraphs = fixture.getElementsByTagName('p');
    var wrapper = dom.wrap(paragraphs);

    for (var i = paragraphs.length - 1; i >= 0; i--) {
      assert.strictEqual(paragraphs[i].parentNode, wrapper);
    }
  });

  it('should keep all children in the same order', function () {
    var paragraphs = selector.queryAll('#cage p');

    dom.wrap(paragraphs);

    var checks = selector.queryAll('#fixture p');

    assert.strictEqual(paragraphs.length, checks.length);
    assert.strictEqual(paragraphs[0], checks[0]);
    assert.strictEqual(paragraphs[1], checks[1]);
    assert.strictEqual(paragraphs[2], checks[2]);
  });

  describe('given a text node', function () {
    it('should wrap the node in the given "tag"', function () {
      var two = document.getElementById('two');
      var text = two.firstChild;
      var fizzlebizzel = dom.wrap(text, 'fizzlebizzel');
      assert.equal(two, fizzlebizzel.parentNode);
      assert.equal('fizzlebizzel', text.parentNode.tagName.toLowerCase());
    });
  });
});
