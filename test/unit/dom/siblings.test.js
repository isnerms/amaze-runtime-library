
describe('dom.siblings', function () {
  'use strict';

  var dom = require('dom');
  var selector = require('selector');
  var array = require('array');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    document.body.appendChild(fixture);
    fixture.innerHTML =
        '<ul id="list">'
      + '  <li id="first">Thing (#first)</li>'
      + '  <li class="yo" id="two">Thing</li>'
      + '  <!-- Im a comment node -->'
      + '  <!-- Im a comment node -->'
      + '  <li class="yo" id="three">Thing</li>'
      + '  <li class="yo" id="four">Thing</li>'
      + '  <li id="mid">'
      + '    Thing (#mid)'
      + '    <a href="#">child</a>'
      + '  </li>'
      + '  <!-- Im a comment node -->'
      + '  <li id="six">Thing</li>'
      + '  <li id="seven">Thing</li>'
      + '</ul>'
      + '<div>Im a div</div>';
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should not include the target in the array that is returned', function () {
    var first = document.getElementById('first');
    var foo = dom.siblings(first);
    assert.equal(false, array.inArray(foo, first));
  });

  it('should return an array with a length of 6', function () {
    var first = document.getElementById('first');
    var foo = dom.siblings(first);
    assert.equal(foo.length, 6);
  });

  it('should return all of the element\'s siblings', function () {
    var mid = document.getElementById('mid');
    var sibs = dom.siblings(mid);

    //there are a total of 7 <li />'s, so the length should be 6
    //there should be the first, second, theird fourth, sixth and seventh <li />
    //NOT the fifth because it is the element being passed in
    assert.equal(sibs.length, 6);
    assert.strictEqual(sibs[0], document.getElementById('first'));
    assert.strictEqual(sibs[1], document.getElementById('two'));
    assert.strictEqual(sibs[2], document.getElementById('three'));
    assert.strictEqual(sibs[3], document.getElementById('four'));
    assert.strictEqual(sibs[4], document.getElementById('six'));
    assert.strictEqual(sibs[5], document.getElementById('seven'));
  });

  it('should not return any sibling\'s children', function () {
    var child = selector.query('#list li a');
    var first = document.getElementById('first');
    var foo = dom.siblings(first);
    assert.equal(false, array.inArray(foo, child));
  });

  it('should return an empty array for dom.siblings()', function () {
    var onlyChild = selector.query('#mid a');
    var foo = dom.siblings(onlyChild);
    assert.equal(foo.length, 0);
  });

  it('should filter elements by the given selector', function () {
    var element = selector.query('#first');
    var siblings = dom.siblings(element, '.yo');
    assert.equal(3, siblings.length);
    assert.strictEqual(siblings[0], selector.query('#two'));
    assert.strictEqual(siblings[1], selector.query('#three'));
    assert.strictEqual(siblings[2], selector.query('#four'));
  });

  describe('dom.siblings.prev', function () {
    it('should not include the target in the array that is returned', function () {
      var mid = document.getElementById('mid');
      var foo = dom.siblings.prev(mid);
      assert.equal(false, array.inArray(foo, mid));
    });

    it('should only return the previous siblings (those above the target in the DOM)', function () {
      var mid = document.getElementById('mid');
      var sibs = dom.siblings.prev(mid);

      assert.equal(sibs.length, 4);
      assert.strictEqual(sibs[0], document.getElementById('four'));
      assert.strictEqual(sibs[1], document.getElementById('three'));
      assert.strictEqual(sibs[2], document.getElementById('two'));
      assert.strictEqual(sibs[3], document.getElementById('first'));
    });

    it('should filter elements by a selector', function () {
      var mid = document.getElementById('mid');
      var sibs = dom.siblings.prev(mid, '.yo');

      assert.equal(sibs.length, 3);
      assert.strictEqual(sibs[0], document.getElementById('four'));
      assert.strictEqual(sibs[1], document.getElementById('three'));
      assert.strictEqual(sibs[2], document.getElementById('two'));
    });

    it('should return an empty array if there are no previous siblings', function () {
      var first = document.getElementById('first');
      var foo = dom.siblings.prev(first);
      assert.equal(foo.length, 0);
    });

    it('should not return any sibling\'s children', function () {
      var six = document.getElementById('six');
      var child = selector.query('#list li a');
      var sibs = dom.siblings.prev(six);
      assert.equal(false, array.inArray(sibs, child));
    });
  });

  describe('dom.siblings.next', function () {
    it('should not include the target in the array that is returned', function () {
      var mid = document.getElementById('mid');
      var sibs = dom.siblings.next(mid);
      assert.equal(false, array.inArray(sibs, mid));
    });

    it('should only return the next siblings (those below the target in the DOM', function () {
      var mid = document.getElementById('mid');
      var sibs = dom.siblings.next(mid);
      assert.equal(sibs.length, 2);
      assert.strictEqual(sibs[0], document.getElementById('six'));
      assert.strictEqual(sibs[1], document.getElementById('seven'));
    });

    it('should filter elements by a selector', function () {
      var first = selector.query('#first');
      var siblings = dom.siblings.next(first, '.yo');

      assert.equal(3, siblings.length);
      assert.strictEqual(siblings[0], selector.query('#two'));
      assert.strictEqual(siblings[1], selector.query('#three'));
      assert.strictEqual(siblings[2], selector.query('#four'));
    });

    it('should return an empty array if there are no next siblings', function () {
      var seven = document.getElementById('seven');
      var sibs = dom.siblings.next(seven);
      assert.equal(sibs.length, 0);
    });

    it('should not return any sibling\'s children', function () {
      var child = selector.query('#mid a');
      var first = document.getElementById('first');
      var sibs = dom.siblings.next(first);
      assert.equal(false, array.inArray(sibs, child));
    });
  });
});


