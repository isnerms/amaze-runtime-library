
describe('dom.setAttributes', function () {
  'use strict';

  var dom = require('dom');
  var div = null;

  beforeEach(function () {
    div = document.createElement('div');
  });

  it('should set legal attributes', function () {
    var attrs = {
      id: 'hi-' + Math.floor(Math.random() * 0x0deadbeef),
      'class': 'classes are cool',
      'data-foo': 'bar',
      whatever: 'awesome'
    };
    dom.setAttributes(div, attrs);
    assert.deepEqual(attrs, dom.getAttributes(div));
  });

  it('should not set illegal attributes', function () {
    dom.setAttributes(div, {
      '0a': true,
      '-b': true,
      '(c)': true,
      '&d': true
    });
    assert.equal(false, div.hasAttribute('0a'));
    assert.equal(false, div.hasAttribute('-b'));
    assert.equal(false, div.hasAttribute('(c)'));
    assert.equal(false, div.hasAttribute('&d'));
  });
});
