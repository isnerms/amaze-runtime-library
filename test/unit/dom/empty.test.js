
describe('dom.empty', function () {
  'use strict';

  var dom = require('dom');

  it('should remove all child elements', function () {
    var div = document.createElement('div');
    div.innerHTML =
        '<div>'
      + '  <!-- hi -->'
      + '  yeah'
      + '  <button>yo</button>'
      + '  <div><div><div>'
      + '    yo'
      + '  </div></div></div>'
      + '</div>';
    dom.empty(div);
    assert.equal(0, div.childNodes.length);
  });
});
