
describe('amaze#replaceTag', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div id="fixture">'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '  <span>hi</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.replaceTag);
    assert.equal('function', typeof amaze('div').replaceTag);
  });

  it('should chain', function () {
    var $ = amaze('#fixture span')
      .replaceTag('span')
      .replaceTag('p', { foo: 'bar' })
      .replaceTag('h2');

    assert.equal(5, $.length);

    $.each(function () {
      assert.equal('H2', this.tagName);
      assert.equal('bar', this.getAttribute('foo'));
    });
  });
});
