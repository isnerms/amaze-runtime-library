
describe('amaze#hasClass', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<span class="a"></span>'
      + '<span class="a b"></span>'
      + '<span class="a b c"></span>'
      + '<span class="a b c d"></span>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.hasClass);
    assert.equal('function', typeof amaze('div').hasClass);
  });

  it('should return false no element in the list has the class', function () {
    var $spans = amaze('span', element);
    assert.isFalse($spans.hasClass('e'));
  });

  it('should return true if an element in the list has the class', function () {
    var $spans = amaze('span', element);
    assert.isTrue($spans.hasClass('a'));
  });
});
