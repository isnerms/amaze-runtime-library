
describe('amaze#find', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div>'
      + '  <span id="a" class="a"><b>hello</b></span>'
      + '  <span id="b" class="a b"><b>hello</b></span>'
      + '  <span id="c" class="a b c"><b>hello</b></span>'
      + '  <span id="d" class="a b c d"><b>hello</b></span>'
      + '  <span id="e" class="a b c d e"><b>hello</b></span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.find);
    assert.equal('function', typeof amaze('div').find);
  });

  it('should find all elements matching the selector', function () {
    var $list = amaze('span.c', element);
    var $b = $list.find('b');
    assert.equal(3, $b.length);
    $b.each(function () {
      assert.equal('B', this.tagName);
    });
  });

  it('should chain', function () {
    var i = 0;
    amaze('div', element)
    .find('span')
    .find('b')
    .each(function () {
      i++;
    });
    assert.equal(5, i);
  });
});
