describe('amaze#parent', function () {
  'use strict';

  var amaze = require('list');
  var array = require('array');
  var dom = require('dom');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div id="parent">'
      + '  <ul id="main-ul">'
      + '    <li id="the-a-parent" class="a"><b>hello: a</b></li>'
      + '    <li class="b"><b>hello: b</b></li>'
      + '    <li class="c"><b>hello: c</b>'
      + '      <ul id="c-ul">'
      + '        <li>c 1</li><li id="c-2">c 2</li>'
      + '      </ul>'
      + '    </li>'
      + '    <li class="d"><b>hello: d</b></li>'
      + '  </ul>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.parent);
    assert.equal('function', typeof amaze('div').parent);
  });

  it('should find the parent of each element in the list', function () {
    var theParents = amaze('#parent li').parent();
    var mainUl = document.getElementById('main-ul');
    var cUl = document.getElementById('c-ul');
    assert.equal(theParents.length, 2);
    assert.ok(~array.indexOf(theParents, mainUl));
    assert.ok(~array.indexOf(theParents, cUl));
  });

  it('should only return parents that match the selector if one is provided', function () {
    var classAParents = amaze('#parent b').parent('.a');
    var theAParent = document.getElementById('the-a-parent');
    assert.equal(classAParents.length, 1); // there should only be one match...
    assert.ok(dom.hasClass(classAParents[0], 'a'));
    assert.strictEqual(classAParents[0], theAParent);
  });

  it('should not return duplicates', function () {
    var liParents = amaze('#parent li').parent();
    var mainUl = document.getElementById('main-ul');
    var cUl = document.getElementById('c-ul');

    var mainIdx = array.indexOf(liParents, mainUl);
    var mainCheck = array.indexOf(liParents, mainUl, mainIdx + 1);
    var cIdx = array.indexOf(liParents, cUl);
    var cCheck = array.indexOf(liParents, cUl, cIdx + 1);
    assert.equal(mainCheck, -1);
    assert.equal(cCheck, -1);
  });

  it('should return an empty array if no parents are found', function () {
    var emptyParents = amaze('#parent li').parent('nobr');
    assert.equal(emptyParents.length, 0);
  });

  it('should chain', function () {
    amaze('#c-2')
      .parent('ul')
      .parent();
  });
});
