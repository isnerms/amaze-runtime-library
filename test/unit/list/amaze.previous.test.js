
describe('amaze#previous', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div>'
      + '  <span id="a" class="a">span</span>'
      + '  <span id="b" class="a b">span</span>'
      + '  <span id="c" class="a b c">span</span>'
      + '  <span id="d" class="a b c d">span</span>'
      + '  <span id="e" class="a b c d e">span</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.previous);
    assert.equal('function', typeof amaze('div').previous);
    assert.equal('function', typeof List.prototype.prev);
    assert.equal('function', typeof amaze('div').prev);
  });

  it('should be aliased as #prev', function () {
    assert.strictEqual(List.prototype.prev, List.prototype.previous);
  });

  it('should return the previous elements', function () {
    var $list = amaze('#d, #e');
    var $next = $list.previous();
    assert.equal(4, $next.length);
  });

  it('should chain', function () {
    amaze('#a, #b').prev().previous();
  });

  describe('given a selector', function () {
    it('should return the matching "previous" elements', function () {
      var $list = amaze('#d, #e');
      var c = 0;
      $list.previous('.b').each(function () {
        c++;
        assert.equal('SPAN', this.tagName);
        assert.equal(true, amaze(this).hasClass('b'));
      });
      assert.equal(3, c);
    });
  });
});
