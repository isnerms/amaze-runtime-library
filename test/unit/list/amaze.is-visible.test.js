
describe('amaze#isVisible', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div id="a">'
      + '  <span id="b">span</span>'
      + '  <span id="c">span</span>'
      + '  <span id="d">span</span>'
      + '  <span id="e">span</span>'
      + '  <span id="f">span</span>'
      + '  <span id="g">span</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.isVisible);
    assert.equal('function', typeof amaze('div').isVisible);
  });

  describe('if all elements in the list are visible', function () {
    var $list;

    beforeEach(function () {
      $list = amaze('span', element).css('display', 'block');
    });

    it('should return true', function () {
      assert.equal(true, $list.isVisible());
    });
  });

  describe('if no elements in the list are visible', function () {
    var $list;

    beforeEach(function () {
      $list = amaze('span', element).css('display', 'none');
    });

    it('should return false', function () {
      assert.equal(false, $list.isVisible());
    });
  });

  describe('if one element in the list is not visible', function () {
    var $list;

    beforeEach(function () {
      $list = amaze('span', element).css('display', 'block');
      amaze('#d').css('display', 'none');
    });

    it('should return false', function () {
      assert.equal(false, $list.isVisible());
    });
  });
});
