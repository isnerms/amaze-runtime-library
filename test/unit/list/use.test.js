
describe('amaze.use', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;

  it('should add "fn" to the List prototype as "name"', function () {
    amaze.use('blah', function () {
      return 'blah';
    });

    assert.equal('blah', amaze('div').blah());
    delete List.prototype.blah;
  });

  it('should allow for functions to receive arguments', function () {
    amaze.use('greet', function (name) {
      return 'hello ' + name;
    });

    assert.equal('hello world', amaze('div').greet('world'));
    delete List.prototype.greet;
  });

  it('should allow functions to use "this"', function () {
    amaze.use('a', function () {
      return this;
    });

    var $divs = amaze('div');
    assert.strictEqual($divs, $divs.a());
    delete List.prototype.a;
  });

  it('should chain', function () {
    amaze
    .use('foo', function () {
      return 'foo';
    })
    .use('bar', function () {
      return 'bar';
    });

    var $divs = amaze('div');
    assert.equal('foobar', $divs.foo() + $divs.bar());
    delete List.prototype.foo;
    delete List.prototype.bar;
  });

  describe('given an object', function () {
    it('should add each of its keys as methods', function () {
      var obj = {
        foo: function () { return 'foo'; },
        bar: function () { return 'bar'; }
      };

      amaze.use(obj);
      assert.strictEqual(List.prototype.foo, obj.foo);
      assert.strictEqual(List.prototype.bar, obj.bar);

      var $divs = amaze('div');
      assert.equal('foo', $divs.foo());
      assert.equal('bar', $divs.bar());

      delete List.prototype.foo;
      delete List.prototype.bar;
    });
  });

  describe('given a practical addition', function () {
    var fixture;

    before(function () {
      fixture = document.createElement('div');
      fixture.id = 'use-fixture';
      document.body.appendChild(fixture);
    });

    after(function () {
      document.body.removeChild(fixture);
    });

    it('should actually work', function (done) {
      amaze.use('addClickListener', function (clickListener) {
        this.on('click', clickListener);
      });

      var $fixture = amaze('#use-fixture');

      $fixture.addClickListener(function () {
        delete List.prototype.addClickListener;
        done();
      });

      $fixture.fire('click');
    });
  });
});
