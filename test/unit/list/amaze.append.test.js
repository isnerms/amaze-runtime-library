
describe('amaze#append', function () {
  'use strict';

  var amaze = require('list');
  var dom = require('dom');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
      + '<div class="foo"></div>'
      + '<div class="foo"></div>'
      + '<div class="foo"></div>'
      + '<div class="bar"><span>BAR</span></div>'
      + '<div class="bar"><span>BAR</span></div>'
      + '<div class="bar"><span>BAR</span></div>'
      + '<div class="bar"><span>BAR</span></div>'
      + '<div class="bar"><span>BAR</span></div>'
      + '<div class="shoobie-cage">'
      + '  <div class="shoo">SHOOBIE 1</div>'
      + '  <div class="shoo">SHOOBIE2</div>'
      + '  <div class="shoo">SHOOBIE3</div>'
      + '  <div class="shoo">SHOOBIE4</div>'
      + '  <div class="shoo">SHOOBIE5</div>'
      + '  <div class="shoo">SHOOBIE6</div>'
      + '  <div class="shoo">SHOOBIE7</div>'
      + '</div>'
      + '<div id="bro">BRO</div>'
      + '<div id="brahs">'
      + '  <div class="brah">Brah</div>'
      + '  <div class="brah">Brah</div>'
      + '  <div class="brah">Brah</div>'
      + '</div>';

    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.append);
    assert.equal('function', typeof amaze('div').append);
  });

  it('should append the content(s) to the END the element', function () {
    var bro = document.getElementById('bro');
    var initFirstChild = bro.firstChild;
    amaze(bro).append('<div class="baz">Bazzy</div>');
    var broChilds = amaze(bro)[0].childNodes;
    assert.equal(amaze(bro)[0].firstChild, initFirstChild);
    assert.ok(dom.hasClass(broChilds[broChilds.length - 1], 'baz'));
  });

  it('should append to each element in the list', function () {
    amaze('.foo').append('<div class="foo-inner">Im foo inner!</div>');
    amaze('.foo').each(function () {
      assert.isDefined(amaze('.foo-inner', this)[0]);
    });
  });

  it('should accept a text node', function () {
    amaze('.foo').append('I am a text node');
    amaze('.foo').each(function () {
      assert.equal(this.childNodes.length, 1);
    });

  });

  it('should accept an array of text nodes', function () {
    amaze('.foo').append(['text node 1', 'text node 2', 'text node 3']);
    amaze('.foo').each(function () {
      assert.equal(this.childNodes.length, 3);
    });
  });

  it('should accept a node', function () {
    amaze('.foo').append(document.getElementById('bro'));
    // disclaimer: this in no way, shape or form
    // condones the use of duplicate IDs
    amaze('.foo').each(function () {
      assert.equal(this.firstChild.id, 'bro');
    });
  });

  it('should accept an array of nodes', function () {
    var bro = document.getElementById('bro');
    var brah = document.getElementById('brahs');
    amaze('.foo').append([bro, brah]);
    amaze('.foo').each(function () {
      var iBro = amaze('#bro', this);
      var iBrah = amaze('#brah', this);

      assert.isDefined(iBro);
      assert.isDefined(iBrah);
    });
  });

  it('should accept an html string', function () {
    amaze('#brahs').append('<div id="testing123">Hello World</div>');
    assert.isDefined(amaze('#brahs #testing123')[0]);
  });

  it('should accept an array of html strings', function () {
    amaze('#bro').append(['<div id="hello">Hello</div>', '<div id="world">World</div>']);
    assert.isDefined(amaze('#bro #hello')[0]);
    assert.isDefined(amaze('#bro #world')[0]);
  });

  it('should accept an amaze/jquery arrayish thing', function () {
    var barLength = amaze('.bar').length;
    amaze('.foo').append(amaze('.bar'));
    amaze('.foo').each(function () {
      var bars = amaze('.bar', this);
      assert.equal(bars.length, barLength);
    });
  });

  it('should accept an array of amaze/jquery arrayish things', function () {
    var barLength = amaze('.bar').length;
    var shooLength = amaze('.shoo').length;

    amaze('.foo').append([amaze('.bar'), amaze('.shoo')]);
    amaze('.foo').each(function () {
      var bars = amaze('.bar', this);
      var shoos = amaze('.shoo', this);
      assert.equal(bars.length, barLength);
      assert.equal(shoos.length, shooLength);
    });
  });

  it('should accept a mixed array ([amaze, node, text node, html string])', function () {
    var mixedArray = [amaze('#bro'), document.getElementById('brahs'), 'text node', '<div id="testing123">Test</div>'];

    amaze('.foo').append(mixedArray);

    amaze('.foo').each(function () {
      // ensures #bro has been added
      assert.isDefined(amaze('[id="bro"]', this)[0]);
      // ensures #brahs has been added
      assert.isDefined(amaze('[id="brahs"]', this)[0]);
      // ensures the html string has been appended
      assert.isDefined(amaze('[id="testing123"]', this)[0]);
      // ensures everything (including text node) has been appended
      assert.equal(this.childNodes.length, 4);
    });
  });

  it('should chain', function () {
    amaze('.foo')
      .append('Text node')
      .append('other text node')
      .append(amaze('#bro'));
  });
});
