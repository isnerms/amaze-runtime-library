
describe('amaze#delegate', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML = '<span id="span">hi</span>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.on);
    assert.equal('function', typeof amaze('div').on);
  });

  it('should bind a delegated event listener', function (done) {
    var $element = amaze(element);

    $element.delegate('span', 'click', function (e) {
      assert.strictEqual(e.target, document.getElementById('span'));
      assert.strictEqual(this, document.getElementById('span'));
      done();
    });

    amaze('span', $element).fire('click');
  });

  it('should chain', function (done) {
    amaze(element)
      .delegate('span', 'click', function () {})
      .delegate('span', 'click', function () {})
      .delegate('span', 'click', function () { done(); });

    amaze('span', element).fire('click');
  });
});
