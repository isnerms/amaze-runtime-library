
describe('amaze#undelegate', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML = '<span id="span">hi</span>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.on);
    assert.equal('function', typeof amaze('div').on);
  });

  it('should unbind a delegated event listener', function (done) {
    var $element = amaze(element);
    var clicks = 0;

    $element.delegate('span', 'click', function (e) {
      $element.undelegate('click');
      assert.strictEqual(e.target, document.getElementById('span'));
      amaze(this).fire('click');
      clicks++;
    });

    amaze('span', $element).fire('click');

    setTimeout(function () {
      assert.equal(1, clicks);
      done();
    }, 50);
  });

  it('should chain', function () {
    amaze('#span')
      .undelegate('click', function () {})
      .undelegate('click', function () {})
      .undelegate('click');
  });
});
