
describe('amaze#ariaTabs', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
      // markup from test/markup/aria-tabs.test.js
        '<div id="tabs">'
      + '  <ul id="tablist1">'
      + '    <li id="tablist1tab1"><a href="#">Tab 1</a></li>'
      + '    <li id="tablist1tab2" class="selected">'
      + '      <a href="tab2.html">Tab 2</a>'
      + '    </li>'
      + '    <li id="tablist1tab3"><a href="tab3.html">Tab 3</a></li>'
      + '    <li id="tablist1tab4"><a href="tab4.html">Tab 4</a></li>'
      + '    <li id="tablist1tab5"><a href="tab5.html">Tab 5</a></li>'
      + '    <li id="tablist1tab6"><a href="tab6.html">Tab 6</a></li>'
      + '  </ul>'
      + '  <div class="tabpanel" id="tabpanel1">'
      + '    <a href="#">First focussable element in tab panel 1</a>'
      + '  </div>'
      + '  <div class="tabpanel" id="tabpanel2">'
      + '    <a href="#">First focussable element in tab panel 2</a>'
      + '  </div>'
      + '  <div class="tabpanel" id="tabpanel3">'
      + '    <a href="#">First focussable element in tab panel 3</a>'
      + '  </div>'
      + '  <div class="tabpanel" id="tabpanel4">'
      + '    <a href="#">First focussable element in tab panel 4</a>'
      + '  </div>'
      + '  <div class="tabpanel" id="tabpanel5">'
      + '    <a href="#">First focussable element in tab panel 5</a>'
      + '  </div>'
      + '  <div class="tabpanel" id="tabpanel6">'
      + '    <a href="#">First focussable element in tab panel 6</a>'
      + '  </div>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.ariaTabs);
    assert.equal('function', typeof amaze('div').ariaTabs);
  });

  // TODO write real tests
  it('should chain', function () {
    // options stolen from /test/markup/aria-tabs.test.js
    var options = {
      indexGetter: function (tab) {
        return tab.id.substring(11);
      }
    };

    amaze('#tablist1')
    .ariaTabs(options)
    .ariaTabs(options)
    .ariaTabs(options);
  });
});
