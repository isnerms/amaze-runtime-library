
describe('amaze#setAttribute', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<b class=foo>hi</b>'
      + '<b class=bar>hi</b>'
      + '<b class=baz>hi</b>'
      + '<b class=qax>hi</b>'
      + '<b class=quz>hi</b>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.setAttribute);
    assert.equal('function', typeof amaze('div').setAttribute);
  });

  it('should set an attribute on every element in the list', function () {
    amaze('b', element)
    .setAttribute('foo', 'bar')
    .each(function () {
      assert.equal('bar', this.getAttribute('foo'));
    });
  });

  it('should chain', function () {
    amaze('b', element)
    .setAttribute('foo', 'bar')
    .setAttribute('baz', 'bang');
  });
});
