
describe('amaze#once', function () {
  'use strict';

  var amaze = require('list');
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML = '<span id="span">hi</span>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof amaze('div').once);
  });

  it('should bind a single-shot event listener', function (done) {
    var clicks = 0;

    amaze(element)
    .once('click', function () { clicks++; })
    .fire('click')
    .fire('click');

    setTimeout(function () {
      assert.equal(1, clicks);
      done();
    }, 50);
  });

  it('should chain', function (done) {
    amaze('#span')
      .once('click', function () {})
      .once('click', function () { done(); })
      .fire('click');
  });
});
