
describe('amaze#removeClass', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<b class=foo>hi</b>'
      + '<b class=bar>hi</b>'
      + '<b class=baz>hi</b>'
      + '<b class=qax>hi</b>'
      + '<b class=quz>hi</b>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.removeClass);
    assert.equal('function', typeof amaze('div').removeClass);
  });

  it('should remove a class from every element in the list', function () {
    amaze('b', element)
    .removeClass('foo')
    .each(function () {
      assert.equal(false, amaze(this).hasClass('foo'));
    });
  });

  it('should chain', function () {
    var $b = amaze('b', element);
    $b.removeClass('foo').removeClass('bar');
  });
});
