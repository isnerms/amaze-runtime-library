
describe('amaze#indexOf', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var jQuery = window.jQuery;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div>'
      + '  <span id="a" class="a">span</span>'
      + '  <span id="b" class="a b">span</span>'
      + '  <span id="c" class="a b c">span</span>'
      + '  <span id="d" class="a b c d">span</span>'
      + '  <span id="e" class="a b c d e">span</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.indexOf);
    assert.equal('function', typeof amaze('div').indexOf);
  });

  describe('given a selector', function () {
    it('should return the index of the element matching it', function () {
      assert.equal(1, amaze('span', element).indexOf('.b'));
    });

    it('should return -1 if nothing matches', function () {
      assert.equal(-1, amaze('span', element).indexOf('hi'));
    });
  });

  describe('given an element', function () {
    it('should return the index of it', function () {
      var b = document.getElementById('b');
      assert.equal(1, amaze('span', element).indexOf(b));
    });
  });

  describe('given an amaze object', function () {
    it('should return the index of the element it wraps', function () {
      assert.equal(1, amaze('span', element).indexOf(amaze('#b')));
    });
  });

  describe('given a jQuery obejct', function () {
    it('should return the index of the element it wraps', function () {
      assert.equal(1, amaze('span', element).indexOf(jQuery('#b')));
    });
  });
});
