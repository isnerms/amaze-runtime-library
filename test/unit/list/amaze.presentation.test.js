
describe('amaze#presentation', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.presentation);
    assert.equal('function', typeof amaze('div').presentation);
  });

  // TODO write real tests
  it('should chain', function () {
    amaze(element)
      .presentation()
      .presentation()
      .presentation();
  });
});
