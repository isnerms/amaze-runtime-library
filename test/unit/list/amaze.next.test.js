
describe('amaze#next', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div>'
      + '  <span id="a" class="a">span</span>'
      + '  <span id="b" class="a b">span</span>'
      + '  <span id="c" class="a b c">span</span>'
      + '  <span id="d" class="a b c d">span</span>'
      + '  <span id="e" class="a b c d e">span</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.next);
    assert.equal('function', typeof amaze('div').next);
  });

  it('should return the next elements', function () {
    var $list = amaze('#a, #b');
    var $next = $list.next();
    assert.equal(4, $next.length);
  });

  it('should chain', function () {
    amaze('#a, #b').next().next();
  });

  describe('given a selector', function () {
    it('should return the matching "next" elements', function () {
      var $list = amaze('#a, #b');
      var c = 0;
      $list.next('.c').each(function () {
        c++;
        assert.equal('SPAN', this.tagName);
        assert.equal(true, amaze(this).hasClass('c'));
      });
      assert.equal(3, c);
    });
  });
});
