
describe('amaze#empty', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div id="a">'
      + '  <div id="b">'
      + '    <div id="c">'
      + '      <div id="d">'
      + '      </div>'
      + '    </div>'
      + '  </div>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.empty);
    assert.equal('function', typeof amaze('div').empty);
  });

  it('should empty all elements in the list', function () {
    amaze(element).empty();
    assert.equal(0, element.childNodes.length);
  });

  it('should chain', function () {
    amaze(element)
    .empty()
    .empty()
    .empty();
  });
});
