
describe('amaze#each', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof amaze('div').each);
  });

  it('should set `this`', function () {
    var b = document.createElement('b');
    var $b = amaze(b);

    $b.each(function () {
      assert.equal(this, b);
    });
  });

  it('should provide the element and its index', function () {
    element.innerHTML =
        '<span>hi</span>'
      + '<div>hi</div>';

    var elements = amaze('*', element);

    elements.each(function (element, index) {
      if (index === 0) {
        assert.equal(element.tagName, 'SPAN');
      } else {
        assert.equal(element.tagName, 'DIV');
        assert.equal(index, 1);
      }
    });
  });

  it('should chain', function (done) {
    amaze(element)
    .each(function () {})
    .each(function () { done(); });
  });

  it('should be aliased as #forEach', function () {
    assert.strictEqual(List.prototype.each, List.prototype.forEach);
  });
});
