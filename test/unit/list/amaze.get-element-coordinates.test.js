
describe('amaze#getElementCoordinates', function () {
  'use strict';

  var amaze = require('list');
  var dom = require('dom');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<span>hi</span>'
      + '<span>hi</span>'
      + '<span>hi</span>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.ariaMenu);
    assert.equal('function', typeof amaze('div').ariaMenu);
  });

  it('should return the coordinates of the first element', function () {
    var $spans = amaze('span', element);
    var expected = dom.getElementCoordinates($spans[0]);
    var actual = $spans.getElementCoordinates();
    assert.deepEqual(expected, actual);
  });
});
