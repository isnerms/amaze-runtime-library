
describe('amaze#getVisibleText', function () {
  'use strict';

  var amaze = require('list');
  var dom = require('dom');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<b class=foo>a</b>'
      + '<b class=bar>b</b>'
      + '<b class=baz>c</b>'
      + '<b class=qax>d</b>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.getVisibleText);
    assert.equal('function', typeof amaze('div').getVisibleText);
  });

  it('should return the same thing as dom.getVisibleText', function () {
    var $b = amaze('b', element);
    var expected = '';
    $b.each(function () {
      expected += dom.getVisibleText(this);
    });
    var actual = $b.getVisibleText();
    assert.equal(actual, expected);
  });
});
