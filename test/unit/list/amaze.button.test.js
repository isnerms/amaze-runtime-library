
describe('amaze#button', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('a');
    element.href = '';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.button);
    assert.equal('function', typeof amaze('a').button);
  });

  // TODO write real tests
  it('should chain', function () {
    amaze(element)
      .button()
      .button()
      .button();
  });
});
