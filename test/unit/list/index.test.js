
describe('amaze query (list)', function () {
  'use strict';

  var selector = require('selector');
  var amaze = require('list');
  var each = require('collections').each;
  var jQuery = window.jQuery;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof amaze);
  });

  it('should expose List', function () {
    assert.equal('function', typeof amaze.List);
  });

  it('should take an array of nodes', function () {
    var divs = selector('div');
    var $ = amaze(divs);

    assert.equal(divs.length, $.length);
    for (var i = divs.length - 1; i >= 0; i--) {
      assert.strictEqual(divs[i], $[i]);
    }
  });

  if (!window.mochaPhantomJS) {
    it('should take a NodeList', function () {
      var divs = document.getElementsByTagName('div');
      var $ = amaze(divs);

      assert.equal(divs.length, $.length);
      for (var i = divs.length - 1; i >= 0; i--) {
        assert.strictEqual(divs[i], $[i]);
      }
    });
  }

  it('should take a jQuery object', function () {
    var $divs = jQuery('div');
    var $ = amaze($divs);

    assert.equal($divs.length, $.length);
    for (var i = $divs.length - 1; i >= 0; i--) {
      assert.strictEqual($divs[i], $[i]);
    }
  });

  if (!window.mochaPhantomJS) {
    it('should take an amaze object', function () {
      var divs = document.getElementsByTagName('div');
      var $ = amaze(amaze(divs));

      assert.equal(divs.length, $.length);
      for (var i = divs.length - 1; i >= 0; i--) {
        assert.strictEqual(divs[i], $[i]);
      }
    });
  }

  it('should take a node', function () {
    var div = document.getElementsByTagName('div')[0];
    var $ = amaze(div);

    assert.equal(1, $.length);
    assert.strictEqual(div, $[0]);
  });

  it('should take a chunk of HTML', function () {
    var $ = amaze('<div id="swanzzz">hello world</div>');
    assert.equal('DIV', $[0].nodeName);
    assert.equal('swanzzz', $[0].id);
    assert.strictEqual(null, document.getElementById('swanzzz'));
  });

  it('should take a selector', function () {
    var divs = selector('div');
    var $ = amaze('div');

    assert.equal(divs.length, $.length);
    for (var i = divs.length - 1; i >= 0; i--) {
      assert.strictEqual(divs[i], $[i]);
    }
  });

  describe('context', function () {
    beforeEach(function () {
      element.innerHTML =
          '<div id="fixture">'
        + '  <span class="span">span</span>'
        + '  <p>p</p>'
        + '  <div id="magic">'
        + '    <span class="foo">'
        + '      <div id="arrrrr"></div>'
        + '    </span>'
        + '  </div>'
        + '</div>';
    });

    it('should take a selector as a context', function () {
      var $span = amaze('span', '#fixture');
      assert.equal('SPAN', $span[0].nodeName);
      assert.strictEqual($span[0], selector.query('#fixture span'));
    });

    it('should take an amaze object as a context', function () {
      var $fixture = amaze('#fixture');
      var $span = amaze('span', $fixture);
      assert.strictEqual($span[0], selector.query('#fixture span'));
    });

    it('should take a node as a context', function () {
      var fixture = selector.query('#fixture');
      var $span = amaze('span', fixture);
      assert.strictEqual($span[0], selector.query('#fixture span'));
    });

    if (!window.mochaPhantomJS) {
      it('should take a NodeList as a context', function () {
        var magic = document.getElementById('magic');
        var spans = magic.getElementsByTagName('span');
        var $foo = amaze('div', spans);
        assert.equal('arrrrr', $foo[0].id);
        assert.strictEqual($foo[0], selector.query('#arrrrr'));
      });
    }

    it('should take an array of nodes as a context', function () {
      var magic = selector('#magic');
      var $foo = amaze('.foo', magic);
      assert.equal('SPAN', $foo[0].nodeName);
      assert.strictEqual($foo[0], selector.query('#magic .foo'));
    });

    each({
      object: {},
      regexp: new RegExp,
      date: new Date,
      error: new Error
    }, function (obj, type) {
      var article = /^[aeiou]/.test(type) ? 'an' : 'a';
      var should =
          'should throw when given '
        + article
        + ' '
        + type
        + ' as a context';
      it(should, function () {
        var err = null;
        try {
          amaze('div', obj);
        } catch (e) {
          err = e;
        }
        assert.equal('invalid context', err.message);
      });
    });
  });

  each({
    object: {},
    regexp: new RegExp,
    date: new Date,
    error: new Error
  }, function (obj, type) {
    var article = /^[aeiou]/.test(type) ? 'an' : 'a';
    it('should throw when given ' + article + ' ' + type, function () {
      var err = null;
      try {
        amaze(obj);
      } catch (e) {
        err = e;
      }
      assert.equal('invalid selector', err.message);
    });
  });
});
