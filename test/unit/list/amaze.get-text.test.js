
describe('amaze#getText', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<b class=foo>a</b>'
      + '<b class=bar>b</b>'
      + '<b class=baz>c</b>'
      + '<b class=qax>d</b>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.getText);
    assert.equal('function', typeof amaze('div').getText);
  });

  it('should get the combined text of every element in the list', function () {
    var text = amaze('b', element).getText();
    assert.equal('abcd', text);
  });
});
