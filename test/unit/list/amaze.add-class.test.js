
describe('amaze#addClass', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<b class=foo>hi</b>'
      + '<b class=bar>hi</b>'
      + '<b class=baz>hi</b>'
      + '<b class=qax>hi</b>'
      + '<b class=quz>hi</b>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.addClass);
    assert.equal('function', typeof amaze('div').addClass);
  });

  it('should add a class to every element in the list', function () {
    amaze('b', element)
    .addClass('foo')
    .each(function () {
      assert.equal(true, amaze(this).hasClass('foo'));
    });
  });

  it('should chain', function () {
    var $b = amaze('b', element);
    $b.addClass('foo').addClass('bar');
  });
});
