
describe('amaze#off', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.off);
    assert.equal('function', typeof amaze('div').off);
  });

  it('should remove a bound event listener', function (done) {
    var $element = amaze(element);
    var clicks = 0;

    function handler() {
      $element.off('click', handler);
      clicks++;
      $element.fire('click');
    }

    $element.on('click', handler);
    $element.fire('click');
    setTimeout(function () {
      assert.equal(1, clicks);
      done();
    }, 50);
  });

  it('should chain', function () {
    amaze(element)
      .off('click')
      .off('click')
      .off('click');
  });
});
