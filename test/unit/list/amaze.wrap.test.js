
describe('amaze#wrap', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div>'
      + '  <span id="a" class="a">span</span>'
      + '  <span id="b" class="a b">span</span>'
      + '  <span id="c" class="a b c">span</span>'
      + '  <span id="d" class="a b c d">span</span>'
      + '  <span id="e" class="a b c d e">span</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.wrap);
    assert.equal('function', typeof amaze('div').wrap);
  });

  it('should wrap all elements in the given tag', function () {
    var $list = amaze('span', element)
      .wrap('div')
      .each(function () {
        assert.equal('DIV', this.parentNode.tagName);
      });
    assert.equal(5, $list.length);
  });

  it('should chain', function () {
    amaze('span', element)
    .wrap('div')
    .wrap('span')
    .wrap('div')
    .wrap('b')
    .each(function () {
      assert.equal('B', this.parentNode.tagName);
    });
  });
});
