
describe('amaze#is', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
      '<div id="brew" class="brew beer">Brew</div>'
    + '<a href="#" id="empty"></a>'
    + '<div class="foo">Foo</div>'
    + '<p class="foo">Foo</p>'
    + '<h1 id="foo-head" class="foo">Foo head</h1>'
    + '<input type="text" required/>'
    + '<input type="text" />'
    + '<button disabled class="foo" type="button" id="foo-button">Foo Button</button>';

    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.is);
    assert.equal('function', typeof amaze('div').is);
  });

  it('should return a boolean', function () {
    assert.equal(typeof amaze('#brew').is('div'), 'boolean');
  });

  it('should accept a string', function () {
    amaze('#brew').is('.asdf');
  });

  it('should accept a function', function () {
    amaze('#brew').is(function () {
      return 'foobar';
    });
  });

  it('should return true if at least one element in the set matches', function () {
    assert.strictEqual(amaze('input').is(':required'), true);
    assert.strictEqual(amaze('.foo').is('h1'), true);
  });

  it('should return false if there are no matches', function () {
    assert.strictEqual(amaze('.foo').is('h4'), false);
  });
});
