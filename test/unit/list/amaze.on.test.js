
describe('amaze#on', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.on);
    assert.equal('function', typeof amaze('div').on);
  });

  it('should bind an event listener', function (done) {
    var $element = amaze(element);

    $element.on('click', function (e) {
      assert.strictEqual(element, e.target);
      assert.strictEqual(this, element);
      done();
    });

    $element.fire('click');
  });

  it('should chain', function (done) {
    amaze(element)
      .on('click', function () {})
      .on('click', function () {})
      .on('click', function () { done(); })
      .fire('click');
  });
});
