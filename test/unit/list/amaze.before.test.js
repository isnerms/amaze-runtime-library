
describe('amaze#before', function () {
  'use strict';

  var amaze = require('list');
  var trim = require('string').trim;
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div id="a">'
      + '  <span>span</span>'
      + '  <span>span</span>'
      + '  <span>span</span>'
      + '  <span>span</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.before);
    assert.equal('function', typeof amaze('div').before);
  });

  it('should add some HTML before all elements in the list', function () {
    var html = '<p>hello world</p>';
    var count = 0;

    amaze('span', element)
    .before(html)
    .each(function () {
      count++;
      var iesux = trim(this.previousSibling.outerHTML).toLowerCase();
      assert.equal(html, iesux);
    });
    assert.equal(4, count);
  });

  it('should clone nodes', function () {
    var node = document.createElement('b');
    node.className = 'yo';
    element.appendChild(node);

    amaze('span', element)
    .before(node)
    .each(function (element, index) {
      if (index === 0) {
        assert.strictEqual(node, element.previousSibling);
      } else {
        assert.notStrictEqual(node, element.previousSibling);
        assert.equal(node.className, element.previousSibling.className);
      }
    });
  });
});
