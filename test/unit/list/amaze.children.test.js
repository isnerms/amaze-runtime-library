
describe('amaze#children', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div id="a" class="foo">'
      + '  <div id="a1" class="bar"></div>'
      + '</div>'
      + '<div id="b" class="foo">'
      + '  <div id="b1" class="bar qax"></div>'
      + '</div>'
      + '<div id="c" class="foo">'
      + '  <div id="c1" class="bar qax"></div>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.children);
    assert.equal('function', typeof amaze('div').children);
  });

  it('should return children from all elements in the list', function () {
    var $list = amaze('div', element);
    var $children = $list.children();
    $children.each(function () {
      assert.equal(true, amaze(this).hasClass('bar'));
    });
    assert.equal(3, $children.length);
  });

  describe('given a selector', function () {
    it('should return matching children', function () {
      var $list = amaze('div', element);
      var $children = $list.children('.qax');
      $children.each(function () {
        assert.equal(true, amaze(this).hasClass('qax'));
      });
      assert.equal(2, $children.length);
    });
  });

  it('should chain', function () {
    amaze(element)
    .children()
    .children('.qax')
    .each(function () {
      assert.equal(true, amaze(this).hasClass('qax'));
    });
  });
});
