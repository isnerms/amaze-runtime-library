
describe('amaze#filter', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div>'
      + '  <span id="a" class="a">span</span>'
      + '  <span id="b" class="a b">span</span>'
      + '  <span id="c" class="a b c">span</span>'
      + '  <span id="d" class="a b c d">span</span>'
      + '  <span id="e" class="a b c d e">span</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.filter);
    assert.equal('function', typeof amaze('div').filter);
  });

  describe('given a selector', function () {
    it('should remove elements not matching the given selector', function () {
      var $list = amaze('span', element);
      var $b = $list.filter('.b');
      assert.equal(4, $b.length);
      $b.each(function () {
        assert.equal(true, amaze(this).hasClass('b'));
      });
    });
  });

  describe('given a function', function () {
    it('should set context', function () {
      amaze('span', element).filter(function (element) {
        assert.strictEqual(element, this);
      });
    });

    it('should remove elements not passing the function', function () {
      var $b = amaze('span', element).filter(function () {
        return amaze(this).hasClass('b');
      });
      assert.equal(4, $b.length);
    });
  });
});
