
describe('amaze#fire', function () {
  'use strict';

  var amaze = require('list');
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof amaze('div').fire);
  });

  it('should fire an event', function (done) {
    var $element = amaze(element);

    $element.on('click', function (e) {
      assert.strictEqual(element, e.target);
      assert.strictEqual(this, element);
      done();
    });

    $element.fire('click');
  });

  it('should augment the event', function (done) {
    amaze(element)
    .on('click', function (e) {
      assert.equal('bar', e.foo);
      done();
    })
    .fire('click', { foo: 'bar' });
  });

  it('should chain', function (done) {
    amaze(element)
      .fire('click')
      .on('click', function () {})
      .on('click', function () { done(); })
      .fire('click');
  });
});
