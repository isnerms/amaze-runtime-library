
describe('amaze#after', function () {
  'use strict';

  var amaze = require('list');
  var trim = require('string').trim;
  var List = amaze.List;
  var element = null;

  afterEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div id="a">'
      + '  <span>span</span>'
      + '  <span>span</span>'
      + '  <span>span</span>'
      + '  <span>span</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.after);
    assert.equal('function', typeof amaze('div').after);
  });

  it('should add some HTML after all elements in the list', function () {
    var html = '<p>hello world</p>';
    var count = 0;

    amaze('span', element)
    .after(html)
    .each(function () {
      count++;
      var iesux = trim(this.nextSibling.outerHTML).toLowerCase();
      assert.equal(html, iesux);
    });
    assert.equal(4, count);
  });

  it('should clone nodes', function () {
    var node = document.createElement('b');
    node.className = 'yo';
    element.appendChild(node);

    amaze('span', element)
    .after(node)
    .each(function (element, index) {
      if (index === 0) {
        assert.strictEqual(node, element.nextSibling);
      } else {
        assert.notStrictEqual(node, element.nextSibling);
        assert.equal(node.className, element.nextSibling.className);
      }
    });
  });
});
