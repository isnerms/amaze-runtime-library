
describe('amaze.css', function () {
  'use strict';

  var amaze = require('list');
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.id = 'yo';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof amaze.css);
  });

  it('should chain', function () {
    amaze
    .css('#foo { display: block }')
    .css('#foo { display: none }');
  });

  it('should write styles', function () {
    amaze.css('#yo { display: none }');
    assert.equal('none', amaze(element).css('display'));
  });
});
