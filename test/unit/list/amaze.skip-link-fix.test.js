
describe('amaze#skipLinkFix', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML = '<div id="hi"></div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.skipLinkFix);
    assert.equal('function', typeof amaze('div').skipLinkFix);
  });

  // TODO write real tests
  it('should chain', function () {
    amaze(element)
      .skipLinkFix('#hi')
      .skipLinkFix('#hi', {})
      .skipLinkFix('#hi', { options: 'are fun' });
  });
});
