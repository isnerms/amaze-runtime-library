describe('amaze#attr', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div id="foo" aria-label="foo label">F00</div>'
      + '<div class="fez" title="fez1" aria-describedby="foo">Fez</div>'
      + '<div class="fez" title="fez2" aria-describedby="foo">Fez</div>'
      + '<div class="fez" title="fez3" aria-describedby="foo">Fez</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.attr);
    assert.equal('function', typeof amaze('div').attr);
  });

  it('should return the value for an attribute if only an attribute name is passed in (getter)', function () {
    assert.equal(amaze('#foo').attr('aria-label'), 'foo label');
  });
  it('should return only the value from the FIRST element in the set (getter)', function () {
    assert.equal(amaze('.fez').attr('title'), 'fez1');
  });
  it('should return null if the attribute does not exist (getter)', function () {
    assert.strictEqual(amaze('#foo').attr('aria-describedby'), null);
  });
  it('should set a single attribute to each element in the set (setter)', function () {
    amaze('.fez')
      .attr('role', 'presentation')
      .each(function () {
        assert.equal(this.getAttribute('role'), 'presentation');
      });
  });
  it('should accept an object of attributes and set them to each element in the set (setter)', function () {
    amaze('.fez')
      .attr({
        'title': 'Fez',
        'aria-label': 'Fez Label',
        'data-stuff': 'my data'
      })
      .each(function () {
        assert.equal(this.getAttribute('title'), 'Fez');
        assert.equal(this.getAttribute('aria-label'), 'Fez Label');
        assert.equal(this.getAttribute('data-stuff'), 'my data');
      });
  });
  it('should override existing attribute values', function () {
    var foo = document.getElementById('foo');
    var initialValue = foo.getAttribute('aria-label');
    amaze(foo).attr('aria-label', 'New Label');
    var newValue = foo.getAttribute('aria-label');
    assert.notEqual(initialValue, newValue);
  });
  it('should accept an integer value (tabindex)', function () {
    amaze('.fez')
      .attr('tabindex', 0)
      .each(function () {
        assert.strictEqual(this.getAttribute('tabindex'), '0');
      });
  });

  it('should chain', function () {
    amaze('.fez')
      .attr('title', 'asdf')
      .attr('aria-label', 'hjkl');
  });
});
