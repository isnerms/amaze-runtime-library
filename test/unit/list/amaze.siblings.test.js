
describe('amaze#siblings', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div>'
      + '  <span id="a" class="a">span</span>'
      + '  <span id="b" class="a b">span</span>'
      + '  <span id="c" class="a b c">span</span>'
      + '  <span id="d" class="a b c d">span</span>'
      + '  <span id="e" class="a b c d e">span</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.siblings);
    assert.equal('function', typeof amaze('div').siblings);
  });

  it('should return the sibling elements', function () {
    assert.equal(4, amaze('#c').siblings().length);
  });

  it('should chain', function () {
    amaze('#c').siblings().siblings();
  });

  describe('given a selector', function () {
    it('should return the matching sibling elements', function () {
      var $list = amaze('#c').siblings('.c').each(function () {
        assert.equal(true, amaze(this).hasClass('c'));
      });
      assert.equal(2, $list.length);
    });
  });
});
