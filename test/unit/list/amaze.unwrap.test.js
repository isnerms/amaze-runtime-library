
describe('amaze#unwrap', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div id="a">'
      + '  <span id="a0">'
      + '    <span id="c">span</span>'
      + '  </span>'
      + '  <span id="a1">span</span>'
      + '  <span id="a2">span</span>'
      + '  <span id="a3">span</span>'
      + '</div>'
      + '<div id="b">'
      + '  <span id="b0">span</span>'
      + '  <span id="b1">span</span>'
      + '  <span id="b2">span</span>'
      + '  <span id="b3">span</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.unwrap);
    assert.equal('function', typeof amaze('div').unwrap);
  });

  it('should unwrap every element in the list', function () {
    var $spizzles = amaze('#a0, #b1');
    assert.equal(2, $spizzles.length);
    $spizzles.unwrap();
    $spizzles.each(function (e, i) {
      var j = e.id.substr(e.id.length - 1);
      assert.equal(j, i);
      assert.strictEqual(e.parentNode, element);
    });
  });

  it('should chain', function () {
    amaze('#c').unwrap().unwrap();
  });
});
