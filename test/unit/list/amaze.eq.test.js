
describe('amaze#eq', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div>'
      + '  <span id="a" class="a">span</span>'
      + '  <span id="b" class="a b">span</span>'
      + '  <span id="c" class="a b c">span</span>'
      + '  <span id="d" class="a b c d">span</span>'
      + '  <span id="e" class="a b c d e">span</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.eq);
    assert.equal('function', typeof amaze('div').eq);
  });

  it('should return element at the given index', function () {
    var $c = amaze('span', element).eq(2);
    assert.strictEqual($c[0], document.getElementById('c'));
    assert.isInstanceOf($c, List);
  });

  it('should return an empty list given a bad index', function () {
    var $empty = amaze('span', element).eq(1234567890);
    assert.strictEqual($empty[0], undefined);
    assert.isInstanceOf($empty, List);
  });
});
