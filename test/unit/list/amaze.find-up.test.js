
describe('amaze#findUp', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div id="a">'
      + '  <div id="b">'
      + '    <div id="c">'
      + '      <div id="d">'
      + '      </div>'
      + '    </div>'
      + '  </div>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.findUp);
    assert.equal('function', typeof amaze('div').findUp);
  });

  it('should return a list of matching parents', function () {
    amaze('div', '#b')
    .findUp('#b')
    .each(function () {
      assert.equal('b', this.id);
    });
  });

  it('should chain', function () {
    amaze('#d')
    .findUp('#c')
    .findUp('#b')
    .findUp('#a');
  });
});
