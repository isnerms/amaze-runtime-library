
describe('amaze#css', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<div id="a">'
      + '  <span id="b">span</span>'
      + '  <span id="c">span</span>'
      + '  <span id="d">span</span>'
      + '  <span id="e">span</span>'
      + '  <span id="f">span</span>'
      + '  <span id="g">span</span>'
      + '</div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.css);
    assert.equal('function', typeof amaze('div').css);
  });

  describe('given an object', function () {
    it('should set styles on every element in the list', function () {
      var c = 0;
      amaze('span', element)
      .css({ display: 'block', position: 'absolute' })
      .each(function () {
        c++;
        assert.equal('block', amaze(this).css('display'));
        assert.equal('absolute', amaze(this).css('position'));
      });
      assert.equal(6, c);
    });
  });

  describe('given a property and a value', function () {
    it('should set property=value on every element in the list', function () {
      var c = 0;
      amaze('span', element)
      .css('display', 'block')
      .each(function () {
        c++;
        assert.equal('block', amaze(this).css('display'));
      });
      assert.equal(6, c);
    });
  });

  describe('given a property', function () {
    it('should return the first element in the list\'s value', function () {
      var b = document.getElementById('b');
      b.style.display = 'none';
      var display = amaze('span', element).css('display');
      assert.equal('none', display);
    });
  });
});
