
describe('amaze#toggleClass', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML =
        '<span class="a"></span>'
      + '<span class="a b"></span>'
      + '<span class="a b c"></span>'
      + '<span class="a b c d"></span>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.toggleClass);
    assert.equal('function', typeof amaze('div').toggleClass);
  });

  it('should toggle the class of every element in the collection', function () {
    var $spans = amaze('span', element);

    $spans.toggleClass('a');
    assert.isFalse($spans.hasClass('a'));
    $spans.toggleClass('a');
    assert.isTrue($spans.hasClass('a'));
  });

  it('should chain', function () {
    var $spans = amaze('span', element)
    .toggleClass('a')
    .toggleClass('a')
    .toggleClass('a')
    .toggleClass('a');

    assert.isTrue($spans.hasClass('a'));
  });
});
