
describe('amaze#toolTipFix', function () {
  'use strict';

  var amaze = require('list');
  var List = amaze.List;
  var element = null;

  beforeEach(function () {
    element = document.createElement('div');
    element.innerHTML = '<div id="hi"></div>';
    document.body.appendChild(element);
  });

  afterEach(function () {
    document.body.removeChild(element);
  });

  it('should be a function', function () {
    assert.equal('function', typeof List.prototype.toolTipFix);
    assert.equal('function', typeof amaze('div').toolTipFix);
  });

  // TODO write real tests
  it('should chain', function () {
    amaze(element)
      .toolTipFix('#hi')
      .toolTipFix('#hi')
      .toolTipFix('#hi');
  });
});
