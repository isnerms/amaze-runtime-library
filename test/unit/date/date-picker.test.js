
describe('date.datePicker', function () {
  'use strict';

  var date = require('date');
  var events = require('events');
  var jQuery = window.jQuery;

  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');
    fixture.id = 'fixture';
    document.body.appendChild(fixture);
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  var DAY_IN_MS = 86400000;


  describe('keyboard events', function () {

    /**
     * Select the current day by sending RETURN on `#input`
     *
     * @api private
     * @param {Number} [wait]
     * @param {Function} [cb]
     */
    function selectCurrentDay(wait, cb) {
      if (typeof wait === 'function') {
        cb = wait;
        wait = 10;
      }

      cb = cb || function () {};

      setTimeout(function () {
        events.once('#input', 'keydown', function () {
          cb();
        });

        events.fire('#input', 'keydown', { which: 13 });
      }, wait || 10);
    }

    /**
     * Press the given key `code` on `#input`
     *
     * @api private
     * @param {Number} code
     */
    function press(code) {
      events.fire('#input', 'keydown', { which: code });
    }

    /**
     * Get the number of days of the month provided in `date`
     *
     * @api private
     * @param {Date} date
     * @return {Number}
     */
    function daysInMonth(date) {
      var month = date.getMonth() + 1,
          year = date.getYear();

      return new Date(year, month, 0).getDate();
    }

    beforeEach(function (done) {
      document.getElementById('fixture').innerHTML = [
        '<div id="div">',
        '  <input id="input">',
        '</div>'
      ].join('');

      jQuery('#input').datepicker({
        numberOfMonths: 3,
        minDate: 0,
        maxDate: 60,
        showOn: 'both',
        duration: 'fast'
      });

      date.datePicker('#input', {
        trigger: {
          selector: '#effectiveDateId img.ui-datepicker-trigger',
          event: null
        },
        container: { selector: '#ui-datepicker-div' },
        months: {
          count: undefined,
          selector: '.ui-datepicker-group',
          prev: { selector: 'a.ui-datepicker-prev', event: null },
          next: { selector: 'a.ui-datepicker-next', event: null },
          monthName: '.ui-datepicker-month',
          yearName: '.ui-datepicker-year',
          daysOfTheWeek: [
            'Sunday', 'Monday', 'Tuesday', 'Wednesday',
            'Thursday', 'Friday', 'Saturday'
          ]
        },
        days: {
          selector: '>table>tbody>tr>td>a',
          selectedClass: 'ui-state-highlight',
          event: 'click',
          eventSelector: null,
          unselectable: null
        },
        removeJQueryHandlers: true
      });


      // it appears you've got to click the button
      // before ANYTHING will work.  idk why.
      events.fire('#div button', 'click');

      // let the datepicker bind all its listeners and stuff
      setTimeout(function () {
        // wait for focus, then select the first date
        events.on('#input', 'focus', function () {
          selectCurrentDay(function () {
            setTimeout(done, 0);
          });
        });
        events.fire('#input', 'focus');
      }, 10);
    });

    after(function () {
      try {
        jQuery('#input').datepicker('destroy');
      } catch (e) {}
    });

    it('should move forward one day per RIGHT keypress', function (done) {
      var start = jQuery('#input').datepicker('getDate');

      // RIGHT
      press(39);
      // send RETURN and wait for the keypress event
      selectCurrentDay();

      // give IE8 a chance to catch up
      setTimeout(function () {
        var result = jQuery('#input').datepicker('getDate');

        // only one day, as we only sent one RIGHT
        assert.equal(DAY_IN_MS, result - start);
        done();
      }, 100);
    });

    it('should move forward five days with five RIGHT keypresses', function (done) {
      var start = jQuery('#input').datepicker('getDate');

      // RIGHT
      press(39);
      // RIGHT
      press(39);
      // RIGHT
      press(39);
      // RIGHT
      press(39);
      // RIGHT
      press(39);

      // send RETURN and wait for the keypress event
      selectCurrentDay();

      // give IE8 a chance to catch up
      setTimeout(function () {
        var result = jQuery('#input').datepicker('getDate');

        // 5 days
        assert.equal(DAY_IN_MS * 5, result - start);

        done();
      }, 100);
    });

    it('should move back one day per LEFT keypress', function (done) {
      var start = jQuery('#input').datepicker('getDate');

      // RIGHT to advance a day
      press(39);
      // LEFT to backtrack
      press(37);

      // send RETURN and wait for the keypress event
      selectCurrentDay();

      // give IE8 a chance to catch up
      setTimeout(function () {
        var result = jQuery('#input').datepicker('getDate');

        // only one day, as we only sent one LEFT
        assert.equal(result.getTime(), start.getTime());
        done();
      }, 100);
    });

    it('should move forward one week per DOWN keypress', function (done) {
      var start = jQuery('#input').datepicker('getDate');

      // DOWN
      press(40);

      // send RETURN and wait for the keypress event
      selectCurrentDay();

      // give IE8 a chance to catch up
      setTimeout(function () {
        var result = jQuery('#input').datepicker('getDate');

        // 1 week
        assert.equal(DAY_IN_MS * 7, result - start);
        done();
      }, 100);
    });


    it('should move back one week per UP keypress', function (done) {
      var start = jQuery('#input').datepicker('getDate');

      // DOWN (move forward a week)
      press(40);
      // UP (to reset)
      press(38);

      // send RETURN and wait for the keypress event
      selectCurrentDay();

      // give IE8 a chance to catch up
      setTimeout(function () {
        var result = jQuery('#input').datepicker('getDate');

        assert.equal(start.getTime(), result.getTime());

        done();
      }, 100);
    });

    it('should move up one month per PAGEDOWN keypress', function (done) {
      var start = jQuery('#input').datepicker('getDate');

      // PAGEDOWN
      press(34);

      // send RETURN and wait for the keypress event
      selectCurrentDay();

      // give IE8 a chance to catch up
      setTimeout(function () {
        var result = jQuery('#input').datepicker('getDate');

        assert.equal(DAY_IN_MS * 28, result - start);

        done();
      }, 100);
    });

    it('should move back one month per PAGEUP keypress', function (done) {
      var start = jQuery('#input').datepicker('getDate');

      // PAGEDOWN to move forward a month
      press(34);
      // PAGEUP to move back
      press(33);

      // send RETURN and wait for the keypress event
      selectCurrentDay();

      // give IE8 a chance to catch up
      setTimeout(function () {
        var result = jQuery('#input').datepicker('getDate');

        assert.equal(result.getTime(), start.getTime());

        done();
      }, 100);
    });

    it('should move to the end of of the month on an END keypress', function (done) {
      var start = jQuery('#input').datepicker('getDate');

      // END
      press(35);

      // send RETURN and wait for the keypress event
      selectCurrentDay();

      // give IE8 a chance to catch up
      setTimeout(function () {
        var result = jQuery('#input').datepicker('getDate'),
            expected = daysInMonth(start);

        assert.equal(result.getDate(), expected);

        done();
      }, 100);
    });

    it('should move to the beginning of of the month on a HOME keypress', function (done) {
      var start = jQuery('#input').datepicker('getDate');

      // END to move to the end of the month
      press(35);
      // HOME to move back
      press(36);

      // send RETURN and wait for the keypress event
      selectCurrentDay();

      // give IE8 a chance to catch up
      setTimeout(function () {
        var result = jQuery('#input').datepicker('getDate');

        assert.equal(result.getTime(), start.getTime());

        done();
      }, 100);
    });
  });

});
