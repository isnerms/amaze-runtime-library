describe('forms.Validator', function () {
  'use strict';

  // jshint -W064


  var forms = require('forms');
  var Validator = forms.Validator;
  var selector = require('selector');
  var each = require('collections').each;

  it('should be a function', function () {
    assert.isFunction(Validator);
  });

  it('should not throw when not given a form', function () {
    // jshint nonew:false
    new Validator();
  });

  it('should support invocation without `new`', function () {
    Validator();
  });

  it('should start with no listeners', function () {
    assert.deepEqual(Validator()._listeners, {});
  });

  it('should start with no inputs', function () {
    assert.isUndefined(Validator()._inputs);
  });

  describe('#addInput()', function () {
    var validator;
    before(function () {
      validator = Validator();
    });

    it('should create the _inputs array', function () {
      delete validator._inputs;
      validator.addInput(document.body.firstChild);
      assert.isArray(validator._inputs);
    });

    it('should add an input', function () {
      var m = document.getElementById('mocha');
      validator.addInput(m);
      assert.includes(validator._inputs, m);
    });

    it('should chain', function () {
      validator.addInput().addInput().addInput();
    });
  });

  describe('#validate()', function () {
    var validator, fixture;
    beforeEach(function () {
      fixture = document.createElement('div');

      fixture.id = 'fixture';
      fixture.innerHTML = [
        '<div>',
        '  <form id=form>',
        '    <div class="input">',
        '      <input type="text" id="stuff" />',
        '    </div>',
        '    <div class="input">',
        '      <input type="text" id="things" required />',
        '    </div>',
        '    <div class="input">',
        '      <input type="text" id="junk" required />',
        '    </div>',
        '    <div class="input">',
        '      <input type="text" id="doors" />',
        '    </div>',
        '  </form>',
        '</div>'
      ].join('');

      document.body.appendChild(fixture);

      validator = new Validator(selector.query('#fixture form'));
      validator
        .addInput(document.getElementById('stuff'))
        .addInput(document.getElementById('things'))
        .addInput(document.getElementById('junk'))
        .addInput(document.getElementById('doors'));
    });

    afterEach(function () {
      validator._listeners = {};
      document.body.removeChild(fixture);
    });

    it('should chain', function () {
      validator.validate().validate();
    });

    it('should fire the callback', function (done) {
      validator.validate(function () {
        done();
      });
    });

    it('should fail when not given inputs', function (done) {
      delete validator._inputs;
      var errored = false;
      validator
        .on('error', function (err) {
          if (err) {
            errored = true;
          }
        })
        .validate();

      setTimeout(function () {
        assert.isTrue(errored);
        done();
      }, 50);
    });

    describe('given an incomplete form', function () {
      it('should fire the callback', function (done) {
        validator.validate(function () {
          done();
        });
      });

      it('should skip optional inputs', function (done) {
        validator.validate(function (err, report) {
          assert.isTrue(report.stuff.skipped);
          assert.isTrue(report.doors.skipped);
          done();
        });
      });

      it('should emit "invalid"', function (done) {
        validator
          .on('invalid', function () {
            done();
          })
          .validate();
      });
    });

    describe('given a complete form', function () {
      it('should emit "valid"', function (done) {
        var things = document.getElementById('things'),
          junk = document.getElementById('junk');

        things.value = 'hi';
        junk.value = 'bye';

        validator.on('valid', function () {
          done();
        }).validate();
      });

      it('should pass a valid report to the callback', function (done) {
        var things = document.getElementById('things'),
          junk = document.getElementById('junk');

        things.value = 'hi';
        junk.value = 'bye';

        validator.validate(function (err, report) {
          each(report, function (item) {
            assert.isTrue(item.isValid);
          });
          done();
        });
      });
    });

    it('should support required checkboxes', function (done) {
      var form = document.getElementById('form'),
          checkbox = document.createElement('input');

      form.appendChild(checkbox);

      checkbox.type = 'checkbox';
      checkbox.value = 'stuff';
      checkbox.required = true;
      checkbox.id = 'checkbox';
      checkbox.setAttribute('required', true);

      validator
        .addInput(checkbox)
        .validate(function (err, report) {
          assert.isFalse(report.checkbox.isValid);
          done();
        });
    });

  });

  describe('#on()', function () {
    var validator;

    before(function () {
      validator = new Validator();
    });

    it('should add a listener for the provided event name', function (done) {
      validator.on('foo', done).emit('foo');
    });
  });

  describe('#emit()', function () {
    var validator;

    before(function () {
      validator = new Validator();
    });

    it('should emit the event', function (done) {
      validator
        .on('foo', done)
        .emit('foo');
    });

    it('should fire all listeners', function (done) {
      var count = 0;
      validator
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .on('bar', function () {
          count++;
        })
        .emit('bar');

      setTimeout(function () {
        assert.equal(count, 11);
        done();
      }, 0);
    });

    it('should be possible to emit more than one event at a time', function (done) {
      var firstFired = false;
      validator
        .on('first', function () {
          firstFired = true;
        })
        .on('second', function () {
          assert.isTrue(firstFired);
          done();
        })
        .emit('first, second');
    });
  });


});
