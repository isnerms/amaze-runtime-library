describe('forms.generic', function () {
  'use strict';

  var forms = require('forms');
  var generic = forms.generic;
  var Validator = forms.Validator;
  var selector = require('selector');
  var each = require('collections').each;
  var events = require('events');
  var dom = require('dom');
  var fixture = null;

  beforeEach(function () {
    fixture = document.createElement('div');

    fixture.id = 'fixture';
    fixture.innerHTML = [
      '<div>',
      '  <form>',
      '    <div class=input>',
      '      <label>stuff</label>',
      '      <input type=text id=lol />',
      '    </div>',
      '    <div class=input>',
      // add a span to a required label, so ie8 picks it up as required
      '      <label><span>*</span> things</label>',
      '      <input type=text class=things name=foo required />',
      '    </div>',
      '    <div class=input>',
      '      <label>junk</label>',
      '      <input type=text class=junk name=bar required />',
      '    </div>',
      '    <div class=input>',
      '      <label>doors</label>',
      '      <input type=text id=doors />',
      '    </div>',
      '  </form>',
      '</div>'
    ].join('');

    document.body.appendChild(fixture);
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });

  it('should be a function', function () {
    assert.isFunction(generic);
  });

  it('should export its defaults', function () {
    assert.isObject(generic.defaults);
    assert.isBoolean(generic.defaults.ariaRequired);
  });

  it('should not adjust its defaults', function () {
    generic('#fixture form', { foo: 'bar' });
    assert.isUndefined(generic.defaults.foo);
  });

  it('should fail when not given an element', function () {
    try {
      generic();
      assert.fail('Should have thrown...');
    } catch (err) {}
  });

  it('should handle a selector', function () {
    generic('#fixture form');
  });

  it('should handle a HTMLElement', function () {
    generic(selector.query('#fixture form'));
  });

  it('should not require options', function () {
    generic('#fixture form');
  });

  it('should return a Validator', function () {
    assert.isInstanceOf(generic('#fixture form'), Validator);
  });

  it('should find all fields', function () {
    var fields = selector.queryAll('div.input'),
      found = [];

    generic('#fixture form', {
      getInput: function (field) {
        assert.includes(fields, field);
        found.push(field);
      }
    });

    assert.deepEqual(fields, found);
  });

  describe('AM-217', function () {
    it('should respect noValidate', function () {
      var form = selector.query('#fixture form');

      // intentional false
      generic(form, { noValidate: false });
      assert.isFalse(form.hasAttribute('novalidate'));
      form.removeAttribute('novalidate');

      // defaults
      generic(form);
      assert.equal(form.getAttribute('novalidate'), 'true');
      form.removeAttribute('novalidate');

      // intentional true
      generic(form, { noValidate: true });
      assert.equal(form.getAttribute('novalidate'), 'true');
    });
  });

  describe('fields', function () {
    it('should give each input an id', function () {
      var inputs = [];

      generic('#fixture form', {
        getInput: function (field) {
          var input = selector.query('input', field);
          inputs.push(input);
          return input;
        }
      });

      each(inputs, function (input) {
        assert.ok(input.id);
      });
    });
    it('should associate each input with a label', function () {
      var inputs = [];

      generic('#fixture form', {
        getInput: function (field) {
          var input = selector.query('input', field);
          inputs.push(input);
          return input;
        }
      });

      each(inputs, function (input) {
        assert.ok(selector.query('label[for="' + input.id + '"]'));
      });

    });
    it('should deterime if the input is required (and mark it so)', function () {

      var inputs = [];

      generic('#fixture form', {
        getInput: function (field) {
          var input = selector.query('input', field);
          inputs.push(input);
          return input;
        },
        isRequired: function () { return true; }
      });

      each(inputs, function (input) {
        assert.isTrue(input.required);
      });
    });
    it('should run "preInput" on each input', function () {
      var called = false;
      generic('#fixture form', {
        preInput: function () {
          called = true;
        }
      });
      assert.isTrue(called);
    });
  });

  describe('on form submission', function () {
    describe('invalid submission', function () {
      it('should stop the form from submitting', function (done) {
        var form = selector.query('#fixture form');

        form.onsubmit = function () {
          assert.fail('form was submitted');
        };

        generic(form);
        events.fire(form, 'submit');

        setTimeout(done, 100);
      });
      it('should focus the first erroneous input', function (done) {
        var form = selector.query('#fixture form');
        generic(form, {
          isRequired: function () {
            return true;
          }
        });
        events.fire(form, 'submit');
        setTimeout(function () {
          assert.equal(document.activeElement.id, 'lol');
          done();
        }, 100);
      });
      it('should add a tooltip for each erroneous input', function (done) {
        var form = selector.query('#fixture form');

        form.onsubmit = function () {
          assert.fail('form was submitted');
        };

        generic(form);
        events.fire(form, 'submit');

        setTimeout(function () {


          var tooltips = selector.queryAll('.amaze-tooltip');
          assert.ok(tooltips.length);
          done();

        }, 100);

      });
    });
    describe('valid submission', function () {
      it('should let the form submit', function (done) {
        var form = selector.query('#fixture form'),
          inputs = selector.queryAll('input', form);

        each(inputs, function (input) {
          input.value = 'hello world';
        });

        form.onsubmit = function () {
          done();
        };

        generic(form);
        events.fire(form, 'submit');
      });
    });
  });

  describe('tooltip options', function () {
    it('should use the provided tooltip options', function (done) {
      var form = selector.query('#fixture form');

      generic(form, {
        tooltips: {
          align: 'e'
        }
      }).on('invalid', function (report) {
        // let other invalid handlers finish first
        setTimeout(function () {
          each(report, function (input) {
            var describedby = input.element.getAttribute('aria-describedby'),
                tip = describedby && document.getElementById(describedby);

            if (tip) {
              assert.isTrue(dom.classList(tip).has('e'));
            }
          });

          done();
        }, 10);
      });

      events.fire(form, 'submit');

    });
  });

  describe('given `singleTip: true`', function () {

    before(function () {
      // remove all previous tooltips so we don't get false positives
      each(selector('.amaze-tooltip'), function (tip) {
        tip.parentNode.removeChild(tip);
      });
    });

    it('should only display a single tooltip', function (done) {
      var validator = generic('#fixture form', {
        singleTip: true
      });


      validator.on('invalid', function () {
        setTimeout(function () {
          var tips = selector('.amaze-tooltip'),
              count = 0;

          each(tips, function (tip) {
            if (dom.isVisible(tip)) {
              count++;
            }
          });

          assert.equal(count, 1);
          done();
        }, 10);
      });

      events.fire('#fixture form', 'submit');
    });
  });
});
