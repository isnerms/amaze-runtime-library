describe('locale.text', function () {
  'use strict';

  var locale = require('locale');
  var html = null;
  var originalLanguage = null;

  before(function () {
    html = document.documentElement;
    originalLanguage = html.getAttribute('lang');

    // define amaze if it isn't already there
    window.amaze = window.amaze || {};
    window.amaze.__text = {
      greeting: {
        en: 'hi!',
        es: '¡hola!'
      }
    };
  });

  beforeEach(function () {
    // reset the cache
    locale.text.htmlLanguage = null;
  });

  afterEach(function () {
    // reset language after each test
    html.setAttribute('lang', originalLanguage);
  });

  after(function () {
    // reset language after everything
    html.setAttribute('lang', originalLanguage);
  });

  it('should properly default to en if no language is provided and no lang attribute is present', function () {
    // remove `<html lang=".." />`
    html.setAttribute('lang', '');
    var greeting = locale.text('greeting');
    assert.equal(greeting, window.amaze.__text.greeting.en);
  });

  it('should return null if the requested `lang` is not set', function () {
    assert.isNull(locale.text('greeting', 'not real'));
  });

  it('should default to the language set in the HTML lang attribute, when present', function () {
    html.setAttribute('lang', 'es');
    var greeting = locale.text('greeting');
    assert.equal(greeting, window.amaze.__text.greeting.es);
  });

  it('should take the language specified and change text accordingly, if/when language is specified', function () {
    var greeting = locale.text('greeting', 'es');
    assert.equal(greeting, window.amaze.__text.greeting.es);
  });

  it('should work when required in an overlay');
});
