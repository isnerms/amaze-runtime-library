
describe('amaze.css', function () {
  'use strict';

  var amaze = require('core');

  afterEach(function () {
    document.getElementById('fixture').innerHTML = '';
  });

  it('should be a function', function () {
    assert.isFunction(amaze.css);
  });

  it('should inject a <style> element', function (done) {
    var div, actual, expected,
        css = '#fixture #some-awesome-div { color: red !important }'.toLowerCase(),
        fixture = document.getElementById('fixture');

    fixture.innerHTML = '<div id="some-awesome-div" class="foo bar baz">hi</div>';

    div = document.getElementById('some-awesome-div');

    amaze.css(css);

    setTimeout(function () {
      if (window.getComputedStyle === undefined) {
        actual = div.currentStyle.color;
        expected = 'red';

      } else {
        actual = window.getComputedStyle(div).getPropertyValue('color');
        expected = 'rgb(255, 0, 0)'; // red
      }

      assert.equal(actual, expected, 'Our style rule should be applied after running `amaze.css`');
      done();
    }, 10);
  });

  it('should return the <style> element', function () {
    var css = 'cats{dogs:10}';
    var style = amaze.css(css);
    assert.ok(style);
  });

  it('should be "require-able"', function () {
    var css = amaze.css;
    assert.strictEqual(amaze.require('css'), css);
  });

  it('should append styles to the same element', function () {
    var a = amaze.css('#foo{bar:100px}');
    var b = amaze.css('#bar{baz:bold}');
    assert.strictEqual(b, a);
  });
});
