
describe('amaze.require()', function () {
  'use strict';

  var amaze = require('core');
  var r = amaze.require;

  it('should be a function', function () {
    assert.isFunction(r);
  });

  describe('.get()', function () {
    var get;
    before(function () {
      get = r.get;
    });

    it('should be a function', function () {
      assert.isFunction(get);
    });
  });

  it('should allow `require("css")`', function () {
    var css = r('css');
    assert.isFunction(css);
    assert.strictEqual(css, amaze.css);
  });

  it('should allow `require("debugOut")`', function () {
    var debugOut = r('debugOut');
    assert.isFunction(debugOut);
    assert.strictEqual(debugOut, amaze.debugOut);
  });

  it('should not allow `require("lib")`', function () {
    assert.throws(function () {
      r('lib');
    });
  });

  it('should not allow `require("utils")`', function () {
    assert.throws(function () {
      r('utils');
    });
  });

  describe('require("utils/...")', function () {
    beforeEach(function () {
      amaze.utility('foo', function (w, d, r, o, e) {
        e.foo = 'foo';
        e.bar = 'bar';
        e.foobar = {
          foo: e.foo,
          bar: e.bar
        };
        e.hello = function () {
          return 'hello';
        };
      });
    });

    it('should handle setup utilities', function () {
      var foo = r('utils/foo');
      assert.isObject(foo);
      assert.equal(foo.hello(), 'hello');
    });

    it('should throw on non-existing utilities', function () {
      assert.throws(function () {
        r('utils/apples');
      });
    });
  });

  describe('require("lib/...")', function () {
    it('should work', function () {
      var events;

      try {
        events = r('lib/events');
      } catch (err) {
        assert.equal(err.message, 'No library provided');
      }

      if (events) {
        assert.isObject(events);
        assert.isFunction(events.on);
      }
    });
  });
});
