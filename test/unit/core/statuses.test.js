
describe('amaze.status', function () {
  'use strict';

  var amaze = require('core');
  var originalStatus;

  before(function () {
    originalStatus = amaze.status;
  });

  afterEach(function () {
    amaze.status = originalStatus;
  });

  it('should be a string', function () {
    assert.isString(amaze.status);
  });

  it('should be set on "status" events', function (done) {
    amaze.once('status', function () {
      // wait for the other callbacks to fire
      setTimeout(function () {
        assert.equal(amaze.status, 'foo');
        done();
      }, 0);
    });
    amaze.emit('status', 'foo');
  });
});
