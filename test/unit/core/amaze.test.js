
describe('amaze', function () {
  'use strict';

  var amaze = require('core');

  it('should be an object', function () {
    assert.isObject(amaze);
  });

  describe('emitter inheritance', function () {
    it('should emit and receive events', function (done) {
      amaze.once('foo', function (one, two) {
        assert.equal(one, 1);
        assert.equal(two, 2);

        done();
      });
      amaze.emit('foo', 1, 2);
    });
  });
});
