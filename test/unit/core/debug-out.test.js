
describe('amaze.debugOut', function () {
  'use strict';

  var amaze = require('core');
  var debugOut = amaze.debugOut;

  it('should be a function', function () {
    assert.isFunction(debugOut);
  });

  it('should be callable', function () {
    debugOut('foo', 'bar', 'baz', 1, 2, 3, 4, 5, 6, 7, 8);
  });

  it('should be "require-able"', function () {
    assert.strictEqual(amaze.require('debugOut'), debugOut);
  });
});
