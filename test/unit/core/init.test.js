var amazeFlags;

describe('amaze.init', function () {

  it('should be a function', function () {
    assert.isFunction(amaze.init);
  });

  it('should fetch overlays and emit a "DONE" status', function (done) {
    amaze
    .on('status', onStatus)
    .once('path', function() {
      setTimeout(function () {
        assert.isString(amaze.PAGE_PATH);
      }, 0);
    })
    .once('url', function () {
      setTimeout(function () {
        assert.isString(amaze.SERVER_URL);
      }, 0);
    })
    .init('/test/fixtures');

    function onStatus(status) {
      if (status === 'DONE') {
        amaze.removeListener('status', onStatus);
        done();
      }
    }
  });

  describe('with a singleFile flag', function () {

    function status() {
      assert.fail();
    }

    before(function() {
      amazeFlags = {
        singleFile: true
      };
    });

    after(function() {
      amaze.off('status', status);
    });

    describe('and no regular expressions', function() {

      it('should emit an "ERROR" event only', function (done) {
        amaze
        .on('status', status)
        .once('path', function() {
          assert.fail();
        })
        .once('url', function () {
          assert.fail();
        })
        .once('error', function(err) {
          assert.isInstanceOf(err, Error);
          done();
        })
        .init('/test/fixtures');

      });

    });

  });

});
