
describe('amaze.utility', function () {
  'use strict';

  var amaze = require('core');

  it('should be a function', function () {
    assert.isFunction(amaze.utility);
  });

  it('should emit an error given a bad utility', function (done) {
    amaze.once('error', function () {
      done();
    });
    amaze.utility('hello', 'world');
  });

  it('should provide the expected variables to utilities', function (done) {
    amaze.utility('foo', function (w, d, r, o, e) {
      // window (back-compat)
      assert.strictEqual(w, window);
      // document (back-compat)
      assert.strictEqual(d, document);
      // require
      assert.strictEqual(r, amaze.require);
      // overlay / module
      assert.isFunction(o);
      // exports (overlay.exports)
      assert.isObject(e);
      // overlay.exports === exports
      assert.strictEqual(o.exports, e);
      done();
    });

    amaze.require('utils/foo');
  });

  it('should allow for exporting methods on "exports"', function (done) {
    amaze.utility('bar', function (window, document, require, overlay, exports) {
      exports.hello = function () {
        done();
      };
    });

    var bar = amaze.require('utils/bar');
    bar.hello();
  });
});
