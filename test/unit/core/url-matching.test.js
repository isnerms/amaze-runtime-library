
describe('url-matching', function () {
  'use strict';

  var pathMatching = require('./url-matching').matchesPathRules;
  var siteMatching = require('./url-matching').matchesSiteRules;

  // Tests

  describe('should be an object', function() {
    assert.equal(typeof require('url-matching'), 'object', 'should be an object');
  });

  describe('matchesPathRules', function() {

    it('should be a function', function() {
      assert.equal(typeof pathMatching, 'function', 'should be a function');
    });

    it('should match on the include only', function() {
      var result = pathMatching('pathone/pathtwo', {include:/pathone/});
      assert.isTrue(result);
    });

    it('should not match on the include only', function() {
      var result = pathMatching('pathone/pathtwo', {include:/paththree/});
      assert.isFalse(result);
    });

    it('should match on the include and exclude', function() {
      var result = pathMatching('pathone/pathtwo', {include:/pathone/,exclude:/paththree/});
      assert.isTrue(result);
    });

    it('should not match on the include and exclude', function() {
      var result = pathMatching('pathone/pathtwo', {include:/pathone/,exclude:/pathtwo/});
      assert.isFalse(result);
    });
  });

  describe('matchesSiteRules', function() {

    it('should be a function', function() {
      assert.equal(typeof siteMatching, 'function', 'should be a function');
    });

    it('should match on the hostname include only', function() {
      var result = siteMatching('google.com', 'pathone/pathtwo', {include:/google/});
      assert.isTrue(result);
    });

    it('should not match on the hostname include only', function() {
      var result = siteMatching('google.com', 'pathone/pathtwo', {include:/froogle/});
      assert.isFalse(result);
    });

    it('should match on the hostname include and exclude only', function() {
      var result = siteMatching('google.com', 'pathone/pathtwo', {include:/google/, exclude:/mail\.google\.gom/});
      assert.isTrue(result);
    });

    it('should not match on the hostname include and exclude only', function() {
      var result = siteMatching('mail.google.com', 'pathone/pathtwo', {include:/google/, exclude:/mail/});
      assert.isFalse(result);
    });

    it('should match on the hostname and path include only', function() {
      var result = siteMatching('google.com', 'pathone/pathtwo', {include:/google/, path:{include:/pathone/}});
      assert.isTrue(result);
    });

    it('should not match on the hostname and path include only', function() {
      var result = siteMatching('google.com', 'pathone/pathtwo', {include:/google/, path:{include:/paththree/}});
      assert.isFalse(result);
    });

    it('should not match on the hostname and path exclude only', function() {
      var result = siteMatching('google.com', 'pathone/pathtwo', {include:/google/, path:{exclude:/pathone/}});
      assert.isFalse(result);
    });

    it('should match on the hostname and path include and exclude', function() {
      var result = siteMatching('google.com', 'pathone/pathtwo', {include:/google/, path:{include:/pathone/, exclude:/paththree/}});
      assert.isTrue(result);
    });

    it('should not match on the hostname and path include and exclude', function() {
      var result = siteMatching('google.com', 'pathone/pathtwo', {include:/google/, path:{include:/pathone/, exclude:/pathtwo/}});
      assert.isFalse(result);
    });

    it('should not match on the both the hostname exclude and path exclude', function() {
      var result = siteMatching('mail.google.com', 'pathone/pathtwo', {include:/google/, exclude:/mail/, path:{include:/pathone/, exclude:/pathtwo/}});
      assert.isFalse(result);
    });
  });

});
