
describe('amaze.injection', function () {
  'use strict';

  var amaze = require('core');
  var originalStatus = null;

  before(function () {
    originalStatus = amaze.status;
  });

  beforeEach(function () {
    amaze.emit('status', originalStatus);
  });

  it('should be a function', function () {
    assert.isFunction(amaze.injection);
  });

  it('should throw on bad injections', function (done) {
    amaze.once('error', function () {
      done();
    });
    amaze.injection('hello world!');
  });

  it('should emit an "injection" event when registered', function (done) {
    var foo;

    function fn() {
      foo = 'bar';
    }

    amaze.once('injection', function (injection) {
      assert.strictEqual(injection.fn, fn);
      assert.equal(injection.priority, 'default');
      done();
    });

    amaze.injection(fn);
  });

  it('should fire injections on "ready"', function (done) {
    amaze.injection(function () {
      done();
    });

    // TODO don't hard-code this
    amaze.emit('status', 'READY');
  });

  it('should provide "require" to injections', function (done) {
    amaze.injection(function (window, document, require) {
      assert.equal('function', typeof require);
      done();
    });
    amaze.emit('status', 'READY');
  });

  it('should provide "amaze" to injections', function (done) {
    amaze.injection(function (window, document, require, amaze) {
      assert.equal('function', typeof amaze);
      done();
    });
    amaze.emit('status', 'READY');
  });
});
