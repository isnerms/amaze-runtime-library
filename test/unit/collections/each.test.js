
describe('collections.each', function () {
  'use strict';

  var each = require('collections').each;

  it('should not set the callback\'s context', function () {
    var obj = { a: 'b' };
    each(obj, function () {
      assert.notEqual(obj, this);
    });
  });

  it('should iterate through NodeLists', function () {
    var list = document.getElementsByTagName('*');
    each(list, function () {
      assert.equal(3, arguments.length);
    });
  });

  it('should iterate through Arguments', function (done) {
    (function () {
      each(arguments, function () {
        assert.equal(3, arguments.length);
      });
      done();
    }(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
  });

  describe('given an array', function () {
    it('should iterate through each memeber', function () {
      var arr = [ 'foo', 1, 'bar', 2, 'baz', 3 ];
      var count = 0;

      each(arr, function () {
        assert.equal(3, arguments.length);
      });

      each(arr, function () {
        count++;
      });

      assert.equal(arr.length, count);
    });

    it('should provide the element as the first param', function () {
      each([ 'yo dog' ], function (item) {
        assert.equal('yo dog', item);
      });
    });

    it('should provide the index as the second param', function () {
      each([ 'yo dog' ], function (item, index) {
        assert.equal(0, index);
      });
    });

    it('should provide the array as the third param', function () {
      var arr = [ 'yo', 'dog' ];
      each(arr, function (item, index, array) {
        assert.strictEqual(arr, array);
      });
    });
  });

  describe('given an object', function () {
    var obj = { foo: 1, bar: 2, baz: 3 };

    it('should iterate through each property', function () {
      var count = 0;

      each(obj, function () {
        assert.equal(3, arguments.length);
      });

      each(obj, function () {
        count++;
      });

      assert.equal(3, count);
    });

    it('should provide the value as the first param', function () {
      each(obj, function (value) {
        assert.equal('number', typeof value);
      });
    });

    it('should provide the property name as the second param', function () {
      each(obj, function (value, name) {
        assert.equal('string', typeof name);
      });
    });

    it('should provide the object as the third param', function () {
      each(obj, function (value, name, object) {
        assert.strictEqual(obj, object);
      });
    });
  });
});
