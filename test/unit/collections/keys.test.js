
describe('collections.keys', function () {
  'use strict';

  var keys = require('collections').keys;

  it('should get all enumerable properties of an object', function () {
    var obj = {
      foo: 'bar',
      baz: true
    };

    if ('function' === typeof Object.create) {
      Object.defineProperty(obj, 'no', {
        value: 37,
        enumerable: false
      });
    }

    assert.deepEqual([ 'foo', 'baz' ], keys(obj));
  });
});
