describe('mutation', function () {
  'use strict';


  // as of phantom 1.9.1, neither a MutationObserver
  // nor MutationEvents are supported; we have to poll
  var mutation = window.mochaPhantomJS
    ? require('mutation').polling
    : require('mutation');
  var selector = require('selector');
  var query = selector.query;
  var matches = selector.matches;
  var each = require('collections').each;
  var array = require('array');
  var isArray = array.isArray;
  var DELAY = mutation.mode === 'polling' ? 500 : 0;
  var fixture = null;



  beforeEach(function () {
    fixture = document.createElement('div');
    fixture.innerHTML =
        '<div id="div">'
      + '  <span id="span">'
      + '    <p class="p">this is a paragraph</p>'
      + '    <p class="p yes" foo="nope" bar="0" tabindex="100">'
      + '      this is a paragraph'
      + '    </p>'
      + '    <p class="p">this is a paragraph</p>'
      + '    <p class="p no">this is a paragraph</p>'
      + '    <p class="p">this is a paragraph</p>'
      + '    <p class="p maybe">this is a paragraph</p>'
      + '  </span>'
      + '</div>';
    document.body.appendChild(fixture);
  });

  afterEach(function () {
    document.body.removeChild(fixture);
  });


  function tests(selector, done, count) {
    if (count) {
      count -= 1;
    }
    return function (nodes) {
      assert.equal(typeof nodes, 'object', 'typeof nodes == "object"');
      assert.ok(isArray(nodes), 'nodes is an Array');
      each(nodes, function (node) {
        assert.ok(matches(node, selector), 'provided node should match selector');
      });
      if (!count) {
        done();
      } else {
        count -= 1;
        each(nodes, function (node) {
          node.parentNode.removeChild(node);
        });
      }
    };
  }


  /////////////////
  // attributes  //
  /////////////////

  it('attributes: adding new attribute', function (done) {
    var span = query('#span'),
      selector = 'p.yes.p';

    mutation(span, selector, 'attribute', tests(selector, done));
    setTimeout(function () {
      query(selector, span).setAttribute('qaz', 'bar');

    }, DELAY);
  });

  it('attributes: changing existing attribute: element', function (done) {
    var span = query('#span'),
      selector = 'p.yes.p';

    mutation(span, selector, 'attribute', tests(selector, done));
    setTimeout(function () {
      query(selector, span).setAttribute('foo', 'bar');

    }, DELAY);
  });

  it('attributes: changing existing attribute: selector', function (done) {
    var span = query('#span'),
      selector = 'p.yes.p';

    mutation('#span', selector, 'attribute', tests(selector, done));
    setTimeout(function () {
      query(selector, span).setAttribute('foo', 'bar');

    }, DELAY);
  });

  it('attributes: removing attribute', function (done) {
    var span = query('#span'),
      selector = 'p.yes.p';

    mutation(span, selector, 'attribute', tests(selector, done));
    setTimeout(function () {
      query(selector, span).removeAttribute('foo');

    }, DELAY);
  });

  it('attributes: class (attr) change: element', function (done) {
    var span = query('#span'),
      selector = 'p.yes.p';

    mutation(span, selector, 'attribute', tests(selector, done));
    setTimeout(function () {
      query(selector, span).setAttribute('class', 'foo yes p whatever');

    }, DELAY);
  });

  it('attributes: class (attr) change: selector', function (done) {
    var span = query('#span'),
      selector = 'p.yes.p';

    mutation('#span', selector, 'attribute', tests(selector, done));
    setTimeout(function () {
      query(selector, span).setAttribute('class', 'foo yes p whatever');

    }, DELAY);
  });

  it('attributes: changing attr property', function (done) {
    var span = query('#span'),
      selector = 'p.yes.p';

    mutation(span, selector, 'attribute', tests(selector, done));
    setTimeout(function () {
      query(selector, span).tabIndex = -1;
    }, DELAY);
  });

  it('attributes: adding attr property', function (done) {
    var span = query('#div'),
      selector = 'p.yes.p';

    mutation(span, selector, 'attribute', tests(selector, done));
    setTimeout(function () {
      query(selector, span).id = 'apples!';
    }, DELAY);
  });

  /////////////////////
  // node insertion  //
  /////////////////////

  it('node inserted: first level: element', function (done) {
    var span = query('#span'),
      selector = 'p.hello',
      p = document.createElement('p');

    p.className = 'hello world';

    mutation(span, selector, 'inserted', tests(selector, done));

    span.appendChild(p);
  });

  it('node inserted: first level: selector', function (done) {
    var span = query('#span'),
      selector = 'p.hello',
      p = document.createElement('p');

    p.className = 'hello world';

    mutation('#span', selector, 'inserted', tests(selector, done));

    span.appendChild(p);
  });

  it('node insertion: second level: element', function (done) {
    var div = query('#div'),
      selector = '#span p.yes.p button.class-name';

    mutation(div, selector, 'inserted', tests(selector, done));
    setTimeout(function () {
      query('#span p.yes.p', div)
        .appendChild((function () {
          var button = document.createElement('button');
          button.innerHTML = 'hello';
          button.className = 'class-name';
          return button;
        }()));
    }, DELAY);

  });

  it('node inserted: third level: element', function (done) {
    var selector = '#div #span p.yes.p button.class-name';

    mutation(fixture, selector, 'inserted', tests(selector, done));
    setTimeout(function () {
      query('#span p.yes.p', fixture)
        .appendChild((function () {
          var button = document.createElement('button');
          button.innerHTML = 'hello';
          button.className = 'class-name';
          return button;
        }()));
    }, DELAY);
  });

  it('node inserted: third level: selector', function (done) {
    var selector = '#div #span p.yes.p button.class-name';

    fixture.id = 'thirdlevelfixture';

    mutation('#thirdlevelfixture', selector, 'inserted', tests(selector, done));
    setTimeout(function () {
      query('#span p.yes.p', fixture)
        .appendChild((function () {
          var button = document.createElement('button');
          button.innerHTML = 'hello';
          button.className = 'class-name';
          return button;
        }()));
    }, DELAY);
  });
  ////////////////////////////
  // persistent node insertion
  ////////////////////////////

  describe('persistance', function () {
    it('should handle node insertion at the second level', function (done) {
      this.timeout(5000);

      var count = 0,
          delay = window.mochaPhantomJS ? 1000 : 100;

      mutation('#div', '#span p button', 'inserted', function () {
        count++;

        if (count === 3) {
          done();
        }
      }, true);

      function insert() {

        var button = document.createElement('button');
        button.innerHTML = 'hello';
        button.className = 'class-name';

        query('#span p.yes.p').appendChild(button);
      }

      setTimeout(insert, delay);
      setTimeout(insert, delay * 2);
      setTimeout(insert, delay * 3);
    });

    it('should handle node insertion at the third level', function (done) {
      this.timeout(5000);

      var count = 0,
          delay = window.mochaPhantomJS ? 1000 : 100;

      fixture.id = 'persistance-fixture';

      mutation('#persistance-fixture', '#div #span p button', 'inserted', function () {
        count++;

        if (count === 3) {
          done();
        }
      }, true);

      function insert() {

        var button = document.createElement('button');
        button.innerHTML = 'hello';
        button.className = 'class-name';

        query('#span p.yes.p').appendChild(button);
      }

      setTimeout(insert, delay);
      setTimeout(insert, delay * 2);
      setTimeout(insert, delay * 3);
    });
  });

  ///////////////////
  // node removal  //
  ///////////////////

  it('node removed: first level', function (done) {
    var selector = '#div';

    mutation(fixture, selector, 'removed', tests(selector, done));
    setTimeout(function () {
      var element = query(selector, fixture);
      fixture.removeChild(element);
    }, DELAY);
  });

  it('node removed: second level: element', function (done) {
    var selector = '#span';

    mutation(fixture, selector, 'removed', tests(selector, done));
    setTimeout(function () {
      var element = query(selector, fixture);
      element.parentNode.removeChild(element);
    }, DELAY);
  });

  it('node removed: second level: selector', function (done) {
    var selector = '#span';

    fixture.id = 'mutation-fixture';

    mutation('#mutation-fixture', selector, 'removed', tests(selector, done));
    setTimeout(function () {
      var element = query(selector, fixture);
      element.parentNode.removeChild(element);
    }, DELAY);
  });

  it('node removed: third level', function (done) {
    var selector = 'p.yes.p';

    mutation(fixture, selector, 'removed', tests(selector, done));
    setTimeout(function () {
      var element = query(selector);
      element.parentNode.removeChild(element);
    }, DELAY);
  });

});
