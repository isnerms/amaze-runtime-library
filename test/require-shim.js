// jshint strict: false
/* global require: true */

//
// little hack to let us `require("namespace")`
// within our unit tests :)
//

if ('function' !== typeof require) {
  require = function (path) {
    if ('amaze' === path || 'core' === path) {
      return amaze;
    }
    if ('list' === path) {
      return amaze.$;
    }
    return amaze.require('lib/' + path);
  };
}
