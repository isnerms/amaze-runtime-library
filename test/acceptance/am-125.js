describe('AM-125: dom.isFocusable()', function () {
  'use strict';

  var amaze, dom, selector, events;

  before(function () {
    window.amazeFlags = { debug: true };
    amaze = window.amaze;
    dom = amaze.require('lib/dom');
    selector = amaze.require('lib/selector');
    events = amaze.require('lib/events');
  });

  after(function () {
    events.fire('#open', 'click');
  });

  it('should handle offscreen elements', function (done) {
    var button = selector.query('#modal li button'),
        open = selector.query('#open');

    // it's offscreen at this point
    assert.isTrue(dom.isFocusable(button));

    events.once(open, 'click', function () {
      // it's not offscreen at this point
      assert.isTrue(dom.isFocusable(button));
      done();
    });

    events.fire(open, 'click');
  });
});
