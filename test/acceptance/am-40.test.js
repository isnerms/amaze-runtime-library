describe('AM-40', function () {
  'use strict';

  var amaze;

  before(function () {
    // expose the amaze global
    amaze = window.amaze;

    // define some mock text
    amaze.__text = {
      greeting: {
        en: 'hi!',
        es: '¡hola!'
      }
    };

    // opt-in to extension mode to prevent
    // server communication
    window.amazeFlags = {
      extension: true,
      debug: true
    };
  });

  describe('amaze.injection', function () {
    it('should have access to "locale" via require', function (done) {
      // create our injection

      // injections receive the `window`, the `document` and `require`
      amaze.injection(function (w, d, require) {
        var greeting,
          locale = require('lib/locale');

        assert.isObject(locale, 'locale should be an object');
        assert.isFunction(locale.text, 'locale.text should be a funktion');

        greeting = locale.text('greeting', 'es');
        assert.equal(greeting, amaze.__text.greeting.es, 'it should return the correct text for provided locale');

        greeting = locale.text('greeting');
        assert.equal(greeting, amaze.__text.greeting.en, 'it should default to english');

        done();
      }, 'low');

      // run stuff
      amaze.init();
    });
  });
});
