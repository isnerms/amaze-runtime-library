
'use strict';

window.amazeFlags = {
  singleFile:true
};

describe('AM-300: Single file amaze', function() {

  describe('calling amaze.init()', function() {

    describe('without a regex', function() {

      it('should emit an "ERROR" event', function(done) {
        amaze.on('error', function () {
          done();
        });

        amaze.init();
      });
    });

    describe('with a non-matching site', function() {

      it('should emit a "SKIPPED" event', function(done) {
        amaze.on('error', function(err) {
          throw err;
        });

        amaze.once('status', function(status) {
          assert.equal(status, 'SKIPPED');
          done();
        });

        amaze.init(undefined, {
          include: /google\.com/i
        });
      });
    });

  });

  describe('registering injections targeted at the current page', function() {

    it('should be executed', function(done) {
      amaze.once('injection', function(injection) {
        assert.equal(injection.name, 'targeted');
        done();
      });
      amaze.once('error', function(err) {
        throw err;
      });
      amaze.injection(function() {}, {
        name: 'targeted',
        regex: {
          include: /./i
        }
      });
      amaze.init(undefined, {
        include: new RegExp(window.location.hostname)
      });
    });

  });

  describe('registering untargeted injections', function() {

    it('will be executed to maintain compatibility with non single page amaze', function(done) {
      amaze.once('injection', function(injection) {
        assert.equal(injection.name, 'untargeted');
        done();
      });
      amaze.once('error', function(err) {
        throw err;
      });
      amaze.injection(function() {}, {
        name: 'untargeted',
        regex: {
          include: /./i
        }
      });
      amaze.init(undefined, {
        include: new RegExp(window.location.hostname)
      });
    });

  });

  describe('registering targeted injections outside of the current page', function() {

    it('should not be executed', function(done) {
      amaze.once('injection', function() {
        throw new Error('this should not be executed');
      });
      amaze.once('error', function(err) {
        throw err;
      });
      amaze.on('status', function(status) {
        if (status === 'READY') {
          done();
        }
      });
      amaze.injection(function() {}, {
        name: 'google',
        regex: {
          include: /google\.com/i
        }
      });
      amaze.init(undefined, {
        include: new RegExp(window.location.hostname)
      });
    });

  });


});
