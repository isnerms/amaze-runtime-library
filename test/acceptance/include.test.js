describe('amazeme=false', function () {
  'use strict';

  var amaze;
  before(function () {

    window.amazeFlags = {
      extension: true
    };

    amaze = window.amaze;
  });

  it('should run the overlays', function (done) {
    amaze.injection(function () {
      done();
    });
    amaze.init();
  });

});
