
window.amazeFlags = {
  extension: true
};

describe('overlays: "amaze" pseudo-global', function () {
  'use strict';

  var amaze = window.amaze;

  beforeEach(function () {
    amaze.emit('status', 'not done');
  });

  it('should be provided to injections', function (done) {
    amaze.injection(function (w, d, require, amaze) {
      assert.equal('function', typeof amaze);
      done();
    });
    amaze.init();
  });

  it('should be provided to utilities', function (done) {
    amaze.utility('mysweeeetutility', function (w, d, r, o, e, amaze) {
      o.exports = function () {
        return amaze;
      };
    });
    amaze.injection(function (w, d, require, amaze) {
      assert.equal('function', typeof require);
      assert.equal('function', typeof amaze);
      var mysweeeetutility = require('utils/mysweeeetutility');
      var amaze2 = mysweeeetutility();
      assert.strictEqual(amaze, amaze2);
      done();
    });
    amaze.init();
  });
});
