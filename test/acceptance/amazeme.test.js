describe('amazeme=true', function () {
  'use strict';

  var amaze;
  before(function () {
    amaze = window.amaze;
  });

  it('should run the overlays', function (done) {
    amaze.on('error', function (err) {
      throw err;
    });

    amaze.on('status', function (status) {
      if (status === 'DONE') {
        setTimeout(done, 10);
      }
    });

    amaze.init('/test/fixtures');
  });

  it('should load the bcl', function () {
    assert.isTrue(window.bcl);
  });

  it('should load the overlays', function () {
    assert.isTrue(window.overlays);
  });
});
