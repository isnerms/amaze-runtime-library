// TODO
//
//  it'd be cool if this file was generated so we
// wouldn't have to update it every time we add
// somethign
//

describe('amaze.require("lib/...")', function () {
  'use strict';

  var amaze;
  before(function () {

    window.amazeFlags = {
      extension: true
    };

    amaze = window.amaze;
  });

  it('should not allow for require("lib")', function () {
    assert.throws(function () {
      amaze.require('lib');
    });
  });

  it('should allow for require("lib/namespace")', function () {
    amaze.require('lib/events');
  });

  it('should allow for deep-requires', function () {
    amaze.require('lib/events/on');
  });

  it('should have a array namespace', function () {
    amaze.require('lib/array');
  });

  it('should have a collections namespace', function () {
    amaze.require('lib/collections');
  });

  it('should have a date namespace', function () {
    amaze.require('lib/date');
  });

  it('should have a dom namespace', function () {
    amaze.require('lib/dom');
  });

  it('should have a events namespace', function () {
    amaze.require('lib/events');
  });

  it('should have a forms namespace', function () {
    amaze.require('lib/forms');
  });

  it('should have a liveregions namespace', function () {
    amaze.require('lib/liveregions');
  });

  it('should have a locale namespace', function () {
    amaze.require('lib/locale');
  });

  it('should have a markup namespace', function () {
    amaze.require('lib/markup');
  });

  it('should have a menu namespace', function () {
    amaze.require('lib/menu');
  });

  it('should have a mutation namespace', function () {
    amaze.require('lib/mutation');
  });

  it('should have a selector namespace', function () {
    amaze.require('lib/selector');
  });

  it('should have a string namespace', function () {
    amaze.require('lib/string');
  });

});
