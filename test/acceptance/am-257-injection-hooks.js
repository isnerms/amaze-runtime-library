
window.amazeFlags = {
  extension: true
};

describe('AM-257: Injection Hooks', function () {
  'use strict';

  var amaze = window.amaze;

  beforeEach(function () {
    amaze.emit('status', 'blah');
  });

  it('should emit events on injection completion', function (done) {
    var oneran = false;

    amaze.injection(function (w, d, require) {
      var emitter = require('emitter');
      emitter.after('injection2', function () {
        assert.equal(oneran, true);
        done();
      });
    }, {
      priority: 'default',
      name:'injection1'
    });

    amaze.injection(function (w, d, require) {
      var emitter = require('emitter');
      emitter.after('injection1', function () {
        oneran = true;
      });
    }, {
      priority:'default',
      name:'injection2'
    });

    amaze.init();
  });

  it('should emit errors when injections fail', function (done) {
    amaze.injection(function (w, d, require) {
      var emitter = require('emitter');
      emitter.once('error', function (name) {
        assert.equal('thrower', name);
        done();
      });
    }, {
      priority:'high',
      name:'tester'
    });

    amaze.injection(function () {
      throw new Error;
    }, {
      priority: 'default',
      name:'thrower'
    });

    amaze.init();
  });

  it('should provide the ability to emit and hook events', function (done) {
    amaze.injection(function (w, d, require) {
      var emitter = require('emitter');
      emitter.once('foo', function (a, b, c) {
        assert.equal(a, 'a');
        assert.equal(b, false);
        assert.equal(c, emitter);
        done();
      });
    });

    amaze.injection(function (w, d, require) {
      var emitter = require('emitter');
      emitter.emit('foo', 'a', false, emitter);
    });

    amaze.init();
  });

  it('should emit "Anonymous injection" without a name', function (done) {
    amaze.injection(function () {});

    amaze.injection(function (w, d, require) {
      var emitter = require('emitter');
      emitter.after('Anonymous injection', function () {
        done();
      });
    });

    amaze.init();
  });
});
