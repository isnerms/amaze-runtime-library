describe('AM-31: overlay sorting', function () {
  'use strict';

  var amaze;

  before(function () {
    // expose the amaze global
    amaze = window.amaze;

    // opt-in to extension mode to preven server communication
    window.amazeFlags = {
      extension: true,
      debug: true
    };
  });

  describe('amaze.injection', function () {
    it('should fire overlays in their respective order', function (done) {
      var high = false,
        defaults = 0;

      amaze.injection(function () {
        assert.isTrue(high, 'The high proprity overlays should have fired first');
        assert.equal(defaults, 2, 'Both default proprity overlays should have fired second');
        done();
      }, { priority: 'low' });

      amaze.injection(function () {
        high = true;
      }, { priority: 'high'});


      // no order; should default
      amaze.injection(function () {
        defaults++;
      });

      amaze.injection(function () {
        defaults++;
      }, { priority: 'default' });

      // run stuff
      amaze.init();
    });
  });
});
