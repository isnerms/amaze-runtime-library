
window.amazeFlags = {
  extension: true
};

describe('AM-228: Export Falsey Values', function () {
  'use strict';

  var amaze = window.amaze;

  beforeEach(function () {
    amaze.emit('status', 'not done');
  });

  it('should allow for utilities to export `0`', function (done) {
    amaze.utility('zero', function (w, d, r, overlay) {
      overlay.exports = 0;
    });

    amaze.injection(function (w, d, require) {
      var zero = require('utils/zero');
      assert.strictEqual(zero, 0);
      done();
    });

    amaze.init();
  });

  it('should allow for utilities to export `false`', function (done) {
    amaze.utility('false', function (w, d, r, overlay) {
      overlay.exports = false;
    });

    amaze.injection(function (w, d, require) {
      var f = require('utils/false');
      assert.strictEqual(f, false);
      done();
    });

    amaze.init();
  });

  it('should allow for utilities to export `""`', function (done) {
    amaze.utility('empty-string', function (w, d, r, overlay) {
      overlay.exports = '';
    });

    amaze.injection(function (w, d, require) {
      var es = require('utils/empty-string');
      assert.strictEqual(es, '');
      done();
    });

    amaze.init();
  });

  it('should allow utilities to export `null`', function (done) {
    amaze.utility('null', function (w, d, r, overlay) {
      overlay.exports = null;
    });

    amaze.injection(function (w, d, require) {
      var nil = require('utils/null');
      assert.strictEqual(nil, null);
      done();
    });

    amaze.init();
  });

  it('should not invoke utilities more than once', function (done) {
    var count = 0;
    amaze.utility('null', function (w, d, r, overlay) {
      count++;
      overlay.exports = null;
    });

    amaze.injection(function (w, d, require) {
      assert.strictEqual(null, require('utils/null'));
      assert.strictEqual(null, require('utils/null'));
      assert.strictEqual(null, require('utils/null'));
      assert.strictEqual(null, require('utils/null'));
      assert.equal(count, 1);
      done();
    });

    amaze.init();
  });
});
