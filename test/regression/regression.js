/*jslint browser:true*/

describe('amaze runtime library', function () {
	'use strict';

	var amaze, fixture;

	beforeEach(function () {
		amaze = window.amaze;
		fixture = document.getElementById('qunit-fixture');

	});

	afterEach(function () {
		fixture.innerHTML = '';
	});

	it('should get bcl and overlays', function (done) {

		// hackery to get tests to pass in IE -- there are very strange caching
		// issues going on which can only be explained as billy gates magic
		var interval = setInterval(function () {
			if (window['amaze-test'] !== undefined) {
				clearInterval(interval);


				assert.ok(amaze.bcl, 'The BCL should exist.');
				assert.ok(window['amaze-test'].overlaysLoaded,
					'The overlays shold have been loaded.');

				done();
			}
		}, 100);
	});


	it('should have some public properties', function () {

		assert.ok(amaze.version, 'The `version` property should exist publically.');
		assert.ok(amaze.init, 'The `init` property should exist publically.');
	});

	it('should have some public methods', function () {
		assert.ok(amaze.css, '`amaze.css` method should exist publically.');
		assert.ok(amaze.injection, '`amaze.injection` method should exist publically.');
		assert.ok(amaze.utility, '`amaze.utility` method should exist publically.');
	});

	it('should have amaze.css', function () {

		var div, actual, expected,
			css = '#qunit-fixture #some-awesome-div { color: red !important }'
				.toLowerCase(),
			styles = document.getElementsByTagName('style'), // live lsit
			styleCount = styles.length;

		fixture.innerHTML = '<div id="some-awesome-div" class="foo bar baz">hi</div>';

		div = document.getElementById('some-awesome-div');

		amaze.css(css);

		assert.equal(styles.length, styleCount + 1,
			'There should be one more style element than before we called `amaze.css`');

		if (window.getComputedStyle === undefined) {
			actual = div.currentStyle.color;
			expected = 'red';

		} else {
			actual = window.getComputedStyle(div).getPropertyValue('color');
			expected = 'rgb(255, 0, 0)'; // red
		}

		assert.equal(actual, expected,
			'Our style rule should be applied after running `amaze.css`');

	});

	it('should has amaze.utility', function () {
		var util,
			name = 'test_util',
			fired = false,
			action = function (window, document, require, module) {
				fired = true;
				module.exports = {
					cats: 1,
					dogs: 2
				};
			};

		amaze.utility(name, action);

		assert.equal(fired, false, 'The utility should not be immediately executed');

		util = amaze.require('utils/' + name);

		assert.ok(util, 'The util should exist');
		assert.equal(fired, true, 'The utility should be executable');
		assert.equal(util.cats, 1, 'The utility should export');
		assert.equal(util.dogs, 2, 'The utility should export');

	});

	it('should has amaze.injection', function () {

		assert.ok(window['amaze-test'].injections.fired,
			'The injections should have been fired.');
		assert.ok(window['amaze-test'].injections.validWin,
			'Injections should be passed a valid `utils` object.');
		assert.ok(window['amaze-test'].injections.validDoc,
			'Injections should be passed a valid `document`.');

	});

	describe('amaze.require', function () {


		//
		// # basic amaze.lib tests
		// assuming these methods have been properly
		// tested before getting to this point
		//

		var require = window.amaze.require;

		it('array - require', function () {
			assert.equal(typeof require('lib/array'), 'object', 'Should be object');
			assert.equal(typeof require('lib/array/isArray'), 'function', 'isArray should be a function');
			assert.equal(typeof require('lib/array/toArray'), 'function', 'toArray should be a function');
			assert.equal(typeof require('lib/array/inArray'), 'function', 'inArray should be a function');
			assert.equal(typeof require('lib/array/indexOf'), 'function', 'indexOf should be a function');
		});

		it('markup - require', function () {
			assert.equal(typeof require('lib/markup'), 'object', 'Should be object');
			assert.equal(typeof require('lib/markup/dataTable'), 'function', 'dataTable should be a function');
		});

		it('query selector - require', function () {
			assert.equal(typeof require('lib/selector'), 'object', 'Should be object');
			assert.equal(typeof require('lib/selector/matches'), 'function', 'matches should be a function');
			assert.equal(typeof require('lib/selector/query'), 'function', 'query should be a function');
			assert.equal(typeof require('lib/selector/queryAll'), 'function', 'queryAll should be a function');
		});

		it('dom - require', function () {
			assert.equal(typeof require('lib/dom'), 'object', 'Should be object');
			assert.equal(typeof require('lib/dom/getAttributes'), 'function', 'getAttributes should be a function');
			assert.equal(typeof require('lib/dom/setAttribute'), 'function', 'setAttribute should be a function');
			assert.equal(typeof require('lib/dom/setAttributes'), 'function', 'setAttributes should be a function');
			assert.equal(typeof require('lib/dom/replaceTag'), 'function', 'replaceTag should be a function');
			assert.equal(typeof require('lib/dom/findUp'), 'function', 'findUp should be a function');

			assert.equal(typeof require('lib/dom/classList'), 'function', 'classList should be a function');
		});

		it('events - require', function () {
			assert.equal(typeof require('lib/events'), 'object', 'Should be object');
			assert.equal(typeof require('lib/events/on'), 'function', 'on should be a function');
			assert.equal(typeof require('lib/events/off'), 'function', 'off should be a function');
			assert.equal(typeof require('lib/events/delegate'), 'function', 'delegate should be a function');
			assert.equal(typeof require('lib/events/fire'), 'function', 'fire should be a function');
			assert.equal(typeof require('lib/events/undelegate'), 'function', 'undelegate should be a function');
			assert.equal(typeof require('lib/events/once'), 'function', 'once should be a function');
		});

		it('liveregions - require', function () {
			assert.equal(typeof require('lib/liveregions'), 'object', 'Should be object');
			assert.equal(typeof require('lib/liveregions/polite'), 'function', '`polite` should be a function');
			assert.equal(typeof require('lib/liveregions/assertive'), 'function', '`assertive` should be a function');
		});

		it('collections - require', function () {
			assert.equal(typeof require('lib/each'), 'function',
				'each should be a function');
			assert.equal(typeof require('lib/keys'), 'function',
				'keys should be a function');
		});

		it('mutation - require', function () {
			assert.equal(typeof require('lib/mutation'), 'function',
				'mutation should be a function');
		});

		it('should have access to "forms"', function () {
			var forms = amaze.require('lib/forms');
			assert.isObject(forms);
			assert.isFunction(forms.Validator);
			assert.isFunction(forms.generic);
		});

		it('should have access to "locale"', function () {
			var locale = amaze.require('lib/locale');

			assert.isObject(locale);
			assert.isFunction(locale.text);
		});

	});


});
