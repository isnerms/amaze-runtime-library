/*jshint strict:false*/
/*global amaze*/

if (window.console) {
  console.log('overlays loaded');
}

amaze.injection(function (window, document, require) {
  function assert(expr, msg) {
    if (!expr) {
      throw new Error(msg || 'oh noes!');
    }
  }

  var events = require('lib/events');

  assert(typeof events === 'object');
});

window.overlays = true;
