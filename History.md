0.10.0-rc.1 / 2014-09-22
========================

 * renamed fetch() to init()
 * added single file capability, if the amazeFlags.singleFile property is defined init() will not
   fetch the bcl or overlay files, as it is assumed they are included
 * init() function now has optional regular-expression object parameter, and if this does not match
   the current page, Amaze will not initialize
 * injections now have an options object that all the optional parameters are passed with
    * name
    * priority
    * regex (will only execute if the regex matches)
    * css (to remove need for a separate css registration)  
 * added 'skipped' status event, emitted when Amaze does not run
 * Add new amaze-query library functions
   - amaze#parent
   - amaze#append
   - amaze#attr
   - amaze#is

0.9.0 / 2014-03-31
==================

 * dom: Add `rndid` function
 * deps: Update stephenmathieson/rndid
 * test: Fix dom.wrap tests in IE
 * dom.wrap: Add support for text nodes
 * pkg: Update amaze-docs to 1.2.1
 * dox: Add deprecated flag to `amaze#getVisibleText`
 * dox: Add missing `@name` to `amaze#getElementCoordinates`
 * dox: Remove methods on the `amaze` global from the documentation
 * Added test coverage reporting
 * AM-276: Sanitize selector context
 * Add `css.get` and `css.set`
 * Add `dom.children`
 * Add `dom.before`
 * Add `dom.after`
 * Let `dom.siblings` optionally filter with a selector
 * Refactored tests
 * Add a clean script
 * pkg: Export the copyright string
 * AM-257: emit events when injections complete or fail
 * Refactored build
 * [AM-254](https://dequesrc.atlassian.net/browse/AM-254): support `<html lang="en-US">` in `text.locale`
 * added `dom.nextElementSibling()`: gets an element's next sibling
 * added `dom.previousElementSibling()`: gets an element's previous sibling
 * fix binding events on `<table />` elements in IE8
 * Added `css(element, property)` getter (alias: `css.get`)
 * Added `css(element, property, value)` setter (alias: `css.set`)
 * Add "Amaze Query":
    - has extremely high test coverage (100% of statements are executed at least once)
    - is exposed as amaze.$ :p
    - supports selectors
    - supports jQuery
    - supports arrays of nodes
    - supports NodeLists
    - supports chunks of HTML
    - supports existing AmazeQuery objects
    - supports nodes
    - supports contexts (jQuery, selector, node, etc.)
    - supports prototype extensions so people can write jQuery-style plugins
passes its test suite in IE5

0.8.1 / 2013-10-09
==================

 * Pin [docs](https://bitbucket.org/dmusser/amaze-docs) version at **1.0.0** to fix build

0.8.0 / 2013-10-07
==================

 * [AM-243](https://dequesrc.atlassian.net/browse/AM-243):
    - added `dom.wrap()`: wraps an element or set of elements in a tag
    - added `dom.unwrap()`: unwraps an element and all of it siblings by removing the parent
 * [AM-242](https://dequesrc.atlassian.net/browse/AM-242) add support for required checkboxes in `forms.Validator` and `forms.generic`
 * `forms.generic` now supports `singleTip: true`
 * [AM-234](https://dequesrc.atlassian.net/browse/AM-234):
    - added `dom.siblings()`: get all siblings of an element
    - added `dom.siblings.prev()`: get all previous siblings of an element
    - added `dom.siblings.next()`: get all next siblings of an element

0.7.1 / 2013-09-03
==================

 * [AM-225](https://dequesrc.atlassian.net/browse/AM-225)
 * `forms.generic` will curry arguments to `markup.tooltip` ([AM-221](https://dequesrc.atlassian.net/browse/AM-221))
 * `forms.generic` will now mark `novalidate=true` by default ([AM-217](https://dequesrc.atlassian.net/browse/AM-217))
 * `dom.getText` now works with `Node.TEXT_NODE` nodes ([AM-226](https://dequesrc.atlassian.net/browse/AM-226))

0.7.0 / 2013-08-16
==================

 * Removed `String#trim()` polyfil / hack per [AM-61](https://dequesrc.atlassian.net/browse/AM-61)
    - Added `string` namespace (`require('lib/string')`)
    - Added `string.trim()`
    - Added `string.truncate()` ([AM-179](https://dequesrc.atlassian.net/browse/AM-179) and [AM-165](https://dequesrc.atlassian.net/browse/AM-165))
 * Converted to use [component](https://github.com/component/component):
    - [AM-118](https://dequesrc.atlassian.net/browse/AM-118)
    - [AM-60](https://dequesrc.atlassian.net/browse/AM-60)
    - [AM-59](https://dequesrc.atlassian.net/browse/AM-59)
    - [AM-58](https://dequesrc.atlassian.net/browse/AM-58)
 * Created standalone test runners
 * Added `EventEmitter` inheritance on `amaze`:
    - Added `status` event: fires when the status changes, providing new status
    - Added `error` event: fires when an error occurs, providing the error
    - Added `url` event: fires when `amaze.SERVER_URL` is set, providing the new URL
    - Added `injection` event: fires when an injection is added, providing the injection
 * Fixed [problems with IE](https://dequesrc.atlassian.net/browse/AM-144):
    - [AM-63](https://dequesrc.atlassian.net/browse/AM-63)
    - [AM-145](https://dequesrc.atlassian.net/browse/AM-145)
    - [AM-146](https://dequesrc.atlassian.net/browse/AM-146)
    - [AM-147](https://dequesrc.atlassian.net/browse/AM-147)
    - [AM-148](https://dequesrc.atlassian.net/browse/AM-148)
    - [AM-149](https://dequesrc.atlassian.net/browse/AM-149)
    - [AM-152](https://dequesrc.atlassian.net/browse/AM-152)
    - [AM-155](https://dequesrc.atlassian.net/browse/AM-155)
    - [AM-157](https://dequesrc.atlassian.net/browse/AM-157)
    - [AM-159](https://dequesrc.atlassian.net/browse/AM-159)
    - [AM-160](https://dequesrc.atlassian.net/browse/AM-160)
    - [AM-161](https://dequesrc.atlassian.net/browse/AM-161)
    - [AM-162](https://dequesrc.atlassian.net/browse/AM-162)
    - [AM-163](https://dequesrc.atlassian.net/browse/AM-163)
 * `markup.ariaTabs` now works with clicks ([AM-81](https://dequesrc.atlassian.net/browse/AM-81))
 * `markup.dataTable` is included in the documentation ([AM-170](https://dequesrc.atlassian.net/browse/AM-170))
 * wrote tests for `dom.moveChildNodes` ([AM-121](https://dequesrc.atlassian.net/browse/AM-121))
 * prevented `markup.ariaTabs` from throwing on bad association ([AM-164](https://dequesrc.atlassian.net/browse/AM-164))
 * added a `watch` script for automatically building when changes are made
 * make runtime module `require`-able in node, exporting:
    - `.code` the code
    - `.version` the version
    - `.docs` the documentation
 * removed `events.off(element)` support ([AM-198](https://dequesrc.atlassian.net/browse/AM-198))
 * fixed bug with `markup.fixModal` not properly removing its `keydown` and `click` handlers ([AM-196](https://dequesrc.atlassian.net/browse/AM-196))
 * added `selector.unique` ([AM-75](https://dequesrc.atlassian.net/browse/AM-75))

0.6.4 / 2013-07-10
==================

 * `Datepicker was overwriting the selected date with the remembered date` [AM-139](https://dequesrc.atlassian.net/browse/AM-139)

0.6.3 / 2013-07-05
==================

 * `Data table should prefer column headers over row header` [AM-137](https://dequesrc.atlassian.net/browse/AM-137)
 * `Data table set role="gridcell" instead of presentation` [AM-137](https://dequesrc.atlassian.net/browse/AM-137)
 * `Data table loop forward so that header association uses logical order` [AM-137](https://dequesrc.atlassian.net/browse/AM-137)

0.6.2 / 2013-06-21
==================

 * `markup.fixModal` now properly handles `tabindex=-1` [AM-122](https://dequesrc.atlassian.net/browse/AM-122)
 * `markup.ariaTabs` newly introduced `hidePanels` option no longer hides all panels by default
 * `markup.fixModal` no longer sets `role="button"` on `closer` [AM-126](https://dequesrc.atlassian.net/browse/AM-126)
 * `dom.isFocusable` will now properly handle offscreen elements` [AM-125](https://dequesrc.atlassian.net/browse/AM-125)
 * fixed failing `bootstrap` test introduced in **0.6.1**

0.6.1 / 2013-06-14
==================

 * BCL request now contains the pathname

0.6.0 / 2013-06-12
==================

 * re-exposed `amaze.debugOut`
 * removed required `amazeFlags.debug==true` to prevent exception swallowing
 * added support for ordered overlays ([AM-31](https://dequesrc.atlassian.net/browse/AM-31))

0.5.34 / unknown
==================

 * Fixed a bug with dom get-text in IE [AM-49](https://dequesrc.atlassian.net/browse/AM-49)
 * Fixed markup label for appending label to aria-labelledby [AM-32](https://dequesrc.atlassian.net/browse/AM-32)
 * Added 'markup.fixPage' [AM-30](https://dequesrc.atlassian.net/browse/AM-30)

0.5.33 / unknown
==================

 * Fix a bug with ariaTabs and ctrl-option-SPACE and Voiceover
 * Fix a bug with the announcements of liveregions deletions on Windows with JAWS

0.5.32 / unknown
==================

 * Fix a bug in the datePicker's options extender
 * Fixed bugs in datepicker in IE with JAWS
 * Fixed bug in liveregions in IE with JAWS
 * Added support for the closer on fixModal to be an element in addition to a selector to handle closers that are outside the modal

0.5.31 / unknown
==================

 * Fixed two defects in the mutation library for MutationObserver
 * Added `markup.fixModal`
 * Added an optional boolean to mutation to allow for persistent mutation listeners
 * fixed a bug in isFocusable where it would return true even when elements were not visible

0.5.30 / unknown
==================

 * Added `forms`:

    - `Validator` a simple (evented) validation class for forms
    - `generic` a simple, generic form "fixer"

0.5.29 / unknown
==================

 * Add support for the `regions` options to `markup.dataTable` for discontinuous and multi-col-multi-row associations

0.5.28 / unknown
==================

 * Make `markup.dataTable` actually work in iOS
 * Added `dom.getVisibleText`

0.5.27 / unknown
==================

 * Added `markup.SkipModal`
 * Added `markup.landmarksModal`

0.5.26 / unknown
==================

 * Changed the ignore options to `columns` and `rows` to make it more obvious what they do
 * Fixed the ignore handling to work properly
 * Fixed all the tests and added additional tests for multiple columns and rows and ignore

0.5.25 / unknown
==================

 * Added more documentation for the dataTable library function
 * Changed the dataTable option structure names to ones that are less confusing
 * Changed the way that the dataTable emptyText option is used - only gets used for ARIA markup

0.5.24 / unknown
==================

 * Added an optional `deep` parameter to `amazelib.extend`
 * Updated the dataTable:
  - Now sets `headers` or `aria-labelledby` on all cells that have headers
  - headers can be specified as only heading a certain range of rows/columns

0.5.23 / unknown
==================

 * Added ARIA tabs library function
 * Fixed some IE specific bugs in getAttributes

0.5.22 / unknown
==================

 * `array.indexOf` and `array.inArray` will now operate on array-like objects, not just arrays
 * updated internal documetation tool to ([docs@0.1.0](https://bitbucket.org/dmusser/amaze-docs))

0.5.21 / unknown
==================

 * Added better debugging support, driven by `amazeFlags.debug`:
    - Injections are now optionally wrapped in `try/catch`
    - `debugOut` will **only** print if `amazeFlags.debug == true`
 * Published the server url as `amaze.SERVER_URL`
 * `amaze.css()` will now return the inserted `<style type="text/css"></style>`

0.5.20 / unknown
==================

 * Removed `support.js`.
 * Adjusted tooltip arrows slightly.

0.5.19 / unknown
==================

 * Added `lib/markup/tooltip`, a utility for creating tooltips.

0.5.18 / unknown
==================

 * Added `lib/markup/skipLink/fix`, a utility for fixing existing, broken skip links.
 * Added `lib/markup/skipLink/add`, a utility for adding new skip links.

0.5.17 / unknown
==================

 * Added `hasClass` method to dom
 * Added `toggleClass` method to dom
 * Added `isVisible` method to dom
 * Added `getElementCoordinates` method to dom
 * Changed `ariaMenu` to use dom `isVisible` and `getElementCoordinates`

0.5.16 / unknown
==================

 * Fixed aria markup of the menu
 * Added tests for disjointed menus and fixed the code for disjointed menus
 * added `timeout`, `indexGetter` options to `ariaMenu` which also adds support for generated selectors

0.5.15 / unknown
==================

 * Removed explicit global publish in outro.stub
 * Renamed `bootstrap` to `amaze.fetch`
 * Removed `AMAZE_SERVER_URL` and replace with passing the server url to `amaze.fetch`
 * Changed `amaze.flags.extension` to `amazeFlags.extension`

0.5.14 / unknown
==================

 * Added `amaze.flags.extension` which will prevent the BCL and Overlays from being automatically loaded from the server.

0.5.13 / unknown
==================

 * Added `aria-describedby` parameter to `lib/markup/addLabel`.

0.5.12 / unknown
==================

 * Added datePicker library function
 * fixed a bug in events.off

0.5.11b / unknown
==================

 * Using [amaze-docs](https://bitbucket.org/dmusser/amaze-docs) for documentation.  Minor code changes and comment formatting updates.

0.5.11 / unknown
==================

 * Standardized exceptions in `events.fire` when attempting to set read-only properties using the `augment` argument.

0.5.10 / unknown
==================

 * Added `lib/dom/isFocusable`.

0.5.9 / unknown
==================

 * Fixed a bug with liveregions and VoiceOver
 * Made liveregions in the library standalone (give the amazelib css & ready)
 * Fixed function declaration naming collision

0.5.8 / unknown
==================

 * Added `lib/markup/addLabel`

0.5.7 / unknown
==================

 * Added the ability to augment the event object with `event.fire`.

0.5.6 / unknown
==================

 * Added `lib/markup/presentation`.

0.5.5 / unknown
==================

 * Added `lib/extend` and `lib/clone`.
 * Added `dom/isNode`

0.5.4 / unknown
==================

 * added lib/menu with ariaMenu utility
 * added clone to lib/array

0.5.3 / unknown
==================

 * fixed `lib/mutations` in WebKit: *TypeError: Cannot call method 'disconnect' of undefined*

0.5.2 / unknown
==================

 * remove AMD support from amazelib (conflicts with Facebook)
 * remove CommonJS support from amazelib (no need)
 * slowed down firing of injections
 * fixed tests @dsturley broke

0.5.1 / unknown
==================

 * Fixed a bug where `amaze` global was not explicitly published to `window`
 * Grunt 0.4.0

0.5.0 / unknown
==================

 * Now builds two files:
    - `runtime.js`, which does not explicitly publish a global or call `bootstrap()`). Intended to be served to extensions.
    - `amazelib.js`, publishes a global (UMDish) and calls `bootstrap()`. Intended for the Amaze/amazeme API
 * `require` for Utilities and Injections
 * `module.exports` for utilities
 * Namespaced many library functions:
    - New namespaces `lib/selector`, `lib/dom`, `lib/array`, `lib/events`, `lib/liveregions`
 * Refactored liveregions to only add elements and poll when there are active announcements
 * Added onDOMReady
 * BCL and overlays are now loaded concurrently
 * BCL calls `amaze.ready` when it has been loaded AUTOMAGICALLY
 * Removed messaging and other "dead" files
 * Moved/renamed many "standalone" fixtures to use the same naming conventions.
 * The amaze "library" is no longer published on the amaze object
 * Mutations will no longer pollute the global scope with an unprefixed MutationObserver
 * Many micro-optimizations

0.4.1 / unknown
==================

 * The following methods will now accept either an `HTMLElement` or a selector:
    - `amaze.lib.classList`
    - `amaze.lib.moveChildNodes`
    - `amaze.lib.setAttributes`
    - `amaze.lib.findUp`
    - `amaze.lib.on`
    - `amaze.lib.once`
    - `amaze.lib.off`
    - `amaze.lib.delegate`
    - `amaze.lib.undelegate`
    - `amaze.lib.fire`
    - `amaze.lib.mutation`

0.4.0 / unknown
==================

 * completely removed support for mutations (as an overlay type)

0.3.9 / unknown
==================

 * added `amaze.lib.mutation` for inline mutation events:

```js
var container = document.getElementById('container'),
  selector = 'div p.hello.world';

amaze.lib.mutation(container, selector, 'inserted', function (nodes) {
  console.log('added nodes:', nodes.join(','));
});
```

0.3.8 / unknown
==================

 * bug fix in `amaze.lib.off`: IE8 event handlers may now be removed
 * added `amaze.lib.undelegate` to remove delegated event handlers
 * added `amaze.lib.once` for a single-use event handler
 * added `amaze.lib.keys` (cross-browser `Object.keys`)
 * added `amaze.lib.each` (`each(object, fn)`)

0.3.7 / unknown
==================

 * removed `amaze.lib.selector`
 * added `amaze.lib.matches` (`Element#matchesSelector` shim)
 * added `amaze.lib.query` (`Element#querySelector` shim)
 * added `amaze.lib.queryAll` (`Element#querySelectorAll` shim); formally `amaze.lib.selector`
 * added `amaze.lib.toArray`
 * added `amaze.lib.inArray`
 * added `amaze.lib.indexOf` (`Array.prototype.indexOf` shim)
 * added `amaze.lib.isArray`
 * added `amaze.lib.dataTable`
 * added `amaze.lib.getAttributes`
 * added `amaze.lib.setAttribute`
 * added `amaze.lib.setAttributes`
 * added `amaze.lib.replaceTag`
 * added `amaze.lib.findUp`
 * added `amaze.lib.classList`
 * added `amaze.lib.on`
 * added `amaze.lib.off`
 * added `amaze.lib.delegate`
 * added `amaze.lib.fire`
 * improved test coverage
 * removed *src/to-array.js* and updated old references to its methods to use the `amaze.lib` namespace

0.3.6 / unknown
==================

 * only try to parse MessageEvents where the first parameter (data) is a String

0.3.5 / unknown
==================

 * no longer "double" declaring `handleMutationEvent`

0.3.4 / unknown
==================

 * introduced `amaze.lib` namespace
 * added qwery as a dependency
 * published qwery as `amaze.lib.selector`

0.3.3 / unknown
==================

 * **another** bug fix for the extension mode: don't run the overlays until the BCL is ready

0.3.2 / unknown
==================

 * Fixed bug introduced in **0.3.1** where in extension mode, overlays are injected twice

0.3.1 / unknown
==================

 * Added `bootstrap` method to support [Amaze Production Server 2.2.2](https://bitbucket.org/dmusser/amaze-production-server)
 * Added `flags` namespace, so extensions can turn on/off sepecifc features
 * No longer clobbering the namespace, but extending it instead (@dstruley)

0.3.0 / unknown
==================

 * Unified the two libraries down to one to remove all user-agent sniffing.  Now, all browsers are served exactly the same library which uses feature detection to deterime what to do rather than the server making these choices for us based on user agent.
 * Micro-optimized `amaze.css`, `toArray`, `isArray`, etc.

0.2.11 / unknown
==================

 * Added messaging support to the standard library
 * Added a `status` property to the Amaze global, which stores information regarding the current status

0.2.10 / unknown
==================

 * Combined IE7 and IE8 libraries
 * Split internal `toArray` function to files: standard and IE, thus greatly improving its performance
 * Updated `amaze.fetch` to no longer send the deprecated `referer` parameter

0.2.9 / unknown
==================

 * Deprecate `referer=` query parameter and replace with `hostname=` and `path=`
 * Resolved [AP-99](https://dequesrc.atlassian.net/browse/AP-99)

0.2.8 / unknown
==================

 * Bug fix in WebKit mutations (resolves [AP-88](https://dequesrc.atlassian.net/browse/AP-88))
 * Restructure `test` directory, moving unit tests into `/test/unit` and regression tests into `/test/regression`
 * Misc. cleanup

0.2.7 / unknown
==================

 * Bug fix in live regions: no more off-by-one errors
 * Added test suite for live regions

0.2.6 / unknown
==================

 * Resolved [issue #8](https://bitbucket.org/dmusser/amaze-runtime-library/issue/8): Added amaze functions for live region functionality

0.2.5 / unknown
==================

 * Resolved [issue #1](https://bitbucket.org/dmusser/amaze-runtime-library/issue/1): non-standard browsers are now provided more "realistic" mutation event
 * Added series of tests to verify the newly added code (*/tests/bind-mutations-ie.test.js*)
 * Updated "built" library tests to suggest mutation events *must* have a `type` property

0.2.4 / unknown
==================

 * Resolved [issue #5](https://bitbucket.org/dmusser/amaze-runtime-library/issue/5): the MutationObserver is no longer instantiated if there are no mutations in the queue
 * Added feature requested in [issue #6](https://bitbucket.org/dmusser/amaze-runtime-library/issue/6): mutations can now be removed from the queue, allowing for issues which only need to be remediated once
 * After mutations have been executed, if there are none left in the queue, we're no longer monitoring the DOM for mutations
 * Released public `amaze.monitoring` property, whose value represents whether or not we're monitoring the DOM for mutations
 * Introduced many more unit tests - now testing the standalone files on top of the build library

0.2.3 / unknown
==================

 * Resolved [issue #4](https://bitbucket.org/dmusser/amaze-runtime-library/issue/4): now supporting the `DOMAttrModified` event in WebKit browsers via the `MutationObserver` API

0.2.2 / unknown
==================

 * Restructured repository
 * Resolved issues [#3](https://bitbucket.org/dmusser/amaze-runtime-library/issue/3) and [#2](https://bitbucket.org/dmusser/amaze-runtime-library/issue/2)
 * Now adding the hostname parameter to the BCL API to support changes in the [Production Server](https://bitbucket.org/dmusser/amaze-production-server)

0.2.1 / unknown
==================

 * Added test suite
